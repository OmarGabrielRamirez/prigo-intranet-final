

$("#btnIniciar").click(function(e)
{   
    getTextInput(); validationAndAccions();
});

var numTicket; var tipoEncuesta; var id_sucursal; var p1; var p2; var p3; var p4; var p5; var p6; var p7; var p8; var pv1; var pv2;

function getTextInput()
{
    numTicket = $("#num_ticket").val();
    tipoEncuesta = $("#sel1").val();
    id_sucursal = $("#id_sucursal").val();

    return numTicket, tipoEncuesta, id_sucursal;
}

function welcome(tipo)
{   
    if(tipo === 1) $("#bienvenidaV").modal("show");
    if(tipo === 2) $("#bienvenidaR").modal("show");
}

function pollV()
{
    $("#btnIniciarV").click(function(e){ 
        $("#bienvenidaV").modal("hide");
        $("#encuestaV1").modal("show");   
    });

    $('input[name="type"]').click(function(e){
        pv1 = $('input[name="type"]:checked').val();
        if(pv1 != "")
        $("#btnSiguienteP1").prop("disabled", false);
        else
        $("#btnSiguienteP1").prop("disabled", true);
    });

    $("#btnSiguienteP1").click(function(e){
        $("#encuestaV1").modal("hide"); 
        $("#encuestaV2").modal("show");
    });

    $('input:radio[name=estrellas-v]').click(function(){
        pv2 = $(this).val();
        if(pv2 != "")
        $("#btnFinal").prop("disabled", false);
        else
        $("#btnFinal").prop("disabled", true);
    });

    $("#btnFinal").click(function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{!! csrf_token() !!}"
            }
        });
        $.ajax({
            type:'post',
            url:'/encuestas/guardar',
            data:{id_sucursal:id_sucursal,numTicket:numTicket, tipoEncuesta:tipoEncuesta, pv1:pv1, pv2:pv2},
            success:function(data){
                $("#encuestaV2").modal("hide");
                $("#encuestaFinal").modal("show");
            }
 
         });
        
    });
    
}


function pollR()
{
    $("#btnIniciarR").click(function(e){ 
        $("#principal").hide();
        $("#bienvenidaR").modal("hide");
        $("#encuestaR1").modal("show");   
    });

    $('input:radio[name=estrellas]').click(function(){
        p1 = $(this).val();
        if(p1 != "")
        $("#btnSig1").prop("disabled", false);
        else
        $("#btnSig1").prop("disabled", true);
    });

    $("#btnSig1").click(function(e){
        $("#encuestaR1").modal("hide"); 
        $("#encuestaR2").modal("show");
    });

    $('input:radio[name=estrellas-2]').click(function(){
        p2 = $(this).val();
        $("#btnSig2").prop("disabled", false);
    });

    $("#btnSig2").click(function(e){
        $("#encuestaR2").modal("hide"); 
        $("#encuestaR3").modal("show");
    });

    $('input:radio[name=estrellas-3]').click(function(){
        p3 = $(this).val();
        $("#btnSig3").prop("disabled", false);
    });

    $("#btnSig3").click(function(e){
        $("#encuestaR3").modal("hide"); 
        $("#encuestaR4").modal("show");
    });

    $("#select_platomes").change(function(){
        p4 = $(this).val();
        $("#btnSig4").prop("disabled", false);
    });
    $("#btnSig4").click(function(e){
        $("#encuestaR4").modal("hide"); 
        p4 = $('#select_platomes').val();
        $("#encuestaR5").modal("show");
    });

    $('input:radio[name=estrellas-5]').click(function(){
        p5 = $(this).val();
        $("#btnSig5").prop("disabled", false);
    });

    $("#btnSig5").click(function(e){
        $("#encuestaR5").modal("hide"); 
        $("#encuestaR6").modal("show");
    });

    $("#select_frecuencia").change(function(){
        p6 = $(this).val();
        $("#btnSig6").prop("disabled", false);
    });

    $("#btnSig6").click(function(e){
        $("#encuestaR6").modal("hide"); 
        $("#encuestaR7").modal("show");
    });

    $("#exampleInputEmail1").keyup(function(){
        p7 = $(this).val();
        $("#btnSig7").prop("disabled", false);
    });

    $("#btnSig7").click(function(e){
        $("#encuestaR7").modal("hide");
        $("#encuestaR8").modal("show");
    });

    $('textarea#form7').keyup(function(){
        p8 = $(this).val();
        $("#btnFinal2").prop("disabled", false);
    });

    $("#btnFinal2").click(function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{!! csrf_token() !!}"
            }
        });
        $.ajax({
            type:'post',
            url:'/encuestas/guardar',
            data:{id_sucursal:id_sucursal,numTicket:numTicket, tipoEncuesta:tipoEncuesta, p1:p1, p2:p2, p3:p3, p4:p4, p5:p5, p6:p6, p7:p7, p8:p8 },
            success:function(data){
                $("#encuestaR8").modal("hide");
                $("#encuestaFinal").modal("show");
            }
 
         });
        
    });

}

function validationAndAccions()
{
    if(!isNaN(numTicket)){ 
        
    if(numTicket == "" ){ 
        alert("Ingresa un número de ticket");
    }else if(typeof tipoEncuesta === 'undefined'){ 
        alert("Selecciona un tipo de encuesta.");
    }else if(numTicket.length = 5){ if(tipoEncuesta === "1"){ $("#principal").hide(); welcome(1); pollV();
    }else if( tipoEncuesta === "2"){ $("#principal").hide(); welcome(2); pollR();} }
    }else{ 
        
    }

}

