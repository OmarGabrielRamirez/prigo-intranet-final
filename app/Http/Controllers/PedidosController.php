<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

use Storage;

class PedidosController extends Controller
{
	public $convertidos = array();

    public function __construct()
    {
		
		$this->middleware('auth', ['except' => ['printProvRequest','printPrigoRequest','showReceivedOrder','getPendingRequest','getPendingTransfer','getPendingTransferTest','getPendingTest','confirmRequest','confirmTransfer','confirmTest','confirmTransferTest','getSBOItems','sendDesabasto']]);
		
		$this->convertidos = array();
		
		$this->middleware(function ($request, $next) {
			$idUsuario = Auth::id();
			$user = Auth::user();
			$sql = "SELECT * FROM config_app_access WHERE idUsuario = ? AND idAplicacion = 1;";
			$accesQuery = DB::select($sql, [$idUsuario]);
			if(!empty($accesQuery))
			{
			   session(['PDRole' => $accesQuery[0]->idRole]);
			   if($accesQuery[0]->idRole != 1)
			   {
					$sql = "SELECT group_concat(`idSucursal` separator ',') as `sucursales` FROM pedidos_sucursal_usuario WHERE idUsuario = ? GROUP BY idUsuario;";
					$sucursales = DB::select($sql, [$idUsuario]);
					if(!empty($sucursales))
					{
						session(['sucursales' => $sucursales[0]->sucursales]);
					}
			   }
			}

            return $next($request);
        });
    }
	
	public function index()
    {
        return view('pedidos.index',['role' =>session('PDRole')]);
    }
	
	public function showNewTransferForm()
	{
		
		$suggestedDate = date('Y-m-d', strtotime('+1 days'));
		
		$almacenes = DB::select("SELECT * FROM pedidos_almacen;");
		
		return view('pedidos.newTransferRequest',['role' =>session('PDRole'), 'suggestedDate' => $suggestedDate, 'almacenes' => $almacenes ]);
		
	}
	
	public function showNewRequestForm()
	{
		$PDRole = session('PDRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		
		if($PDRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (29,30,31) ";
			
		$sucs = DB::select("SELECT sucursales.id AS idSucursal, sucursales.nombre FROM sucursales INNER JOIN pedidos_sucursal_config ON pedidos_sucursal_config.idSucursal = sucursales.id WHERE estado = 1 ".($PDRole != 1 ? "AND id ".$strValSucursales : "" )." GROUP BY id, nombre ORDER BY nombre;");	
		
		if(count($sucs) == 1)
			$selected= $sucs[0]->idSucursal;
		else
			$selected=0;
		
		$suggestedDate = date('Y-m-d', strtotime('+1 days'));
		
		return view('pedidos.newRequest', ['role' =>session('PDRole'),'sucursales' => $sucs, 'selected' => $selected, 'suggestedDate' => $suggestedDate ]);
	}
	
	public function showNewRequestKayserForm()
	{
		$PDRole = session('PDRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		
		if($PDRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (29,30,31) ";
			
		$sucs = DB::select("SELECT sucursales.id AS idSucursal, sucursales.nombre FROM sucursales INNER JOIN pedidos_sucursal_config ON pedidos_sucursal_config.idSucursal = sucursales.id WHERE estado = 1 ".($PDRole != 1 ? "AND id ".$strValSucursales : "" )." GROUP BY id, nombre ORDER BY nombre;");	
		
		return view('pedidos.newKayser', ['role' =>session('PDRole'),'sucursales' => $sucs ]);
	}
	
	public function showRequests()
	{
		return view('pedidos.showRequests',['role' =>session('PDRole')]);
	}
	
	public function showTransfers()
	{
		return view('pedidos.showTransfers',['role' =>session('PDRole')]);
	}
	
	public function listItems()
	{
		return view('pedidos.showItems',['role' =>session('PDRole')]);
	}
	
	public function transferDetail($id)
	{		
		$pedido = DB::table('pedidos_traslado')
		->join('pedidos_almacen', 'pedidos_traslado.idAlmacen', '=', 'pedidos_almacen.idAlmacen')
		->join('users', 'pedidos_traslado.idUsuario', '=', 'users.id')
		->select('pedidos_traslado.idTraslado AS idPedido' ,'pedidos_almacen.almacen AS sucursal' , 'users.name as usuario','pedidos_traslado.codigoSAP', 'pedidos_traslado.fechaCrea AS fecha',  'pedidos_traslado.fechaRequerida', 'pedidos_traslado.horaCrea AS hora', 'pedidos_traslado.estado', 'pedidos_traslado.comentario')
		->where('pedidos_traslado.idTraslado', '=', $id)
		->get();
		
		$partidas = DB::table('pedidos_traslado_partida')
		->join('pedidos_articulo','pedidos_traslado_partida.idArticulo','=','pedidos_articulo.idArticulo')
		->leftJoin('pedidos_categoria_art', 'pedidos_categoria_art.idCategoria', '=', 'pedidos_articulo.idCategoria')
		->select('pedidos_articulo.descripcion','pedidos_articulo.UnidadFood','pedidos_articulo.UnidadPrg','pedidos_traslado_partida.cantidad','pedidos_articulo.CodPrigo','pedidos_articulo.CodFood', 'pedidos_categoria_art.nombre AS cat')
		->where('pedidos_traslado_partida.idTraslado','=',$id)
		->get();
		
		return view('pedidos.detalleTransfer', ['role' =>session('PDRole'),'pedido' => $pedido, 'partidas' => $partidas]);

	}
	public function requestDetail($id)
	{		

		$pedido = DB::table('pedidos_pedido')
		->join('sucursales', 'pedidos_pedido.idSucursal', '=', 'sucursales.id')
		->join('users', 'pedidos_pedido.idUsuario', '=', 'users.id')
		->select('pedidos_pedido.idPedido' ,'sucursales.nombre AS sucursal' , 'users.name as usuario','pedidos_pedido.codigoSAP', 'pedidos_pedido.fechaCrea AS fecha',  'pedidos_pedido.fechaRequerida', 'pedidos_pedido.horaCrea AS hora', 'pedidos_pedido.estado', 'pedidos_pedido.comentario')
		->where('pedidos_pedido.idPedido', '=', $id)
		->get();
		
		$partidas = DB::table('pedidos_pedido_partida')
		->join('pedidos_articulo','pedidos_pedido_partida.idArticulo','=','pedidos_articulo.idArticulo')
		->leftJoin('pedidos_categoria_art', 'pedidos_categoria_art.idCategoria', '=', 'pedidos_articulo.idCategoria')
		->select('pedidos_articulo.descripcion','pedidos_articulo.UnidadFood','pedidos_articulo.UnidadPrg','pedidos_pedido_partida.cantidad','pedidos_pedido_partida.cantidadFood','pedidos_articulo.CodPrigo','pedidos_articulo.CodFood', 'pedidos_categoria_art.nombre AS cat')
		->where('pedidos_pedido_partida.idPedido','=',$id)
		->get();
		
		return view('pedidos.detalle', ['role' =>session('PDRole'),'pedido' => $pedido, 'partidas' => $partidas]);

	}

	public function getStoreInventory($ifile, $inom){
		//$idSucursal = "coppla";
		
		$idSucursal = $ifile;
		$fechaInventario = "2019-11-30"; 
		echo "<h1>$fechaInventario</h1>";
		if(!empty($idSucursal))
		{
		$inputFileName = base_path()."/../templates/".$idSucursal.".xlsx";
			
		$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
				
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
		
		$reader->setReadDataOnly(false);
		
		$spreadsheet = $reader->load($inputFileName);

		$sheets = $spreadsheet->getSheetNames();
		$sn = 'InventoryDetails';
		echo "<b>$sn</b><br>";
		$idSucursal = $inom;//"CARMELAYSAL";
		$idSucursal = strtoupper($idSucursal);
		$sheetData = $spreadsheet->getSheetByName($sn);
		$itemCode ="";
		$itemName ="";
		$totalItemCount = 0;
		$esConversion = 0;
		$stock = 0;
		$unidadconv="";
		$procesados = array();
		echo "<table><tr><td>itemCode</td><td>ItemName</td><td>Store</td><td>Micros Count</td><td>SAP Stock</td><td>Differences</td><td>SAP Buy Unit</td><td>SAP Buy Num</td></tr>";

		for($i=2;(!empty($sheetData->getCell('F'.$i)->getValue()) && $i>3) || (empty($sheetData->getCell('F'.$i)->getValue()) && $i<=3) || (empty($sheetData->getCell('F'.$i)->getValue()) && ! empty($sheetData->getCell('C'.$i)->getValue()));$i++)
		{
			if(!empty($sheetData->getCell('B'.$i)->getValue()))
				$i++;

			if(empty($sheetData->getCell('F'.$i)->getValue()) && ! empty($sheetData->getCell('C'.$i)->getValue()))
				$i++;

			if(!empty($sheetData->getCell('D'.$i)->getValue()))
			{
				if(!empty($itemCode))
				{
					$sql = "SELECT * FROM inventario_mensual WHERE ItemCode = ? AND WhsCode= ? AND fecha = '".$fechaInventario."';";
					$inventarios = DB::select($sql, [$itemCode,$idSucursal]);
					if(empty($inventarios[0])){
						$stock = 0;
					} else {
						$stock = $inventarios[0]->Cantidad;
						$esConversion = $inventarios[0]->Conversion;
						$unidadconv = $inventarios[0]->Unidad;
					}
					$procesados[] = $itemCode;
					if((-$totalItemCount+$stock)> 0 )
						echo "<tr><td>$itemCode</td><td>".(-$totalItemCount+$stock)."</td><td>$idSucursal</td><td>$idSucursal</td><td>5101-0001</td><td>$itemName</td><td>$totalItemCount</td><td>$stock</td><td>".$unidadconv."</td><td>".$esConversion."</td></tr>";
				}
				$totalItemCount = 0;
				$itemCode = $sheetData->getCell('D'.$i)->getValue();
				if($idSucursal == "CARMELAYSAL" && $itemCode < 10000)
					$itemCode = "CS".$itemCode;
				$unidadMicros = $sheetData->getCell('F'.$i)->getValue();
				$itemName = $sheetData->getCell('E'.$i)->getValue();
			}
			if(!empty($sheetData->getCell('G'.$i)->getValue()) || $sheetData->getCell('G'.$i)->getValue() == 0)
			{
				$valor = empty($sheetData->getCell('G'.$i)->getValue()) ? 0:$sheetData->getCell('G'.$i)->getValue();
				$cant_conversion = $valor * $this->conversion($sheetData->getCell('F'.$i), $itemCode,$idSucursal);
				$totalItemCount += $cant_conversion;
			}
		}
		
		$procesadostxt = "'".implode("','",$procesados)."'";
		$sql = "SELECT * FROM inventario_mensual WHERE NOT(ItemCode IN (".$procesadostxt.")) AND Cantidad >0 AND WhsCode= ? AND fecha = '".$fechaInventario."';";
		$inventarios = DB::select($sql, [$idSucursal]);
		foreach($inventarios as $inventario)
			if(($inventario->Cantidad)> 0 )
				echo "<tr><td>$inventario->ItemCode</td><td>".$inventario->Cantidad."</td><td>$idSucursal</td><td>$idSucursal</td><td>5101-0001</td><td>0</td><td>".$inventario->Cantidad."</td><td>".$inventario->Cantidad."</td><td>".$inventario->Unidad."</td><td>".$inventario->Conversion."</td></tr>";

		echo "<table>";

		foreach($this->convertidos as $convertido)
		{
			echo "<br>".$convertido;
		}
	}
	else
	{
		echo "Ingrese una sucursal";
	}
	}
	
	private function conversion($unidad, $ItemCode, $idSucursal){

		if(empty($unidad))
			return 1;
		
		$sql = "SELECT Conversion FROM inventario_mensual WHERE ItemCode = ? AND WhsCode= ? AND fecha = '2019-05-01';";
		$inventarios = DB::select($sql, [$ItemCode,$idSucursal]);
		$esConversion = empty($inventarios[0])?1:$inventarios[0]->Conversion;

		if($esConversion==1)
			return 1;

		$this->convertidos[] = $ItemCode;
		$sql = "SELECT * FROM inventario_conversion WHERE unidad = ? ";
		$conversiones = DB::select($sql, [$unidad]);

		if(empty($conversiones[0]))
			return 1;
		
		$conversion = $conversiones[0]->conversion;
		
		return $conversion;

	}

	public function printProvRequest($prov,$id)
	{		

		$pedido = DB::table('pedidos_pedido')
		->join('sucursales', 'pedidos_pedido.idSucursal', '=', 'sucursales.id')
		->select('pedidos_pedido.idPedido' ,'sucursales.nombre AS sucursal' , 'pedidos_pedido.fechaCrea AS fecha', 'pedidos_pedido.estado', 'pedidos_pedido.comentario')
		->where('pedidos_pedido.idPedido', '=', $id)
		->get();
			
		$partidas = DB::table('pedidos_pedido_partida')
		->join('pedidos_articulo','pedidos_pedido_partida.idArticulo','=','pedidos_articulo.idArticulo')
		->select('pedidos_articulo.descripcion','pedidos_articulo.UnidadFood','pedidos_articulo.UnidadPrg','pedidos_pedido_partida.cantidad','pedidos_pedido_partida.cantidadFood','pedidos_articulo.CodPrigo','pedidos_articulo.CodFood')
		->where('pedidos_pedido_partida.idPedido','=',$id)
		->where('pedidos_articulo.idProveedor','=',$prov)
		->get();
		//		->select('pedidos_articulo.descripcion','pedidos_articulo.UnidadPrg','pedidos_pedido_partida.cantidad','pedidos_pedido_partida.cantidadFood','pedidos_articulo.CodPrigo','pedidos_articulo.CodFood')
		if($prov != 5)
		{
			if($pedido[0]->fecha == date("Y-m-d") || 1 == 1 )
			{
				if($prov == 1)
					$inputFileName = base_path()."/../templates/pedido_plantilla.xlsx";
				else if($prov == 2)
					$inputFileName = base_path()."/../templates/pedido_plantilla_oma_2.xlsx";
				else if($prov == 3)
					$inputFileName = base_path()."/../templates/pedido_plantilla_oma_3.xlsx";
				else
					$inputFileName = base_path()."/../templates/pedido_plantilla_oma.xlsx";
				
				$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
						
				$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
				
				$reader->setReadDataOnly(false);
				
				$spreadsheet = $reader->load($inputFileName);

				$sheets = $spreadsheet->getSheetNames();
				$cont=0;
				
				foreach($partidas as $partida)
				{
					$found = 0;
					
					if($prov == 1 || $prov==2)
					{
						$sn = 'PORTAFOLIO MAISON';
						$sheetData = $spreadsheet->getSheetByName($sn);

						for($i=2;!empty($sheetData->getCell('A'.$i)->getValue()) || !empty($sheetData->getCell('B'.$i)->getValue());$i++)
						{
							if(($sheetData->getCell('A'.$i)->getValue() == $partida->CodFood && !empty($partida->CodFood)) || ($sheetData->getCell('B'.$i)->getValue() == $partida->CodPrigo && !empty($partida->CodPrigo)))
							{
								$sheetData->getCell("G".$i)->setValue($partida->cantidad);
								$found = 1;
								break;
							}
						}
					}
					
					if($found == 0)
					{
						if($prov != 1)
							$sn = 'PORTAFOLIO PROVEEDOR';
						else
							$sn = 'PORTAFOLIO FSG';
						$sheetData = $spreadsheet->getSheetByName($sn);
						for($i=2;!empty($sheetData->getCell('A'.$i)->getValue());$i++)
						{
							if($sheetData->getCell('A'.$i)->getValue() == $partida->CodFood)
							{
								$sheetData->getCell("F".$i)->setValue($partida->cantidad);
								$found =1;
								break;
							}
						}
					}
					if($found)
						$cont++;
				}		
				
				DB::insert('insert into pedidos_recibe_prov (idPedido,idProveedor, fecha, hora ) values (?, ?, ?, ?)', [$id, 1,  date("Y-m-d"),date("H:i:s")]);
				DB::table('pedidos_pedido')
				->where('idPedido', $id)
				->update(['estado' => 2]);
				
				$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment; filename="pedido_'.date("Ymd").'.xlsx"');
				$writer->save("php://output");
			}
			else
			{	
				return view('pedidos.detalle', ['role' =>session('PDRole'),'pedido' => $pedido, 'partidas' => $partidas]);
			}
		}
		else
		{	
			
			return view('pedidos.detalle', ['role' =>session('PDRole'),'pedido' => $pedido, 'partidas' => $partidas]);
		}
	}	
	
	public function printPrigoRequest($id)
	{		

		$pedido = DB::table('pedidos_pedido')
		->join('sucursales', 'pedidos_pedido.idSucursal', '=', 'sucursales.id')
		->select('pedidos_pedido.idPedido' ,'sucursales.nombre AS sucursal' , 'pedidos_pedido.fechaCrea AS fecha', 'pedidos_pedido.estado', 'pedidos_pedido.comentario')
		->where('pedidos_pedido.idPedido', '=', $id)
		->get();
		
		$partidas = DB::table('pedidos_pedido_partida')
		->join('pedidos_articulo','pedidos_pedido_partida.idArticulo','=','pedidos_articulo.idArticulo')
		->select('pedidos_articulo.descripcion','pedidos_articulo.UnidadPrg','pedidos_pedido_partida.cantidad','pedidos_pedido_partida.cantidadFood','pedidos_articulo.CodPrigo','pedidos_articulo.CodFood')
		->where('pedidos_pedido_partida.idPedido','=',$id)
		->get();

		$inputFileName = base_path()."/../templates/pedido_plantilla.xlsx";
		
		$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
				
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
		
		$reader->setReadDataOnly(false);
		
		$spreadsheet = $reader->load($inputFileName);

		$sheets = $spreadsheet->getSheetNames();
		$cont=0;
		
		foreach($partidas as $partida)
		{
			$found = 0;
			$sn = 'PORTAFOLIO MAISON';
			$sheetData = $spreadsheet->getSheetByName($sn);
			
			for($i=2;!empty($sheetData->getCell('A'.$i)->getValue());$i++)
			{
				if($sheetData->getCell('A'.$i)->getValue() == $partida->CodFood || $sheetData->getCell('B'.$i)->getValue() == $partida->CodPrigo)
				{
					$sheetData->getCell("G".$i)->setValue($partida->cantidad);
					$found = 1;
					break;
				}
			}
			
			if($found == 0)
			{
				$sn = 'PORTAFOLIO FSG';
				$sheetData = $spreadsheet->getSheetByName($sn);
				for($i=2;!empty($sheetData->getCell('A'.$i)->getValue());$i++)
				{
					if($sheetData->getCell('A'.$i)->getValue() == $partida->CodFood)
					{
						$sheetData->getCell("F".$i)->setValue($partida->cantidad);
						$found =1;
						break;
					}
				}
			}
			if($found)
				$cont++;
		}
		
		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="pedido.xlsx"');
		$writer->save("php://output");
		
		//return view('pedidos.detalle', ['pedido' => $pedido, 'partidas' => $partidas]);

	}
	
	public function  uploadRequest(Request $request)
	{
		
		$str = '';
		$store= $request->input("sucursal");
		
		$config = DB::select("SELECT tipo FROM pedidos_sucursal_config WHERE idSucursal = ?;",[$store]);
		
		
		if($request->hasFile('xlsPedido') && $request->file('xlsPedido')->isValid() && !empty($store) && !empty($config[0]->tipo))
		{
			$sql = "SELECT GROUP_CONCAT(idProveedor SEPARATOR ',') provs FROM pedidos_sucursal_proveedor WHERE idSucursal = ? GROUP BY idSucursal";
			
			$provs = DB::select($sql,[$store]);
			$provsArray = explode(",",$provs[0]->provs);
			
			$file = $request->file('xlsPedido');
			
			if($file->getMimeType() == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
			{
				$inputFileName = $file->getRealPath();
				
				$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
				
				$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
				
				$reader->setReadDataOnly(true);
				
				//$reader->setLoadSheetsOnly("PORTAFOLIO");
				//$sheetnames = ['PORTAFOLIO MAISON','PORTAFOLIO FSG'];
				//$reader->setLoadSheetsOnly($sheetnames);
				
				$spreadsheet = $reader->load($inputFileName);
				
				$sheets = $spreadsheet->getSheetNames();

				$items = array();
				$noitems = array();
				$itemsDesabasto = array();
				
				foreach($sheets as $sn){
					
					//$sheetData = $spreadsheet->getActiveSheet();
					$sheetData = $spreadsheet->getSheetByName($sn);
					
					if($config[0]->tipo == 1)
					{
						$pedidoLetra = 'E';
						$foodLetra = 'A';
						$prigoLetra = 'A';
						$desLetra = 'B';
						$unidadLetra = 'C';
					}
					else
					{
						$pedidoLetra = 'G';
						$foodLetra = 'A';
						$prigoLetra = 'B';
						$desLetra = 'E';
						$unidadLetra = 'F';
					}
					
					for($i=0;$i<10;$i++)
					{
						if($sheetData->getCellByColumnAndRow($i,1)->getValue() == "Descripción del artículo")
						{
							$desLetra = Coordinate::stringFromColumnIndex($i);
						}
						if($sheetData->getCellByColumnAndRow($i,1)->getValue() == "Pedido" || $sheetData->getCellByColumnAndRow($i,1)->getValue() == "Cantidad" || $sheetData->getCellByColumnAndRow($i,1)->getValue() == "PEDIDO") 
						{
							$pedidoLetra = Coordinate::stringFromColumnIndex($i);
							$i= 10;
						}
					}
					
					for($i=2;!empty($sheetData->getCell($desLetra.$i)->getValue());$i++ )
					{
						$cantidad = $sheetData->getCell($pedidoLetra.$i)->getValue();
						if(is_numeric($cantidad))
						{	
							$cantidad = $sheetData->getCell($pedidoLetra.$i)->getValue();
							$CodPrigo = $sheetData->getCell($prigoLetra.$i)->getValue();
							$CodFood = $sheetData->getCell($foodLetra.$i)->getValue();
							
							if(!empty($cantidad))
							{
								$item = DB::table('pedidos_articulo')	
								->select('idArticulo AS id' ,'CodPrigo AS cod' , 'Descripcion AS name', 'UnidadPrg AS unit', 'UnidadFood AS unitfood', 'Conversion as conv','Estado')
								->whereIn('Estado', ['1','2'])
								->whereIn('idProveedor', $provsArray)
								->where(function ($query) use($CodPrigo,$CodFood){
									$query->orWhere('CodPrigo', '=', $CodPrigo)
									->orWhere('CodFood', '=', $CodFood);
								})->get();
								
								$tmpitem = new \stdClass();
								
								if(!empty($item[0]))
								{
									$tmpitem = $item[0];
									$tmpitem->cant = $cantidad;
									$tmpitem->cantprg = $cantidad * $item[0]->conv;
									if($item[0]->Estado == 1)
										$items[] = $tmpitem;
									else
										$itemsDesabasto[] = $tmpitem;
								}
								else
								{
									$tmpitem->cod =  "".(empty($sheetData->getCell($prigoLetra.$i)->getValue())?$sheetData->getCell($foodLetra.$i)->getValue():$sheetData->getCell($prigoLetra.$i)->getValue())."";
									$tmpitem->name = $sheetData->getCell($desLetra.$i)->getValue();
									$tmpitem->unit = $sheetData->getCell($unidadLetra.$i)->getValue();
									$tmpitem->cant = $cantidad;
									$tmpitem->cantprg = $cantidad;
									$noitems[] = $tmpitem;
								}	
								
								$tmpitem =null;				
							}	
						}
						else
						{
							$cantidad = 0 ; 
						}
					}
				}
				$str = $inputFileName;
			}
			else
			{
				$str ="Archivo no reconocido";
			}
		}
		else
		{
			$str .= "No file to load!!! Seleccione un restaurente antes de cargar el archivo excel" ;
		}
		
		if(!empty($noitems) || !empty($items))
		{
			
			$PDRole = session('PDRole');
			$sucursales = session('sucursales');
			$strValSucursales = "";
			
			if($PDRole != 1)
				if(!empty($sucursales))
					$strValSucursales = " IN (".$sucursales.") ";
				else
					$strValSucursales = " IN (29,30,31) ";
				
			$sucs = DB::select("SELECT sucursales.id AS idSucursal, sucursales.nombre FROM sucursales INNER JOIN pedidos_sucursal_config ON pedidos_sucursal_config.idSucursal = sucursales.id WHERE estado = 1 ".($PDRole != 1 ? "AND id ".$strValSucursales : "" )." GROUP BY id, nombre ORDER BY nombre;");	
			
			if(count($sucs) == 1)
				$selected= $sucs[0]->idSucursal;
			else
				$selected=0;
			
			$nitems = count($items);
			$nnoitems = count($noitems);
			$nitemsDesabasto = count($itemsDesabasto);
			
			$suggestedDate = date('Y-m-d', strtotime('+1 days'));

			return view('pedidos.newRequest', ['items' => $items, 'nitems' => $nitems, 'noitems' => $noitems, 'nnoitems' => $nnoitems, 'dsitems' => $itemsDesabasto, 'ndsitems' => $nitemsDesabasto, 'sucursal' => $store, 'selected' => $selected, 'sucursales' => $sucs, 'suggestedDate' => $suggestedDate ]);
		}
		else
		{
			return $str;
		}
	}
	
	public function saveReceiveRequest(Request $request)
	{
		DB::enableQueryLog();
		$user = Auth::user();		
		$idUsuario = $user->id;
		$uemail = $user->email;
		$uname = $user->name;
		$idPedido = $request->input('idPedido');
		$cantidades = $request->input('cantidad');
		$acciones = $request->input('accion');
		$ids = $request->input('id');
		$comentario = $request->input('comentario');
		$where = array();
		
		if(!empty($idPedido) && ( (!empty($ids) &&!empty($acciones) && !empty($cantidades))|| empty($ids)))
		{
			$comentario = empty($comentario)?"":$comentario;
			DB::insert('insert into pedidos_entrada (idUsuario, fecha, hora, idPedido, comentario) values (?, ?, ?, ?, ?)', [$idUsuario, date("Y-m-d"),date("H:i:s"), $idPedido, $comentario]);
			$lid = DB::getPdo()->lastInsertId();
			if(!empty($lid))
			{
				$sql = "";
				if(!empty($ids))
				{
					foreach($ids as $id=>$value)
					{
						if(!empty($sql))
							$sql .= ", ";
						
						$sql .= "( ".$lid." ,".$value.", ".$cantidades[$id].", ".$acciones[$id].",1)";
						$where[] = $value;
					}
				} 
				else
				{
					$where[] = 0;
				}
				if(!empty($where))
				{
					$items = DB::table('pedidos_pedido_partida')	
					->select('idArticulo', 'cantidad')
					->where('idPedido', '=', $idPedido)
					->whereNotIn('idArticulo', $where)
					->get();
					foreach($items as $item)
					{
						if(!empty($sql))
							$sql .= ", ";
						
						$sql .= "( ".$lid." ,".$item->idArticulo.", ".$item->cantidad.", 1,1)";
					}
				}
				if(!empty($sql))
				{
					DB::insert('insert into pedidos_entrada_partida (idEntrada, idArticulo, cantidad, accion, estado) values '.$sql);
					$url = url('/conrecibirpedido/'.$idPedido);

					Mail::send('pedidos.mailRecepcion', ['url' => $url,'name' => $uname, 'comentario' => $comentario], function ($message)
					{
						$message->from('reportes@prigo.com.mx', 'Reportes PRIGO');
						$message->to('melissagazu@prigo.com.mx');
						$message->cc(['ngallardo@prigo.com.mx']);
						$message->subject("Pedido recibido en sucursal Eric Kayser");
					});
					
					return "{ 'success': true, 'msg': 'Entrada #".$lid."'}";
				}
				else
				{
					return "{ 'success': false, 'msg': 'Error al guardar las partidas!'}";
				}				
			}
			else
			{
				return "{ 'success': false, 'msg': 'Error al guardar los datos!'}";
			}
			
		}
		else
		{
			return "{ 'success': false, 'msg': 'Datos faltantes!'}";
		}
	}
	
	public function saveRequest(Request $request)
	{
		DB::enableQueryLog();
		$user = Auth::user();
		$idUsuario = $user->id;
		$uemail = $user->email;
		$uname = $user->name;
		$sucursal = $request->input('sucursal');
		$ids = $request->input('id');
		$cantidades = $request->input('cantidad');
		$comentario = $request->input('comentario');
		$fechaRequerida = $request->input('fechaRequerida');
		
		if($fechaRequerida <= date("Y-m-d"))
			$fechaRequerida = date('Y-m-d', strtotime('+1 days'));
		
		if(!empty($sucursal) && !empty($ids) && !empty($cantidades) )
		{
			$comentario = empty($comentario)?"":$comentario;
			DB::insert('insert into pedidos_pedido (idUsuario, fechaCrea, horaCrea, fechaRequerida, idSucursal, comentario) values (?, ?, ?, ?, ?, ?)', [$idUsuario, date("Y-m-d"),date("H:i:s"), $fechaRequerida, $sucursal, $comentario]);
			$lid = DB::getPdo()->lastInsertId();
			
			if(!empty($lid))
			{
				$sql = "";
				foreach($ids as $id=>$value)
				{
					
					$items = DB::table('pedidos_articulo')	
					->select('Conversion')
					->where('idArticulo', '=', $value)
					->get();
					
					$conv = $items[0]->Conversion;
					
					$cantidad = 0;
					
					if($conv!=1)
						$cantidad = ceil($conv >0 ?$cantidades[$id]*$conv:0);
					else
						$cantidad = $cantidades[$id]*$conv;
					
					if(!empty($sql))
						$sql .= ", ";
					
					$sql .= "( ".$lid." ,".$value.", ".$cantidades[$id].", ".$cantidad.",1)";
				}
				if(!empty($sql))
				{
					DB::insert('insert into pedidos_pedido_partida (idPedido, idArticulo, cantidad, cantidadFood, estado) values '.$sql);
					
					$proveedores = DB::select("SELECT pedidos_proveedor.idProveedor, pedidos_proveedor.nombre FROM pedidos_pedido_partida INNER JOIN pedidos_articulo ON pedidos_pedido_partida.idArticulo = pedidos_articulo.idArticulo INNER JOIN pedidos_proveedor ON pedidos_proveedor.idProveedor = pedidos_articulo.idProveedor WHERE pedidos_pedido_partida.idPedido = ? GROUP BY pedidos_proveedor.idProveedor, pedidos_proveedor.nombre;",[$lid]);
					
					$sucursal = DB::select("SELECT direccion FROM sucursales WHERE id = ? ;",[$sucursal]);
					
					foreach($proveedores as $proveedor){
						
						$url = url('/pedido/'.$proveedor->idProveedor.'/'.$lid.'/');
						
						$idprov= $proveedor->idProveedor;
						
						$nprov= $proveedor->nombre;
						
						Mail::send('pedidos.mailPedido', ['url' => $url,'name' => $uname, 'proveedor' => $nprov, 'dirent' => $sucursal[0]->direccion, 'fecha' => date("Y-m-d"),'comentario' => $comentario], function ($message) use ($idprov)
						{
							$contactos = DB::select("SELECT GROUP_CONCAT(email SEPARATOR ',') contactos FROM pedidos_proveedor_contacto WHERE idProveedor= ? GROUP BY idProveedor ; ",[$idprov]);
							$ctos = explode(",",$contactos[0]->contactos);
														
							$message->from('reportes@prigo.com.mx', 'Reportes PRIGO');
							$message->to($ctos);
							$message->subject("Pedido generado desde Eric Kayser");
							
							/**/
						});
					
					}
					
					$url = url('/pedidoprigo/xls/'.$lid);

					Mail::send('pedidos.mailPedido', ['url' => $url,'name' => $uname, 'dirent' => $sucursal[0]->direccion, 'fecha' => date("Y-m-d"), 'comentario' => $comentario], function ($message) use ($uemail)
					{
						$message->to($uemail);
						$message->subject("Pedido generado desde Eric Kayser");
					});
					$hora = date("H:i");
					return response()->json([
						'success' => true,
						'msg' => "Pedido #".$lid." ".$hora
					]);
				}
				else
				{
					return "{ 'success': false, 'msg': 'Error al guardar las partidas!'}";
				}
			}
			else
			{
				return "{ 'success': false, 'msg': 'Error al guardar los datos!'}";
			}
		}
		else
		{
			return "{ 'success': false, 'msg': 'Informaci&oacute;n incompleta, verifique los datos capturados'}";
		}		
		
		
		return "{ 'success': true }";
	}
	
	
	public function saveRequestTrans(Request $request)
	{
		DB::enableQueryLog();
		$user = Auth::user();
		$idUsuario = $user->id;
		$uemail = $user->email;
		$uname = $user->name;
		$almacenTO = $request->input('almacen');
		$ids = $request->input('id');
		$cantidades = $request->input('cantidad');
		$comentario = $request->input('comentario');
		$fechaRequerida = $request->input('fechaRequerida');
		
		if($fechaRequerida <= date("Y-m-d"))
			$fechaRequerida = date('Y-m-d', strtotime('+1 days'));
		
		if(!empty($almacenTO) && !empty($ids) && !empty($cantidades) )
		{
			$comentario = empty($comentario)?"":$comentario;
			DB::insert('insert into pedidos_traslado (idUsuario, fechaCrea, horaCrea, fechaRequerida, idAlmacen, comentario) values (?, ?, ?, ?, ?, ?)', [$idUsuario, date("Y-m-d"),date("H:i:s"), $fechaRequerida, $almacenTO, $comentario]);
			$lid = DB::getPdo()->lastInsertId();
			
			if(!empty($lid))
			{
				$sql = "";
				foreach($ids as $id=>$value)
				{
					
					$items = DB::table('pedidos_articulo')	
					->select('idAlmacen')
					->where('idArticulo', '=', $value)
					->get();
					
					$almacenFROM = $items[0]->idAlmacen;
					
					if(!empty($sql))
						$sql .= ", ";
										
					$sql .= "( ".$lid." ,".$value.", '".$almacenFROM."', '".$almacenTO."', ".$cantidades[$id].",1)";
					
					
				}
				if(!empty($sql))
				{
					DB::insert('insert into pedidos_traslado_partida (idTraslado, idArticulo, idAlmacenOrigen, idAlmacenDestino, cantidad, estado) values '.$sql);
					
					/*$proveedores = DB::select("SELECT pedidos_proveedor.idProveedor, pedidos_proveedor.nombre FROM pedidos_pedido_partida INNER JOIN pedidos_articulo ON pedidos_pedido_partida.idArticulo = pedidos_articulo.idArticulo INNER JOIN pedidos_proveedor ON pedidos_proveedor.idProveedor = pedidos_articulo.idProveedor WHERE pedidos_pedido_partida.idPedido = ? GROUP BY pedidos_proveedor.idProveedor, pedidos_proveedor.nombre;",[$lid]);
					
					$sucursal = DB::select("SELECT direccion FROM sucursales WHERE id = ? ;",[$sucursal]);
					
					foreach($proveedores as $proveedor){
						
						$url = url('/pedido/'.$proveedor->idProveedor.'/'.$lid.'/');
						
						$idprov= $proveedor->idProveedor;
						
						$nprov= $proveedor->nombre;
						
						Mail::send('pedidos.mailPedido', ['url' => $url,'name' => $uname, 'proveedor' => $nprov, 'dirent' => $sucursal[0]->direccion, 'fecha' => date("Y-m-d"),'comentario' => $comentario], function ($message) use ($idprov)
						{
							$contactos = DB::select("SELECT GROUP_CONCAT(email SEPARATOR ',') contactos FROM pedidos_proveedor_contacto WHERE idProveedor= ? GROUP BY idProveedor ; ",[$idprov]);
							$ctos = explode(",",$contactos[0]->contactos);
														
							$message->from('reportes@prigo.com.mx', 'Reportes PRIGO');
							$message->to($ctos);
							$message->subject("Pedido generado desde Eric Kayser");
		
						});
					
					}
					
					$url = url('/pedidoprigo/xls/'.$lid);

					Mail::send('pedidos.mailPedido', ['url' => $url,'name' => $uname, 'dirent' => $sucursal[0]->direccion, 'fecha' => date("Y-m-d"), 'comentario' => $comentario], function ($message) use ($uemail)
					{
						$message->to($uemail);
						$message->cc('rgallardo@prigo.com.mx');	
						$message->subject("Pedido generado desde Eric Kayser");
					});
					*/
					
					
					$hora = date("H:i");
					return response()->json([
						'success' => true,
						'msg' => "Pedido #".$lid." ".$hora
					]);
				}
				else
				{
					return "{ 'success': false, 'msg': 'Error al guardar las partidas!'}";
				}
			}
			else
			{
				return "{ 'success': false, 'msg': 'Error al guardar los datos!'}";
			}
		}
		else
		{
			return "{ 'success': false, 'msg': 'Informaci&oacute;n incompleta, verifique los datos capturados'}";
		}		
		
		
		return "{ 'success': true }";
	}
	
	public function getRequest(Request $request)
	{
		$idUsuario = Auth::id();
		$draw = !empty($request->input('draw'))?$request->input('draw'):1;
		$start = !empty($request->input('start'))?$request->input('start'):0;
		$length = !empty($request->input('length'))?$request->input('length'):10;
		$queryarr = !empty($request->input('search'))?$request->input('search'):array();
		$query = !empty($queryarr["value"]) ? $queryarr["value"] : "%%" ;
		$sucursalq = "%%" ;
		$fechaq = "%%" ;
		
		$allitems = null;
		
		$PDRole = session('PDRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";

		if($PDRole != 1)
		{
			if(!empty($sucursales))
			{
				$strValSucursales = " IN (".$sucursales.") ";
			}
			else
			{				
				$strValSucursales = " IN (29,30,31) ";
			}
		}
		
		if(!empty($request->input('columns')))
		{
			$cols = $request->input('columns');
			$busca = array();
			foreach($cols as $col)
			{
				if(!empty($col["search"]["value"]))
				{
					switch($col["data"]){
						case "fecha":
							$fechaq = "%".$col["search"]["value"]."%";
						break;
						case "sucursal":
							$sucursalq = "%".$col["search"]["value"]."%";
						break;
					}
				}
			}
			if(!empty($busca))
			{
				$strBusca = implode(" AND ", $busca); 
			}
			
		}
		
		
		if($PDRole != 1)
		{

			$allitems = DB::table('pedidos_pedido')
			->join('sucursales', 'pedidos_pedido.idSucursal', '=', 'sucursales.id')
			->select('pedidos_pedido.idPedido')
			->where('pedidos_pedido.idPedido','LIKE',$query)
			->where('pedidos_pedido.fechaCrea','LIKE',$fechaq)
			->where('sucursales.nombre','LIKE',$sucursalq)
			->whereRaw('pedidos_pedido.idSucursal '.$strValSucursales)
			->orderBy('pedidos_pedido.idPedido', 'DESC')
			->get();
		}
		else
		{
			$allitems = DB::table('pedidos_pedido')
			->join('sucursales', 'pedidos_pedido.idSucursal', '=', 'sucursales.id')
			->select('pedidos_pedido.idPedido')
			->where('pedidos_pedido.idPedido','LIKE',$query)
			->where('pedidos_pedido.fechaCrea','LIKE',$fechaq)
			->where('sucursales.nombre','LIKE',$sucursalq)
			->orderBy('pedidos_pedido.idPedido', 'DESC')
			->get();
		}

		
		if($PDRole != 1)
		{
			$items = DB::table('pedidos_pedido')
			->join('sucursales', 'pedidos_pedido.idSucursal', '=', 'sucursales.id')
			->join('pedidos_pedido_estado AS ppe','ppe.idEstado','=','pedidos_pedido.estado')
			->select('pedidos_pedido.idPedido' ,'sucursales.nombre AS sucursal' , 'pedidos_pedido.fechaCrea AS fecha', 'pedidos_pedido.horaCrea AS hora', 'ppe.estado', 'pedidos_pedido.comentario','pedidos_pedido.codigoSAP AS codsap')
			->where('pedidos_pedido.idPedido','LIKE',$query)
			->where('pedidos_pedido.fechaCrea','LIKE',$fechaq)
			->where('sucursales.nombre','LIKE',$sucursalq)
			->whereRaw('pedidos_pedido.idSucursal '.$strValSucursales)
			->orderBy('pedidos_pedido.idPedido', 'DESC')
			->skip($start)->take($length)
			->get();
		}
		else 
		{
			$items = DB::table('pedidos_pedido')
			->join('sucursales', 'pedidos_pedido.idSucursal', '=', 'sucursales.id')
			->join('pedidos_pedido_estado AS ppe','ppe.idEstado','=','pedidos_pedido.estado')
			->select('pedidos_pedido.idPedido' ,'sucursales.nombre AS sucursal' , 'pedidos_pedido.fechaCrea AS fecha','pedidos_pedido.horaCrea AS hora', 'ppe.estado', 'pedidos_pedido.comentario','pedidos_pedido.codigoSAP AS codsap')
			->where('pedidos_pedido.idPedido','LIKE',$query)
			->where('pedidos_pedido.fechaCrea','LIKE',$fechaq)
			->where('sucursales.nombre','LIKE',$sucursalq)
			->orderBy('pedidos_pedido.idPedido', 'DESC')
			->skip($start)->take($length)
			->get();
		}
		
		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allitems),
			'recordsFiltered' => count($allitems),
			'data' => $items
		]);
	}	
	
	public function getTransfer(Request $request)
	{
		$idUsuario = Auth::id();
		$draw = !empty($request->input('draw'))?$request->input('draw'):1;
		$start = !empty($request->input('start'))?$request->input('start'):0;
		$length = !empty($request->input('length'))?$request->input('length'):10;
		$queryarr = !empty($request->input('search'))?$request->input('search'):array();
		$query = !empty($queryarr["value"]) ? $queryarr["value"] : "%%" ;
		$sucursalq = "%%" ;
		$fechaq = "%%" ;
		
		$allitems = null;
		
		$PDRole = session('PDRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";

		if($PDRole != 1)
		{
			if(!empty($sucursales))
			{
				$strValSucursales = " IN (".$sucursales.") ";
			}
			else
			{				
				$strValSucursales = " LIKE '%%' ";
			}
		}
		
		if(!empty($request->input('columns')))
		{
			$cols = $request->input('columns');
			$busca = array();
			foreach($cols as $col)
			{
				if(!empty($col["search"]["value"]))
				{
					switch($col["data"]){
						case "fecha":
							$fechaq = "%".$col["search"]["value"]."%";
						break;
						case "sucursal":
							$sucursalq = "%".$col["search"]["value"]."%";
						break;
					}
				}
			}
			if(!empty($busca))
			{
				$strBusca = implode(" AND ", $busca); 
			}
			
		}
		
		
		if($PDRole != 1)
		{

			$allitems = DB::table('pedidos_traslado')
			->join('pedidos_almacen', 'pedidos_traslado.idAlmacen', '=', 'pedidos_almacen.idAlmacen')
			->select('pedidos_traslado.idTraslado')
			->where('pedidos_traslado.idTraslado','LIKE',$query)
			->where('pedidos_traslado.fechaCrea','LIKE',$fechaq)
			->where('pedidos_almacen.almacen','LIKE',$sucursalq)
			->whereRaw('pedidos_traslado.idAlmacen '.$strValSucursales)
			->orderBy('pedidos_traslado.idTraslado', 'DESC')
			->get();
		}
		else
		{
			$allitems = DB::table('pedidos_traslado')
			->join('pedidos_almacen', 'pedidos_traslado.idAlmacen', '=', 'pedidos_almacen.idAlmacen')
			->select('pedidos_traslado.idTraslado')
			->where('pedidos_traslado.idTraslado','LIKE',$query)
			->where('pedidos_traslado.fechaCrea','LIKE',$fechaq)
			->where('pedidos_almacen.almacen','LIKE',$sucursalq)
			->orderBy('pedidos_traslado.idTraslado', 'DESC')
			->get();
		}

		
		if($PDRole != 1)
		{
			$items = DB::table('pedidos_traslado')
			->join('pedidos_almacen', 'pedidos_traslado.idAlmacen', '=', 'pedidos_almacen.idAlmacen')
			->join('pedidos_pedido_estado AS ppe','ppe.idEstado','=','pedidos_traslado.estado')
			->select('pedidos_traslado.idTraslado' ,'pedidos_almacen.almacen' , 'pedidos_traslado.fechaCrea AS fecha', 'pedidos_traslado.horaCrea AS hora', 'ppe.estado', 'pedidos_traslado.comentario','pedidos_traslado.codigoSAP AS codsap')
			->where('pedidos_traslado.idTraslado','LIKE',$query)
			->where('pedidos_traslado.fechaCrea','LIKE',$fechaq)
			->where('pedidos_almacen.almacen','LIKE',$sucursalq)
			->whereRaw('pedidos_traslado.idAlmacen '.$strValSucursales)
			->orderBy('pedidos_traslado.idTraslado', 'DESC')
			->skip($start)->take($length)
			->get();
		}
		else 
		{
			$items = DB::table('pedidos_traslado')
			->join('pedidos_almacen', 'pedidos_traslado.idAlmacen', '=', 'pedidos_almacen.idAlmacen')
			->join('pedidos_pedido_estado AS ppe','ppe.idEstado','=','pedidos_traslado.estado')
			->select('pedidos_traslado.idTraslado' ,'pedidos_almacen.almacen' , 'pedidos_traslado.fechaCrea AS fecha','pedidos_traslado.horaCrea AS hora', 'ppe.estado', 'pedidos_traslado.comentario','pedidos_traslado.codigoSAP AS codsap')
			->where('pedidos_traslado.idTraslado','LIKE',$query)
			->where('pedidos_traslado.fechaCrea','LIKE',$fechaq)
			->where('pedidos_almacen.almacen','LIKE',$sucursalq)
			->orderBy('pedidos_traslado.idTraslado', 'DESC')
			->skip($start)->take($length)
			->get();
		}
		
		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allitems),
			'recordsFiltered' => count($allitems),
			'data' => $items
		]);
	}	
	
	public function getItemsList(Request $request)
	{
		$draw = !empty($request->input('draw'))?$request->input('draw'):1;
		$start = !empty($request->input('start'))?$request->input('start'):0;
		$length = !empty($request->input('length'))?$request->input('length'):10;
		$queryarr = !empty($request->input('search'))?$request->input('search'):array();
		$query = !empty($queryarr["value"]) ? $queryarr["value"] : "" ;
		
		$allitems = DB::table('pedidos_articulo')
		->select('idArticulo')
		->where('Descripcion','LIKE', '%'.$query.'%')
		->get();
		
		$items = DB::table('pedidos_articulo')
		->leftJoin('pedidos_categoria_art', 'pedidos_categoria_art.idCategoria', '=', 'pedidos_articulo.idCategoria')
		->select('CodPrigo', 'CodFood', 'Descripcion', 'UnidadPrg', 'Conversion', 'UnidadFood', 'pedidos_categoria_art.nombre AS cat')
		->where('Descripcion','LIKE', '%'.$query.'%')
		->skip($start)->take($length)
		->get();
		
		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allitems),
			'recordsFiltered' => count($allitems),
			'data' => $items
		]);
	}
	
	public function getItems(Request $request)
	{
		DB::enableQueryLog();
		$q = $request->input('q');
		$store = $request->input('store');
		
		$items= array();
		
		if(!empty($store))
		{
			
			$sql = "SELECT GROUP_CONCAT(idProveedor SEPARATOR ',') provs, MAX(sucursales.idEmpresa)idEmpresa FROM pedidos_sucursal_proveedor INNER JOIN sucursales ON pedidos_sucursal_proveedor.idSucursal = sucursales.id WHERE idSucursal = ? GROUP BY idSucursal";
			$provs = DB::select($sql,[$store]);
			
			//$items = DB::select("select idArticulo AS id, CodPrigo AS cod , Descripcion AS name from pedidos_articulo where (CodPrigo Like '%?%' or CodFood Like '%?%' or Descripcion LIKE '%?%') AND Estado = 1 ", [$query,$query,$query]);
			$items = DB::table('pedidos_articulo')
			->leftJoin('pedidos_categoria_art', 'pedidos_categoria_art.idCategoria', '=', 'pedidos_articulo.idCategoria')
			->select('idArticulo AS id' ,'CodPrigo AS cod' ,'CodFood AS codfs' , 'Descripcion AS name', 'UnidadPrg AS unit', 'UnidadFood AS unitfood', 'Conversion AS conv', 'pedidos_articulo.Estado', 'pedidos_categoria_art.nombre AS cat')
			->whereIn('pedidos_articulo.Estado', ['1','2'])
			->whereIn('pedidos_articulo.idCompania', [3,$provs[0]->idEmpresa])
			->where('idProveedor', '=', $provs[0]->provs)
			->where(function ($query) use($q){
				$query->orWhere('CodPrigo', 'LIKE', '%'.$q. '%')
				->orWhere('CodFood', 'LIKE', '%'.$q. '%')
				->orWhere('Descripcion', 'LIKE', '%'.$q. '%');
			})
			->get();
		}
		
		return  response()->json([
			'total_count' => count($items),
			'items' => $items,
			'page' => 1
		]);
	}
	
	public function getItemsTrans(Request $request)
	{
		DB::enableQueryLog();
		$q = $request->input('q');
		
		$items= array();		

		$items = DB::table('pedidos_articulo')
		->leftJoin('pedidos_categoria_art', 'pedidos_categoria_art.idCategoria', '=', 'pedidos_articulo.idCategoria')
		->select('idArticulo AS id' ,'CodPrigo AS cod' ,'CodFood AS codfs' , 'Descripcion AS name', 'UnidadPrg AS unit', 'UnidadFood AS unitfood', 'Conversion AS conv', 'pedidos_articulo.Estado', 'pedidos_categoria_art.nombre AS cat')
		->whereIn('pedidos_articulo.Estado', ['1','2','3','0'])
		->whereIn('pedidos_articulo.idCompania', ['1','2','3','0'])
		->where('idProveedor', '=', 5)
		->where(function ($query) use($q){
			$query->orWhere('CodPrigo', 'LIKE', '%'.$q. '%')
			->orWhere('Descripcion', 'LIKE', '%'.$q. '%');
		})
		->get();			
		
		return  response()->json([
			'total_count' => count($items),
			'items' => $items,
			'page' => 1
		]);
	}
	
	public function getItemsReceive(Request $request)
	{
		DB::enableQueryLog();
		$q = $request->input('q');
		$id = $request->input('id');
		
		$partidas = DB::table('pedidos_pedido_partida')
                   ->select('idArticulo')
                   ->where('idPedido', '=', $id);

		
		//$items = DB::select("select idArticulo AS id, CodPrigo AS cod , Descripcion AS name from pedidos_articulo where (CodPrigo Like '%?%' or CodFood Like '%?%' or Descripcion LIKE '%?%') AND Estado = 1 ", [$query,$query,$query]);
		$items = DB::table('pedidos_articulo')
		->leftJoin('pedidos_pedido_partida as partidas',DB::raw('(partidas.idArticulo = pedidos_articulo.idArticulo AND idPedido'), '=', DB::raw($id.')'))
		->select('pedidos_articulo.idArticulo AS id' ,'pedidos_articulo.CodPrigo AS cod' , 'pedidos_articulo.Descripcion AS name', 'pedidos_articulo.UnidadPrg AS unit', 'pedidos_articulo.UnidadFood AS unitfood', 'pedidos_articulo.Conversion AS conv', DB::raw('IF(partidas.idArticulo IS NULL OR partidas.idArticulo = 0, 0, 1) AS inRequest'), DB::raw('IF(partidas.idArticulo IS NULL OR partidas.idArticulo = 0, 0, partidas.cantidad) AS requested'))
		->where('pedidos_articulo.Estado', '=', '1')
		->where(function ($query) use($q){
			$query->orWhere('pedidos_articulo.CodPrigo', 'LIKE', '%'.$q. '%')
			->orWhere('pedidos_articulo.CodFood', 'LIKE', '%'.$q. '%')
			->orWhere('pedidos_articulo.Descripcion', 'LIKE', '%'.$q. '%');
        })
		->get();
		
		return  response()->json([
			'total_count' => count($items),
			'items' => $items,
			'page' => 1
		]);
	}
	
	public function showReceivedOrder($id)
	{
		$entrada = DB::table('pedidos_entrada')
		->select('idPedido', 'idEntrada', 'fecha' ,'hora' , 'comentario')
		->where('idPedido','=', $id)
		->get();
		if(count($entrada))
		{
			$items = DB::table('pedidos_entrada_partida AS PEP')
			->join('pedidos_articulo AS PA', 'PA.idArticulo', '=', 'PEP.idArticulo')
			->join('pedidos_entrada_accion AS PEA', 'PEA.idAccion', '=', 'PEP.accion')
			->select('PA.CodPrigo', 'PA.CodFood', 'PA.Descripcion', 'PEP.cantidad', 'PA.UnidadFood', 'PEA.nombre AS accion')
			->where('PEP.idEntrada','=', $entrada[0]->idEntrada)
			->get();
		}
		else
		{
			$items = null;
		}
		return view('pedidos.recibido', [ 'idPedido' => $id, 'flagUser' => 1 ,'entradas' => $entrada, 'partidas' => $items]);
	}	
	
	public function receiveTransfer($id)
	{
		return ".";		
	}
	
	public function receiveOrder($id)
	{
		$entrada = DB::table('pedidos_entrada')
		->select('idPedido', 'idEntrada', 'fecha' ,'hora' , 'comentario')
		->where('idPedido','=', $id)
		->get();
		if(count($entrada))
		{
			$items = DB::table('pedidos_entrada_partida AS PEP')
			->join('pedidos_articulo AS PA', 'PA.idArticulo', '=', 'PEP.idArticulo')
			->join('pedidos_entrada_accion AS PEA', 'PEA.idAccion', '=', 'PEP.accion')
			->select('PA.CodPrigo', 'PA.CodFood', 'PA.Descripcion', 'PEP.cantidad', 'PA.UnidadFood', 'PEA.nombre AS accion')
			->where('PEP.idEntrada','=', $entrada[0]->idEntrada)
			->get();
			
			return view('pedidos.recibido', [ 'idPedido' => $id, 'entradas' => $entrada, 'partidas' => $items]);
		}
		else
		{				
			$items = DB::table('pedidos_pedido')
			->join('sucursales', 'pedidos_pedido.idSucursal', '=', 'sucursales.id')
			->select('pedidos_pedido.idPedido' ,'sucursales.nombre AS sucursal' , 'pedidos_pedido.fechaCrea AS fecha', 'pedidos_pedido.estado', 'pedidos_pedido.comentario')
			->where('pedidos_pedido.idPedido','=', $id)
			->get();
			return view('pedidos.recibir', [ 'idPedido' => $id, 'pedido' => $items]);
		}
	}
	
	public function sendEmail()
	{
		//$url = url('/pedido/food/5');
		$url = url('/pedidoprigo/xls/8');
		$title = "Email ";
        $content = "Notificacion";

        Mail::send('pedidos.mailPedido', ['url' => $url,'name' => 'Nancy Gallardo'], function ($message)
        {			
            $message->from('reportes@prigo.com.mx', 'Reportes PRIGO');	
			$message->to('rgallardo@prigo.com.mx');
            $message->cc('rgallardo@prigo.com.mx');
            $message->bcc('rgallardo@prigo.com.mx');		
			$message->subject("Pedido generado desde Eric Kayser");

        });
		
		$url = url('/pedidoprigo/food/8');
		try{
			Mail::send('pedidos.mailPedido', ['url' => $url,'name' => 'Nancy Gallardo'], function ($message)
			{			
				$message->from('reportes@prigo.com.mx', 'Reportes PRIGO');	
				$message->to('rgallardo@prigo.com.mx');
				$message->cc('rgallardo@prigo.com.mx');
				$message->bcc('rgallardo@prigo.com.mx');		
				$message->subject("Pedido generado desde Eric Kayser");

			});
		}
		catch(\Exception $e)
		{
			dd($e);
		}	
		return "Mensaje enviado";
	}

	public function sendDesabasto()
	{
		
		$items = DB::select("SELECT * FROM pedidos_articulo A WHERE A.Estado = ? AND A.idProveedor = ?;", [2,5]);

		if(count($items)>0){
		
			foreach($items AS $item)
			{
				echo $item->CodPrigo . " - " . $item->Descripcion . "<br>";
			}
			
			$emails = DB::select("SELECT B.id, B.name, B.email FROM config_app_access A INNER JOIN users B ON A.idUsuario = B.id WHERE A.idAplicacion = 1 AND A.idRole = 4 GROUP BY B.id, B.name, B.email ");
			
			if(!empty($emails))
			{
				
				$email = array();
				$nom = array();
				
				foreach($emails as $obj)
				{
					$email[] = $obj->email;
				}
				
				print_r($email);
				
				Mail::send('pedidos.mailDesabasto', ['items' => $items, 'fecha' => date("Y-m-d")], function ($message) use ($email)
				{
					$message->from('reportes@prigo.com.mx', 'Reportes PRIGO');
					$message->to($email);
					$message->cc(['pedidosap@prigo.com.mx','operaciones@maison-kayser.com.mx','rgallardo@prigo.com.mx']);
					$message->subject("Productos en DESABASTO PRIGO");
				});
			
			}
		}
		else
		{
			echo "0";
		}
	}
		
	public function getSBOItems()
	{
		$html = "";
		$updated = 0;
		$created = 0;	
		//$items = DB::connection('sqlsrv')->select("SELECT 5 AS idProv , ItemCode, ItemCode as FSCod ,ItemName,  SalUnitMsr, NumInSale, SalUnitMsr as unitfs, ItmsGrpCod , SellItem,CASE WHEN DfltWH IS NULL THEN '001' ELSE DfltWH END AS DfltWH ,U_TIPO, U_UBICACION, validFor, frozenFor, U_RESTAURANTE FROM OITM WHERE U_RESTAURANTE IN ('COPPLA','AMBOS', 'KAYSER','PRIGO') AND CreationDate = '' OR UpdateDate = '' ORDER BY U_RESTAURANTE ;"); //'ambos', 'kayser'
		//$items = DB::connection('sqlsrv')->select("SELECT CONVERT(char(10), CreateDate,126) AS CreateDate , CONVERT(char(10), UpdateDate,126) UpdateDate, 5 AS idProv , ItemCode, ItemCode as FSCod ,ItemName,  SalUnitMsr, NumInSale, SalUnitMsr as unitfs, ItmsGrpCod ,CASE WHEN DfltWH IS NULL THEN '001' ELSE DfltWH END AS DfltWH , CASE WHEN U_RESTAURANTE = 'KAYSER' THEN 1 WHEN U_RESTAURANTE = 'COPPLA' THEN 2 WHEN U_RESTAURANTE = 'AMBOS' THEN 3 ELSE 0 END AS idEmpresa, CASE WHEN validFor = 'Y' AND SellItem ='Y' AND U_RESTAURANTE IN ('COPPLA','AMBOS','KAYSER') THEN 1 WHEN U_RESTAURANTE='DESABASTO' THEN 2 ELSE 0 END Estado FROM OITM WHERE U_RESTAURANTE IN ('COPPLA','AMBOS', 'KAYSER','PRIGO') AND ( CONVERT (date, CreateDate)  = CONVERT (date, GETDATE()) OR CONVERT (date, UpdateDate) = CONVERT (date, GETDATE()) ) ORDER BY CreateDate DESC, UpdateDate DESC, U_RESTAURANTE ;"); //'ambos', 'kayser'
		$fecha = date("Y-m-d");
		$fechaAnterior = date("Y-m-d",strtotime($fecha." -1 day"));
		$items = DB::connection('sqlsrv')->select("SELECT CONVERT(char(10), CreateDate,126) AS CreateDate , CONVERT(char(10), UpdateDate,126) UpdateDate, 5 AS idProv , ItemCode, ItemCode as FSCod ,ItemName,  SalUnitMsr, NumInSale, SalUnitMsr as unitfs, ItmsGrpCod ,CASE WHEN DfltWH IS NULL THEN '001' ELSE DfltWH END AS DfltWH , CASE WHEN U_RESTAURANTE = 'KAYSER' THEN 1 WHEN U_RESTAURANTE = 'COPPLA' THEN 2 WHEN U_RESTAURANTE = 'AMBOS' THEN 3 ELSE 0 END AS idEmpresa, CASE WHEN validFor = 'Y' AND SellItem ='Y' AND U_RESTAURANTE IN ('COPPLA','AMBOS','KAYSER') THEN 1 WHEN U_RESTAURANTE='DESABASTO' THEN 2 ELSE 0 END Estado FROM OITM WHERE U_RESTAURANTE IN ('COPPLA','AMBOS', 'KAYSER','PRIGO','DESABASTO') AND ( CreateDate >= '$fechaAnterior' OR UpdateDate >= '$fechaAnterior' ) ORDER BY CreateDate DESC, UpdateDate DESC, U_RESTAURANTE ;"); //'ambos', 'kayser'
		$sql = "";
		foreach($items as $item)
		{
			$idArticulo =0;
			$sql = "SELECT idArticulo FROM pedidos_articulo WHERE idProveedor = 5 AND CodPrigo=?";
			$articulo = DB::select($sql,[$item->ItemCode]);
			$idArticulo= empty($articulo) ?0:$articulo[0]->idArticulo;
			//echo "ItemCode: ".$item->ItemCode.", ItemName: ".$item->ItemName."<br>";
			//$html .= "<br>" . $item->idProv."|". $item->ItemCode."|". $item->ItemCode."|".$item->ItemName."|". $item->SalUnitMsr."|1|". $item->SalUnitMsr."|". $item->ItmsGrpCod."|". $item->DfltWH."|validFor=".  $item->validFor."|". $item->U_RESTAURANTE;
			//if(($item->UpdateDate == date("Y-m-d") || $item->CreateDate == date("Y-m-d")) && $item->Estado != 0 && $idArticulo == 0)
			if($item->Estado != 0 && $idArticulo == 0)
			{
				$sql = "INSERT INTO pedidos_articulo VALUES (null,5,?,?,?,?,1,?,?,?,1,?,?) ";
				//echo "<br>".$sql."[$item->ItemCode, $item->ItemCode,$item->ItemName,$item->SalUnitMsr,$item->SalUnitMsr, $item->ItmsGrpCod ,$item->DfltWH,$item->idEmpresa,$item->Estado]<br>";
				DB::insert($sql,[ $item->ItemCode, $item->ItemCode,$item->ItemName,(empty($item->SalUnitMsr)?"PZA":$item->SalUnitMsr),(empty($item->SalUnitMsr)?1:$item->SalUnitMsr), $item->ItmsGrpCod ,$item->DfltWH,$item->idEmpresa,$item->Estado]);
				$created++;
			}
			//else if( ($item->UpdateDate == date("Y-m-d") || $item->CreateDate == date("Y-m-d")) && $idArticulo >0)
			else if( $idArticulo >0)
			{
				$sql = "UPDATE pedidos_articulo SET Descripcion = ? , UnidadPrg= ? , Conversion= '1', UnidadFood= ? , idCategoria = ? , idAlmacen = ?, estado = ?, idCompania=? WHERE idProveedor = 5 AND idTipoRegistro = 1 AND CodPrigo = ? AND idArticulo = ?;";
				//echo "<br>".$sql."[$item->ItemName,$item->SalUnitMsr,$item->SalUnitMsr, $item->ItmsGrpCod ,$item->DfltWH,$item->Estado,$item->idEmpresa, $item->ItemCode, $idArticulo]<br>";
				DB::update($sql,[$item->ItemName,(empty($item->SalUnitMsr)?"PZA":$item->SalUnitMsr),(empty($item->SalUnitMsr)?1:$item->SalUnitMsr), $item->ItmsGrpCod ,$item->DfltWH,$item->Estado,$item->idEmpresa, $item->ItemCode, $idArticulo]);
				$updated++;
			}		
		}
/*
		$items = DB::connection('sqlsrv')->select("SELECT CONVERT(char(10), CreateDate,126) AS CreateDate , CONVERT(char(10), UpdateDate,126) UpdateDate, 5 AS idProv , ItemCode, ItemCode as FSCod ,ItemName,  SalUnitMsr, NumInSale, SalUnitMsr as unitfs, ItmsGrpCod ,CASE WHEN DfltWH IS NULL THEN '001' ELSE DfltWH END AS DfltWH , CASE WHEN U_RESTAURANTE = 'KAYSER' THEN 1 WHEN U_RESTAURANTE = 'COPPLA' THEN 2 WHEN U_RESTAURANTE = 'AMBOS' THEN 3 ELSE 0 END AS idEmpresa, CASE WHEN validFor = 'Y' AND SellItem ='Y' AND U_RESTAURANTE IN ('COPPLA','AMBOS','KAYSER') THEN 1 WHEN U_RESTAURANTE='DESABASTO' THEN 2 ELSE 0 END Estado FROM OITM WHERE U_RESTAURANTE IN ('COPPLA','AMBOS', 'KAYSER','PRIGO') ORDER BY CreateDate DESC, UpdateDate DESC, U_RESTAURANTE ;"); //'ambos', 'kayser'
		
		
		$sql = "";
		
		foreach($items as $item)
		{
			////echo "UPDATE pedidos_articulo SET UnidadFood= '".$item->SalUnitMsr."', UnidadPrg='".$item->SalUnitMsr."'  WHERE idProveedor = 5 AND idTipoRegistro = 1 AND CodPrigo = '".$item->ItemCode."'; <br>";
		}
		
		//return $html."<br><br>" . $sql;
		*/
		return "Updated: $updated <br>Created: $created";
	}
	
	public function getExcelOpts(Request $request){
		$id= $request->input("idSucursal");
		
		if(!($id == 29 || $id ==30 || $id==31))
		{
			$items = DB::table('pedidos_sucursal_proveedor')
			->select('idProveedor')
			->where('idSucursal', '=', $id)
			->get();
				
			return response()->json([
				'success' => 1,
				'idProveedor' => $items[0]->idProveedor,
				'url' => route('descargaplantilla', ['id' => $id])
			]);
		}
		else
		{
			return response()->json([
				'success' => 1,
				'idProveedor' => 2,
				'url' => route('descargaplantilla', ['id' => $id])
			]);
		}
	}
	
	public function getRequestTemplate($id)
	{

		
		
		$sql = "SELECT GROUP_CONCAT(idProveedor SEPARATOR ',') provs, MAX(sucursales.idEmpresa)idEmpresa FROM pedidos_sucursal_proveedor INNER JOIN sucursales ON pedidos_sucursal_proveedor.idSucursal = sucursales.id WHERE idSucursal = ? GROUP BY idSucursal";
		$provs = DB::select($sql,[$id]);

		if(!empty($provs[0]) && !empty($provs[0]->idEmpresa))
		{
			if(!($id == 29 || $id ==30 || $id==31))
			{
				$idPedidos = "";
				$pedidos = array();
				
				$sql = "SELECT idPedido FROM pedidos_pedido WHERE idSucursal = ? AND fechaCrea BETWEEN '".date('Y-m-d', strtotime('-30 days'))."' AND '".date("Y-m-d")."' ORDER BY idPedido LIMIT 0,7;";
				$sql2 = $sql;
				$pedidosArray = DB::select($sql,[$id]);
				
				foreach($pedidosArray as $pedido){
					if(!empty($idPedidos))
						$idPedidos .= " ,";
					$idPedidos .= $pedido->idPedido;
				}


				$sql = "SELECT idArticulo FROM pedidos_pedido_partida WHERE idPedido IN ($idPedidos) GROUP BY idArticulo;";
				
				$articulosArray = DB::select($sql, []);
				
				$strPedidos = "0";
				
				
				foreach($articulosArray as $articulo){
					$pedidos[] = $articulo->idArticulo;
				}
							
				$itemsPedidos = DB::table('pedidos_articulo')
				->join('pedidos_categoria_art', 'pedidos_categoria_art.idCategoria', '=', 'pedidos_articulo.idCategoria')
				->select('idArticulo AS id' ,'CodPrigo AS cod' ,'Descripcion AS name', 'UnidadPrg AS unit','pedidos_categoria_art.nombre AS cat')
				->where('pedidos_articulo.Estado', '=', '1')
				->where('idProveedor', '=', $provs[0]->provs)
				->whereIN('pedidos_articulo.idCompania', [3,$provs[0]->idEmpresa])
				->whereIn('pedidos_articulo.idArticulo', $pedidos)
				->orderBy('pedidos_articulo.idCategoria', 'asc')
				->orderBy('pedidos_articulo.Descripcion', 'asc')
				->get();
				
				$itemsNopedidos = DB::table('pedidos_articulo')
				->join('pedidos_categoria_art', 'pedidos_categoria_art.idCategoria', '=', 'pedidos_articulo.idCategoria')
				->select('idArticulo AS id' ,'CodPrigo AS cod' ,'Descripcion AS name', 'UnidadPrg AS unit','pedidos_categoria_art.nombre AS cat')
				->where('pedidos_articulo.Estado', '=', '1')
				->where('idProveedor', '=', $provs[0]->provs)
				->whereNotIn('idArticulo', $pedidos)
				->orderBy('pedidos_articulo.idCategoria', 'asc')
				->orderBy('pedidos_articulo.Descripcion', 'asc')
				->get();
				
				$spreadsheet = new Spreadsheet();
				
				$sugestedItems = $spreadsheet->getActiveSheet();
				$sugestedItems->setTitle('Sugeridos');
				$sugestedItems->setCellValue('A1', 'Codigo');
				$sugestedItems->setCellValue('B1', 'Descripcion');
				$sugestedItems->setCellValue('C1', 'Unidad');
				$sugestedItems->setCellValue('D1', 'Categoria');
				$sugestedItems->setCellValue('E1', 'Cantidad');
				
				$sugestedItems->getColumnDimension('A')->setWidth(12);
				$sugestedItems->getColumnDimension('B')->setWidth(40);
				$sugestedItems->getColumnDimension('C')->setWidth(12);
				$sugestedItems->getColumnDimension('D')->setWidth(35);
				$sugestedItems->getColumnDimension('E')->setWidth(12);
				
				
				$row =1;
				
				foreach($itemsPedidos as $item)
				{
					$row++;
					$sugestedItems->setCellValue('A'.$row, $item->cod );
					$sugestedItems->setCellValue('B'.$row, $item->name);
					$sugestedItems->setCellValue('C'.$row, $item->unit);
					$sugestedItems->setCellValue('D'.$row, $item->cat);
					$sugestedItems->getStyle('A'.$row.':E'.$row)
					->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				}
				
				
				$sugestedItems->getStyle('A1:E1')
					->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$sugestedItems->getStyle('A1:E'.$row)
				->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$sugestedItems->getStyle('A1:E'.$row)
				->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$sugestedItems->getStyle('A1:E'.$row)
				->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$sugestedItems->getStyle('A1:A'.$row)
				->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$sugestedItems->getStyle('B1:B'.$row)
				->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$sugestedItems->getStyle('C1:C'.$row)
				->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$sugestedItems->getStyle('D1:D'.$row)
				->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$sugestedItems->getStyle('A1:E'.$row)
				->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
							
				$sugestedItems->getProtection()->setSheet(true);
				$spreadsheet->getDefaultStyle()->getProtection()->setLocked(false);
				$sugestedItems->getStyle('E1:E'.$row)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
				
				$allItems = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'Articulos');
				$spreadsheet->addSheet($allItems, 1);
				
				$allItems->setCellValue('A1', 'Codigo');
				$allItems->setCellValue('B1', 'Descripcion');
				$allItems->setCellValue('C1', 'Unidad');
				$allItems->setCellValue('D1', 'Categoria');
				$allItems->setCellValue('E1', 'Cantidad');
				
				$allItems->getColumnDimension('A')->setWidth(12);
				$allItems->getColumnDimension('B')->setWidth(40);
				$allItems->getColumnDimension('C')->setWidth(12);
				$allItems->getColumnDimension('D')->setWidth(35);
				$allItems->getColumnDimension('E')->setWidth(12);
				
				
				$row =1;
				
				foreach($itemsNopedidos as $item)
				{
					$row++;
					$allItems->setCellValue('A'.$row, $item->cod );
					$allItems->setCellValue('B'.$row, $item->name);
					$allItems->setCellValue('C'.$row, $item->unit);							
					$allItems->setCellValue('D'.$row, $item->cat);
					$allItems->getStyle('A'.$row.':E'.$row)
					->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				}
				$allItems->getStyle('A1:E1')
					->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$allItems->getStyle('A1:E'.$row)
				->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$allItems->getStyle('A1:E'.$row)
				->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$allItems->getStyle('A1:E'.$row)
				->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$allItems->getStyle('A1:A'.$row)
				->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$allItems->getStyle('B1:B'.$row)
				->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$allItems->getStyle('C1:C'.$row)
				->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$allItems->getStyle('D1:D'.$row)
				->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$allItems->getStyle('A1:E'.$row)
				->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				
				$allItems->getProtection()->setSheet(true);
				$spreadsheet->getDefaultStyle()->getProtection()->setLocked(false);
				$allItems->getStyle('E1:E'.$row)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
				
				$spreadsheet->setActiveSheetIndex(0);
				
				$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment; filename="plantilla_'.date("Ymd").'.xlsx"');
				$writer->save("php://output");
			}
			else
			{
				$inputFileName = base_path()."/../templates/pedido_plantilla_actual.xlsx";
				$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);

				$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

				$reader->setReadDataOnly(false);

				$spreadsheet = $reader->load($inputFileName);
				$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment; filename="plantilla_'.date("Ymd").'_'.date("H:i:s").'.xlsx"');
				$writer->save("php://output");
				
			}
		}
		else
		{
			return "Error: Restaruante sin proveedor asignado";
		}

	}
	
	public function getPendingTest(Request $request)
	{
		echo date("H:i")."<br>";
		
		//$sql = "SELECT idPedido FROM pedidos_pedido T0 WHERE NOT(T0.idSucursal IN (29,30,31)) ORDER BY idPedido DESC LIMIT 0,50;";
		//$sql = "SELECT T0.idPedido, COUNT(T0.idPedido) Partidas FROM pedidos_pedido T0 INNER JOIN pedidos_pedido_partida T1 ON T0.idPedido = T1.idPedido WHERE T0.procesadoSAP = 0 AND T0.estado != 0 GROUP BY T0.idPedido HAVING COUNT(T0.idPedido) >0 ORDER BY T0.idPedido ASC LIMIT 0,1;";
		$sql = "SELECT T0.idPedido, COUNT(T0.idPedido) Partidas FROM pedidos_pedido T0 INNER JOIN pedidos_pedido_partida T1 ON T0.idPedido = T1.idPedido WHERE T0.idPedido IN (1068) GROUP BY T0.idPedido HAVING COUNT(T0.idPedido) >0 ORDER BY T0.idPedido ASC LIMIT 0,1;";
		
		$arrpedidos = DB::select($sql);
		
		if(count($arrpedidos))
		{
			$idPedidos = "";
			
			foreach($arrpedidos AS $pedido)
			{
				if(!empty($idPedidos))
					$idPedidos .= ", ";
				$idPedidos .= $pedido->idPedido;
			}
			
			//$sql = "SELECT T0.idPedido, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap AS Sucursal, T2.name AS Usuario,  T4.idAlmacen, T5.idSAP idEmpresa FROM pedidos_pedido T0 INNER JOIN sucursales T1 ON T1.id = T0.idSucursal INNER JOIN empresas T5 ON T1.idEmpresa  = T5.idEmpresa INNER JOIN users T2 ON T2.id = T0.idUsuario INNER JOIN pedidos_pedido_partida T3 ON T3.idPedido = T0.idPedido INNER JOIN pedidos_articulo T4 ON T4.idArticulo = T3.idArticulo WHERE T0.idPedido IN(".$idPedidos.") GROUP BY T0.idPedido, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap, T2.name, T4.idAlmacen, T5.idSAP ORDER BY idPedido DESC";
			//$sql = "SELECT T0.idPedido, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap AS Sucursal, T2.name AS Usuario,  T4.idAlmacen, T5.idSAP idEmpresa FROM pedidos_pedido T0 INNER JOIN sucursales T1 ON T1.id = T0.idSucursal INNER JOIN empresas T5 ON T1.idEmpresa  = T5.idEmpresa INNER JOIN users T2 ON T2.id = T0.idUsuario INNER JOIN pedidos_pedido_partida T3 ON T3.idPedido = T0.idPedido INNER JOIN pedidos_articulo T4 ON T4.idArticulo = T3.idArticulo WHERE T0.procesadoSAP = 0 AND T0.estado != 0 AND T0.idPedido IN(".$idPedidos.") GROUP BY T0.idPedido, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap, T2.name, T4.idAlmacen, T5.idSAP ORDER BY idPedido ASC";
			$sql = "SELECT T0.idPedido, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap AS Sucursal, T2.name AS Usuario,  T4.idAlmacen, T5.idSAP idEmpresa FROM pedidos_pedido T0 INNER JOIN sucursales T1 ON T1.id = T0.idSucursal INNER JOIN empresas T5 ON T1.idEmpresa  = T5.idEmpresa INNER JOIN users T2 ON T2.id = T0.idUsuario INNER JOIN pedidos_pedido_partida T3 ON T3.idPedido = T0.idPedido INNER JOIN pedidos_articulo T4 ON T4.idArticulo = T3.idArticulo WHERE T0.estado != 0 AND T0.idPedido IN(".$idPedidos.") AND T4.idTipoRegistro = 1 GROUP BY T0.idPedido, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap, T2.name, T4.idAlmacen, T5.idSAP ORDER BY idPedido ASC;";
//				echo $sql;
			$pedidos = DB::select($sql);
			
			$objs = array();
				
			foreach($pedidos AS $pedido)
			{
				
				$object = new \stdClass();
				
				$object->Id = $pedido->idPedido;
				
				$object->Store = $pedido->Sucursal ;
				
				$object->CardCode = $pedido->idEmpresa;
				
				$object->CreationDate = $pedido->fechaCrea ;
				
				$object->RequiredDate = $pedido->fechaRequerida ;
				
				$object->CreationTime = $pedido->horaCrea ;
				
				$object->Creator = $pedido->Sucursal;
				
				$object->WareHouse = $pedido->idAlmacen ;
				
				$object->Comment = $pedido->comentario . " == Creado por ".$pedido->Usuario;
				
				$sql = "SELECT CodPrigo AS ItemId, cantidadFood AS Quantity FROM pedidos_pedido_partida T0 INNER JOIN pedidos_articulo T1 ON T0.idArticulo = T1.idArticulo WHERE idTipoRegistro = 1 AND T1.idAlmacen = ? AND idPedido = ?;";
				
				$partidas = DB::select($sql,[$pedido->idAlmacen,$pedido->idPedido]);
				
				$object->Lines = count($partidas) ;
				
				$object->Partidas = $partidas;
				
				$objs[] = $object;
				
			}
			
			return response()->json([
				'Total' => count($pedidos),
				'Pedidos' => $objs
			]);
		}
		else
		{
			return response()->json([
				'Total' => 0,
				'Pedidos' => $arrpedidos
			]);
		}
		
	}
	public function getPendingRequest(Request $request)
	{		

		$sql = "SELECT T0.idPedido, COUNT(T0.idPedido) Partidas FROM pedidos_pedido T0 INNER JOIN pedidos_pedido_partida T1 ON T0.idPedido = T1.idPedido WHERE T0.procesadoSAP = 0 AND T0.estado != 0 GROUP BY T0.idPedido HAVING COUNT(T0.idPedido) >0 ORDER BY T0.idPedido ASC LIMIT 0,1;";
		
		$arrpedidos = DB::select($sql);
		
		if(count($arrpedidos))
		{
			$idPedidos = "";
			
			foreach($arrpedidos AS $pedido)
			{
				if(!empty($idPedidos))
					$idPedidos .= ", ";
				$idPedidos .= $pedido->idPedido;
			}
			
			$sql = "SELECT T0.idPedido, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap AS Sucursal, T2.name AS Usuario,  T4.idAlmacen, T5.idSAP idEmpresa FROM pedidos_pedido T0 INNER JOIN sucursales T1 ON T1.id = T0.idSucursal INNER JOIN empresas T5 ON T1.idEmpresa  = T5.idEmpresa INNER JOIN users T2 ON T2.id = T0.idUsuario INNER JOIN pedidos_pedido_partida T3 ON T3.idPedido = T0.idPedido INNER JOIN pedidos_articulo T4 ON T4.idArticulo = T3.idArticulo WHERE T0.procesadoSAP = 0 AND T0.estado != 0 AND T0.idPedido IN(".$idPedidos.") AND T4.idTipoRegistro = 1 GROUP BY T0.idPedido, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap, T2.name, T4.idAlmacen, T5.idSAP ORDER BY idPedido ASC";
			
			$pedidos = DB::select($sql);
			
			$objs = array();
			
			$comentario = "";
			
			foreach($pedidos AS $pedido)
			{
				
				$comentario = $pedido->comentario . " == Creado por ".$pedido->Usuario;
				
				$comentario = substr($comentario,0,250);
				
				$object = new \stdClass();
				
				$object->Id = $pedido->idPedido;
				
				$object->Store = $pedido->Sucursal ;
				
				$object->CardCode = $pedido->idEmpresa;
				
				$object->CreationDate = $pedido->fechaCrea ;
				
				$object->RequiredDate = $pedido->fechaRequerida ;
				
				$object->CreationTime = $pedido->horaCrea ;
				
				$object->Creator = $pedido->Sucursal;
				
				$object->WareHouse = $pedido->idAlmacen ;
				
				$object->Comment = $pedido->comentario . " == Creado por ".$pedido->Usuario;
				
				$sql = "SELECT CodPrigo AS ItemId, cantidadFood AS Quantity FROM pedidos_pedido_partida T0 INNER JOIN pedidos_articulo T1 ON T0.idArticulo = T1.idArticulo WHERE  idTipoRegistro = 1 AND T1.idAlmacen = ? AND idPedido = ?;";
				
				$partidas = DB::select($sql,[$pedido->idAlmacen,$pedido->idPedido]);
				
				$object->Lines = count($partidas) ;
				
				$object->Partidas = $partidas;
				
				$objs[] = $object;				
			}
			
			return response()->json([
				'Total' => count($pedidos),
				'Pedidos' => $objs
			]);
		}
		else
		{
			return response()->json([
				'Total' => 0,
				'Pedidos' => $arrpedidos
			]);
		}
	}
	
	public function confirmRequest(Request $request)
	{
		if(!empty($request->input("idPedido")))
		{
			if(!empty($request->input("DocEntry")))
			{
				$contents = $request->input("DocEntry") . "|" . $request->input("idPedido") ;
				Storage::put('peticion.txt', $contents);
				
				$pedido = DB::select("SELECT idPedido,codigoSAP FROM pedidos_pedido WHERE idPedido = ? ;",[$request->input("idPedido")]);
				
				if(empty($pedido))
				{
					return "El pedido".$request->input("idPedido")." no existe";
				}
				else
				{
					$codigoSAP = (empty($pedido[1]->codigoSAP)? $request->input("DocEntry"): $pedido[1]->codigoSAP.",".$request->input("DocEntry") );
					DB::table('pedidos_pedido')
					->where('idPedido', $request->input("idPedido"))
					->update(['procesadoSAP' => 1, 'estado' => 4, 'codigoSAP' => $codigoSAP]);	
				}
			}
			else
			{
				return 2;
			}			
		}
		else
		{
			return 0;
		}
		
		return 1;
	}
	public function confirmTest(Request $request)
	{
		if(!empty($request->input("idPedido")))
		{
			if(!empty($request->input("DocEntry")))
			{
				$contents = $request->input("DocEntry") . "|" . $request->input("idPedido") ;
				Storage::put('peticion.txt', $contents);
				
				$pedido = DB::select("SELECT codigoSAP FROM pedidos_pedido WHERE idPedido = ? ;",[$request->input("idPedido")]);
				
				if(!empty($pedido))
				{
					return "El pedido".$request->input("idPedido")." no existe";
				}
				else
				{
					$codigoSAP = (empty($pedido[0]->codigoSAP)? $request->input("DocEntry"): $pedido[0]->codigoSAP.",".$request->input("DocEntry") );
					DB::table('pedidos_pedido')
					->where('idPedido', $request->input("idPedido"))
					->update(['procesadoSAP' => 1, 'estado' => 4, 'codigoSAP' => $codigoSAP]);	
				}
				
				return 1;
			}
			else
			{
				return 2;
			}			
		}
		else
		{
			return 0;
		}
		
		return 1;
	}
	
	public function getPendingTransferTest(Request $request)
	{
		echo date("H:i")."<br>";

		$sql = "SELECT T0.idTraslado, COUNT(T0.idTraslado) Partidas FROM pedidos_traslado T0 INNER JOIN pedidos_traslado_partida T1 ON T0.idTraslado = T1.idTraslado WHERE T0.idTraslado IN (1068) GROUP BY T0.idTraslado HAVING COUNT(T0.idTraslado) >0 ORDER BY T0.idTraslado ASC LIMIT 0,1;";
		
		$arrpedidos = DB::select($sql);
		
		if(count($arrpedidos))
		{
			$idTraslados = "";
			
			foreach($arrpedidos AS $pedido)
			{
				if(!empty($idTraslados))
					$idTraslados .= ", ";
				$idTraslados .= $pedido->idTraslado;
			}
			
			$sql = "SELECT T0.idTraslado, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap AS Sucursal, T2.name AS Usuario,  T4.idAlmacen, T5.idSAP idEmpresa FROM pedidos_traslado T0 INNER JOIN sucursales T1 ON T1.id = T0.idSucursal INNER JOIN empresas T5 ON T1.idEmpresa  = T5.idEmpresa INNER JOIN users T2 ON T2.id = T0.idUsuario INNER JOIN pedidos_traslado_partida T3 ON T3.idTraslado = T0.idTraslado INNER JOIN pedidos_articulo T4 ON T4.idArticulo = T3.idArticulo WHERE T0.estado != 0 AND T0.idTraslado IN(".$idTraslados.") AND T4.idTipoRegistro = 1 GROUP BY T0.idTraslado, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap, T2.name, T4.idAlmacen, T5.idSAP ORDER BY idTraslado ASC;";

//				echo $sql;

			$pedidos = DB::select($sql);
			
			$objs = array();
				
			foreach($pedidos AS $pedido)
			{
				
				$object = new \stdClass();
				
				$object->Id = $pedido->idTraslado;
				
				$object->Store = $pedido->Sucursal ;
				
				$object->CardCode = $pedido->idEmpresa;
				
				$object->CreationDate = $pedido->fechaCrea ;
				
				$object->RequiredDate = $pedido->fechaRequerida ;
				
				$object->CreationTime = $pedido->horaCrea ;
				
				$object->Creator = $pedido->Sucursal;
				
				$object->WareHouse = $pedido->idAlmacen ;
				
				$object->Comment = $pedido->comentario . " == Creado por ".$pedido->Usuario;
				
				$sql = "SELECT CodPrigo AS ItemId, idAlmacenOrigen AS WhsFrom, idAlmacenDestino AS WhsTo, cantidad AS Quantity FROM pedidos_traslado_partida T0 INNER JOIN pedidos_articulo T1 ON T0.idArticulo = T1.idArticulo WHERE idTipoRegistro = 1 AND T1.idAlmacen = ? AND idTraslado = ?;";
				
				$partidas = DB::select($sql,[$pedido->idAlmacen,$pedido->idTraslado]);
				
				$object->Lines = count($partidas) ;
				
				$object->Partidas = $partidas;
				
				$objs[] = $object;
				
			}
			
			return response()->json([
				'Total' => count($pedidos),
				'Pedidos' => $objs
			]);
		}
		else
		{
			return response()->json([
				'Total' => 0,
				'Pedidos' => $arrpedidos
			]);
		}
		
	}
	public function getPendingTransfer(Request $request)
	{		

		$sql = "SELECT T0.idTraslado, COUNT(T0.idTraslado) Partidas FROM pedidos_traslado T0 INNER JOIN pedidos_traslado_partida T1 ON T0.idTraslado = T1.idTraslado WHERE T0.procesadoSAP = 0 AND T0.estado != 0 GROUP BY T0.idTraslado HAVING COUNT(T0.idTraslado) >0 ORDER BY T0.idTraslado ASC LIMIT 0,1;";
		
		$arrpedidos = DB::select($sql);
		
		if(count($arrpedidos))
		{
			$idTraslados = "";
			
			foreach($arrpedidos AS $pedido)
			{
				if(!empty($idTraslados))
					$idTraslados .= ", ";
				$idTraslados .= $pedido->idTraslado;
			}
			
			$sql = "SELECT T0.idTraslado, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap AS Sucursal, T2.name AS Usuario, T0.idAlmacen WhsTO , T4.idAlmacen WhsFROM, T5.idSAP idEmpresa FROM pedidos_traslado T0 INNER JOIN sucursales T1 ON T1.id = T0.idSucursal INNER JOIN empresas T5 ON T1.idEmpresa  = T5.idEmpresa INNER JOIN users T2 ON T2.id = T0.idUsuario INNER JOIN pedidos_traslado_partida T3 ON T3.idTraslado = T0.idTraslado INNER JOIN pedidos_articulo T4 ON T4.idArticulo = T3.idArticulo WHERE T0.procesadoSAP = 0 AND T0.estado != 0 AND T0.idTraslado IN(".$idTraslados.") AND T4.idTipoRegistro = 1 GROUP BY T0.idTraslado, T0.fechaCrea , T0.fechaRequerida , T0.horaCrea, T0.comentario, T1.idSap, T2.name,T0.idAlmacen, T4.idAlmacen, T5.idSAP ORDER BY idTraslado ASC";
			
			$pedidos = DB::select($sql);
			
			$objs = array();
			
			$comentario = "";
			
			foreach($pedidos AS $pedido)
			{
				
				$comentario = $pedido->comentario . " == Creado por ".$pedido->Usuario;
				
				$comentario = substr($comentario,0,250);
				
				$object = new \stdClass();
				
				$object->Id = $pedido->idTraslado;
				
				$object->Store = $pedido->Sucursal ;
				
				$object->CardCode = $pedido->idEmpresa;
				
				$object->CreationDate = $pedido->fechaCrea ;
				
				$object->RequiredDate = $pedido->fechaRequerida ;
				
				$object->CreationTime = $pedido->horaCrea ;
				
				$object->Creator = $pedido->Sucursal;
				
				$object->WareHouse = $pedido->WhsTO ;
				
				$object->FromWareHouse = $pedido->WhsFROM ;
				
				$object->Comment = $pedido->comentario . " == Creado por ".$pedido->Usuario;
				
				$sql = "SELECT CodPrigo AS ItemId, idAlmacenOrigen AS WhsFrom, idAlmacenDestino AS WhsTo, cantidad AS Quantity FROM pedidos_traslado_partida T0 INNER JOIN pedidos_articulo T1 ON T0.idArticulo = T1.idArticulo WHERE idTipoRegistro = 1 AND idAlmacen = ? AND idTraslado = ?;";
				
				$partidas = DB::select($sql,[$pedido->WhsFROM,$pedido->idTraslado]);
				
				$object->Lines = count($partidas) ;
				
				$object->Partidas = $partidas;
				
				$objs[] = $object;				
			}
			
			return response()->json([
				'Total' => count($pedidos),
				'Transferencias' => $objs
			]);
		}
		else
		{
			return response()->json([
				'Total' => 0,
				'Pedidos' => $arrpedidos
			]);
		}
	}
	
	public function confirmTransfer(Request $request)
	{
		if(!empty($request->input("idPedido")))
		{
			if(!empty($request->input("DocEntry")))
			{
				$contents = $request->input("DocEntry") . "|" . $request->input("idTraslado") ;
				Storage::put('peticion.txt', $contents);
				
				$pedido = DB::select("SELECT idTraslado,codigoSAP FROM pedidos_traslado WHERE idTraslado = ? ;",[$request->input("idPedido")]);
				
				if(empty($pedido))
				{
					return "El pedido".$request->input("idTraslado")." no existe";
				}
				else
				{
					$codigoSAP = (empty($pedido[1]->codigoSAP)? $request->input("DocEntry"): $pedido[1]->codigoSAP.",".$request->input("DocEntry") );
					DB::table('pedidos_traslado')
					->where('idTraslado', $request->input("idPedido"))
					->update(['procesadoSAP' => 1, 'estado' => 4, 'codigoSAP' => $codigoSAP]);	
				}
			}
			else
			{
				return 2;
			}			
		}
		else
		{
			return 0;
		}
		
		return 1;
	}
	
	public function confirmTransferTest(Request $request)
	{
		if(!empty($request->input("idTraslado")))
		{
			if(!empty($request->input("DocEntry")))
			{
				$contents = $request->input("DocEntry") . "|" . $request->input("idTraslado") ;
				Storage::put('peticion.txt', $contents);
				
				$pedido = DB::select("SELECT codigoSAP FROM pedidos_traslado WHERE idTraslado = ? ;",[$request->input("idTraslado")]);
				
				if(!empty($pedido))
				{
					return "El pedido".$request->input("idTraslado")." no existe";
				}
				else
				{
					$codigoSAP = (empty($pedido[0]->codigoSAP)? $request->input("DocEntry"): $pedido[0]->codigoSAP.",".$request->input("DocEntry") );
					DB::table('pedidos_traslado')
					->where('idTraslado', $request->input("idTraslado"))
					->update(['procesadoSAP' => 1, 'estado' => 4, 'codigoSAP' => $codigoSAP]);	
				}
				
				return 1;
			}
			else
			{
				return 2;
			}			
		}
		else
		{
			return 0;
		}
		
		return 1;
	}
	
}