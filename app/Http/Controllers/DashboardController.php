<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;


class DashboardController extends Controller
{
	public $meses = array(array('Ene','Enero'),array('Feb','Febrero'),array('Mar','Marzo'),array('Abr','Abril'),array('May','Mayo'),array('Jun','Junio'),array('Jul','Julio'),array('Ago','Agosto'),array('Sep','Septiembre'),array('Oct','Octubre'),array('Nov','Noviembre'),array('Dic','Diciembre'));
	
    public function __construct()
    {
			 $this->middleware('auth', ['except' => ['getDiariaPDF']]);
			 $this->middleware(function ($request, $next) {
				$idUsuario = Auth::id();
				$user = Auth::user();
				$sql = "SELECT * FROM config_app_access WHERE idUsuario = ? AND idAplicacion = 6; ";
				$accesQuery = DB::select($sql, [$idUsuario]);
				if(!empty($accesQuery))
				{
					session(['DASHRole' => $accesQuery[0]->idRole]);
					 if($accesQuery[0]->idRole != 1 && $accesQuery[0]->idRole != 6)
					 {
						
						$sql = "SELECT group_concat( `idSucursal` separator ',') as `sucursales` FROM dashboard_sucursal_usuario INNER JOIN sucursales ON sucursales.id = dashboard_sucursal_usuario.idSucursal  WHERE idUsuario = ? GROUP BY idUsuario;";
						
						$sucursales = DB::select($sql, [$idUsuario]);
						if(!empty($sucursales))
						{
							session(['sucursales' => $sucursales[0]->sucursales]);
						}
					 }
				}
	
				return $next($request);
		});
    }

	public function indexAlex()
	{
		
		$Role = session('DASHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		$strValSucursalesSAP="";
		$strValSucursalesMicros="";
		$idUsuario = Auth::id();

		$sinUber = array(3,1,2,21,37,19,25,28,29,30,32,38,14);
		$aer = array(3,1,2,21,29,30);
		$tiendach= array(37,38);
		$tiendamd= array(7,5,6,36,32,25);
		$tiendabg= array(16,27,28,9,10,12,13,14);
		$tiendaxl= array(8,22,4,26,20,18,11,15,17);
		$bloqueadas = "30,34";
		
		if($Role != 1 && $Role != 2 && $Role != 6)
			if(!empty($sucursales))
			{
				$sql = "SELECT group_concat( `idSucursal` separator ',') as `sucursales`, group_concat(CONCAT('\"',`idSAP`,'\"') separator ',') as `sucursalesSap` , group_concat(CONCAT('\"',`idMicros`,'\"') separator ',') as `sucursalesMicros` FROM dashboard_sucursal_usuario INNER JOIN sucursales ON sucursales.id = dashboard_sucursal_usuario.idSucursal  WHERE idUsuario = ? GROUP BY idUsuario;";
				$idsSucursales = DB::select($sql, [$idUsuario]);
				$strValSucursales = "AND idSucursal IN (".$sucursales.") AND NOT(idSucursal  IN ($bloqueadas)) ";

				if(!empty($sucursales))
				{
					$strValSucursalesSAP = "AND idSucursal IN (".$idsSucursales[0]->sucursalesSap.") ";
					$strValSucursalesMicros = "AND idSucursal IN (".$idsSucursales[0]->sucursalesMicros.") ";
				}
			}
			else
			{
				$strValSucursales = "AND idSucursal  IN (0) ";
				$strValSucursalesSAP = "AND idSucursal  IN ('') ";
				$strValSucursalesMicros = "AND idSucursal  IN ('') ";
			}

			$sql = "SELECT CONCAT(idSap,'|',idMicros,'|',id) AS id, nombre FROM sucursales WHERE idEmpresa = 1 AND idTipo > 0 ".(empty($sucursales)?"":" AND id IN (".$sucursales.") ")." AND NOT( id IN (30)) ORDER BY nombre;";
			$sucursales = DB::select($sql);

			$sinUber = array(3,1,2,21,37,19,25,28,29,30,32,38,14);
			$aer = array(3,1,2,21,29,30);
			$tiendach= array(37,38);
			$tiendamd= array(7,5,6,36,32,25);
			$tiendabg= array(16,27,28,9,10,12,13,14);
			$tiendaxl= array(8,22,4,26,20,18,11,15,17);


		if($Role != 5)
		{

			$sql = "SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , SUM(if(monto is null, 0, monto)) total FROM finanzas_venta_mes_sap WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursalesSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha);";
			$venta = DB::select($sql);
			$sql = "SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , SUM(if(monto is null, 0, monto)) total FROM finanzas_costo_mes_sap WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursalesSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha);";
			$costo = DB::select($sql);
			
			$TIPer = array();
			$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100  crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 1 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 1 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
			$aux = DB::select($sql);
			$TIPer[0] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100  crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 2 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 2 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
			$aux = DB::select($sql);
			$TIPer[1] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100  crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 3 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 3 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
			$aux = DB::select($sql);
			$TIPer[2] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100  crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 4 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 4 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
			$aux = DB::select($sql);
			$TIPer[3] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100  crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 5 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 5 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
			$aux = DB::select($sql);
			$TIPer[4] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100  crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 6 AND NOT(idSucursal IN('KAYSARTZ','KAYSELK','KAYSACOX','KAYSOMEA','KAYSOMCA')) $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 6 AND NOT(idSucursal IN('KAYSARTZ','KAYSELK','KAYSACOX','KAYSOMEA','KAYSOMCA')) $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
			$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100  crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_dia_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 6 AND (NOT(idSucursal IN('KAYSARTZ','KAYSELK','KAYSACOX','KAYSOMEA','KAYSOMCA')) OR (idSucursal IN('KAYSARTZ') AND fecha>='2019-06-09') OR (idSucursal IN('KAYSELK') AND fecha>='2019-06-12') OR (idSucursal IN('KAYSACOX') AND fecha>='2019-06-20') OR (idSucursal IN('KAYSOMCA') AND fecha>='2019-06-27') )  GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_dia_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 6 AND (NOT(idSucursal IN('KAYSARTZ','KAYSELK','KAYSACOX','KAYSOMEA','KAYSOMCA')) OR (idSucursal IN('KAYSARTZ') AND fecha>='2018-06-09') OR (idSucursal IN('KAYSELK') AND fecha>='2018-06-12') OR (idSucursal IN('KAYSACOX') AND fecha>='2018-06-20') OR (idSucursal IN('KAYSOMCA') AND fecha>='2018-06-27') ) GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
			$aux = DB::select($sql);
			$TIPer[5] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;			
			$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100  crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 7 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 7 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
			$aux = DB::select($sql);
			$TIPer[6] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;	
			$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100  crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 8 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 8 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
			$aux = DB::select($sql);
			$TIPer[7] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100  crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 9 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 9 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
			$aux = DB::select($sql);
			$TIPer[8] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100  crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 10 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 10 $strValSucursalesSAP GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
			$aux = DB::select($sql);
			$TIPer[9] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			
			$TOTPer = array();
			$sql = "SELECT actual.mes, (actual.monto / anterior.monto -1)* 100 crecimiento FROM (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 1 $strValSucursalesSAP GROUP BY MONTH(fecha))actual INNER JOIN (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 1 $strValSucursalesSAP GROUP BY MONTH(fecha)) anterior ON actual.mes = anterior.mes;";
			$aux = DB::select($sql);
			$TOTPer[0] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT actual.mes, (actual.monto / anterior.monto -1)* 100 crecimiento FROM (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 2 $strValSucursalesSAP GROUP BY MONTH(fecha))actual INNER JOIN (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 2 $strValSucursalesSAP GROUP BY MONTH(fecha)) anterior ON actual.mes = anterior.mes;";
			$aux = DB::select($sql);
			$TOTPer[1] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT actual.mes, (actual.monto / anterior.monto -1)* 100 crecimiento FROM (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 3 $strValSucursalesSAP GROUP BY MONTH(fecha))actual INNER JOIN (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 3 $strValSucursalesSAP GROUP BY MONTH(fecha)) anterior ON actual.mes = anterior.mes;";
			$aux = DB::select($sql);
			$TOTPer[2] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT actual.mes, (actual.monto / anterior.monto -1)* 100 crecimiento FROM (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 4 $strValSucursalesSAP GROUP BY MONTH(fecha))actual INNER JOIN (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 4 $strValSucursalesSAP GROUP BY MONTH(fecha)) anterior ON actual.mes = anterior.mes;";
			$aux = DB::select($sql);
			$TOTPer[3] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT actual.mes, (actual.monto / anterior.monto -1)* 100 crecimiento FROM (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 5 $strValSucursalesSAP GROUP BY MONTH(fecha))actual INNER JOIN (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 5 $strValSucursalesSAP GROUP BY MONTH(fecha)) anterior ON actual.mes = anterior.mes;";
			$aux = DB::select($sql);
			$TOTPer[4] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT actual.mes, (actual.monto / anterior.monto -1)* 100 crecimiento FROM (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 6 $strValSucursalesSAP GROUP BY MONTH(fecha))actual INNER JOIN (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 6 $strValSucursalesSAP GROUP BY MONTH(fecha)) anterior ON actual.mes = anterior.mes;";
			$aux = DB::select($sql);
			$TOTPer[5] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT actual.mes, (actual.monto / anterior.monto -1)* 100 crecimiento FROM (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 7 $strValSucursalesSAP GROUP BY MONTH(fecha))actual INNER JOIN (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 7 $strValSucursalesSAP GROUP BY MONTH(fecha)) anterior ON actual.mes = anterior.mes;";
			$aux = DB::select($sql);
			$TOTPer[6] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT actual.mes, (actual.monto / anterior.monto -1)* 100 crecimiento FROM (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 8 $strValSucursalesSAP GROUP BY MONTH(fecha))actual INNER JOIN (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 8 $strValSucursalesSAP GROUP BY MONTH(fecha)) anterior ON actual.mes = anterior.mes;";
			$aux = DB::select($sql);
			$TOTPer[7] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT actual.mes, (actual.monto / anterior.monto -1)* 100 crecimiento FROM (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 9 $strValSucursalesSAP GROUP BY MONTH(fecha))actual INNER JOIN (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 9 $strValSucursalesSAP GROUP BY MONTH(fecha)) anterior ON actual.mes = anterior.mes;";
			$aux = DB::select($sql);
			$TOTPer[8] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;
			$sql = "SELECT actual.mes, (actual.monto / anterior.monto -1)* 100 crecimiento FROM (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 10 $strValSucursalesSAP GROUP BY MONTH(fecha))actual INNER JOIN (SELECT MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 10 $strValSucursalesSAP GROUP BY MONTH(fecha)) anterior ON actual.mes = anterior.mes;";
			$aux = DB::select($sql);
			$TOTPer[9] = empty($aux[0]->crecimiento)?0:$aux[0]->crecimiento;

			$gastos = array();
			$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-10-01' AND '2019-10-31' AND NOT(fcc.idCategoria = 1) $strValSucursalesSAP AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100') ) GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-10-01' AND '2019-10-31' GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
			$gastos[] = DB::select($sql);
			$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-09-01' AND '2019-09-30' AND NOT(fcc.idCategoria = 1) $strValSucursalesSAP AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100') ) GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-09-01' AND '2019-09-30' GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
			$gastos[] = DB::select($sql);
			$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-08-01' AND '2019-08-31' AND NOT(fcc.idCategoria = 1) $strValSucursalesSAP AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100') ) GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-08-01' AND '2019-08-31' GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
			$gastos[] = DB::select($sql);			
			$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-07-01' AND '2019-07-31' AND NOT(fcc.idCategoria = 1) $strValSucursalesSAP AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100') ) GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-07-01' AND '2019-07-31' GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
			$gastos[] = DB::select($sql);			
			$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-06-01' AND '2019-06-30' AND NOT(fcc.idCategoria = 1) $strValSucursalesSAP AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100') ) GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-06-01' AND '2019-06-30' GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
			$gastos[] = DB::select($sql);			
			$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-05-01' AND '2019-05-31' AND NOT(fcc.idCategoria = 1) $strValSucursalesSAP AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100') ) GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-05-01' AND '2019-05-31' GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
			$gastos[] = DB::select($sql);
			$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-04-01' AND '2019-04-30' AND NOT(fcc.idCategoria = 1) $strValSucursalesSAP AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100') ) GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-04-01' AND '2019-04-30' GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
			$gastos[] = DB::select($sql);				
			$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-03-01' AND '2019-03-31' AND NOT(fcc.idCategoria = 1) $strValSucursalesSAP AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100') ) GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-03-01' AND '2019-03-31' GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
			$gastos[] = DB::select($sql);
			$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-02-01' AND '2019-02-28' AND NOT(fcc.idCategoria = 1) $strValSucursalesSAP AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100') ) GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-02-01' AND '2019-02-28' GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
			$gastos[] = DB::select($sql);
			$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-01-01' AND '2019-01-31' AND NOT(fcc.idCategoria = 1) $strValSucursalesSAP AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100') ) GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-01-01' AND '2019-01-31' GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
			$gastos[] = DB::select($sql);
			
			$sql = "SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo ,  SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursalesSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha)";
			$gasto = DB::select($sql);

			$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.categoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' AND NOT(fcc.idCategoria = 1) AND fc.idCuenta IN ('6203-0200','6104-0300') $strValSucursalesSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.categoria;";
			$papeleria = DB::select($sql);

			$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.categoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' AND NOT(fcc.idCategoria = 1) AND fc.idCuenta IN ('6107-0100') $strValSucursalesSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.categoria;";
			$limpieza = DB::select($sql);

			$sql ="SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(puntos) puntos, AVG(cashman) cashman  FROM dashboard_auditoria_interna WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursales GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo";
			$audOBj = DB::select($sql);
			$audita = array();
			
			foreach($audOBj AS $aud )
			{
				$audita[$aud->periodo] = $aud;
			}

			$sql ="SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(calidad) calidad, AVG(costo) costo FROM dashboard_calidad_costo WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursales GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo";
			$calOBj = DB::select($sql);
			$calidad = array();
			
			foreach($calOBj AS $cal )
			{
				$calidad[$cal->periodo] = $cal;
			}

			$sql ="SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(error) pedido FROM dashboard_proceso_pedidos WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursales GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo";
			$ppedOBj = DB::select($sql);
			$ppedidos = array();
			
			foreach($ppedOBj AS $ped )
			{
				$ppedidos[$ped->periodo] = $ped;
			}

			$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(rotacion) rotacion, SUM(empleados) empleados FROM dashboard_rh_empleados WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursales GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo;";
			$rhOBj = DB::select($sql);
			$rh = array();
			
			foreach($rhOBj AS $rhi )
			{
				$rh[$rhi->periodo] = $rhi;
			}

			$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(tiempoCon) tiempoCon, AVG(tiempoPausa) tiempoPausa, SUM(aceptados) aceptados, AVG(tiempoAcepta) tiempoAcepta, SUM(ventaPerdida) ventaPerdida, AVG(tiempoPrepara) tiempoPrepara FROM dashboard_uber_eats WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursales GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo;";
			$ubrOBj = DB::select($sql);
			$uber = array();
			
			foreach($ubrOBj AS $ubr )
			{
				$uber[$ubr->periodo] = $ubr;
			}

			$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(calificacion) califica FROM dashboard_redes_soc WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursales AND calificacion > 0 AND NOT(idRed =7) GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo;";
			$redesOBj = DB::select($sql);
			$redes = array();
			
			foreach($redesOBj AS $red )
			{
				$redes[$red->periodo] = $red->califica;
			}
			
			$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(calificacion) califica FROM dashboard_redes_soc WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursales AND calificacion > 0 AND idRed =7 GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo;";
			$redesUberOBj = DB::select($sql);
			$redesUber = array();

			foreach($redesUberOBj AS $red )
			{
				$redesUber[$red->periodo] = $red->califica;
			}

			$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo,  AVG(eraa) eraa, AVG(erca) erca, AVG(ercs) ercs, AVG(err) err, AVG(evaa)evaa, AVG(evrz)evrz, AVG(evcs) evcs, AVG(evr) evr FROM  dashboard_encuesta WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursales GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo, idEncuesta;";
			$encuestaOBj = DB::select($sql);
			$encuestas = array();
			
			foreach($encuestaOBj AS $encuesta )
			{
				$encuestas[$encuesta->periodo] = $encuesta;
			}

			$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(capacita) capacita, AVG(uso) uso FROM  dashboard_capacita INNER JOIN sucursales ON dashboard_capacita.idSucursal = sucursales.id  WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursales GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo, idCapacita;";
			$capacitaOBj = DB::select($sql);
			$capacitaciones = array();
			
			foreach($capacitaOBj AS $capacita )
			{
				$capacitaciones[$capacita->periodo] = $capacita;
			}

			$sql = "SELECT periodo ,total, Salon, ClientesSalon, IF(ClientesSalon = 0, 0, (Salon/ClientesSalon)) AS ChequeSalon, vitrina, ClientesVitrina, IF(ClientesVitrina = 0, 0, (vitrina/ClientesVitrina)) AS ChequeVitrina, Domicilio, ClientesDomicilio, IF(ClientesDomicilio = 0, 0, (Domicilio/ClientesDomicilio)) AS ChequeDomicilio FROM (SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo , SUM(total) total, Sum(IF(idCentroIngreso='Salon',total,0)) Salon, Sum(IF(idCentroIngreso='Salon',if(clientes <13,clientes,1),0)) ClientesSalon ,Sum(IF(idCentroIngreso='Vitrina' OR idCentroIngreso='Institucional' OR idCentroIngreso='Kayser para llevar',total,0)) vitrina, Sum(IF(idCentroIngreso='Vitrina' OR idCentroIngreso='Institucional' OR idCentroIngreso='Kayser para llevar',1,0)) ClientesVitrina, Sum(IF(idCentroIngreso='UBER' OR idCentroIngreso ='OrderType 8' OR idCentroIngreso ='Rappi', total, 0 )) Domicilio, Sum(IF(idCentroIngreso='UBER' OR idCentroIngreso ='OrderType 8' OR idCentroIngreso ='Rappi', 1, 0 )) ClientesDomicilio FROM venta_cheque WHERE NOT(idSucursal IN ('Carmela y Sal')) AND fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursalesMicros GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY EXTRACT(YEAR_MONTH FROM fecha)) datos ;";
			$chequesOBj = DB::select($sql);
			$cheques = array();
			
			foreach($chequesOBj AS $cheque )
			{
				$cheques[$cheque->periodo] = $cheque;
			}


			$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha)periodo,SUM(monto) total FROM finanzas_budget_venta_dia_sap WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursalesSAP GROUP BY  EXTRACT(YEAR_MONTH FROM fecha);";
			$ventaBudgetOBj = DB::select($sql);
			$ventaBudget = array();
			
			foreach($ventaBudgetOBj AS $vbud )
			{
				$ventaBudget[$vbud->periodo] = $vbud->total;
			}

			$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha)periodo,SUM(monto) total FROM finanzas_budget_costo_dia_sap WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursalesSAP GROUP BY  EXTRACT(YEAR_MONTH FROM fecha);";
			$costoBudgetOBj = DB::select($sql);
			$costoBudget = array();
			
			foreach($costoBudgetOBj AS $vbud )
			{
				$costoBudget[$vbud->periodo] = $vbud->total;
			}

			$manto = array();
			$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-01-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-01-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-01-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-01-31',1,0)) abiertos FROM manto_solicitud solicitud WHERE estado != 4 AND fechaCrea BETWEEN '2019-01-01' AND '2019-01-31' AND tiempo = 24 GROUP BY tiempo) tiempos;";
			$mantos = DB::select($sql);
			$manto[201901] = empty($mantos[0]) ? 0: $mantos[0];
			$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-02-28',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-02-28',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-02-28',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-02-28',1,0)) abiertos FROM manto_solicitud solicitud WHERE estado != 4 AND fechaCrea BETWEEN '2019-02-01' AND '2019-02-28' AND tiempo = 24 GROUP BY tiempo) tiempos;";
			$mantos = DB::select($sql);
			$manto[201902] = empty($mantos[0]) ? 0: $mantos[0];				
			$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-03-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-03-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-03-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-03-31',1,0)) abiertos FROM manto_solicitud solicitud WHERE estado != 4 AND fechaCrea BETWEEN '2019-03-01' AND '2019-03-31' AND tiempo = 24 GROUP BY tiempo) tiempos;";
			$mantos = DB::select($sql);
			$manto[201903] = empty($mantos[0]) ? 0: $mantos[0];				
			$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-04-30',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-04-30',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-04-30',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-04-30',1,0)) abiertos FROM manto_solicitud solicitud WHERE estado != 4 AND fechaCrea BETWEEN '2019-04-01' AND '2019-04-30' AND tiempo = 24 GROUP BY tiempo) tiempos;";
			$mantos = DB::select($sql);
			$manto[201904] = empty($mantos[0]) ? 0: $mantos[0];					
			$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-05-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-05-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-05-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-05-31',1,0)) abiertos FROM manto_solicitud solicitud WHERE estado != 4 AND fechaCrea BETWEEN '2019-05-01' AND '2019-05-31' AND tiempo = 24 GROUP BY tiempo) tiempos;";
			$mantos = DB::select($sql);
			$manto[201905] = empty($mantos[0]) ? 0: $mantos[0];					
			$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-06-30',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-06-30',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-06-30',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-06-30',1,0)) abiertos FROM manto_solicitud solicitud WHERE estado != 4 AND fechaCrea BETWEEN '2019-06-01' AND '2019-06-30' AND tiempo = 24 GROUP BY tiempo) tiempos;";
			$mantos = DB::select($sql);
			$manto[201906] = empty($mantos[0]) ? 0: $mantos[0];						
			$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-07-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-07-31',1,0)) abiertos FROM manto_solicitud solicitud WHERE estado != 4 AND fechaCrea BETWEEN '2019-07-01' AND '2019-07-31' AND tiempo = 24 GROUP BY tiempo) tiempos;";
			$mantos = DB::select($sql);
			$manto[201907] = empty($mantos[0]) ? 0: $mantos[0];						
			$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-07-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-07-31',1,0)) abiertos FROM manto_solicitud solicitud WHERE estado != 4 AND fechaCrea BETWEEN '2019-08-01' AND '2019-08-31' AND tiempo = 24 GROUP BY tiempo) tiempos;";
			$mantos = DB::select($sql);
			$manto[201908] = empty($mantos[0]) ? 0: $mantos[0];						
			$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-07-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-07-31',1,0)) abiertos FROM manto_solicitud solicitud WHERE estado != 4 AND fechaCrea BETWEEN '2019-09-01' AND '2019-09-30' AND tiempo = 24 GROUP BY tiempo) tiempos;";
			$mantos = DB::select($sql);
			$manto[201909] = empty($mantos[0]) ? 0: $mantos[0];					
			$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-07-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-07-31',1,0)) abiertos FROM manto_solicitud solicitud WHERE estado != 4 AND fechaCrea BETWEEN '2019-10-01' AND '2019-10-31' AND tiempo = 24 GROUP BY tiempo) tiempos;";
			$mantos = DB::select($sql);
			$manto[201910] = empty($mantos[0]) ? 0: $mantos[0];

			return view('dashboard.indexAlex',['Role'=> $Role, 'selSucId' => 0,"sinUber" => $sinUber ,"aer" => $aer ,"tiendach" => $tiendach ,"tiendamd" => $tiendamd ,"tiendabg" => $tiendabg , "tiendaxl" => $tiendaxl , 'sucursales' => $sucursales, "sinUber" => $sinUber ,"aer" => $aer ,"tiendach" => $tiendach ,"tiendamd" => $tiendamd , 'encuestas' => $encuestas, "tiendabg" => $tiendabg , "tiendaxl" => $tiendaxl ,'nPeriodos'=> 11, 'periodos'=> ['ENE-19','FEB-19','MAR-19','ABR-19','MAY-19','JUN-19','JUL-19','AGO-19','SEP-19','OCT-19'], 'venta'=> $venta, 'costo'=> $costo, 'gasto'=> $gasto, 'gastos'=> $gastos, 'TIPer'=>$TIPer, 'TOTPer'=>$TOTPer, 'papeleria' => $papeleria, 'cheque' => $cheques, 'limpieza' =>$limpieza, 'redes' => $redes, 'redesUber' => $redesUber, 'uber' => $uber, 'rh' => $rh,'audita' => $audita, 'calidad'=> $calidad, 'ppedidos' => $ppedidos, 'manto' => $manto, 'costoBudget' => $costoBudget, 'ventaBudget'=> $ventaBudget,'capacitaciones' => $capacitaciones,'dperiodos'=> ['201901','201902','201903','201904','201905','201906','201907','201908','201909','201910'] ]);
		}
		else 
		{
			$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(rotacion) rotacion, SUM(empleados) empleados FROM dashboard_rh_empleados WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $strValSucursales GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo;";
			$rhOBj = DB::select($sql);
			$rh = array();
			
			foreach($rhOBj AS $rhi )
			{
				$rh[$rhi->periodo] = $rhi;
			}
			return view('dashboard.indexAlex',['Role'=> $Role, 'selSucId' => 0,"sinUber" => $sinUber ,"aer" => $aer ,"tiendach" => $tiendach ,"tiendamd" => $tiendamd ,"tiendabg" => $tiendabg , "tiendaxl" => $tiendaxl , 'sucursales' => $sucursales, 'rh' => $rh, 'nPeriodos'=> 11, 'periodos'=> ['NOV-18','DIC-18','ENE-19','FEB-19','MAR-19','ABR-19','MAY-19','JUN-19','JUL-19','AGO-19','SEP-19','OCT-19'], 'dperiodos'=> ['201901','201902','201903','201904','201905','201906','201907','201908','201909','201910']]);
		}
	}
	
	public function detSucKpi($suc)
	{

		$Role = session('DASHRole');

		$whereSuc = "";
		$whereSucMic = "";
		$whereSucSAP = "";
		$whereSucmICc = "";
		$selSuc="";
		
		if(!empty($suc)){
				$suc = base64_decode($suc);
				$selSuc = $suc;
				$sucArr = explode("|",$suc);
				$selSucName = $sucArr[1];
				$selSucId = $sucArr[2];
				$whereSuc = " AND idSucursal = '".$sucArr[0]."' ";
				$whereSucMic = " AND idSucursal = '".$sucArr[1]."' ";
				$whereSucSAP = " AND idSap = '".$sucArr[0]."' ";
				$whereSucmICc = " AND idMicros = '".$sucArr[1]."' ";
			}
			$bloqueadas = "30,34";

			$sql = "SELECT CONCAT(idSap,'|',idMicros,'|',id) AS id, nombre FROM sucursales WHERE idEmpresa = 1 AND NOT(id  IN ($bloqueadas)) AND idTipo > 0 ORDER BY nombre;";
			$sucursales = DB::select($sql);


			$sinUber = array(3,1,2,21,37,19,25,28,29,30,32,38,14);
			$aer = array(3,1,2,21,29,30);
			$tiendach= array(37,38);
			$tiendamd= array(7,5,6,36,32,25);
			$tiendabg= array(16,27,28,9,10,12,13,14);
			$tiendaxl= array(8,22,4,26,20,18,11,15,17);


			if($Role != 5)
			{
				$sql = "SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , SUM(if(monto is null, 0, monto)) total FROM finanzas_venta_mes_sap WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' GROUP BY EXTRACT(YEAR_MONTH FROM fecha);";
				$ventaTOT = DB::select($sql);

				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo,SUM(monto) total FROM finanzas_gasto_mes_sap WHERE idSucursal IN ('GIOTTO', 'ALLIORI') AND fecha BETWEEN '2019-01-01' AND '2019-10-31' GROUP BY EXTRACT(YEAR_MONTH FROM fecha);";

				$giotto = DB::select($sql);

				$sql = "SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , SUM(if(monto is null, 0, monto)) total FROM finanzas_venta_mes_sap WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha);";
				$venta = DB::select($sql);
				$sql = "SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , SUM(if(monto is null, 0, monto)) total FROM finanzas_costo_mes_sap WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha);";
				$costo = DB::select($sql);
				/*$sql = "SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , SUM(if(monto is null, 0, monto)) total FROM finanzas_venta_mes_sap WHERE fecha BETWEEN '2018-11-01' AND '2019-03-31' GROUP BY EXTRACT(YEAR_MONTH FROM fecha);";
				$gasto = DB::select($sql);*/
				
				$TIPer = array();
				//$TIPer[0] = empty($aux[0]->crecimiento) ? 0 : $aux[0]->crecimiento;
				$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100 crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 1 $whereSuc GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 1 $whereSuc GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
				$aux = DB::select($sql);
				$TIPer[0] = empty($aux[0]->crecimiento) ? 0 : $aux[0]->crecimiento;
				$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100 crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 2 $whereSuc GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 2 $whereSuc GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
				$aux = DB::select($sql);
				$TIPer[1] = empty($aux[0]->crecimiento) ? 0 : $aux[0]->crecimiento;
				$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100 crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 3 $whereSuc GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 3 $whereSuc GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
				$aux = DB::select($sql);
				$TIPer[2] = empty($aux[0]->crecimiento) ? 0 : $aux[0]->crecimiento;
				$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100 crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 4 $whereSuc GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 4 $whereSuc GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
				$aux = DB::select($sql);
				$TIPer[3] = empty($aux[0]->crecimiento) ? 0 : $aux[0]->crecimiento;				
				$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100 crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 5 $whereSuc GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 5 $whereSuc GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
				$aux = DB::select($sql);
				$TIPer[4] = empty($aux[0]->crecimiento) ? 0 : $aux[0]->crecimiento;
				$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100 crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 6 $whereSuc GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 6 $whereSuc GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
				$aux = DB::select($sql);
				$TIPer[5] = empty($aux[0]->crecimiento) ? 0 : $aux[0]->crecimiento;
				$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100 crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 7 $whereSuc GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 7 $whereSuc GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
				$aux = DB::select($sql);
				$TIPer[6] = empty($aux[0]->crecimiento) ? 0 : $aux[0]->crecimiento;				
				$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100 crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 8 $whereSuc GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 8 $whereSuc GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
				$aux = DB::select($sql);
				$TIPer[7] = empty($aux[0]->crecimiento) ? 0 : $aux[0]->crecimiento;				
				$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100 crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 9 $whereSuc GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 9 $whereSuc GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
				$aux = DB::select($sql);
				$TIPer[8] = empty($aux[0]->crecimiento) ? 0 : $aux[0]->crecimiento;				
				$sql = "SELECT (SUM(Actual) / SUM(Anterior) -1) * 100 crecimiento FROM (SELECT actual.idSucursal, actual.mes,actual.monto Actual, anterior.monto Anterior, actual.monto / anterior.monto -1 AS TI FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2019 AND MONTH(fecha) = 10 $whereSuc GROUP BY idSucursal,MONTH(fecha)) actual INNER JOIN  (SELECT idSucursal, MONTH(fecha) mes, SUM(if(monto is null, 0, monto)) monto FROM finanzas_venta_mes_sap venta WHERE YEAR(fecha) = 2018 AND MONTH(fecha) = 10 $whereSuc GROUP BY idSucursal,MONTH(fecha)) anterior ON actual.idSucursal = anterior.idSucursal) base GROUP BY mes;";
				$aux = DB::select($sql);
				$TIPer[9] = empty($aux[0]->crecimiento) ? 0 : $aux[0]->crecimiento;			
		
				$gastos = array();
				$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-09-01' AND '2019-10-31' AND NOT(fcc.idCategoria = 1) AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100')  ) $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-10-01' AND '2019-10-31' $whereSuc GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
				$gastos[] = DB::select($sql);
				$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-09-01' AND '2019-10-31' AND NOT(fcc.idCategoria = 1) AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100')  ) $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-09-01' AND '2019-09-30' $whereSuc GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
				$gastos[] = DB::select($sql);
				$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-08-01' AND '2019-08-31' AND NOT(fcc.idCategoria = 1) AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100')  ) $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-08-01' AND '2019-08-31' $whereSuc GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
				$gastos[] = DB::select($sql);
				$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-07-01' AND '2019-07-31' AND NOT(fcc.idCategoria = 1) AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100')  ) $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-07-01' AND '2019-07-31' $whereSuc GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
				$gastos[] = DB::select($sql);
				$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-06-01' AND '2019-06-30' AND NOT(fcc.idCategoria = 1) AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100')  ) $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-06-01' AND '2019-06-30' $whereSuc GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
				$gastos[] = DB::select($sql);
				$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-05-01' AND '2019-05-31' AND NOT(fcc.idCategoria = 1) AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100')  ) $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-05-01' AND '2019-05-31' $whereSuc GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
				$gastos[] = DB::select($sql);
				$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-04-01' AND '2019-04-30' AND NOT(fcc.idCategoria = 1) AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100')  ) $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-04-01' AND '2019-04-30' $whereSuc GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
				$gastos[] = DB::select($sql);
				$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-03-01' AND '2019-03-31' AND NOT(fcc.idCategoria = 1) AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100')  ) $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-03-01' AND '2019-03-31' $whereSuc GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
				$gastos[] = DB::select($sql);
				$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-02-01' AND '2019-02-28' AND NOT(fcc.idCategoria = 1) AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100')  ) $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-02-01' AND '2019-02-28' $whereSuc GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
				$gastos[] = DB::select($sql);
				$sql = "SELECT actual.periodo, actual.total, budget.monto FROM (SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.idCategoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-01-01' AND '2019-01-31' AND NOT(fcc.idCategoria = 1) AND NOT( fc.idCuenta IN ('6205-0200','6203-0200','6107-0100')  ) $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.idCategoria) actual LEFT JOIN (SELECT categoria, SUM(monto) monto FROM finanzas_budget_gasto_cuenta WHERE fecha BETWEEN '2019-01-01' AND '2019-01-31' $whereSuc GROUP BY categoria ) budget ON budget.categoria = actual.idCategoria ;";
				$gastos[] = DB::select($sql);				
				
				$sql = "SELECT  EXTRACT(YEAR_MONTH FROM fecha) periodo ,  SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha)";
				$gasto = DB::select($sql);
		
				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.categoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' AND NOT(fcc.idCategoria = 1) AND fc.idCuenta IN ('6203-0200','6104-0300') $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.categoria;";
				$papeleria = DB::select($sql);
		
				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo , fcc.categoria, SUM(if(monto is null, 0, monto)) total FROM finanzas_gasto_mes_sap gs INNER JOIN finanzas_cuentas fc ON gs.idCuenta = fc.idCuenta INNER JOIN finanzas_categoria_cuenta fcc ON fc.idCategoria = fcc.idCategoria WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' AND NOT(fcc.idCategoria = 1) AND fc.idCuenta IN ('6107-0100') $whereSuc GROUP BY EXTRACT(YEAR_MONTH FROM fecha),fcc.categoria;";
				$limpieza = DB::select($sql);

				/*$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha), idCentroIngreso, SUM(clientes) clientes, SUM(total)/SUM(clientes) cheque FROM venta_cheque WHERE fecha BETWEEN '2018-11-01' AND '2019-03-31' AND idSucursal != 'Carmela y Sal' AND clientes <13 AND idCentroIngreso ='Salon' $whereSucMic GROUP BY EXTRACT(YEAR_MONTH FROM fecha), idCentroIngreso;";
			//$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha), idCentroIngreso, SUM(IF(clientes=0,1,clientes)) clientes, SUM(total)/SUM(IF(clientes=0,1,clientes)) cheque FROM venta_cheque WHERE fecha BETWEEN '2018-11-01' AND '2019-03-31' AND idSucursal != 'Carmela y Sal' AND clientes <13 $whereSucMic GROUP BY EXTRACT(YEAR_MONTH FROM fecha), idCentroIngreso;";
				$cheque = DB::select($sql);
				//var_dump($cheque);
				//exit;*/

				$sql ="SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(puntos) puntos, AVG(cashman) cashman  FROM dashboard_auditoria_interna INNER JOIN sucursales ON dashboard_auditoria_interna.idSucursal = sucursales.id  WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSucSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo";
				$audOBj = DB::select($sql);
				$audita = array();
				
				foreach($audOBj AS $aud )
				{
					$audita[$aud->periodo] = $aud;
				}


				$sql ="SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(calidad) calidad, AVG(costo) costo FROM dashboard_calidad_costo  INNER JOIN sucursales ON dashboard_calidad_costo.idSucursal = sucursales.id WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSucSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo";
				$calOBj = DB::select($sql);
				$calidad = array();
				
				foreach($calOBj AS $cal )
				{
					$calidad[$cal->periodo] = $cal;
				}
	
				$sql ="SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(error) pedido FROM dashboard_proceso_pedidos  INNER JOIN sucursales ON dashboard_proceso_pedidos.idSucursal = sucursales.id  WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSucSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo";
				$ppedOBj = DB::select($sql);
				$ppedidos = array();
				
				foreach($ppedOBj AS $ped )
				{
					$ppedidos[$ped->periodo] = $ped;
				}

				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(rotacion) rotacion, SUM(empleados) empleados FROM dashboard_rh_empleados INNER JOIN sucursales ON dashboard_rh_empleados.idSucursal = sucursales.id  WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSucSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo;";
				$rhOBj = DB::select($sql);
				$rh = array();

				foreach($rhOBj AS $rhi )
				{
					$rh[$rhi->periodo] = $rhi;
				}

				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo,  AVG(eraa) eraa, AVG(erca) erca, AVG(ercs) ercs, AVG(err) err, AVG(evaa)evaa, AVG(evrz)evrz, AVG(evcs) evcs, AVG(evr) evr FROM  dashboard_encuesta  INNER JOIN sucursales ON dashboard_encuesta.idSucursal = sucursales.id  WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSucSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo, idEncuesta;";
				$encuestaOBj = DB::select($sql);
				$encuestas = array();
				
				foreach($encuestaOBj AS $encuesta )
				{
					$encuestas[$encuesta->periodo] = $encuesta;
				}

				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(capacita) capacita, AVG(uso) uso FROM  dashboard_capacita INNER JOIN sucursales ON dashboard_capacita.idSucursal = sucursales.id  WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSucSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo, idCapacita;";
				$capacitaOBj = DB::select($sql);
				$capacitaciones = array();
				
				foreach($capacitaOBj AS $capacita )
				{
					$capacitaciones[$capacita->periodo] = $capacita;
				}
				
				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(tiempoCon) tiempoCon, AVG(tiempoPausa) tiempoPausa, SUM(aceptados) aceptados, AVG(tiempoAcepta) tiempoAcepta, SUM(ventaPerdida) ventaPerdida, AVG(tiempoPrepara) tiempoPrepara FROM dashboard_uber_eats  INNER JOIN sucursales ON dashboard_uber_eats.idSucursal = sucursales.id WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSucSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo;";
				$ubrOBj = DB::select($sql);
				$uber = array();
				
				foreach($ubrOBj AS $ubr )
				{
					$uber[$ubr->periodo] = $ubr;
				}

				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo,SUM(monto) total FROM finanzas_budget_venta_dia_sap WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSuc GROUP BY  EXTRACT(YEAR_MONTH FROM fecha);";
				$ventaBudgetOBj = DB::select($sql);
				$ventaBudget = array();
				
				foreach($ventaBudgetOBj AS $vbud )
				{
					$ventaBudget[$vbud->periodo] = $vbud->total;
				}

				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo,SUM(monto) total FROM finanzas_budget_costo_dia_sap WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSuc GROUP BY  EXTRACT(YEAR_MONTH FROM fecha);";
				$costoBudgetOBj = DB::select($sql);
				$costoBudget = array();
				
				foreach($costoBudgetOBj AS $vbud )
				{
					$costoBudget[$vbud->periodo] = $vbud->total;
				}
					
				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(calificacion) califica FROM dashboard_redes_soc INNER JOIN sucursales ON dashboard_redes_soc.idSucursal = sucursales.id  WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSucSAP AND calificacion > 0 AND NOT(idRed =7) $whereSucSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo;";
				$redesOBj = DB::select($sql);
				$redes = array();
				
				foreach($redesOBj AS $red )
				{
					$redes[$red->periodo] = $red->califica;
				}
				
				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(calificacion) califica FROM dashboard_redes_soc INNER JOIN sucursales ON dashboard_redes_soc.idSucursal = sucursales.id  WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSucSAP AND calificacion > 0 AND idRed =7 $whereSucSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo;";
				$redesUberOBj = DB::select($sql);
				$redesUber = array();

				foreach($redesUberOBj AS $red )
				{
					$redesUber[$red->periodo] = $red->califica;
				}			
				

				$sql = "SELECT periodo ,total, Salon, ClientesSalon, IF(ClientesSalon = 0, 0, (Salon/ClientesSalon)) AS ChequeSalon, vitrina, ClientesVitrina, IF(ClientesVitrina = 0, 0, (vitrina/ClientesVitrina)) AS ChequeVitrina, Domicilio, ClientesDomicilio, IF(ClientesDomicilio = 0, 0, (Domicilio/ClientesDomicilio)) AS ChequeDomicilio FROM (SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo , SUM(total) total, Sum(IF(idCentroIngreso='Salon',total,0)) Salon, Sum(IF(idCentroIngreso='Salon',if(clientes <13,clientes,1),0)) ClientesSalon ,Sum(IF(idCentroIngreso='Vitrina' OR idCentroIngreso='Institucional' OR idCentroIngreso='Kayser para llevar',total,0)) vitrina, Sum(IF(idCentroIngreso='Vitrina' OR idCentroIngreso='Institucional' OR idCentroIngreso='Kayser para llevar',1,0)) ClientesVitrina, Sum(IF(idCentroIngreso='UBER' OR idCentroIngreso ='OrderType 8' OR idCentroIngreso ='Rappi', total, 0 )) Domicilio, Sum(IF(idCentroIngreso='UBER' OR idCentroIngreso ='OrderType 8' OR idCentroIngreso ='Rappi', 1, 0 )) ClientesDomicilio FROM venta_cheque WHERE NOT(idSucursal IN ('Carmela y Sal')) AND fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSucMic GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY EXTRACT(YEAR_MONTH FROM fecha)) datos ;";
				$chequesOBj = DB::select($sql);
				$cheques = array();
				
				foreach($chequesOBj AS $cheque )
				{
					$cheques[$cheque->periodo] = $cheque;
				}

				$manto = array();
				$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-01-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-01-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-01-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-01-31',1,0)) abiertos FROM manto_solicitud solicitud  INNER JOIN rh_oficinas ON solicitud.idSucursal= rh_oficinas.id WHERE solicitud.estado != 4 AND solicitud.fechaCrea BETWEEN '2019-01-01' AND '2019-01-31' AND solicitud.tiempo = 24 $whereSucSAP GROUP BY tiempo) tiempos;";
				$mantos = DB::select($sql);
				$manto[201901] = empty($mantos[0]) ? 0: $mantos[0];			
				$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-02-28',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-02-28',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-02-28',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-02-28',1,0)) abiertos FROM manto_solicitud solicitud  INNER JOIN rh_oficinas ON solicitud.idSucursal= rh_oficinas.id WHERE solicitud.estado != 4 AND solicitud.fechaCrea BETWEEN '2019-02-01' AND '2019-02-28' AND solicitud.tiempo = 24 $whereSucSAP GROUP BY tiempo) tiempos;";
				$mantos = DB::select($sql);
				$manto[201902] = empty($mantos[0]) ? 0: $mantos[0];
				$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-03-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-03-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-03-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-03-31',1,0)) abiertos FROM manto_solicitud solicitud  INNER JOIN rh_oficinas ON solicitud.idSucursal= rh_oficinas.id WHERE solicitud.estado != 4 AND solicitud.fechaCrea BETWEEN '2019-03-01' AND '2019-03-31' AND solicitud.tiempo = 24 $whereSucSAP GROUP BY tiempo) tiempos;";
				$mantos = DB::select($sql);
				$manto[201903] = empty($mantos[0]) ? 0: $mantos[0];
				$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-04-30',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-04-30',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-04-30',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-04-30',1,0)) abiertos FROM manto_solicitud solicitud  INNER JOIN rh_oficinas ON solicitud.idSucursal= rh_oficinas.id WHERE solicitud.estado != 4 AND solicitud.fechaCrea BETWEEN '2019-04-01' AND '2019-04-30' AND solicitud.tiempo = 24 $whereSucSAP GROUP BY tiempo) tiempos;";
				$mantos = DB::select($sql);
				$manto[201904] = empty($mantos[0]) ? 0: $mantos[0];
				$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-05-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-05-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-05-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-05-31',1,0)) abiertos FROM manto_solicitud solicitud  INNER JOIN rh_oficinas ON solicitud.idSucursal= rh_oficinas.id WHERE solicitud.estado != 4 AND solicitud.fechaCrea BETWEEN '2019-05-01' AND '2019-05-31' AND solicitud.tiempo = 24 $whereSucSAP GROUP BY tiempo) tiempos;";
				$mantos = DB::select($sql);
				$manto[201905] = empty($mantos[0]) ? 0: $mantos[0];
				$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-06-30',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-06-30',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-06-30',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-06-30',1,0)) abiertos FROM manto_solicitud solicitud  INNER JOIN rh_oficinas ON solicitud.idSucursal= rh_oficinas.id WHERE solicitud.estado != 4 AND solicitud.fechaCrea BETWEEN '2019-06-01' AND '2019-06-30' AND solicitud.tiempo = 24 $whereSucSAP GROUP BY tiempo) tiempos;";
				$mantos = DB::select($sql);
				$manto[201906] = empty($mantos[0]) ? 0: $mantos[0];
				$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-07-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-07-31',1,0)) abiertos FROM manto_solicitud solicitud  INNER JOIN rh_oficinas ON solicitud.idSucursal= rh_oficinas.id WHERE solicitud.estado != 4 AND solicitud.fechaCrea BETWEEN '2019-07-01' AND '2019-07-31' AND solicitud.tiempo = 24 $whereSucSAP GROUP BY tiempo) tiempos;";
				$mantos = DB::select($sql);
				$manto[201907] = empty($mantos[0]) ? 0: $mantos[0];
				$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-07-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-07-31',1,0)) abiertos FROM manto_solicitud solicitud  INNER JOIN rh_oficinas ON solicitud.idSucursal= rh_oficinas.id WHERE solicitud.estado != 4 AND solicitud.fechaCrea BETWEEN '2019-08-01' AND '2019-08-31' AND solicitud.tiempo = 24 $whereSucSAP GROUP BY tiempo) tiempos;";
				$mantos = DB::select($sql);
				$manto[201908] = empty($mantos[0]) ? 0: $mantos[0];
				$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-07-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-07-31',1,0)) abiertos FROM manto_solicitud solicitud  INNER JOIN rh_oficinas ON solicitud.idSucursal= rh_oficinas.id WHERE solicitud.estado != 4 AND solicitud.fechaCrea BETWEEN '2019-09-01' AND '2019-09-30' AND solicitud.tiempo = 24 $whereSucSAP GROUP BY tiempo) tiempos;";
				$mantos = DB::select($sql);
				$manto[201909] = empty($mantos[0]) ? 0: $mantos[0];
				$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF('2019-07-31',CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3 AND solicitud.fechaTermina <= '2019-07-31',1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)) OR solicitud.fechaTermina>'2019-07-31',1,0)) abiertos FROM manto_solicitud solicitud  INNER JOIN rh_oficinas ON solicitud.idSucursal= rh_oficinas.id WHERE solicitud.estado != 4 AND solicitud.fechaCrea BETWEEN '2019-10-01' AND '2019-10-31' AND solicitud.tiempo = 24 $whereSucSAP GROUP BY tiempo) tiempos;";
				$mantos = DB::select($sql);
				$manto[201910] = empty($mantos[0]) ? 0: $mantos[0];
				
				return view('dashboard.indexAlex',['Role'=> $Role,'selSuc' => $selSuc , 'selSucId' => $selSucId,"sinUber" => $sinUber ,"aer" => $aer ,"tiendach" => $tiendach ,"tiendamd" => $tiendamd ,"tiendabg" => $tiendabg , "tiendaxl" => $tiendaxl ,'selSucName' => $selSucName,'selSucId' => $selSucId,'sucursales' => $sucursales,'nPeriodos'=> 11, 'periodos'=> ['ENE-19','FEB-19','MAR-19','ABR-19','MAY-19','JUN-19','JUL-19','AGO-19','SEP-19','OCT-19'], 'venta'=> $venta, 'costo'=> $costo, 'gasto'=> $gasto, 'gastos'=> $gastos, 'TIPer'=>$TIPer, 'papeleria' => $papeleria, 'cheque' => $cheques, 'limpieza' =>$limpieza, 'rh' => $rh, 'calidad'=> $calidad, 'ppedidos' => $ppedidos ,'redes' => $redes , 'redesUber' => $redesUber,  'uber' => $uber, 'audita' => $audita, 'manto' => $manto, 'costoBudget' => $costoBudget, 'ventaBudget'=> $ventaBudget, 'ventaTOT' => $ventaTOT, 'giotto' =>$giotto, 'encuestas' => $encuestas, 'capacitaciones'=> $capacitaciones, 'dperiodos'=> ['201901','201902','201903','201904','201905','201906','201907','201908','201909','201910'] ]);
			}
			else
			{
				$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, AVG(rotacion) rotacion, SUM(empleados) empleados FROM dashboard_rh_empleados INNER JOIN sucursales ON dashboard_rh_empleados.idSucursal = sucursales.id  WHERE fecha BETWEEN '2019-01-01' AND '2019-10-31' $whereSucSAP GROUP BY EXTRACT(YEAR_MONTH FROM fecha) ORDER BY periodo;";
				$rhOBj = DB::select($sql);
				$rh = array();

				foreach($rhOBj AS $rhi )
				{
					$rh[$rhi->periodo] = $rhi;
				}
				return view('dashboard.indexAlex',['Role'=> $Role, 'sucursales' => $sucursales, 'rh' => $rh, 'calidad'=> $calidad, 'ppedidos' => $ppedidos, 'nPeriodos'=> 11, 'periodos'=> ['ENE-19','FEB-19','MAR-19','ABR-19','MAY-19','JUN-19','JUL-19','AGO-19','SEP-19','OCT-19'], 'dperiodos'=> ['201901','201902','201903','201904','201905','201906','201907','201908','201909','201910']]);
			}
	}

	public function alexRedes()
	{
		$sql = "SELECT id, nombre FROM sucursales WHERE idEmpresa = 1 AND idTipo > 0;";
		$sucursales = DB::select($sql);

		return view('dashboard.redes',['sucursales'=>$sucursales]);
	}
	public function alexAuditi()
	{
		$sql = "SELECT id, nombre FROM sucursales WHERE idEmpresa = 1 AND idTipo > 0;";
		$sucursales = DB::select($sql);

		return view('dashboard.auditi',['sucursales'=>$sucursales]);
	}
	public function alexDCalidad()
	{
		$sql = "SELECT id, nombre FROM sucursales WHERE idEmpresa = 1 AND idTipo > 0;";
		$sucursales = DB::select($sql);

		return view('dashboard.calidad',['sucursales'=>$sucursales]);
	}
	public function alexUber()
	{
		$sql = "SELECT id, nombre FROM sucursales WHERE idEmpresa = 1 AND idTipo > 0;";
		$sucursales = DB::select($sql);

		return view('dashboard.ubercal',['sucursales'=>$sucursales]);
	}
	
	public function alexRH()
	{
		$sql = "SELECT id, nombre FROM sucursales WHERE idEmpresa = 1 AND idTipo > 0;";
		$sucursales = DB::select($sql);

		return view('dashboard.rhemps',['sucursales'=>$sucursales]);
	}
	
	public function index()
    {
		$mes = date("m")==1?12:date("m")-1;
		$mesanterior = date("m")==2?12:$mes-1;

		$anio = date("Y") - (date("m")==1?1:0);
		$anioanterior = date("Y") - (date("m")<=2?1:0);

		$venta_mes = DB::table('finanzas_venta_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('YEAR(fecha)'))
		->get();
		
		$venta_budget_mes = DB::table('finanzas_budget_venta_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('YEAR(fecha)'))
		->get();
		
		$costo_mes = DB::table('finanzas_costo_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('YEAR(fecha)'))
		->get();
		
		$costo_budget_mes = DB::table('finanzas_budget_costo_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('YEAR(fecha)'))
		->get();
		
		$gasto_mes = DB::table('finanzas_gasto_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('YEAR(fecha)'))
		->get();
		
		$gasto_budget_mes = DB::table('finanzas_budget_gasto_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('YEAR(fecha)'))
		->get();

		$ventaTotal = (empty($venta_mes[0]->total)?0:$venta_mes[0]->total);
		$costoTotal = (empty($costo_mes[0]->total)?0:$costo_mes[0]->total);
		$gastoTotal = (empty($gasto_mes[0]->total)?0:$gasto_mes[0]->total);

		$ventaTotalB = (empty($venta_budget_mes[0]->total)?0:$venta_budget_mes[0]->total);
		$costoTotalB = (empty($costo_budget_mes[0]->total)?0:$costo_budget_mes[0]->total);
		$gastoTotalB = (empty($gasto_budget_mes[0]->total)?0:$gasto_budget_mes[0]->total);
		
		$ventaTotal = ($ventaTotal <=0 || empty($ventaTotal) ? 1: $ventaTotal);

		$ebit = ($ventaTotal - $costoTotal - $gastoTotal)/$ventaTotal*100;
		$ebit_budget = ($ventaTotalB - $costoTotalB - $gastoTotalB)/$ventaTotalB*100;
		$difebit = $ebit - $ebit_budget;

		$venta = DB::table('finanzas_venta_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'<=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('YEAR(fecha)'))
		->get();
		
		$venta_budget = DB::table('finanzas_budget_venta_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'<=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('YEAR(fecha)'))
		->get();
		
		$ventasaYTD = number_format($venta[0]->total, 0,"","").",0";
		$ventasaYTDL = $venta[0]->total;
		$ventasbYTD = "0,".number_format($venta_budget[0]->total, 0,"","");
		$ventasbYTDL = $venta_budget[0]->total;
		
		$ventasaYTDM = number_format($venta_mes[0]->total, 0,"","").",0";
		$ventasaYTDML = $venta_mes[0]->total;
		$ventasbYTDM = "0,".number_format($venta_budget_mes[0]->total, 0,"","");
		$ventasbYTDML = $venta_budget_mes[0]->total;
		
		$gastos = DB::table('finanzas_gasto_dia_sap')
		->select(DB::raw('MONTH(fecha) as mes, SUM(monto) AS totalMes'))
		->where(DB::raw('MONTH(fecha)'),'<', date("m"))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->orderBy(DB::raw('MONTH(fecha)'))
		->get();
		
		$costos = DB::table('finanzas_costo_dia_sap')
		->select(DB::raw('MONTH(fecha) as mes, SUM(monto) AS totalMes'))
		->where(DB::raw('MONTH(fecha)'),'<', date("m"))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->orderBy(DB::raw('MONTH(fecha)'))
		->get();
		
		$gastopYTD = DB::select("SELECT SUM(IF(tipo = 'venta',venta,0)) AS venta, SUM(IF(tipo = 'gasto',venta,0)) AS gasto FROM (SELECT 'gasto' AS tipo ,SUM(monto) venta FROM finanzas_gasto_dia_sap WHERE MONTH(fecha) < 7 AND YEAR(fecha) = 2018 GROUP BY YEAR(fecha) UNION ALL SELECT 'venta' AS tipo , SUM(monto) gasto FROM finanzas_venta_dia_sap WHERE MONTH(fecha) < 7 AND YEAR(fecha) = 2018 GROUP BY YEAR(fecha)) montos");
		$venta100 = $gastopYTD[0]->venta;
		$gastoper = $gastopYTD[0]->gasto;
		$pergasto = $gastoper*100/$venta100;
		
		$gastopBudgetYTD = DB::select("SELECT SUM(IF(tipo = 'venta',venta,0)) AS venta, SUM(IF(tipo = 'gasto',venta,0)) AS gasto FROM (SELECT 'gasto' AS tipo ,SUM(monto) venta FROM finanzas_budget_gasto_dia_sap WHERE MONTH(fecha) < 7 AND YEAR(fecha) = 2018 GROUP BY YEAR(fecha) UNION ALL SELECT 'venta' AS tipo , SUM(monto) gasto FROM finanzas_budget_venta_dia_sap WHERE MONTH(fecha) < 7 AND YEAR(fecha) = 2018 GROUP BY YEAR(fecha)) montos");
		$ventaBudget100 = $gastopBudgetYTD[0]->venta;
		$gastoBudgetper = $gastopBudgetYTD[0]->gasto;
		$pergastoBudget = $gastoBudgetper*100/$ventaBudget100;
		
		$difpergasto = $pergastoBudget - $pergasto;
		
		
		$costopYTD = DB::select("SELECT SUM(IF(tipo = 'venta',venta,0)) AS venta, SUM(IF(tipo = 'costo',venta,0)) AS costo FROM (SELECT 'costo' AS tipo ,SUM(monto) venta FROM finanzas_costo_dia_sap WHERE MONTH(fecha) < 7 AND YEAR(fecha) = 2018 GROUP BY YEAR(fecha) UNION ALL SELECT 'venta' AS tipo , SUM(monto) costo FROM finanzas_venta_dia_sap WHERE MONTH(fecha) < 7 AND YEAR(fecha) = 2018 GROUP BY YEAR(fecha)) montos");
		$venta100 = $costopYTD[0]->venta;
		$costoper = $costopYTD[0]->costo;
		$percosto = $costoper*100/$venta100;
		
		$costopBudgetYTD = DB::select("SELECT SUM(IF(tipo = 'venta',venta,0)) AS venta, SUM(IF(tipo = 'costo',venta,0)) AS costo FROM (SELECT 'costo' AS tipo ,SUM(monto) venta FROM finanzas_budget_costo_dia_sap WHERE MONTH(fecha) < 7 AND YEAR(fecha) = 2018 GROUP BY YEAR(fecha) UNION ALL SELECT 'venta' AS tipo , SUM(monto) costo FROM finanzas_budget_venta_dia_sap WHERE MONTH(fecha) < 7 AND YEAR(fecha) = 2018 GROUP BY YEAR(fecha)) montos");
		$ventaBudget100 = $costopBudgetYTD[0]->venta;
		$costoBudgetper = $costopBudgetYTD[0]->costo;
		$percostoBudget = $costoBudgetper*100/$ventaBudget100;
		
		$difpercosto = $percostoBudget - $percosto;
		
		$kpiGastoMes="";
		$minGastoMes=0;
		$maxGastoMes=0;
		
		foreach($gastos AS $gasto)
		{
			if(!empty($kpiGastoMes))
				$kpiGastoMes .= ", ";
			$kpiGastoMes .= $gasto->totalMes;
			
			$minGastoMes= ($gasto->totalMes < $minGastoMes || $minGastoMes == 0) ? $gasto->totalMes : $minGastoMes;
			$maxGastoMes= ($gasto->totalMes > $maxGastoMes || $maxGastoMes == 0) ? $gasto->totalMes : $maxGastoMes;
		}
		
		$minGastoMes= $minGastoMes;
		$maxGastoMes= $maxGastoMes;		
		
		$kpiCostoMes="";
		$minCostoMes=0;
		$maxCostoMes=0;
		
		foreach($costos AS $costo)
		{
			if(!empty($kpiCostoMes))
				$kpiCostoMes .= ", ";
			$kpiCostoMes .= $costo->totalMes;
			
			$minCostoMes= ($costo->totalMes < $minCostoMes || $minCostoMes == 0) ? $costo->totalMes : $minCostoMes;
			$maxCostoMes= ($costo->totalMes > $maxCostoMes || $maxCostoMes == 0) ? $costo->totalMes : $maxCostoMes;
		}
		
		$minCostoMes= $minCostoMes;
		$maxCostoMes= $maxCostoMes;
	
		
//		$sql = "SELECT ((SUM(monto2)-SUM(monto1)) / SUM(monto1) )* 100 crecimiento FROM ( SELECT idSucursal, SUM(IF(YEAR(fecha)=2017,monto,0)) AS monto1 , SUM(IF(YEAR(fecha)=2018,monto,0)) AS monto2 FROM finanzas_venta_dia_sap AS fvds WHERE MONTH(fvds.fecha) < ".(date("m"))." AND YEAR(fvds.fecha) IN (2017,2018) AND NOT(idSucursal = 'KAYSGIO') GROUP BY idSucursal HAVING SUM(IF(YEAR(fecha)=".($anio-1).",monto,0)) > 0 AND SUM(IF(YEAR(fecha)=".($anio).",monto,0)) > 0) montos ;";
		$sql = "SELECT SUM(crecimiento)/SUM(montoAnterior)*100 crecimiento FROM (SELECT actual.idSucursal, actual.mes, actual.monto montoActual, anterior.mesAnterior, anterior.monto montoAnterior, ((actual.monto/anterior.monto)*100)-100 factor, actual.monto - anterior.monto crecimiento FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(monto) monto FROM finanzas_venta_dia_sap venta WHERE YEAR(fecha) = ".($anio)." AND MONTH(fecha)< ".$mes." AND NOT(idSucursal IN ('KAYSGIO','GIOTTO','GIOTTONR') ) GROUP BY idSucursal , MONTH(fecha)) actual INNER JOIN (SELECT idSucursal, MONTH(fecha) mesAnterior,SUM(monto) monto FROM finanzas_venta_dia_sap venta WHERE YEAR(fecha) = ".($anio-1)." AND MONTH(fecha)< ".(date("m"))." AND NOT(idSucursal IN ('KAYSGIO','GIOTTO','GIOTTONR') ) GROUP BY idSucursal , MONTH(fecha)) anterior ON (actual.idSucursal = anterior.idSucursal AND actual.mes = anterior.mesAnterior ))crecimiento WHERE factor < 60;";
		$crecimientoAnual = DB::select($sql);
		
		$sql = "SELECT (SUM(IF(YEAR(fecha) = ".($anio).",monto,0)) - SUM(IF(YEAR(fecha) =  ".($anio-1).",monto,0)))/SUM(IF(YEAR(fecha) = ".($anio-1).",monto,0))*100 crecimiento FROM finanzas_venta_dia_sap WHERE MONTH(fecha) <  ".($mes+1)." AND NOT(idSucursal IN ('KAYSGIO','GIOTTO','GIOTTONR') ) GROUP BY idMoneda;";
		
		$crecimientoAnualizado = DB::select($sql);
		
//		$sql = "SELECT ((SUM(monto2)-SUM(monto1)) / SUM(monto1) )* 100 crecimiento FROM ( SELECT idSucursal, SUM(IF(YEAR(fecha)=2017,monto,0)) AS monto1 , SUM(IF(YEAR(fecha)=2018,monto,0)) AS monto2 FROM finanzas_venta_dia_sap AS fvds WHERE MONTH(fvds.fecha) = ".($mes)." AND YEAR(fvds.fecha) IN (2017,2018) AND NOT(idSucursal = 'KAYSGIO') GROUP BY idSucursal HAVING SUM(IF(YEAR(fecha)=".($anio-1).",monto,0)) > 0 AND SUM(IF(YEAR(fecha)=".($anio).",monto,0)) > 0) montos ;";
		$sql = "SELECT SUM(crecimiento)/SUM(montoAnterior)*100 crecimiento FROM (SELECT actual.idSucursal, actual.mes, actual.monto montoActual, anterior.mesAnterior, anterior.monto montoAnterior, ((actual.monto/anterior.monto)*100)-100 factor, actual.monto - anterior.monto crecimiento FROM (SELECT idSucursal, MONTH(fecha) mes,SUM(monto) monto FROM finanzas_venta_dia_sap venta WHERE YEAR(fecha) = ".($anio)." AND MONTH(fecha)<= ".($mes)." AND NOT(idSucursal IN ('KAYSGIO','GIOTTO','GIOTTONR') ) GROUP BY idSucursal , MONTH(fecha)) actual INNER JOIN (SELECT idSucursal, MONTH(fecha) mesAnterior,SUM(monto) monto FROM finanzas_venta_dia_sap venta WHERE YEAR(fecha) = ".($anio-1)." AND MONTH(fecha)= ".($mes)." AND NOT(idSucursal IN ('KAYSGIO','GIOTTO','GIOTTONR') ) GROUP BY idSucursal , MONTH(fecha)) anterior ON (actual.idSucursal = anterior.idSucursal AND actual.mes = anterior.mesAnterior ))crecimiento WHERE factor < 60;";
		$crecimientoMes = DB::select($sql);	
		
		/********************* QUERY CHEQUE - revisar si manda info usada o no
		
		$sql  ="SELECT MONTH(fecha) mes, SUM(total) total, Sum(IF(idCentroIngreso='Salon',total,0)) Salon, Sum(IF(idCentroIngreso='Salon',clientes,0)) ClientesSalon,     Sum(IF(idCentroIngreso='Salon',total,0)) / Sum(IF(idCentroIngreso='Salon',clientes,0)) ChequePromSalon  ,Sum(IF(idCentroIngreso='Vitrina' OR idCentroIngreso='Kayser para llevar',total,0)) vitrina, Sum(IF(idCentroIngreso='UBER' OR idCentroIngreso ='OrderType 8' OR idCentroIngreso ='Rappi', total, 0 )) Domicilio FROM venta_cheque WHERE clientes <13 AND ( MONTH(fecha) = ".($mes-1)." OR MONTH(fecha) = ".($mes)." ) GROUP BY MONTH(fecha) ORDER BY MONTH(fecha);";
		$cheque = DB::select($sql);	
		
		$ChequePromSalonAnt = empty($cheque[0]->ChequePromSalon) ? 0:$cheque[0]->ChequePromSalon;
		$ChequePromSalon = empty($cheque[1]->ChequePromSalon) ? 0:$cheque[1]->ChequePromSalon;
		*/
		
		//$sql = "SELECT YEAR(fecha), SUM(monto) AS total, SUM(if(MONTH(fecha) = ".$mesanterior." AND YEAR(fecha) = ".$anioanterior.", monto,0)) mesanterior,  SUM(if(MONTH(fecha) = ".($mes).", monto,0)) mes FROM finanzas_venta_dia_sap WHERE YEAR(fecha) IN (".$anio.",".$anioanterior.") GROUP BY YEAR(fecha);";
		$sql = "SELECT idMoneda, SUM(if(YEAR(fecha)=".$anio.",monto,0)) AS total, SUM(if(MONTH(fecha) = ".$mesanterior." AND YEAR(fecha) = ".$anioanterior.", monto,0)) mesanterior,  SUM(if(MONTH(fecha) = ".$mes." AND YEAR(fecha) = ".$anio.", monto,0)) mes FROM finanzas_venta_dia_sap WHERE YEAR(fecha) IN (".$anio.",".$anioanterior.") GROUP BY idMoneda;";
		$ingresoAnual = DB::select($sql);	
		$sql = "SELECT idMoneda, SUM(if((MONTH(fecha) = ".$mesanterior." AND YEAR(fecha) = ".$anioanterior.") OR (MONTH(fecha) = ".$mes." AND YEAR(fecha) = ".$anio."), monto,0)) AS total, SUM(if(MONTH(fecha) = ".$mesanterior." AND YEAR(fecha) = ".$anioanterior.", monto,0)) mesanterior,  SUM(if(MONTH(fecha) = ".($mes)." AND YEAR(fecha) = ".$anio.", monto,0)) mes FROM finanzas_budget_venta_dia_sap WHERE YEAR(fecha) IN (".$anio.",".$anioanterior.") AND (MONTH(fecha) <= ".$mes." OR MONTH(fecha) = ".$mesanterior." ) GROUP BY idMoneda;";
		$ingresoBudgetAnual = DB::select($sql);	
		
		$sql = "SELECT idMoneda, SUM(if(YEAR(fecha)=".$anio.",monto,0)) AS total, SUM(if(MONTH(fecha) = ".$mesanterior." AND YEAR(fecha) = ".$anioanterior.", monto,0)) mesanterior,  SUM(if(MONTH(fecha) = ".($mes)." AND YEAR(fecha) = ".$anio.", monto,0)) mes FROM finanzas_costo_dia_sap WHERE YEAR(fecha) IN (".$anio.",".$anioanterior.") GROUP BY idMoneda;";
		$costoAnual = DB::select($sql);	
		$sql = "SELECT idMoneda, SUM(if((MONTH(fecha) = ".$mesanterior." AND YEAR(fecha) = ".$anioanterior.") OR (MONTH(fecha) = ".$mes." AND YEAR(fecha) = ".$anio."), monto,0)) AS total, SUM(if(MONTH(fecha) = ".$mesanterior." AND YEAR(fecha) = ".$anioanterior.", monto,0)) mesanterior,  SUM(if(MONTH(fecha) = ".($mes)." AND YEAR(fecha) = ".$anio.", monto,0)) mes FROM finanzas_budget_costo_dia_sap WHERE YEAR(fecha) IN (".$anio.",".$anioanterior.") AND (MONTH(fecha) <= ".$mes." OR MONTH(fecha) = ".$mesanterior." ) GROUP BY idMoneda;";
		$costoBudgetAnual = DB::select($sql);
		
		$sql = "SELECT idMoneda, SUM(if(YEAR(fecha)=".$anio.",monto,0)) AS total, SUM(if(MONTH(fecha) = ".$mesanterior." AND YEAR(fecha) = ".$anioanterior.", monto,0)) mesanterior,  SUM(if(MONTH(fecha) = ".($mes)." AND YEAR(fecha) = ".$anio.", monto,0)) mes FROM finanzas_gasto_dia_sap WHERE YEAR(fecha) IN (".$anio.",".$anioanterior.") GROUP BY idMoneda;";
		$gastoAnual = DB::select($sql);	
		$sql = "SELECT idMoneda, SUM(if((MONTH(fecha) = ".$mesanterior." AND YEAR(fecha) = ".$anioanterior.") OR (MONTH(fecha) = ".$mes." AND YEAR(fecha) = ".$anio."), monto,0)) AS total, SUM(if(MONTH(fecha) = ".$mesanterior." AND YEAR(fecha) = ".$anioanterior.", monto,0)) mesanterior,  SUM(if(MONTH(fecha) = ".($mes)." AND YEAR(fecha) = ".$anio.", monto,0)) mes FROM finanzas_budget_gasto_dia_sap WHERE YEAR(fecha) IN (".$anio.",".$anioanterior.") AND (MONTH(fecha) <= ".$mes." OR MONTH(fecha) = ".$mesanterior." ) GROUP BY idMoneda;";
		$gastoBudgetAnual = DB::select($sql);	
		
		$ebitAc = ($ingresoAnual[0]->total - $costoAnual[0]->total - $gastoAnual[0]->total )/$ingresoAnual[0]->total * 100;
		$ebitAn = ($ingresoAnual[0]->mesanterior - $costoAnual[0]->mesanterior - $gastoAnual[0]->mesanterior )/$ingresoAnual[0]->mesanterior * 100;
		$ebitMes = ($ingresoAnual[0]->mes - $costoAnual[0]->mes - $gastoAnual[0]->mes )/(empty($ingresoAnual[0]->mes)? 0.1:$ingresoAnual[0]->mes) * 100;
		
		$budgetEbitAc = ($ingresoBudgetAnual[0]->total - $costoBudgetAnual[0]->total - $gastoBudgetAnual[0]->total )/$ingresoBudgetAnual[0]->total * 100;
		$budgetEbitAn = ($ingresoBudgetAnual[0]->mesanterior - $costoBudgetAnual[0]->mesanterior - $gastoBudgetAnual[0]->mesanterior )/$ingresoBudgetAnual[0]->mesanterior * 100;
		$budgetEbitMes = ($ingresoBudgetAnual[0]->mes - $costoBudgetAnual[0]->mes - $gastoBudgetAnual[0]->mes )/$ingresoBudgetAnual[0]->mes * 100;
		
		$sql = "SELECT rvc, SUM(promedioClientes) clientes, AVG(chequePromedio) cheque, SUM(ventaPromedio) venta FROM dashboard_cheque_prom_sucursal WHERE anio = ".($anio)." GROUP BY rvc;";
		$cheque = DB::select($sql);

		return view('dashboard.index', [ 'mes' => $this->meses[($mes-1)][1],'ebitAc' => $ebitAc,'ebitAn' => $ebitAn,'ebitMes' => $ebitMes, 'budgetEbitAc' => $budgetEbitAc,'budgetEbitAn' => $budgetEbitAn,'budgetEbitMes' => $budgetEbitMes, 'ingresoMes' => $ingresoAnual[0]->mes ,'budgetIngMes' => $ingresoBudgetAnual[0]->mes , 'ingresoAn' => $ingresoAnual[0]->mesanterior ,'budgetIngAn' => $ingresoBudgetAnual[0]->mesanterior ,'ingresoAc' => $ingresoAnual[0]->total ,'budgetIngAc' => $ingresoBudgetAnual[0]->total , 'costoMes' => $costoAnual[0]->mes ,'budgetCosMes' => $costoBudgetAnual[0]->mes , 'costoAn' => $costoAnual[0]->mesanterior ,'budgetCosAn' => $costoBudgetAnual[0]->mesanterior ,'costoAc' => $costoAnual[0]->total ,'budgetCosAc' => $costoBudgetAnual[0]->total , 'ventasaYTDML' => $ventasaYTDML, 'ventasbYTDML' => $ventasbYTDML,'ventasaYTDL' => $ventasaYTDL, 'ventasbYTDL' => $ventasbYTDL, 'ventasaYTD' => $ventasaYTD, 'ventasbYTD' => $ventasbYTD, 'kpiGastoMes' => $kpiGastoMes, 'minGastoMes' => $minGastoMes, 'maxGastoMes' => $maxGastoMes, 'kpiCostoMes' => $kpiCostoMes, 'minCostoMes' => $minCostoMes, 'maxCostoMes' => $maxCostoMes, 'crecimientoAnualizado' => $crecimientoAnualizado[0]->crecimiento, 'crecimientoAnual' => $crecimientoAnual[0]->crecimiento, 'crecimientoMes' => $crecimientoMes[0]->crecimiento, 'difebitda' =>	$difebit, 'perebit' =>$ebit,'perebitBudget' => $ebit_budget,'difpergasto' => $difpergasto , 'pergastoBudget' => $pergastoBudget , 'pergasto' => $pergasto, 'difpercosto' => $difpercosto , 'percostoBudget' => $percostoBudget , 'percosto' => $percosto, 'cheque' => $cheque]);
    }
	
	public function rhDashboard()
    {

			$mes = date("m")==1?12:date("m")-1;
			$mesanterior = date("m")==2?12:$mes-1;
	
			$anio = date("Y") - (date("m")==1?1:0);
			$anioanterior = date("Y") - (date("m")<=2?1:0);

		$sql = "SELECT empleados.*, plazas.autorizados, (plazas.autorizados - empleados.total) AS diferencia  FROM ( SELECT rh_empleado.estado, COUNT(rh_empleado.estado) total FROM rh_empleado INNER JOIN sucursales ON rh_empleado.idSucursal = sucursales.id WHERE rh_empleado.estado = 1 GROUP BY rh_empleado.estado ) empleados  LEFT JOIN ( SELECT estado, SUM(cantidad) autorizados FROM rh_plazas_autorizadas WHERE estado = 1 GROUP BY estado ) plazas ON empleados.estado = plazas.estado;";
		$vacantes= DB::select($sql);
		$actuales = empty($vacantes[0]->total)?0:$vacantes[0]->total;
		$autorizados = empty($vacantes[0]->autorizados)?0:$vacantes[0]->autorizados;
		$diferencia = empty($vacantes[0]->diferencia)?0:$vacantes[0]->diferencia;
		
		$sql = "SELECT COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM (SELECT sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF(NOW() > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (1,2,3)) data GROUP BY estado;;";
		$abiertas = DB::select($sql);
		/*$sql = "";
		$retrasadas = DB::select($sql);
		$sql = "";
		$entiempo = DB::select($sql);*/
		$sql = "SELECT COUNT(estado) total FROM rh_vacante_solicitud_partida WHERE (MONTH(lastUpdateDate) = ".date("m")." AND YEAR(lastUpdateDate) = ".$anio.") AND estado IN (4);";
		$cerradas = DB::select($sql);
		
		return view('dashboard.rh', ['autorizados' => $autorizados, "cerradas" => $cerradas[0]->total, "abiertas" => $abiertas[0]->total, "retrasadas" => $abiertas[0]->atraso, "entiempo" => $abiertas[0]->bien, 'actuales' => $actuales, 'diferencia' => $diferencia]);
	}
	
	
	public function comprasDashboard($rtype = 1)
	{
		$mes = date("m")==1?12:date("m")-1;
		$anio = date("Y") - (date("m")==1?1:0);
		$sql ="SELECT prov.idProveedor ,prov.proveedorName, prov.total compraTotal, (prov.total/tot.total)*100 porcentaje FROM (SELECT idProveedor, proveedorName, anio, SUM(compraTotal) total FROM dashboard_pca WHERE NOT((idProveedor ='P00120' AND idCompania= 1) OR (idProveedor = 'P00119' AND idCompania = 2)) AND anio = ".$anio." GROUP BY proveedorName,idProveedor, anio) prov INNER JOIN (SELECT anio, SUM(compraTotal) total FROM dashboard_pca WHERE NOT((idProveedor ='P00120' AND idCompania= 1) OR (idProveedor = 'P00119' AND idCompania = 2)) AND anio=".$anio." GROUP BY anio) tot ON prov.anio = tot.anio ORDER BY porcentaje DESC";
		$compraTotalProv = DB::select($sql);
		$sql ="SELECT prov.idItem ,  prov.itemName, prov.total compraTotal, (prov.total/tot.total)*100 porcentaje FROM ( SELECT idItem , itemName , anio, SUM(total) total FROM dashboard_pca1 WHERE anio = ".$anio."  AND ( ( idCompania =1 AND NOT(idProveedor = 'P00120')) OR ( idCompania =2 AND NOT(idProveedor = 'P00119'))) GROUP BY idItem , itemName , anio ) prov INNER JOIN ( SELECT anio, SUM(total) total FROM dashboard_pca1 WHERE anio=".$anio." AND ( ( idCompania =1 AND NOT(idProveedor = 'P00120')) OR ( idCompania =2 AND NOT(idProveedor = 'P00119'))) GROUP BY anio) tot ON prov.anio = tot.anio ORDER BY porcentaje DESC";
		$compraTotalItemProv = DB::select($sql);
		
		$sql = "SELECT * FROM dashboard_pca WHERE anio = ".$anio." AND idCompania = 1 AND NOT(idProveedor = 'P00120') ORDER BY porcentaje desc;";
		
		$compraProv = DB::select($sql);
		
		$sql = "SELECT * FROM dashboard_pca WHERE anio = ".$anio." AND idCompania = 2 AND NOT(idProveedor = 'P00119') ORDER BY porcentaje desc;";
		
		$compraProvPrigo = DB::select($sql);
		 
		#$sql = "SELECT * FROM dashboard_aca WHERE anio = ".$anio." AND idCompania = 1 ORDER BY porcentaje desc;";
		$sql = "SELECT * FROM dashboard_aca LEFT JOIN (SELECT idItem, SUM(IF(mes=".($mes-1).",precioPromedio,0)) anterior, SUM(IF(mes=".($mes).",precioPromedio,0)) actual, SUM(precioPromedio)/COUNT(distinct(mes)) acumulado FROM dashboard_precio_promedio WHERE anio = ".$anio." AND idCompania = 1 GROUP BY idItem) precios ON precios.idItem = dashboard_aca.itemCode WHERE dashboard_aca.anio = ".$anio." AND dashboard_aca.idCompania = 1 ORDER BY dashboard_aca.porcentaje desc";
		$compraItem = DB::select($sql);
		 
		#$sql = "SELECT * FROM dashboard_aca WHERE anio = ".$anio." AND idCompania = 2 ORDER BY porcentaje desc;";
		$sql = "SELECT * FROM dashboard_aca LEFT JOIN (SELECT idItem, SUM(IF(mes=".($mes-1).",precioPromedio,0)) anterior, SUM(IF(mes=".($mes).",precioPromedio,0)) actual, SUM(precioPromedio)/COUNT(distinct(mes)) acumulado FROM dashboard_precio_promedio WHERE anio = ".$anio." AND idCompania = 2 GROUP BY idItem) precios ON precios.idItem = dashboard_aca.itemCode WHERE dashboard_aca.anio = ".$anio." AND dashboard_aca.idCompania = 2 ORDER BY dashboard_aca.porcentaje desc";
		
		$compraItemPrigo = DB::select($sql);
		 
		$sql = "SELECT dashboard_pca1.*, precios.acumulado, precios.anterior, precios.actual FROM dashboard_pca1 LEFT JOIN (SELECT idItem, SUM(IF(mes=".($mes-1).",precioPromedio,0)) anterior, SUM(IF(mes=".($mes).",precioPromedio,0)) actual, SUM(precioPromedio)/COUNT(distinct(mes)) acumulado FROM dashboard_precio_promedio WHERE anio = ".$anio." AND idCompania = 1 GROUP BY idItem) precios ON precios.idItem = dashboard_pca1.idItem WHERE dashboard_pca1.anio = ".$anio." AND dashboard_pca1.idCompania = 1 AND idProveedor = 'P00120'  ORDER BY dashboard_pca1.total desc";
		
		$primosItem = DB::select($sql);
		
		return view('dashboard.compras', ['tiempo' => $rtype,'mes' => date('F'),"compraProvPrigo" => $compraProvPrigo,"compraProv" => $compraProv, "compraItem" => $compraItem, "compraItemPrigo" => $compraItemPrigo, "primosItem" => $primosItem, "compraTotalProv" => $compraTotalProv, "compraTotalItemProv" => $compraTotalItemProv]);

	}
	public function guardaCalidad(Request $request){
		$user = Auth::user();
		$idUsuario = $user->id;
		if(!empty($request->input('sucursal')) && !empty($request->input('periodo'))&& !empty($request->input('costo')) && !empty($request->input('calidad')))
		{
			$fecha = "";
			$fecha .= substr($request->input('periodo'), 0, 4)."-";
			$fecha .= substr($request->input('periodo'), -2)."-01";
			$sql = "INSERT INTO dashboard_calidad_costo (idSucursal, fecha, calidad, costo, idUsuario, fechaCrea, horaCrea) VALUES (?,?,?,?,?,?,?);";
			DB::insert($sql , [$request->input('sucursal'),$fecha ,$request->input('calidad'),$request->input('costo'),$idUsuario,date("Y-m-d"),date("H:i:s")] );

			echo "{ \"success\": true }";
		}
		else
		{
			echo "{ \"success\": false }";
		}
	}
	public function guardaAudInterna(Request $request){
		$user = Auth::user();
		$idUsuario = $user->id;
		if(!empty($request->input('sucursal')) && !empty($request->input('periodo'))&& !empty($request->input('puntos')) && !empty($request->input('cash')))
		{
			$fecha = "";
			$fecha .= substr($request->input('periodo'), 0, 4)."-";
			$fecha .= substr($request->input('periodo'), -2)."-01";
			$sql = "INSERT INTO dashboard_auditoria_interna (idSucursal, fecha, puntos, cashman, idUsuario, fechaCrea, horaCrea) VALUES (?,?,?,?,?,?,?);";
			DB::insert($sql , [$request->input('sucursal'),$fecha ,$request->input('puntos'),$request->input('cash'),$idUsuario,date("Y-m-d"),date("H:i:s")] );

			echo "{ \"success\": true }";
		}
		else 
		{
			  echo "{ \"success\": false }";
		}

	}

	public function guardaRHdata(Request $request){
		$user = Auth::user();
		$idUsuario = $user->id;
		if(!empty($request->input('sucursal')) && !empty($request->input('periodo'))&& !empty($request->input('rotacion')) && !empty($request->input('nemp')))
		{
			$fecha = "";
			$fecha .= substr($request->input('periodo'), 0, 4)."-";
			$fecha .= substr($request->input('periodo'), -2)."-01";
			$sql = "INSERT INTO dashboard_rh_empleados (idSucursal, fecha, rotacion, empleados, idUsuario, fechaCrea, horaCrea) VALUES (?,?,?,?,?,?,?);";
			DB::insert($sql , [$request->input('sucursal'),$fecha ,$request->input('rotacion'),$request->input('nemp'),$idUsuario,date("Y-m-d"),date("H:i:s")] );

			echo "{ \"success\": true }";
		}
		else 
		{
			  echo "{ \"success\": false }";
		}

	}

	public function comparativo($isPdf=0, $order="desc", $col="Costo")
	{
		$Role = session('DASHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		$idUsuario = Auth::id();
		if($Role != 1 && $Role != 6)
			if(!empty($sucursales))
				$strValSucursales = "AND idSucursal IN (".$sucursales.") ";
			else
				$strValSucursales = "AND idSucursal IN (0) ";
		
		$mes = date("m")==1?12:date("m")-1;
		$mes2= $mes;
		$mes = $mes < 10 ? "0".$mes:$mes;
		$anio = date("Y") - (date("m")==1?1:0);

		$orderby = " ORDER BY $col $order";

		$sql = "SELECT DATE_FORMAT(fecha, '%c') mes, DATE_FORMAT(fecha, '%y') anio FROM dashboard_evaluacion WHERE bono >= 0  $strValSucursales GROUP BY fecha ORDER BY DATE_FORMAT(fecha, '%c') DESC LIMIT 0,1";
		$periodos = DB::select($sql);

		$sql = "SELECT montos.nombre, users.name supervisor,montos.Costo/(venta.monto) Costo, montos.Nomina/(venta.monto) Nomina, montos.Renta/(venta.monto) Renta, montos.Gasto/(venta.monto) Gasto, ebit.monto EbitM, ebit.porcentaje EbitP FROM (SELECT sucursales.nombre ,  sucursales.idSap idSucursal ,SUM(IF(tipo=1, datos.monto, 0))*100 Costo, SUM(IF(tipo=2, datos.monto, 0))*100 Nomina, SUM(IF(tipo=3, datos.monto, 0))*100 Renta, SUM(IF(tipo=4, datos.monto, 0))*100 Gasto FROM (SELECT 1 tipo, idSucursal, SUM(monto)monto FROM finanzas_costo_mes_sap WHERE EXTRACT(YEAR_MONTH FROM fecha) = '".$anio.$mes."' GROUP BY idSucursal , EXTRACT(YEAR_MONTH FROM fecha)
		UNION ALL
		SELECT 2 tipo, idSucursal, SUM(monto)monto FROM finanzas_gasto_mes_sap WHERE EXTRACT(YEAR_MONTH FROM fecha) = '".$anio.$mes."' AND idCuenta= '6207-0700' GROUP BY idSucursal , EXTRACT(YEAR_MONTH FROM fecha)
		UNION ALL
		SELECT 3 tipo, idSucursal, SUM(monto)monto FROM finanzas_gasto_mes_sap WHERE EXTRACT(YEAR_MONTH FROM fecha) = '".$anio.$mes."' AND idCuenta= '6105-0100' GROUP BY idSucursal , EXTRACT(YEAR_MONTH FROM fecha)
		UNION ALL
		SELECT 4 tipo, idSucursal, SUM(monto)monto FROM finanzas_gasto_mes_sap WHERE EXTRACT(YEAR_MONTH FROM fecha) = '".$anio.$mes."' AND idCuenta IN ('1000-0003','6104-1000','6105-0300','6105-0400','6110-0100','6110-0300','6111-0100','6112-0200','6113-0200','6113-0300','6114-0200','6115-0200','6117-0100','6117-0200','6119-0400','6120-0100','6121-0100','6121-0200','6122-0100','6122-0200','6203-0600','6206-0100','6207-0200','6207-0400','6207-0600','6209-0200','6211-0100','6212-0100','6212-0200','6212-0300','6212-0500','6212-0600','6212-0700','6213-0100','7104-0100','7104-0300','6104-0100','6104-0200','6104-0300','6104-0400','6104-0600','6104-0900','6203-0200') GROUP BY idSucursal , EXTRACT(YEAR_MONTH FROM fecha)
		) datos INNER JOIN sucursales ON sucursales.idSap = datos.idSucursal WHERE sucursales.id != 30 GROUP BY sucursales.nombre,sucursales.idSap ) montos INNER JOIN (SELECT idSucursal, SUM(monto) monto FROM finanzas_venta_mes_sap WHERE EXTRACT(YEAR_MONTH FROM fecha) = '".$anio.$mes."' GROUP BY idSucursal) venta ON venta.idSucursal= montos.idSucursal LEFT JOIN finanzas_ebit ebit ON (ebit.idSucursal=montos.idSucursal AND EXTRACT(YEAR_MONTH FROM ebit.fecha) = '".$anio.$mes."' ) LEFT JOIN sucursales ON montos.idSucursal= sucursales.idSap LEFT JOIN supervisor_sucursal ON supervisor_sucursal.idSucursal = sucursales.id LEFT JOIN users ON users.id = supervisor_sucursal.idUsuario  $orderby;";
		$gastos = DB::select($sql);
		$meses = array('1' => 'Ene' ,'2' => 'Feb' ,'3' => 'Mar' ,'4' => 'Abr' ,'5' => 'May' ,'6' => 'Jun' ,'7' => 'Jul' ,'8' => 'Ago' ,'9' => 'Sep' ,'10' => 'Oct' ,'11' => 'Nov' ,'12' => 'Dic' );

		$data = [ 'isPdf' => $isPdf, 'order'=> $order, 'col' => $col,'Role' => $Role, 'sucursales' =>$sucursales, 'gastos' => $gastos, 'anio' => $anio,"mes"=>$mes2, 'meses' => $meses ];
		if($isPdf)
		{
			$pdf = PDF::loadView('dashboard.comparativopdf', $data);
			return $pdf->download('comparativo'.date("Y-m").'.pdf');
		}	
		else{
			return view('dashboard.comparativo',$data);
		}
	}

	public function evaluacion($isPdf=0)
	{
		$Role = session('DASHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		$idUsuario = Auth::id();
		if($Role != 1 && $Role != 6)
			if(!empty($sucursales))
				$strValSucursales = "AND idSucursal IN (".$sucursales.") ";
			else
				$strValSucursales = "AND idSucursal IN (0) ";
		
		$sql = "SELECT MONTH(fecha) mes, DATE_FORMAT(fecha, '%y') anio FROM dashboard_evaluacion WHERE bono >= 0  $strValSucursales GROUP BY fecha ORDER BY MONTH(fecha) DESC LIMIT 0,1";
		$periodos = DB::select($sql);
		$sql = "SELECT s.nombre ,e.evaluacion, e.bono, e.venta, e.bonoSupervisor, e.bonoGerente, e.bonoChef FROM dashboard_evaluacion e INNER JOIN sucursales s ON e.idSucursal = s.id WHERE DATE_FORMAT(fecha, '%c')  = ".$periodos[0]->mes." AND DATE_FORMAT(fecha, '%y') = ".$periodos[0]->anio." AND  bono >= 0  $strValSucursales ORDER BY evaluacion DESC;";

		//$sql = "SELECT EXTRACT(YEAR_MONTH FROM fecha) periodo, s.nombre ,e.evaluacion, e.bono, e.venta, e.bonoSupervisor, e.bonoGerente FROM dashboard_evaluacion e INNER JOIN sucursales s ON e.idSucursal = s.id WHERE bono >= 0  $strValSucursales GROUP BY EXTRACT(YEAR_MONTH FROM fecha), e.idSucursal ORDER BY evaluacion DESC;";
		$bonos = DB::select($sql);
		$meses = array('1' => 'Ene' ,'2' => 'Feb' ,'3' => 'Mar' ,'4' => 'Abr' ,'5' => 'May' ,'6' => 'Jun' ,'7' => 'Jul' ,'8' => 'Ago' ,'9' => 'Sep' ,'10' => 'Oct' ,'11' => 'Nov' ,'12' => 'Dic' );

		$data = [ 'isPdf' => $isPdf,'Role' => $Role, 'sucursales' =>$sucursales, 'bonos' => $bonos, 'periodos' => $periodos, 'meses' => $meses ];
		if($isPdf)
		{
			$pdf = PDF::loadView('dashboard.evaluacionpdf', $data);
			return $pdf->download('bonos'.date("Y-m").'.pdf');
		}	
		else{
			return view('dashboard.evaluacion',$data);
		}
			
	}

	public function bitacora($selSuc=0)
	{

		$Role = session('DASHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		$idUsuario = Auth::id();
		if($Role != 1 && $Role != 6)
			if(!empty($sucursales))
				$strValSucursales = "AND idSucursal IN (".$sucursales.") ";
			else
				$strValSucursales = "AND idSucursal IN (0) ";

		$sql = "SELECT id, nombre FROM sucursales WHERE idEmpresa = 1 AND idTipo > 0 ".(empty($sucursales)?"":" AND id IN (".$sucursales.") ")." ORDER BY nombre;";
		$sucursales = DB::select($sql);
		
		if(empty($selSuc))
			$selSuc = 0;

		if($Role == 1 || $Role == 6 || $selSuc > 0 )
		{
			$sql= "SELECT T0.idAccion id, T0.accion, T0.fecha,T0.prioridad, T1.tipo FROM dashboard_bitacora_acciones AS T0 INNER JOIN dashboard_bitacora_accion_tipo AS T1 ON T0.idTipo = T1.idTipoAccion WHERE T0.estado = 1 AND idSucursal=$selSuc ORDER BY T0.idTipo;";
			$pendientes = DB::select($sql);		

			$sql = "SELECT idTipoAccion AS id, tipo FROM dashboard_bitacora_accion_tipo ORDER BY idTipoAccion;";
			$tipos = DB::select($sql);

			$sql = "SELECT T0.idAccion id, T0.accion,  T0.fecha,T0.prioridad, T1.tipo FROM dashboard_bitacora_acciones AS T0 INNER JOIN dashboard_bitacora_accion_tipo AS T1 ON T0.idTipo = T1.idTipoAccion WHERE T0.estado = 2 AND idSucursal=$selSuc ORDER BY T0.idTipo;";
			$terminadas = DB::select($sql);	
		}
		else {
			$pendientes = null;
			$tipos = null;
			$terminadas = null;	
		}

		return view('dashboard.bitacora',[ 'Role' => $Role,'selSuc'=>$selSuc, 'sucursales' =>$sucursales ,'tipos' =>$tipos, 'pendientes'=> $pendientes, 'terminadas'=> $terminadas]);
	}

	public function getBitacora($idSucursal=0, $tipo=1)
	{
		$sql= "SELECT T0.idAccion id, T0.accion, T0.fecha, T0.prioridad ,T1.tipo FROM dashboard_bitacora_acciones AS T0 INNER JOIN dashboard_bitacora_accion_tipo AS T1 ON T0.idTipo = T1.idTipoAccion WHERE T0.estado = $tipo AND idSucursal=$idSucursal ORDER BY T0.idTipo;";
		$acciones = DB::select($sql);

		return view('dashboard.detBit',['acciones' => $acciones, 'tipo' => $tipo]);
	}

	public function guardaBitacora(Request $request)
	{
		$user = Auth::user();
		$idUsuario = $user->id;

		$sql = "INSERT INTO dashboard_bitacora_acciones (periodo, idSucursal, idTipo, accion, fecha, idUsuario, prioridad ,estado) VALUES (?,?,?,?,?,?,?,?);";
		DB::insert($sql , [date("Ym"), $request->input('sucursal'), $request->input('tipo'),$request->input('accion') , date("Y-m-d"), $idUsuario,$request->input('prioridad'), 1 ] );
		
		echo "{ \"success\": true }";
	}

	public function getMensajesBitacora(Request $request)
	{
		$sql ="SELECT mensaje.*, users.name FROM dashboard_bitacora_acciones_mensaje AS mensaje INNER JOIN users ON mensaje.idUsuario = users.id WHERE idAccion = ? ORDER BY fecha DESC, hora DESC;";
		$mensajes = DB::select($sql, [$request->input('accion')]);
		$txtMensajes = "";
		foreach($mensajes as $mensaje){
			$txtMensajes .= "=================================<br>";
			$txtMensajes .= $mensaje->fecha . "  ".$mensaje->hora . "  ".$mensaje->name."<br>";
			$txtMensajes .= $mensaje->mensaje."<br>";
		}
		return $txtMensajes;
	}
	
	public function saveMensaje(Request $request)
	{

		$user = Auth::user();
		$idUsuario = $user->id;

		$sql = "INSERT INTO dashboard_bitacora_acciones_mensaje (idAccion, idUsuario, fecha, hora, mensaje) VALUES (?,?,?,?,?);";
		DB::insert($sql , [$request->input('accion'), $idUsuario, date("Y-m-d"), date("H:i:s") ,$request->input("valor")] );
		
		echo "{ \"success\": true }";
	}

	

	public function eliminaTaskBitacora(Request $request)
	{
		$user = Auth::user();
		$idUsuario = $user->id;
		$accion = $request->input('accion');
		if(!empty($accion)){
			$sql= "DELETE FROM dashboard_bitacora_acciones WHERE idAccion = ? ;";
			DB::delete($sql, [$accion]);
		}
		echo "{ \"success\": true }";
	}

	public function actualizaTaskBitacora(Request $request)
	{
		$user = Auth::user();
		$idUsuario = $user->id;
		$accion = $request->input('accion');
		if(!empty($accion)){
			$sql= "UPDATE dashboard_bitacora_acciones SET accion = ?, prioridad= ? WHERE idAccion = ? ;";
			DB::update($sql, [ $request->input('valor'), $request->input('prioridad'), $accion]);
		}
		echo "{ \"success\": true }";
	}
	
	public function actualizaBitacora(Request $request)
	{
		$user = Auth::user();
		$idUsuario = $user->id;

		$acciones = $request->input('accion');
		if(!empty($acciones))
			foreach($acciones AS  $idAccion=>$val){
				$sql= "UPDATE dashboard_bitacora_acciones SET estado = 2 , fechaFin= ?, horaFin= ?, idUsuarioFin=? WHERE idAccion = ? ;";
				DB::update($sql, [date("Y-m-d"), date("H:i:s"),$idUsuario, $idAccion]);
			}
		echo "{ \"success\": true }";
	}

	public function guardaCapacita(Request $request)
	{
		$user = Auth::user();
		$idUsuario = $user->id;
		if(!empty($request->input('sucursal')) && !empty($request->input('periodo')) )
		{
			$fecha = "";
			$fecha .= substr($request->input('periodo'), 0, 4)."-";
			$fecha .= substr($request->input('periodo'), -2)."-01";
			$sql = "INSERT INTO dashboard_capacita (idSucursal, fecha, capacita, uso , idUsuario, fechaCrea, horaCrea) VALUES (?,?,?,?,?,?,?);";
			DB::insert($sql , [$request->input('sucursal'),$fecha,$request->input('capacita') ,$request->input('uso') ,$idUsuario,date("Y-m-d"),date("H:i:s")] );

			echo "{ \"success\": true }";
		}
		else 
		{
			echo "{ \"success\": false }";
		}
	}

	public function guardaEncuesta(Request $request)
	{
		$user = Auth::user();
		$idUsuario = $user->id;
		if(!empty($request->input('sucursal')) && !empty($request->input('periodo')) )
		{
			$fecha = "";
			$fecha .= substr($request->input('periodo'), 0, 4)."-";
			$fecha .= substr($request->input('periodo'), -2)."-01";
			$sql = "INSERT INTO dashboard_encuesta (idSucursal, fecha, eraa, erca, ercs, err, evaa, evrz, evcs, evr , idUsuario, fechaCrea, horaCrea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
			DB::insert($sql , [$request->input('sucursal'),$fecha,$request->input('eraa'), $request->input('erca'), $request->input('ercs'), $request->input('err'), $request->input('evaa'), $request->input('evrz'), $request->input('evcs'), $request->input('evr') ,$idUsuario,date("Y-m-d"),date("H:i:s")] );

			echo "{ \"success\": true }";
		}
		else 
		{
			echo "{ \"success\": false }";
		}
	}

	public function guardaRedes(Request $request){
		$user = Auth::user();
		$idUsuario = $user->id;
		if(!empty($request->input('sucursal')) && !empty($request->input('periodo'))&& !empty($request->input('red')) && !empty($request->input('califica')))
		{
			 	$fecha = "";
				$fecha .= substr($request->input('periodo'), 0, 4)."-";
				$fecha .= substr($request->input('periodo'), -2)."-01";
				$sql = "INSERT INTO dashboard_redes_soc (idSucursal, fecha, idRed, calificacion, idUsuario, fechaCrea, horaCrea) VALUES (?,?,?,?,?,?,?);";
				DB::insert($sql , [$request->input('sucursal'),$fecha ,$request->input('red'),$request->input('califica'),$idUsuario,date("Y-m-d"),date("H:i:s")] );

				echo "{ \"success\": true }";
		}
		else 
		{
			  echo "{ \"success\": false }";
		}

	}

	public function guardaUberEats(Request $request){
		$user = Auth::user();
		$idUsuario = $user->id;
		if(!empty($request->input('sucursal')) && !empty($request->input('periodo')))
		{
			 	$fecha = "";
				$fecha .= substr($request->input('periodo'), 0, 4)."-";
				$fecha .= substr($request->input('periodo'), -2)."-01";
				$sql = "INSERT INTO dashboard_uber_eats (idSucursal, fecha, tiempoCon, tiempoPausa, aceptados, tiempoAcepta, ventaPerdida, tiempoPrepara, idUsuario, fechaCrea, horaCrea) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
				DB::insert($sql , [$request->input('sucursal'),$fecha ,$request->input('contime'),$request->input('paustime'),$request->input('accrequest'),$request->input('accepttime'),$request->input('lostsale'),$request->input('madetime'),$idUsuario,date("Y-m-d"),date("H:i:s")] );

				echo "{ \"success\": true }";
		}
		else 
		{
			  echo "{ \"success\": false }";
		}
	}
	

	public function detAlexTable(Request $request){
		
		if($request->input('idp') == 1)
		{
				$sql = "SELECT  idSucursal , SUM(monto) total FROM finanzas_venta_dia_sap WHERE EXTRACT(YEAR_MONTH FROM fecha) = '".$request->input('periodo')."' GROUP BY idSucursal;";
				$det = DB::select($sql);
		}
		
		return view('dashboard.detAlexTable', ['det' => $det ]);

	}
	
	public function detArtTable(Request $request){
		
		$sql = "SELECT * FROM dashboard_pca1 WHERE anio = ".$request->input('anio')." AND idCompania =  ".$request->input('idc')." AND idItem= '".$request->input('idi')."' ORDER BY total desc;";
		
		$detArt = DB::select($sql);
		
		return view('dashboard.detArtTable', ['detArt' => $detArt ]);
	}
	
	public function detProvTable(Request $request){
		
		if($request->input('idc')==0)
			$compania = " ";
		else
			$compania = " AND idCompania =  ".$request->input('idc');
		
		$sql = "SELECT * FROM dashboard_pca1 WHERE anio = ".$request->input('anio')." ".$compania." AND idProveedor= '".$request->input('idp')."' ORDER BY total desc;";
		
		$detProv = DB::select($sql);
		
		return view('dashboard.detProvTable', ['detProv' => $detProv ]);
	}
	
	public function operacionDashboard($rtype = 1)
	{
		$mes = date("m")==1?12:date("m")-1;
		$anio = date("Y") - (date("m")==1?1:0); 

		$mes = date("m")==1?12:date("m")-1;
		$mesanterior = date("m")==2?12:$mes-1;

		$anio = date("Y") - (date("m")==1?1:0);
		$anioanterior = date("Y") - (date("m")<=2?1:0);
		
		#$sql = "SELECT horaInicio, SUM(total) total FROM venta_cheque WHERE MONTH(fecha) = ".($mes-1)." AND YEAR(fecha) = ".$anio." GROUP BY horaInicio ORDER BY horaInicio ASC;";
		$sql = "SELECT vc.horaFin horaInicio, SUM(IF(suc.idCategoria = 1, vc.subtotal,0)) cat_1, SUM(IF(suc.idCategoria = 2, vc.subtotal,0)) cat_2, SUM(IF(suc.idCategoria = 3, vc.subtotal,0)) cat_3, SUM(IF(suc.idCategoria = 4, vc.subtotal,0)) cat_4, SUM(IF(suc.idCategoria = 5, vc.subtotal,0)) cat_5, SUM(vc.subtotal) Total FROM venta_cheque vc INNER JOIN sucursales suc ON suc.idMicros = vc.idSucursal WHERE MONTH(vc.fecha) = ".($mes-2)." AND YEAR(vc.fecha) = ".$anio." GROUP BY vc.horaFin ORDER BY vc.horaFin;";
		$ventasHora = DB::select($sql);
		
		//$sql = "SELECT * FROM dashboard_vpa2 WHERE mes= ".($mes-2)." AND anio = ".$anio." ORDER BY total;";
		$sql = "SELECT current.RVC, current.mes, current.anio, current.total, current.porcentaje, past.total past, average.total average FROM (SELECT * FROM dashboard_vpa2 WHERE mes = ".($mes-2)." AND anio = ".$anio." ORDER BY total) current LEFT JOIN (SELECT RVC, total FROM dashboard_vpa2 WHERE mes = ".($mes-3)." AND anio = ".$anio." ORDER BY total) past ON current.RVC = past.RVC LEFT JOIN (SELECT RVC, AVG(total) total FROM dashboard_vpa2 WHERE anio = ".$anio." GROUP BY RVC ORDER BY total) average ON current.RVC = average.RVC;";
		$ventasRVC = DB::select($sql);		
		
		$sql = "SELECT sucursales_categoria.categoria, meses, Clientes/meses Clientes_Acumulado, Clientes_Anterior, Clientes_Actual,IF(Anterior = 0, 0, (total/Clientes)) AS Cheque_Acumulado ,IF(Anterior = 0, 0, (Anterior/Clientes_Anterior)) AS Cheque_Anterior, IF(Clientes_Anterior = 0, 0, (Actual/Clientes_Actual)) AS Cheque_Actual FROM (SELECT COUNT(distinct(MONTH(fecha))) meses, sucursales.idCategoria, SUM(subtotal) total, SUM(clientes) Clientes, Sum(IF(MONTH(fecha) = ".($mes-3).",subtotal,0)) Anterior, Sum(IF(MONTH(fecha) = ".($mes-2).",subtotal,0)) Actual,Sum(IF(MONTH(fecha) = ".($mes-3).",clientes,0)) Clientes_Anterior,	Sum(IF(MONTH(fecha) = ".($mes-2).",clientes,0)) Clientes_Actual FROM venta_cheque INNER JOIN sucursales ON sucursales.idMicros = venta_cheque.idSucursal WHERE clientes <13 AND YEAR(fecha) = $anio AND idCentroIngreso='Salon' AND idCategoria != 8 GROUP BY sucursales.idCategoria ORDER BY idCategoria, MONTH(fecha)) datos INNER JOIN sucursales_categoria ON datos.idCategoria = sucursales_categoria.idCategoriaSucursal;";
		$chequeSalon = DB::select($sql);
		
		$sql = "SELECT sucursales_categoria.categoria, meses, Clientes/meses Clientes_Acumulado, Clientes_Anterior, Clientes_Actual,IF(Anterior = 0, 0, (total/Clientes)) AS Cheque_Acumulado ,IF(Anterior = 0, 0, (Anterior/Clientes_Anterior)) AS Cheque_Anterior, IF(Clientes_Anterior = 0, 0, (Actual/Clientes_Actual)) AS Cheque_Actual FROM (SELECT COUNT(distinct(MONTH(fecha))) meses, sucursales.idCategoria, SUM(subtotal) total, COUNT(clientes) Clientes, Sum(IF(MONTH(fecha) = ".($mes-3).",subtotal,0)) Anterior, Sum(IF(MONTH(fecha) = ".($mes-2).",subtotal,0)) Actual,Sum(IF(MONTH(fecha) = ".($mes-3).",1,0)) Clientes_Anterior, Sum(IF(MONTH(fecha) = ".($mes-2).",1,0)) Clientes_Actual FROM venta_cheque INNER JOIN sucursales ON sucursales.idMicros = venta_cheque.idSucursal WHERE clientes <13 AND YEAR(fecha) = $anio AND (idCentroIngreso='Vitrina' OR idCentroIngreso='Kayser para llevar') AND idCategoria != 8 GROUP BY sucursales.idCategoria ORDER BY idCategoria, MONTH(fecha)) datos INNER JOIN sucursales_categoria ON datos.idCategoria = sucursales_categoria.idCategoriaSucursal;";
		$chequeVitrina = DB::select($sql);
		
		return view('dashboard.operaciones', ['tiempo' => $rtype,'mes' => $this->meses[($mes-1)][1], 'ventasHora' => $ventasHora, "ventasRVC" => $ventasRVC, 'chequeSalon' => $chequeSalon, 'chequeVitrina' => $chequeVitrina]);
		//return view('dashboard.operaciones', ['tiempo' => $rtype,'mes' => date('F'), 'ventasHora' => $ventasHora, 'meses'=>$mes ,'chequeProm' => $chequePromStr,'chequePromVit' => $chequePromVitStr,'chequePromDom' => $chequePromDomStr, 'minchequeProm' => $minchequeProm, 'maxchequeProm' => $maxchequeProm, 'Comensales'=>$comensalesStr, 'ComensalesVit'=>$comensalesVitStr, 'ComensalesDom'=>$comensalesDomStr, 'minClientesSalon'=>$minClientesSalon,'maxClientesSalon'=>$maxClientesSalon, 'repcheques' => $cheques, 'sucursalestab' => $sucursalestab, 'montosd' => $montosd]);
	}
	
	public function rvcDetMajor(Request $request)
	{
		$anio = empty($request->input('anio'))?$anio:$request->input('anio');
		$mes = empty($request->input('mes'))?($mes):$request->input('mes');
		if(!empty($request->input('filtro')))
		{
			$sql = "SELECT * FROM dashboard_vpa2_major WHERE idCompania=1 AND RVC='".$request->input('filtro')."' AND mes = '".$request->input('mes')."' AND anio = '".$request->input('anio')."';";
			$major = DB::select($sql);
			return response()->json([ 'data' => $major ]);
		}
		else
		{
			return "No data to show!!!";
		}		
	}
	
	public function rvcDetItem(Request $request){
		$anio = empty($request->input('anio'))?$anio:$request->input('anio');
		$mes = empty($request->input('mes'))?($mes):$request->input('mes');
		if(!empty($request->input('filtro')))
		{
			if(!empty($request->input('filtro2')))
			{
				if(!empty($request->input('tipo')) && $request->input('tipo')==1)
				{
					$sql = "SELECT RVC,MajorGroup, menuItem,MONTH(fecha) mes, SUM(total) total FROM dashboard_vpa_temp WHERE total>0 AND NOT(FamilyGroup LIKE '%Mod %') AND YEAR(fecha)=".$anio." AND MONTH(fecha)=".$mes." AND MajorGroup= '".$request->input('filtro2')."' AND RVC= '".$request->input('filtro')."' AND NOT(idSucursal = 'Carmela y Sal') GROUP BY RVC,MajorGroup,menuItem,MONTH(fecha) ORDER BY total DESC LIMIT 0,20;";
				}
				else
				{
					$sql = "SELECT art.menuItem, art.articulo, IF(com.combo IS null, 0, com.combo) combo, art.articulo+IF(com.combo IS null, 0, com.combo) total FROM 
	(SELECT menuItem, SUM(cantidad) articulo FROM dashboard_vpa_temp WHERE combo='0' AND NOT(FamilyGroup LIKE '%Mod %' OR FamilyGroup LIKE 'Mod.%') AND YEAR(fecha)=".$anio." AND MONTH(fecha)=".$mes." AND MajorGroup= '".$request->input('filtro2')."' AND RVC= '".$request->input('filtro')."' AND NOT(idSucursal = 'Carmela y Sal') GROUP BY menuItem) art
	LEFT JOIN
	(SELECT menuItem, SUM(cantidad) combo FROM dashboard_vpa_temp WHERE combo='2' AND NOT(FamilyGroup LIKE '%Mod %' OR FamilyGroup LIKE 'Mod.%') AND YEAR(fecha)=".$anio." AND MONTH(fecha)=".$mes." AND MajorGroup= '".$request->input('filtro2')."' AND RVC= '".$request->input('filtro')."' AND NOT(idSucursal = 'Carmela y Sal') GROUP BY menuItem) com ON art.menuItem = com.menuItem ORDER BY total DESC LIMIT 0,20;";
				}
			}
			else
			{
				if(!empty($request->input('tipo')) && $request->input('tipo')==1)
				{
					$sql = "SELECT RVC, menuItem,MONTH(fecha) mes, SUM(total) total FROM dashboard_vpa_temp WHERE total>0 AND NOT(FamilyGroup LIKE '%Mod %') AND YEAR(fecha)=".$anio." AND MONTH(fecha)=".$mes." AND RVC= '".$request->input('filtro')."' AND NOT(idSucursal = 'Carmela y Sal') GROUP BY RVC, menuItem,MONTH(fecha) ORDER BY total DESC LIMIT 0,20;";
				}
				else
				{
					$sql = "SELECT art.menuItem, art.articulo, IF(com.combo IS null, 0, com.combo) combo, art.articulo+IF(com.combo IS null, 0, com.combo) total FROM 
	(SELECT menuItem, SUM(cantidad) articulo FROM dashboard_vpa_temp WHERE combo='0' AND NOT(FamilyGroup LIKE '%Mod %' OR FamilyGroup LIKE 'Mod.%') AND YEAR(fecha)=".$anio." AND MONTH(fecha)=".$mes." AND RVC= '".$request->input('filtro')."' AND NOT(idSucursal = 'Carmela y Sal') GROUP BY menuItem) art
	LEFT JOIN
	(SELECT menuItem, SUM(cantidad) combo FROM dashboard_vpa_temp WHERE combo='2' AND NOT(FamilyGroup LIKE '%Mod %' OR FamilyGroup LIKE 'Mod.%') AND YEAR(fecha)=".$anio." AND MONTH(fecha)=".$mes." AND RVC= '".$request->input('filtro')."' AND NOT(idSucursal = 'Carmela y Sal') GROUP BY menuItem) com ON art.menuItem = com.menuItem ORDER BY total DESC LIMIT 0,20;";
				}				
			}

			$items = DB::select($sql);
			return view('dashboard.detArtSaleTable', [ 'detArt' => $items, 'tipo' => $request->input('tipo'), 'seccion' => (!empty($request->input('filtro2'))?$request->input('filtro2'):$request->input('filtro')) ]);
		}
		else
		{
			return "NO DATA TO DISPLAY!!!";
			
		}
	}
	
	public function getMes($mes){
		switch($mes)
		{
			case 1:
				$strMes ="Enero";
			break;
			case 2:
				$strMes ="Febrero";
			break;
			case 3:
				$strMes ="Marzo";
			break;
			case 4:
				$strMes ="Abril";
			break;
			case 5:
				$strMes ="Mayo";
			break;
			case 6:
				$strMes ="Junio";
			break;
			case 7:
				$strMes ="Julio";
			break;
			case 8:
				$strMes ="Agosto";
			break;
			case 9:
				$strMes ="Septiembre";
			break;
			case 10:
				$strMes ="Octubre";
			break;
			case 11:
				$strMes ="Noviembre";
			break;
			case 12:
				$strMes ="Diciembre";
			break;
		}
		return $strMes;
	}
	public function finanzasDashboard($rtype = 1)
	{
/*******************Tabla actual VS budget VS forecast***********************/
		$mes = date("m")==1?12:date("m")-1;		
		$anio = date("Y") - (date("m")==1?1:0);

		$venta = DB::table('finanzas_venta_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', ($mes))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();
		$venta_anterior = DB::table('finanzas_venta_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', ($mes-1))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();

		$venta_acumulada = DB::table('finanzas_venta_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('YEAR(fecha)'))
		->get();
		
		$venta_budget = DB::table('finanzas_budget_venta_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', ($mes))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();
		
		$costo = DB::table('finanzas_costo_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', ($mes))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();

		$costo_anterior = DB::table('finanzas_costo_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', ($mes-1))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();

		$costo_acumulado = DB::table('finanzas_costo_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('YEAR(fecha)'))
		->get();
		
		$costo_budget = DB::table('finanzas_budget_costo_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', ($mes))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();
		
		$gasto = DB::table('finanzas_gasto_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', ($mes))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();

		$gasto_anterior = DB::table('finanzas_gasto_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', ($mes-1))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();
		

		$gasto_acumulado = DB::table('finanzas_gasto_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('YEAR(fecha)'))
		->get();
		
		$gasto_budget = DB::table('finanzas_budget_gasto_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', ($mes))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();
		
		$ventasMes = DB::table('finanzas_venta_dia_sap')
		->select(DB::raw('MONTH(fecha) AS mes, SUM(monto) AS totalMes'))
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->orderBy(DB::raw('MONTH(fecha)'))
		->get();
		
		$kpiVentaMes="";
		$minVentaMes=0;
		$maxVentaMes=0;
		
		foreach($ventasMes AS $ventam)
		{
			if(!empty($kpiVentaMes))
				$kpiVentaMes .= ", ";
			$kpiVentaMes .= $ventam->totalMes/1000;
			
			$minVentaMes= ($ventam->totalMes < $minVentaMes || $minVentaMes == 0) ? $ventam->totalMes : $minVentaMes;
			$maxVentaMes= ($ventam->totalMes > $maxVentaMes || $maxVentaMes == 0) ? $ventam->totalMes : $maxVentaMes;
		}
		
		$minVentaMes= $minVentaMes/1000;
		$maxVentaMes= $maxVentaMes/1000;

/******************************* GRAFICAS *********************************************/
		
		$ventaActual = empty($venta[0])?0:$venta[0]->total;
		$ventaBudget = empty($venta_budget[0]->total)?1:$venta_budget[0]->total;
		$alarmaVenta = $ventaActual/$ventaBudget*100;
		$ventaActual = number_format($ventaActual, 0,"", ",");
		$ventaBudget = number_format($ventaBudget, 0,"", ",");
		
		$gastoActual = empty($gasto[0])?0:$gasto[0]->total;
		$gastoBudget = empty($gasto_budget[0]->total)?1:$gasto_budget[0]->total;
		$alarmaGasto = $gastoActual/$gastoBudget*100;
		$gastoActual = number_format($gastoActual, 0,"", ",");
		$gastoBudget = number_format($gastoBudget, 0,"", ",");
		
		$costoActual = empty($costo[0])?0:$costo[0]->total;
		$costoBudget = empty($costo_budget[0]->total)?1:$costo_budget[0]->total;
		$alarmaCosto = $costoActual/$costoBudget*100;
		$costoActual = number_format($costoActual, 0,"", ",");
		$costoBudget = number_format($costoBudget, 0,"", ",");
		
		$unetaActual = number_format((empty($venta[0])?0:$venta[0]->total) - (empty($costo[0])?0:$costo[0]->total), 0,"", ",");
		$unetaBudget = number_format($venta_budget[0]->total - $costo_budget[0]->total, 0,"", ",");
		$alarmaUneta = $unetaActual/$unetaBudget*100;
		
		$ebidtaActual = number_format((empty($venta[0])?0:$venta[0]->total) - (empty($costo[0])?0:$costo[0]->total) - (empty($gasto[0])?0:$gasto[0]->total), 0,"", ",");
		$ebidtaBudget = number_format($venta_budget[0]->total - $costo_budget[0]->total - $gasto_budget[0]->total, 0,"", ",");
		$alarmaEbidta = $ebidtaActual/$ebidtaBudget*100;

		$ebidta_anterior = number_format((empty($venta_anterior[0])?0:$venta_anterior[0]->total) - (empty($costo_anterior[0])?0:$costo_anterior[0]->total) - (empty($gasto_anterior[0])?0:$gasto_anterior[0]->total), 0,"", ",");
		$ebidta_acumulado = number_format((empty($venta_acumulada[0])?0:$venta_acumulada[0]->total) - (empty($costo_acumulado[0])?0:$costo_acumulado[0]->total) - (empty($gasto_acumulado[0])?0:$gasto_acumulado[0]->total), 0,"", ",");
		$alarmaEbidtaAnt = (empty($venta[0])?0:$venta[0]->total) - (empty($costo[0])?0:$costo[0]->total) - (empty($gasto[0])?0:$gasto[0]->total) > (empty($venta_anterior[0])?0:$venta_anterior[0]->total) - (empty($costo_anterior[0])?0:$costo_anterior[0]->total) - (empty($gasto_anterior[0])?0:$gasto_anterior[0]->total) ? 1: 0;


		$uneta_anterior = number_format((empty($venta_anterior[0])?0:$venta_anterior[0]->total) - (empty($costo_anterior[0])?0:$costo_anterior[0]->total), 0,"", ",");
		$uneta_acumulada = number_format((empty($venta_acumulada[0])?0:$venta_acumulada[0]->total) - (empty($costo_acumulado[0])?0:$costo_acumulado[0]->total), 0,"", ",");
		$alarmaUnetaAnt = (empty($venta[0])?0:$venta[0]->total) - (empty($costo[0])?0:$costo[0]->total) >= (empty($venta_anterior[0])?0:$venta_anterior[0]->total) - (empty($costo_anterior[0])?0:$costo_anterior[0]->total) ? 1: 0;
		//$gastomes = DB::select('SELECT '.$strGroup.' AS tiempo, categoria.categoria , SUM(monto) AS monto FROM finanzas_gasto_dia_sap AS gasto INNER JOIN finanzas_cuentas AS cuentas ON cuentas.idCuenta = gasto.idCuenta INNER JOIN finanzas_categoria_cuenta categoria ON categoria.idCategoria = cuentas.idCategoria '. (empty($strWhere)?"":" WHERE ".$strWhere).(empty($strGroup)?"":" GROUP BY ".$strGroup.",categoria.categoria").(empty($strOrder)?"":" ORDER BY ".$strOrder).';');	
		
		//dd($gastomes);
		
		$sql = "SELECT idSucursal, SUM(IF(MONTH(fecha)=1, monto , 0 )) Ene ,SUM(IF(MONTH(fecha)=2, monto, 0)) Feb, SUM(IF(MONTH(fecha)=3, monto, 0)) Mar, SUM(IF(MONTH(fecha)=4, monto, 0)) Abr, SUM(IF(MONTH(fecha)=5, monto, 0)) May, SUM(IF(MONTH(fecha)=6, monto, 0)) Jun, SUM(IF(MONTH(fecha)=7, monto, 0)) Jul, SUM(IF(MONTH(fecha)=8, monto, 0)) Ago, SUM(IF(MONTH(fecha)=9, monto, 0)) Sep,SUM(IF(MONTH(fecha)=10, monto, 0)) Oct,SUM(IF(MONTH(fecha)=11, monto, 0)) Nov, SUM(IF(MONTH(fecha)=12, monto, 0)) Dic,  SUM(monto)total FROM finanzas_venta_dia_sap WHERE YEAR(fecha) = 2019 AND NOT( idSucursal IN ('GIOTTO','GIOTTONR')) GROUP BY idSucursal ORDER BY idSucursal;";
		$ventaAcumula = DB::select($sql);
		
		$sql = "SELECT finanzas_categoria_cuenta.categoria, SUM(IF(MONTH(fecha) = ".($mes).",monto, 0)) totalActual,SUM(IF(MONTH(fecha) = ".($mes-1).",monto, 0)) totalAnterior, SUM(monto) totalAcumulado FROM finanzas_costo_dia_sap INNER JOIN finanzas_cuentas ON finanzas_cuentas.idCuenta = finanzas_costo_dia_sap.idCuenta INNER JOIN finanzas_categoria_cuenta ON finanzas_categoria_cuenta.idCategoria = finanzas_cuentas.idCategoria  WHERE YEAR(fecha) = ".$anio." GROUP BY finanzas_categoria_cuenta.idCategoria, finanzas_categoria_cuenta.categoria;";
		$costo_mes_det = DB::select($sql);

		//$sql = "SELECT finanzas_categoria_cuenta.categoria, SUM(monto) total FROM finanzas_gasto_dia_sap INNER JOIN finanzas_cuentas ON finanzas_cuentas.idCuenta = finanzas_gasto_dia_sap.idCuenta INNER JOIN finanzas_categoria_cuenta ON finanzas_categoria_cuenta.idCategoria = finanzas_cuentas.idCategoria  WHERE MONTH(fecha) = ".($mes)." AND YEAR(fecha) = ".$anio." GROUP BY finanzas_categoria_cuenta.idCategoria, finanzas_categoria_cuenta.categoria;";
		$sql = "SELECT finanzas_categoria_cuenta.categoria, SUM(IF(MONTH(fecha) = ".($mes).",monto, 0)) totalActual,SUM(IF(MONTH(fecha) = ".($mes-1).",monto, 0)) totalAnterior, SUM(monto) totalAcumulado FROM finanzas_gasto_dia_sap INNER JOIN finanzas_cuentas ON finanzas_cuentas.idCuenta = finanzas_gasto_dia_sap.idCuenta INNER JOIN finanzas_categoria_cuenta ON finanzas_categoria_cuenta.idCategoria = finanzas_cuentas.idCategoria  WHERE YEAR(fecha) = ".$anio." GROUP BY finanzas_categoria_cuenta.idCategoria, finanzas_categoria_cuenta.categoria;";
		$gasto_mes_det = DB::select($sql);
		//$sql = "SELECT idSucursal, SUM(monto) total FROM finanzas_venta_dia_sap WHERE MONTH(fecha) = ".($mes)." AND YEAR(fecha) = ".$anio." GROUP BY idSucursal;";
		$sql = "SELECT idSucursal, SUM(IF(MONTH(fecha) = ".($mes).",monto, 0)) totalActual, SUM(IF(MONTH(fecha) = ".($mes-1).",monto, 0)) totalAnterior, SUM(monto) totalAcumulado FROM finanzas_venta_dia_sap WHERE YEAR(fecha) = ".$anio." GROUP BY idSucursal;";
		$venta_mes_det = DB::select($sql);
		
		$sql = "SELECT idSucursal, SUM(IF(mes=".($mes-1).",monto,0)) AS anterior, SUM(IF(mes=".($mes).",monto,0)) AS actual, (SUM(IF(mes=".($mes).",monto,0))-SUM(IF(mes=".($mes-1)." ,monto,0)))/SUM(IF(mes=".($mes-1).",monto,0))*100 Diferencia FROM dashboard_inventario_costo WHERE idCompania=1 AND mes IN (".($mes-1).",".($mes).") AND anio = ".($anio)." GROUP BY idSucursal UNION ALL SELECT 'Total' idSucursal, SUM(IF(mes=".($mes-1).",monto,0)) AS anterior, SUM(IF(mes=".($mes)." ,monto,0)) AS actual, (SUM(IF(mes=".($mes).",monto,0))-SUM(IF(mes=".($mes-1)." ,monto,0)))/SUM(IF(mes=".($mes-1).",monto,0))*100 Diferencia FROM dashboard_inventario_costo WHERE idCompania =1 AND mes IN (".($mes-1).",".($mes).") AND anio = ".($anio)." GROUP BY anio;";
		$inventario = DB::select($sql);		
		$sql = "SELECT idSucursal, SUM(IF(mes=".($mes-1).",monto,0)) AS anterior, SUM(IF(mes=".($mes).",monto,0)) AS actual, (SUM(IF(mes=".($mes).",monto,0))-SUM(IF(mes=".($mes-1)." ,monto,0)))/SUM(IF(mes=".($mes-1).",monto,0))*100 Diferencia FROM dashboard_inventario_costo WHERE idCompania=2 AND mes IN (".($mes-1).",".($mes).") AND anio = ".($anio)." GROUP BY idSucursal UNION ALL SELECT 'Total' idSucursal, SUM(IF(mes=".($mes-1).",monto,0)) AS anterior, SUM(IF(mes=".($mes)." ,monto,0)) AS actual, (SUM(IF(mes=".($mes).",monto,0))-SUM(IF(mes=".($mes-1)." ,monto,0)))/SUM(IF(mes=".($mes-1).",monto,0))*100 Diferencia FROM dashboard_inventario_costo WHERE idCompania =2 AND  mes IN (".($mes-1).",".($mes).") AND anio = ".($anio)." GROUP BY anio;";
		$inventarioprigo = DB::select($sql);
		
		return view('dashboard.finanzas', [ 'tiempo' => $rtype, 'meses' => $this->meses, 'mes' => (date('m')-1),  'ventaAcumula' => $ventaAcumula, 'venta'=> $ventaActual, 'venta_anterior'=>  number_format( empty($venta_anterior[0])?0:$venta_anterior[0]->total, 0,"", ",") , 'venta_acumulada'=> number_format( empty($venta_acumulada[0])?0:$venta_acumulada[0]->total, 0,"", ",") , 'venta_budget'=> $ventaBudget,  'costo'=> $costoActual,  'costo_budget'=> $costoBudget, 'costo_anterior'=>  number_format( empty($costo_anterior[0])?0:$costo_anterior[0]->total, 0,"", ",") , 'costo_acumulado'=> number_format( empty($costo_acumulado[0])?0:$costo_acumulado[0]->total, 0,"", ","),  'gasto'=> $gastoActual,  'gasto_budget'=> $gastoBudget, 'gasto_anterior'=>  number_format( empty($gasto_anterior[0])?0:$gasto_anterior[0]->total, 0,"", ",") , 'gasto_acumulado'=> number_format( empty($gasto_acumulado[0])?0:$gasto_acumulado[0]->total, 0,"", ","),  'uneta' => $unetaActual,  'uneta_budget' => $unetaBudget,  'ebidta' => $ebidtaActual,  'ebidta_budget' => $ebidtaBudget, 'ebidta_anterior' => $ebidta_anterior, 'ebidta_acumulado' => $ebidta_acumulado,  'alarmaVenta' => $alarmaVenta,  'alarmaGasto' => $alarmaGasto,  'alarmaCosto' => $alarmaCosto,  'alarmaUneta' => $alarmaUneta, 'uneta_anterior'=> $uneta_anterior ,'uneta_acumulada'=> $uneta_acumulada, 'alarmaUnetaAnt'=> $alarmaUnetaAnt,   'alarmaEbidta' => $alarmaEbidta, 'alarmaEbidtaAnt'=> $alarmaEbidtaAnt,'gasto_mes_det' => $gasto_mes_det ,'costo_mes_det' => $costo_mes_det ,  'venta_mes_det' => $venta_mes_det, 'ventasMes' => $kpiVentaMes,  'kpiVentasYTDMin' => $minVentaMes,  'kpiVentasYTDMax' => $maxVentaMes, 'inventario' =>$inventario, 'inventarioprigo' =>$inventarioprigo ]);
	}
	public function kpiOperacionesable(Request $request)
	{
		echo "table";
	}
	public function kpiVentasTable(Request $request)
	{
		$where = array();
		
		if(!empty($request->input('anio')))
			$where[] = "YEAR(fecha) = '".$request->input('anio')."'";		
		if(!empty($request->input('mes')))
			$where[] = "MONTH(fecha) = '".$request->input('mes')."'";
		if(!empty($request->input('tipoFiltro')))
		{
			if(!empty($request->input('filtro')))
			{
				if($request->input('filtro') == "sucursal")
					$where[] = "idSucursal = '".$request->input('filtro')."'";
			}
		}
		else if(!empty($request->input('filtro')))
		{
			$where[] = "idSucursal = '".$request->input('filtro')."'";
		}
		
		$strWhere ="";
		
		if(!empty($where))
			$strWhere = implode(" AND ",$where);
		
		if(!empty($request->input('tiempo')))
		{
			if($request->input('tiempo') == 1)
			{
				$strGroup = "MONTH(fecha)";
				$strOrder = "MONTH(fecha)";
			}
			else if($request->input('tiempo') == 4)
			{
				$strGroup = "MONTH(fecha)";
				$strOrder = "MONTH(fecha)";
			}
		}
		else
		{
			$strGroup = "YEAR(fecha)";
			$strOrder = "YEAR(fecha)";
		}		
		
		$ventasRes = DB::select('SELECT '.$strGroup.' AS tiempo, SUM(monto) AS monto FROM finanzas_venta_dia_sap '. (empty($strWhere)?"":" WHERE ".$strWhere).(empty($strGroup)?"":" GROUP BY ".$strGroup).(empty($strOrder)?"":" ORDER BY ".$strOrder));
					
		return view('dashboard.tableVentas', ["filtro" => $request->input('filtro'),"anio" => $request->input('anio') ,"datos" => $ventasRes, "tiempo" => $request->input('tiempo') ]);
	}
	
	public function kpiFinanzas(Request $request)
	{
		
		$anio = empty($request->input('anio'))?$anio:$request->input('anio');
		$mes = empty($request->input('mes'))?date("m"):$request->input('mes');
				
		$venta = DB::table('finanzas_venta_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();
		
		$venta_budget = DB::table('finanzas_budget_venta_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();
		
		$costo = DB::table('finanzas_costo_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();
		
		$costo_budget = DB::table('finanzas_budget_costo_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();
		
		$gasto = DB::table('finanzas_gasto_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();
		
		$gasto_budget = DB::table('finanzas_budget_gasto_dia_sap')
		->select(DB::raw('SUM(monto) AS total'))
		->where(DB::raw('MONTH(fecha)'),'=', $mes)
		->where(DB::raw('YEAR(fecha)'),'=',$anio)
		->groupBy(DB::raw('MONTH(fecha)'))
		->get();
				
		$ventaActual = empty($venta[0])?0:$venta[0]->total;
		$ventaBudget = empty($venta_budget[0]->total)?1:$venta_budget[0]->total;
		$alarmaVenta = $ventaActual/$ventaBudget*100;
		$ventaActual = number_format($ventaActual, 0,"", ",");
		$ventaBudget = number_format($ventaBudget, 0,"", ",");
		
		$gastoActual = empty($gasto[0])?0:$gasto[0]->total;
		$gastoBudget = empty($gasto_budget[0]->total)?1:$gasto_budget[0]->total;
		$alarmaGasto = $gastoActual/$gastoBudget*100;
		$gastoActual = number_format($gastoActual, 0,"", ",");
		$gastoBudget = number_format($gastoBudget, 0,"", ",");
		
		$costoActual = empty($costo[0])?0:$costo[0]->total;
		$costoBudget = empty($costo_budget[0]->total)?1:$costo_budget[0]->total;
		$alarmaCosto = $costoActual/$costoBudget*100;
		$costoActual = number_format($costoActual, 0,"", ",");
		$costoBudget = number_format($costoBudget, 0,"", ",");
		
		$unetaActual = number_format((empty($venta[0])?0:$venta[0]->total) - (empty($costo[0])?0:$costo[0]->total), 0,"", ",");
		$unetaBudget = number_format($venta_budget[0]->total - $costo_budget[0]->total, 0,"", ",");
		$alarmaUneta = $unetaActual/$unetaBudget*100;
		
		$ebidtaActual = number_format((empty($venta[0])?0:$venta[0]->total) - (empty($costo[0])?0:$costo[0]->total) - (empty($gasto[0])?0:$gasto[0]->total), 0,"", ",");
		$ebidtaBudget = number_format($venta_budget[0]->total - $costo_budget[0]->total - $gasto_budget[0]->total, 0,"", ",");
		$alarmaEbidta = $ebidtaActual/$ebidtaBudget*100;
		
		$sql = "SELECT finanzas_categoria_cuenta.categoria, SUM(monto) total FROM finanzas_gasto_dia_sap INNER JOIN finanzas_cuentas ON finanzas_cuentas.idCuenta = finanzas_gasto_dia_sap.idCuenta INNER JOIN finanzas_categoria_cuenta ON finanzas_categoria_cuenta.idCategoria = finanzas_cuentas.idCategoria  WHERE MONTH(fecha) = ".$mes." AND YEAR(fecha) = ".$anio." GROUP BY finanzas_categoria_cuenta.idCategoria, finanzas_categoria_cuenta.categoria;";
		$gasto_mes_det = DB::select($sql);
		$sql = "SELECT idSucursal, SUM(monto) total FROM finanzas_venta_dia_sap WHERE MONTH(fecha) = ".$mes." AND YEAR(fecha) = ".$anio." GROUP BY idSucursal;";
		$venta_mes_det = DB::select($sql);
		
		return view('dashboard.kpiFinanzas', ['mes' => date('F',  strtotime($anio."-".$mes."-01")), 'venta'=> $ventaActual, 'venta_budget'=> $ventaBudget, 'costo'=> $costoActual, 'costo_budget'=> $costoBudget, 'gasto'=> $gastoActual, 'gasto_budget'=> $gastoBudget, 'uneta' => $unetaActual, 'uneta_budget' => $unetaBudget, 'ebidta' => $ebidtaActual, 'ebidta_budget' => $ebidtaBudget, 'alarmaVenta' => $alarmaVenta, 'alarmaGasto' => $alarmaGasto, 'alarmaCosto' => $alarmaCosto, 'alarmaUneta' => $alarmaUneta, 'alarmaEbidta' => $alarmaEbidta, 'venta_mes_det' => $venta_mes_det, 'gasto_mes_det' => $gasto_mes_det]);
	}
	
	
	public function getDiariaPDF()
	{
		
		$mbox = imap_open("{box1332.bluehost.com:993/imap/ssl}INBOX", "reportes@prigo.com.mx", "S@pmic18!");
		
		$msgs = imap_search($mbox, 'UNSEEN SUBJECT "Resumen de Ventas "');
			
		//print_r($msgs);
		
		if(!empty($msgs))
		{
			foreach($msgs as $idmail)
			{
				//$idmail = $msgs[0];
				if(!empty($idmail))
				{
					$structure = imap_fetchstructure($mbox, $idmail);
					$attachments = array();
					
					if(isset($structure->parts) && count($structure->parts))
					{
						for($i = 0; $i < count($structure->parts); $i++)
						{
							
							$attachments[$i] = array(
								'is_attachment' => false,
								'filename' => '',
								'name' => '',
								'attachment' => ''
							);
							
							if($structure->parts[$i]->ifdparameters) 
							{
								foreach($structure->parts[$i]->dparameters as $object) 
								{
									if(strtolower($object->attribute) == 'filename') 
									{
										$attachments[$i]['is_attachment'] = true;
										$attachments[$i]['filename'] = $object->value;
									}
								}  
							}

							if($structure->parts[$i]->ifparameters) 
							{
								foreach($structure->parts[$i]->parameters as $object) 
								{
									if(strtolower($object->attribute) == 'name') 
									{
										$attachments[$i]['is_attachment'] = true;
										$attachments[$i]['name'] = $object->value;
									}
								}
							}

							if($attachments[$i]['is_attachment']) 
							{
								$attachments[$i]['attachment'] = imap_fetchbody($mbox, $idmail, $i+1);
								if($structure->parts[$i]->encoding == 3) 
								{ // 3 = BASE64
									$attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
								}
								elseif($structure->parts[$i]->encoding == 4) 
								{ // 4 = QUOTED-PRINTABLE
									$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
								}
							}
							
						}
					}
					
					$filePath = storage_path('app/public/temp/mailattachment')."/".$attachments[1]["filename"];
					
					if(file_put_contents($filePath, $attachments[1]['attachment']))
					{
						echo $filePath."<br>";
						$spreadsheet = IOFactory::load($filePath);
						$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
						
						$fecha = explode("/",$sheetData[2]["B"]);
						$strFecha = $fecha[2]."-".$fecha[0]."-".$fecha[1];
						$sucursal = DB::select("SELECT * FROM sucursales WHERE idMicros = ? ;",[$sheetData[3]["B"]]);
						
						if(!empty($sucursal))
						{
							
							$nrows = count($sheetData);
							echo $sucursal[0]->idSap."<br>";
							echo $sheetData[2]["B"]."<br>";
							echo $sheetData[3]["B"]."<br>";
							echo $sheetData[8]["C"]."<br>";
							$netSales = str_replace([",",")","("],["","","-"],$sheetData[8]["C"]);
							//echo $sheetData[9]["C"]."<br>";
							$serviceCharges = str_replace([",",")","("],["","","-"],$sheetData[9]["C"]);
							//echo $sheetData[10]["C"]."<br>";
							$taxCollected = str_replace([",",")","("],["","","-"],$sheetData[10]["C"]);
							//echo $sheetData[13]["C"]."<br>";
							$totalCollected = str_replace([",",")","("],["","","-"],$sheetData[13]["C"]);
							//echo $sheetData[17]["C"]."<br>";
							$totalDiscount = str_replace([",",")","("],["","","-"],$sheetData[17]["C"]);
							//echo $sheetData[20]["C"]."<br>";
							$returns = str_replace([",",")","("],["","","-"],$sheetData[20]["C"]);
							//echo $sheetData[21]["C"]."<br>";
							$voids = str_replace([",",")","("],["","","-"],$sheetData[21]["C"]);
							//echo $sheetData[25]["C"]."<br>";
							$mgrVoids = str_replace([",",")","("],["","","-"],$sheetData[25]["C"]);
							//echo $sheetData[26]["C"]."<br>";
							$errorCorrects = str_replace([",",")","("],["","","-"],$sheetData[26]["C"]);
							//echo $sheetData[27]["C"]."<br>";
							$cancels = str_replace([",",")","("],["","","-"],$sheetData[27]["C"]);
							echo "Cancels: ".$sheetData[27]["C"]."<br>";
							echo "Cancels: ".$cancels."<br>";
							//echo $sheetData[46]["C"]."<br>";
							$checks = str_replace([",",")","("],["","","-"],$sheetData[46]["B"]);
							//echo $sheetData[46]["C"]."<br>";
							$amountChecks = str_replace([",",")","("],["","","-"],$sheetData[46]["C"]);
							
							
							
							$iniOT=0;							
							$iniTP=0;
							$iniTT=0;
							$guests	=0;
							$avgCheck =0;
							$avgGuest =0;
							
							$rvcs = array();
							$daysp = array();
							$tendert = array();
							echo "rows: ".$nrows."<br>";
							for($i=63; $i<= $nrows; $i++)
							{
								if($sheetData[$i]["A"] == "Order Type")
									$iniOT = $i+1;
								if($sheetData[$i]["A"] == "Day Part")
									$iniTP = $i+1;
								if($sheetData[$i]["A"] == "Tender Type")
									$iniTT = $i+1;
								
								echo $sheetData[$i]["A"]."<br>";
								
								if($iniOT != 0 && $iniOT == $i && $sheetData[$i]["A"] != "Totals" )
								{								
									$rvcs[] = array($sheetData[$i]["A"], str_replace([",",")","("],["","","-"],$sheetData[$i]["B"]),str_replace([",",")","("],["","","-"],$sheetData[$i]["D"]),str_replace([",",")","("],["","","-"],$sheetData[$i]["F"]),str_replace([",",")","("],["","","-"],$sheetData[$i]["G"]),str_replace([",",")","("],["","","-"],$sheetData[$i]["I"]),str_replace([",",")","("],["","","-"],$sheetData[$i]["J"]),str_replace([",",")","("],["","","-"],$sheetData[$i]["L"]),str_replace([",",")","("],["","","-"],$sheetData[$i]["M"]));
									$iniOT++;
								}

								if($sheetData[$i]["A"] == "Totals" && $iniOT>0)
								{
									$guests = str_replace([",",")","("],["","","-"],$sheetData[$i]["G"]);
									$avgCheck = str_replace([",",")","("],["","","-"],$sheetData[$i]["I"]);
									$avgGuest = str_replace([",",")","("],["","","-"],$sheetData[$i]["F"]);
									
							/*		if($iniOT>0)
										echo "Termina RVC";*/
									
									$iniOT = 0;
								}
								
								if($iniTP != 0 && $iniTP == $i && $sheetData[$i]["A"] != "Totals" )
								{
									$daysp[] = array($sheetData[$i]["A"],str_replace([",",")","("],["","","-"],$sheetData[$i]["B"]),str_replace([",",")","("],["","","-"],$sheetData[$i]["D"]),str_replace([",",")","("],["","","-"],$sheetData[$i]["E"]),str_replace([",",")","("],["","","-"],$sheetData[$i]["F"]),str_replace([",",")","("],["","","-"],$sheetData[$i]["G"]));
									$iniTP++;
								}
								if($sheetData[$i]["A"] == "Totals" && $iniTP>0)
								{
									/*
									if($iniTP>0)
										echo "Termina Day Parts";
									*/
									$iniTP = 0;
									
								}
								
								if($iniTT != 0 && $iniTT == $i && $sheetData[$i]["A"] != "Totals" )
								{
									$tendert[] = array($sheetData[$i]["A"],str_replace([",",")","("],["","","-"],$sheetData[$i]["B"]), str_replace([",",")","("],["","","-"],$sheetData[$i]["D"]), str_replace([",",")","("],["","","-"],$sheetData[$i]["E"]));
									$iniTT++;
								}
								if($sheetData[$i]["A"] == "Totals" && $iniTT>0)
								{
									/*
									if($iniTT>0)
										echo "Termina Tender Type";
									*/
									$iniTT = 0;
									
								}
								
							}
							
							/*** DB Insert ***/
							DB::insert("INSERT INTO venta_diaria_sucursal VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[$sucursal[0]->id,$strFecha,$netSales,$taxCollected,$totalCollected,$totalDiscount,$returns,$voids,$mgrVoids,$errorCorrects,$cancels,$serviceCharges,$checks,$amountChecks,$guests,$avgGuest,$avgCheck]);
							
							foreach($rvcs AS $rvc)
							{
								DB::insert("INSERT INTO vds_rvc VALUES (?,?,?,?,?,?,?,?,?,?,?)",[$sucursal[0]->id,$strFecha,$rvc[0],$rvc[1],$rvc[2],$rvc[3],$rvc[4],$rvc[5],$rvc[6],$rvc[7],$rvc[8]]);
							}
							
							foreach($daysp AS $rvc)
							{
								DB::insert("INSERT INTO vds_dayp VALUES (?,?,?,?,?,?,?,?)",[$sucursal[0]->id,$strFecha,$rvc[0],$rvc[1],$rvc[2],$rvc[3],$rvc[4],$rvc[5]]);
							}
							
							foreach($tendert AS $rvc)
							{
								DB::insert("INSERT INTO vds_tender VALUES (?,?,?,?,?,?)",[$sucursal[0]->id,$strFecha,$rvc[0],$rvc[1],$rvc[2],$rvc[3]]);
							}
							
//							unlink($filePath);
							
						}
						else
						{
							echo $sheetData[3]["B"];
						}
						
					}
					else
					{
						echo "No File";
					}
					
				}
			}
		}
		
	}
	
	public function ventaDiariaPDF($idSucursal)
	{
		
		$styleArray = [
			'font' => [
				'bold' => true,
			],
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			],
			'fill' => [
				'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				'startColor' => [
					'argb' => 'FFC3C3C3',
				]
			],
		];	
		
		$styleArray2 = [
			'font' => [
				'bold' => true,
			],
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			],
		];
		
		$styleArrayGood = [
			'font' => [
				'bold' => true,
				'color' => ['argb' => 'FF00FF00']
			],
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			],
		];
		
		$styleArrayAlert = [
			'font' => [
				'bold' => true,
				'color' => ['argb' => 'FFFF0000']
			],
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			],
		];

		$styleDefaultBorder = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => ['argb' => 'FFFFFFFF'],
				],
			],
		];		
				
				$fechaActual = date("Y-m-d",strtotime('-1 day'));
				$fechaAnterior = date("Y-m-d",strtotime($fechaActual.' -1 year'));


				if($idSucursal == 0)
					$sucursales = DB::select("SELECT * FROM sucursales WHERE not(idSap IS null);");
				else
					$sucursales = DB::select("SELECT * FROM sucursales WHERE id = ?;",[$idSucursal]);
				
				$procesados = 0;
				
				foreach($sucursales AS $sucursal)
				{
					$datos = DB::select("SELECT T3.nombre ,T1.fecha, T2.fecha, T1.netSales AS ventaActual, T2.netSales AS ventaAnterior, T1.avgCheck AS chequeActual, T2.avgCheck chequeAnterior, T1.checks AS transActual , T2.checks AS transAnterior, T1.guests AS guestActual, T2.guests AS guestAnterior FROM venta_diaria_sucursal T1 INNER JOIN sucursales T3 ON T1.idSucursal = T3.id LEFT JOIN venta_diaria_sucursal T2 ON T1.idSucursal = T2.idSucursal WHERE T1.idSucursal= ? AND T1.fecha = ? AND (T2.fecha = ? OR T2.fecha IS null)",[$sucursal->id, $fechaActual, $fechaAnterior]);
					$rvcs = DB::select("SELECT T1.rvc, T1.netSales ventaActual, T2.netSales ventaAnterior, T1.guests guestActual, T2.guests guestAnterior, T1.avgCheck chequeActual, T2.avgCheck chequeAnterior FROM vds_rvc T1 LEFT JOIN vds_rvc T2 ON (T1.idSucursal = T2.idSucursal AND T1.rvc = T2.rvc) WHERE T1.idSucursal= ? AND T1.fecha =? AND (T2.fecha = ? OR T2.fecha IS NULL);",[$sucursal->id, $fechaActual, $fechaAnterior]);
					$days = DB::select("SELECT T1.dayPart , T1.netSales ventaActual, T2.netSales ventaAnterior, T1.guests guestActual, T2.guests guestAnterior, T1.avgCheck chequeActual, T2.avgCheck chequeAnterior FROM vds_dayp T1 LEFT JOIN vds_dayp T2 ON (T1.idSucursal = T2.idSucursal AND T1.dayPart = T2.dayPart) WHERE T1.idSucursal= ? AND T1.fecha =? AND (T2.fecha = ? OR T2.fecha IS NULL);",[$sucursal->id, $fechaActual, $fechaAnterior]);
					
					$i=0;
					
					if(!empty($datos))
					{
						$spreadsheet = new Spreadsheet();
						$spreadsheet->setActiveSheetIndex($i);
						$sugestedItems = $spreadsheet->getActiveSheet();
						
						$spreadsheet->getDefaultStyle()->getFont()->setSize(10);

						$spreadsheet->getActiveSheet()->getStyle('A1:J40')->applyFromArray($styleDefaultBorder);
						
						
						$spreadsheet->getActiveSheet()->getPageMargins()->setTop(0.5);
						$spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.15);
						$spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.15);
						$spreadsheet->getActiveSheet()->getPageMargins()->setBottom(0.5);


						$sugestedItems->setTitle('Daily Sales Summary');
						
						$spreadsheet->getActiveSheet()->mergeCells('A1:C1');
						$sugestedItems->setCellValue('A1', 'Daily Sales Summary');
						$spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
						
						$sugestedItems->setCellValue('A2', 'Fecha');
						$spreadsheet->getActiveSheet()->mergeCells('B2:C2');
						$sugestedItems->setCellValue('B2', $fechaActual);
						$sugestedItems->setCellValue('A3', 'Restaurante');
						$spreadsheet->getActiveSheet()->mergeCells('B3:C3');
						$sugestedItems->setCellValue('B3', $datos[0]->nombre);
						
						$spreadsheet->getActiveSheet()->mergeCells('A5:C5');
						$sugestedItems->setCellValue('A5', 'Venta');
						$spreadsheet->getActiveSheet()->getStyle('A5')->applyFromArray($styleArray);
						$sugestedItems->setCellValue('A6', 'Actual');	
						$sugestedItems->setCellValue('B6', 'Año Anterior');	
						$sugestedItems->setCellValue('C6', 'Diferencia');
						$spreadsheet->getActiveSheet()->getStyle('A6:C6')->applyFromArray($styleArray2);
						$sugestedItems->setCellValue('A7', number_format($datos[0]->ventaActual,2,'.',','));	
						$sugestedItems->setCellValue('B7', number_format($datos[0]->ventaAnterior,2,'.',','));	
						$difVenta = empty($datos[0]->ventaAnterior)?"No Disponible":($datos[0]->ventaActual*100/$datos[0]->ventaAnterior)-100;
						$sugestedItems->setCellValue('C7', number_format($difVenta,2,'.',',')."%");
						$style= $difVenta>0?$styleArrayGood :$styleArrayAlert ;
						$spreadsheet->getActiveSheet()->getStyle('C7')->applyFromArray($style);
						
						
						$spreadsheet->getActiveSheet()->mergeCells('A9:C9');
						$sugestedItems->setCellValue('A9', 'Cheque Promedio');
						$spreadsheet->getActiveSheet()->getStyle('A9')->applyFromArray($styleArray);
						$sugestedItems->setCellValue('A10', 'Actual');	
						$sugestedItems->setCellValue('B10', 'Año Anterior');	
						$sugestedItems->setCellValue('C10', 'Diferencia');
						$spreadsheet->getActiveSheet()->getStyle('A10:C10')->applyFromArray($styleArray2);					
						$sugestedItems->setCellValue('A11', $datos[0]->guestActual);	
						$sugestedItems->setCellValue('B11', $datos[0]->guestAnterior);	
						$difCheque = empty($datos[0]->guestAnterior)?"No Disponible":($datos[0]->guestActual*100/$datos[0]->guestAnterior)-100;
						$sugestedItems->setCellValue('C11', number_format($difCheque,2,'.',',')."%");
						$style= $difCheque>0?$styleArrayGood :$styleArrayAlert ;
						$spreadsheet->getActiveSheet()->getStyle('C11')->applyFromArray($style);
						
						$spreadsheet->getActiveSheet()->mergeCells('A13:C13');
						$sugestedItems->setCellValue('A13', 'Transacciones');
						$spreadsheet->getActiveSheet()->getStyle('A13')->applyFromArray($styleArray);
						$sugestedItems->setCellValue('A14', 'Actual');	
						$sugestedItems->setCellValue('B14', 'Año Anterior');	
						$sugestedItems->setCellValue('C14', 'Diferencia');
						$spreadsheet->getActiveSheet()->getStyle('A14:C14')->applyFromArray($styleArray2);
						$sugestedItems->setCellValue('A15', $datos[0]->transActual);	
						$sugestedItems->setCellValue('B15', $datos[0]->transAnterior);	
						$difTrans = empty($datos[0]->transAnterior)?"No Disponible":($datos[0]->transActual*100/$datos[0]->transAnterior)-100;
						$sugestedItems->setCellValue('C15', number_format($difTrans,2,'.',',')."%");
						$style= $difTrans>0?$styleArrayGood :$styleArrayAlert ;
						$spreadsheet->getActiveSheet()->getStyle('C15')->applyFromArray($style);
						
						$spreadsheet->getActiveSheet()->mergeCells('A17:C17');
						$sugestedItems->setCellValue('A17', 'Clientes');
						$spreadsheet->getActiveSheet()->getStyle('A17')->applyFromArray($styleArray);
						$sugestedItems->setCellValue('A18', 'Actual');	
						$sugestedItems->setCellValue('B18', 'Año Anterior');	
						$sugestedItems->setCellValue('C18', 'Diferencia');
						$spreadsheet->getActiveSheet()->getStyle('A18:C18')->applyFromArray($styleArray2);
						$sugestedItems->setCellValue('A19', '0');	
						$sugestedItems->setCellValue('B19', '0');	
						$sugestedItems->setCellValue('C19', '0');
						
						
						$spreadsheet->getActiveSheet()->mergeCells('A21:J21');
						$sugestedItems->setCellValue('A21', 'Detalle RVC');
						$spreadsheet->getActiveSheet()->getStyle('A21')->applyFromArray($styleArray);
						$spreadsheet->getActiveSheet()->mergeCells('B22:D22');
						$sugestedItems->setCellValue('B22', 'Venta');
						$spreadsheet->getActiveSheet()->getStyle('A22:J22')->applyFromArray($styleArray);
						$spreadsheet->getActiveSheet()->mergeCells('E22:G22');
						$sugestedItems->setCellValue('E22', 'Clientes');
						$spreadsheet->getActiveSheet()->mergeCells('H22:J22');
						$sugestedItems->setCellValue('H22', 'Cheque Promedio');
						$sugestedItems->setCellValue('A23', 'RVC');
						$sugestedItems->setCellValue('B23', 'Actual');
						$sugestedItems->setCellValue('C23', 'Anterior');
						$sugestedItems->setCellValue('D23', 'Diferencia');
						$sugestedItems->setCellValue('E23', 'Actual');
						$sugestedItems->setCellValue('F23', 'Anterior');
						$sugestedItems->setCellValue('G23', 'Diferencia');
						$sugestedItems->setCellValue('H23', 'Actual');
						$sugestedItems->setCellValue('I23', 'Anterior');
						$sugestedItems->setCellValue('J23', 'Diferencia');	
						$spreadsheet->getActiveSheet()->getStyle('A23:J23')->applyFromArray($styleArray2);	
						
						$i=24;
						
						$guestActualPrincipal = 0;
						$guestAnteriorPrincipal = 0;
						
						foreach($rvcs as $rvc)
						{
							$sugestedItems->setCellValue('A'.$i, $rvc->rvc);
							$sugestedItems->setCellValue('B'.$i, $rvc->ventaActual);
							$sugestedItems->setCellValue('C'.$i, $rvc->ventaAnterior);
							$difventarvc = empty($rvc->ventaAnterior)?0:($rvc->ventaActual*100/$rvc->ventaAnterior)-100;
							$sugestedItems->setCellValue('D'.$i, number_format($difventarvc,2,'.',',')."%");
							$style= $difventarvc>0?$styleArrayGood :$styleArrayAlert ;
							$spreadsheet->getActiveSheet()->getStyle('D'.$i)->applyFromArray($style);
							$sugestedItems->setCellValue('E'.$i, $rvc->guestActual);
							
							$guestActualPrincipal += $rvc->guestActual; 
							
							$sugestedItems->setCellValue('F'.$i, $rvc->guestAnterior);
							
							$guestAnteriorPrincipal += $rvc->guestAnterior;
							
							$difguestrvc = empty($rvc->guestAnterior)?0:($rvc->guestActual*100/$rvc->guestAnterior)-100;
							$sugestedItems->setCellValue('G'.$i, number_format($difguestrvc,2,'.',',')."%");
							$style= $difguestrvc>0?$styleArrayGood :$styleArrayAlert ;
							$spreadsheet->getActiveSheet()->getStyle('G'.$i)->applyFromArray($style);
							$sugestedItems->setCellValue('H'.$i, $rvc->chequeActual);
							$sugestedItems->setCellValue('I'.$i, $rvc->chequeAnterior);
							$difchequervc = empty($rvc->chequeAnterior)?0:($rvc->chequeActual*100/$rvc->chequeAnterior)-100;
							$sugestedItems->setCellValue('J'.$i, number_format($difchequervc,2,'.',',')."%");
							$style= $difchequervc>0?$styleArrayGood :$styleArrayAlert ;
							$spreadsheet->getActiveSheet()->getStyle('J'.$i)->applyFromArray($style);
							$i++;
						}
						
						$sugestedItems->setCellValue('A19', $guestActualPrincipal);	
						$sugestedItems->setCellValue('B19', $guestAnteriorPrincipal);	
						$difguestpr = empty($guestAnteriorPrincipal)?0:($guestActualPrincipal*100/$guestAnteriorPrincipal)-100;
						$sugestedItems->setCellValue('C19', number_format($difguestpr,2,'.',',')."%");
						$style= $difguestpr>0?$styleArrayGood :$styleArrayAlert ;
						$spreadsheet->getActiveSheet()->getStyle('C19')->applyFromArray($style);

						
						$spreadsheet->getActiveSheet()->mergeCells('A29:J29');
						$sugestedItems->setCellValue('A29', 'Detalle Day Part');
						$spreadsheet->getActiveSheet()->getStyle('A29')->applyFromArray($styleArray);
						$spreadsheet->getActiveSheet()->mergeCells('B30:D30');
						$sugestedItems->setCellValue('B30', 'Venta');
						$spreadsheet->getActiveSheet()->getStyle('A30:J30')->applyFromArray($styleArray);
						$spreadsheet->getActiveSheet()->mergeCells('E30:G30');
						$sugestedItems->setCellValue('E30', 'Clientes');
						$spreadsheet->getActiveSheet()->mergeCells('H30:J30');
						$sugestedItems->setCellValue('H30', 'Cheque Promedio');				
						$sugestedItems->setCellValue('A31', 'Time');
						$sugestedItems->setCellValue('B31', 'Actual');
						$sugestedItems->setCellValue('C31', 'Anterior');
						$sugestedItems->setCellValue('D31', 'Diferencia');
						$sugestedItems->setCellValue('E31', 'Actual');
						$sugestedItems->setCellValue('F31', 'Anterior');
						$sugestedItems->setCellValue('G31', 'Diferencia');
						$sugestedItems->setCellValue('H31', 'Actual');
						$sugestedItems->setCellValue('I31', 'Anterior');
						$sugestedItems->setCellValue('J31', 'Diferencia');	
						$spreadsheet->getActiveSheet()->getStyle('A31:J31')->applyFromArray($styleArray2);				

						$i=32;
						
						foreach($days as $day)
						{
							$sugestedItems->setCellValue('A'.$i, $day->dayPart);
							$sugestedItems->setCellValue('B'.$i, $day->ventaActual);
							$sugestedItems->setCellValue('C'.$i, $day->ventaAnterior);
							$difventarvc = empty($day->ventaAnterior)?0:($day->ventaActual*100/$day->ventaAnterior)-100;
							$sugestedItems->setCellValue('D'.$i, number_format($difventarvc,2,'.',',')."%");
							$style= $difventarvc>0?$styleArrayGood :$styleArrayAlert ;
							$spreadsheet->getActiveSheet()->getStyle('D'.$i)->applyFromArray($style);
							$sugestedItems->setCellValue('E'.$i, $day->guestActual);
							$sugestedItems->setCellValue('F'.$i, $day->guestAnterior);
							$difguestrvc = empty($day->guestAnterior)?0:($day->guestActual*100/$day->guestAnterior)-100;
							$sugestedItems->setCellValue('G'.$i, number_format($difguestrvc,2,'.',',')."%");
							$style= $difguestrvc>0?$styleArrayGood :$styleArrayAlert ;
							$spreadsheet->getActiveSheet()->getStyle('G'.$i)->applyFromArray($style);						
							$sugestedItems->setCellValue('H'.$i, $day->chequeActual);
							$sugestedItems->setCellValue('I'.$i, $day->chequeAnterior);
							$difchequervc = empty($day->chequeAnterior)?0:($day->chequeActual*100/$day->chequeAnterior)-100;
							$sugestedItems->setCellValue('J'.$i, number_format($difchequervc,2,'.',',')."%");
							$style= $difchequervc>0?$styleArrayGood :$styleArrayAlert ;
							$spreadsheet->getActiveSheet()->getStyle('J'.$i)->applyFromArray($style);
							$i++;
						}						
						echo asset(storage_path('app/public/temp')."/DSS_".str_replace(" ","_",$datos[0]->nombre)."_".(date("Y-m-d")).".pdf");
						
						$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Dompdf');
						$writer->save(storage_path('app/public/temp')."/DSS_".str_replace(" ","_",$datos[0]->nombre)."_".(date("Y-m-d")).".pdf");
						
						
						$i++;
						$procesados = 1;
			}
			else
			{
				echo "$fechaActual - $fechaAnterior <br>";
			}
		}
		
		if($procesados>0)
		{		

/*	
				$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment; filename="DSS_ALL_'.date("Ymd").'.xlsx"');
				$writer->save("php://output");
*/
			//$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Dompdf');
			//$writer->writeAllSheets();
	/*		header('Content-Type: application/pdf');	
			header('Content-Disposition: attachment; filename="plantilla_'.date("Ymd").'.pdf"');*/
			//$writer->writeAllSheets();
			//$writer->save(storage_path('app/public/temp')."/DSS_".(date("Y-m-d")).".pdf");
			//$writer->save("php://output");
			//_".str_replace(" ","_",$datos[0]->nombre)."
		}
	}

	public function getInventory()
	{
		
		$fecha = date("Y-m-d",strtotime(date("Y-m-d")." -".date("d")." days"));
		$mes = date("m");
		$anio = date("Y");
		echo $fecha."<br><br>";
		$sql = "SELECT OITM.ItemCode, OITM.ItemName, '$fecha' fecha, WhsCode,INVENTARIO.OnHand, OITM.BuyUnitMsr, OITM.NumInBuy FROM (SELECT Warehouse WhsCode,ItemCode, SUM(InQty - OutQty) OnHand FROM OINM WHERE DocDate < '".$anio."-".$mes."-01' GROUP BY Warehouse ,ItemCode) INVENTARIO INNER JOIN OITM ON OITM.ItemCode = INVENTARIO.ItemCode LEFT JOIN OITT ON OITT.Code = OITM.ItemCode;";
		$items = DB::connection('sqlsrvkay')->select($sql);
		
		echo "$sql <br><br>";
		$cont = 0;
		$sql ="";
		foreach($items as $item)
		{
			if($cont == 100)
			{
				DB::select("REPLACE INTO inventario_mensual VALUES $sql;");
				echo "REPLACE INTO inventario_mensual VALUES $sql; <br><br>";
				$cont = 0;
				$sql = "";
			}
			
			if(!empty($sql))
				$sql .= ", ";
			
			$sql .= "( '".$item->ItemCode."', '".str_replace("'","",$item->ItemName)."', '".$item->fecha."', '".$item->WhsCode."', '".$item->OnHand."', '".$item->BuyUnitMsr."', '".$item->NumInBuy."' )";
			
			$cont++;
		}
	}
	
}