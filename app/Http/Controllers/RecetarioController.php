<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use League\Event\Emitter;
use PhpParser\Node\Stmt\Echo_;
use PhpParser\Node\Stmt\Foreach_;

class RecetarioController extends Controller
{
	
    public function __construct()
    {
       $this->middleware('auth');
    }
	public function index()
    {
        return view('recetario.index', []);
    }
    
    public function recetas()
    {
        return view('recetario.recetas', []);
    }
	public function newRecipe()
	{
		return view('recetario.nueva', []);
	}
    public function localizar()
    {
        return view('recetario.localizar', []);
	}
	public function autorizar(){
		return view('recetario.autorizar', []);
	}

	public function getIngredienteFormat(Request $request)
	{
		DB::enableQueryLog();
		$q = $request->input('q');

		$sql = "SELECT 1 tipo, idPlatillo AS id, nombre, unidad, costo precio FROM recetas_platillo WHERE estado = 1 AND nombre LIKE '%".$q."%' UNION ALL SELECT 2 tipo, idSAP AS id, nombre, unidadSAP AS unidad, precio.precio FROM recetas_ingrediente ingrediente LEFT JOIN ( SELECT idItem, AVG(precioPromedio) precio FROM dashboard_precio_promedio WHERE anio=".date("Y")." GROUP BY idItem,anio ) precio ON precio.idItem = ingrediente.idSap WHERE estado = 1  AND nombre LIKE '%".$q."%';";

		$items = DB::select($sql);
		//dd(DB::getQueryLog());
		return  response()->json([
			'total_count' => count($items),
			'items' => $items,
			'page' => 1,
			'q' => $q,
			'sql' => $sql
		]);
	}

    public function findIngrediente(Request $request)
    {
		$recetas= DB::select("SELECT recetas_platillo.idPlatillo AS irec, recetas_platillo.nombre receta, recetas_platillo.unidad ureceta, recetas_platillo.cantidad creceta, recetas_platillo_ingrediente.unidad uing, recetas_platillo_ingrediente.cantidad cing FROM recetas_platillo_ingrediente INNER JOIN recetas_platillo ON recetas_platillo_ingrediente.idPlatillo = recetas_platillo.idPlatillo WHERE recetas_platillo_ingrediente.idIngrediente = ?",  [$request->input('ingrediente')]);

        if(!empty($recetas[0]))
		{
			return  response()->json([
			'data' => $recetas
			]);
		}
		else
		{
			return  response()->json([
			'tipo' => 0,
			'data' => []
			]);
		}
    }

    public function getIngrediente(Request $request)
    {
        DB::enableQueryLog();
		$q = $request->input('q');
		
		$items = DB::table('recetas_ingrediente')	
		->select('idIngrediente AS id','idSap AS cod','nombre')
		->where('nombre', 'LIKE', '%'.$q. '%')
		->get();
		
		return  response()->json([
			'total_count' => count($items),
			'items' => $items,
			'page' => 1
		]);
    }
    public function getRecetasPendientes(Request $request)
    {
		$idUsuario = Auth::id();
		$draw = !empty($request->input('draw'))?$request->input('draw'):1;
		$start = !empty($request->input('start'))?$request->input('start'):0;
		$length = !empty($request->input('length'))?$request->input('length'):10;
		$queryarr = !empty($request->input('search'))?$request->input('search'):array();
		$query = !empty($queryarr["value"]) ? $queryarr["value"] : "" ;
		$allitems = null;
        $strBusca =  "";
        $strOrdena =  "";
		
		if(!empty($request->input('columns')))
		{
			$cols = $request->input('columns');
			$busca = array();
			foreach($cols as $col)
			{
				if(!empty($col["search"]["value"]))
				{
					switch($col["data"]){
						case "receta":
							$busca[] = " recetas_platillo.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
					}
				}
			}
			if(!empty($busca))
			{
				$strBusca = " AND ".implode(" AND ", $busca); 
			}
			
        }
		if(!empty($request->input('order')))
		{
            $orderby = $request->input('order');
            $ordena = array();
            $cols = array("receta","unidad","cantidad","costo","fechaModifica","estado.estado");
            foreach($orderby as $ord)
			{
                if(!empty($ord["column"]))
                    $ordena[] = " " . $cols[$ord["column"]] ." ". (!empty($ord["dir"]) ?$ord["dir"]: " ASC " );
                    
            }
			if(!empty($ordena))
			{
				$strOrdena = " ORDER BY ".implode(" , ", $ordena); 
			}
        }
		if(!empty($strBusca))
		{
			$allitems = DB::select("SELECT idPlatillo AS idReceta FROM recetas_platillo WHERE recetas_platillo.estado = 2 ".$strBusca.";");
			$items = DB::select("SELECT idPlatillo AS idReceta, nombre AS receta, unidad, cantidad, ROUND(costo, 2) costo, fechaCrea, fechaModifica, estado.estado FROM recetas_platillo INNER JOIN recetas_platillo_estado estado ON estado.idEstadoPlatillo = recetas_platillo.estado WHERE recetas_platillo.estado = 2 ".$strBusca." $strOrdena LIMIT ".$start.", ".$length.";");
		}
		else
		{
            $allitems = DB::select("SELECT idPlatillo AS idReceta FROM recetas_platillo WHERE recetas_platillo.estado = 2 ;");
			$items = DB::select("SELECT idPlatillo AS idReceta, nombre AS receta, unidad, cantidad, ROUND(costo, 2) costo, fechaCrea, fechaModifica, estado.estado FROM recetas_platillo INNER JOIN recetas_platillo_estado estado ON estado.idEstadoPlatillo = recetas_platillo.estado WHERE recetas_platillo.estado = 2 $strOrdena LIMIT ".$start.", ".$length.";");
		}

		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allitems),
			'recordsFiltered' => count($allitems),
			'data' => $items
		]);
	}
	
    public function getRecetas(Request $request)
    {
		$idUsuario = Auth::id();
		$draw = !empty($request->input('draw'))?$request->input('draw'):1;
		$start = !empty($request->input('start'))?$request->input('start'):0;
		$length = !empty($request->input('length'))?$request->input('length'):10;
		$queryarr = !empty($request->input('search'))?$request->input('search'):array();
		$query = !empty($queryarr["value"]) ? $queryarr["value"] : "" ;
		$allitems = null;
        $strBusca =  "";
        $strOrdena =  "";
		
		if(!empty($request->input('columns')))
		{
			$cols = $request->input('columns');
			$busca = array();
			foreach($cols as $col)
			{
				if(!empty($col["search"]["value"]))
				{
					switch($col["data"]){
						case "receta":
							$busca[] = " recetas_platillo.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
					}
				}
			}
			if(!empty($busca))
			{
				$strBusca = " AND ".implode(" AND ", $busca); 
			}
			
        }
		if(!empty($request->input('order')))
		{
            $orderby = $request->input('order');
            $ordena = array();
            $cols = array("receta","unidad","cantidad","costo","fechaModifica","estado.estado");
            foreach($orderby as $ord)
			{
                if(!empty($ord["column"]))
                    $ordena[] = " " . $cols[$ord["column"]] ." ". (!empty($ord["dir"]) ?$ord["dir"]: " ASC " );
                    
            }
			if(!empty($ordena))
			{
				$strOrdena = " ORDER BY ".implode(" , ", $ordena); 
			}
        }
		if(!empty($strBusca))
		{
			$allitems = DB::select("SELECT idPlatillo AS idReceta FROM recetas_platillo WHERE recetas_platillo.estado = 1 ".$strBusca.";");
			$items = DB::select("SELECT idPlatillo AS idReceta, nombre AS receta, unidad, cantidad, ROUND(costo, 2) costo, fechaCrea, fechaModifica, estado.estado FROM recetas_platillo INNER JOIN recetas_platillo_estado estado ON estado.idEstadoPlatillo = recetas_platillo.estado WHERE recetas_platillo.estado = 1 ".$strBusca." $strOrdena LIMIT ".$start.", ".$length.";");
		}
		else
		{
            $allitems = DB::select("SELECT idPlatillo AS idReceta FROM recetas_platillo WHERE recetas_platillo.estado = 1 ;");
			$items = DB::select("SELECT idPlatillo AS idReceta, nombre AS receta, unidad, cantidad, ROUND(costo, 2) costo, fechaCrea, fechaModifica, estado.estado FROM recetas_platillo INNER JOIN recetas_platillo_estado estado ON estado.idEstadoPlatillo = recetas_platillo.estado WHERE recetas_platillo.estado = 1 $strOrdena LIMIT ".$start.", ".$length.";");
		}

		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allitems),
			'recordsFiltered' => count($allitems),
			'data' => $items
		]);
	}
	
	public function getDetallesRecetasPendientes($id){
		 $receta = DB::select("SELECT * FROM recetas_platillo WHERE idPlatillo =  '".$id."'");
		 $ingredientes = DB::select("SELECT * FROM recetas_platillo_ingrediente WHERE idPlatillo =  '".$id."'");
		 return view('recetario.detallesPendientes', ['ingredientes' => $ingredientes, 'receta' => $receta]);
		// return $receta;
	}

	public function EliminarRecetaPediente($id){
		$delete_receta = DB::delete('delete from recetas_platillo where idPlatillo = :idPlatillo', ['idPlatillo' => $id]);
		$delete_ingredientes = DB::delete('delete from recetas_platillo_ingrediente where idPlatillo = :idPlatillo', ['idPlatillo' => $id]);
		return view('recetario.autorizar', ['delete_receta' => $delete_receta, 'delete_ingredientes' => $delete_ingredientes]);
	
	}
    
    public function detalleReceta($id)
	{
		$receta = DB::select("SELECT * FROM recetas_platillo WHERE idPlatillo =  '".$id."'");
		$ingredientes = DB::select("SELECT * FROM recetas_platillo_ingrediente WHERE idPlatillo =  '".$id."'");
		return view('recetario.detalle', ['ingredientes' => $ingredientes, 'receta' => $receta]);
	
	}

	public function actualizarReceta(Request $request){
		$json_response = response()->json($request);
		$content = $json_response->getContent();

		return $json_response;
		// $json = json_decode($content, true);
		// $json_strings = (json_decode($json_response->getContent()));
		// $idPlatillo = $json_strings->idPlatillo;
		// $nombre = $json_strings->nombre_receta;
		// $unidad = $json_strings->unidad_receta;
		// $costo = $json_strings->precio_receta;
		// $estado = $json_strings->estado; 
		// $cantidad = $json_strings->cantidad_receta;
		// $tipo_receta = $json_strings->tipo_receta;
		// $subreceta = $json_strings->subreceta;
		// $fecha_actualizacion = date("Y-m-d");
		// $hora_modificacion = date("H:i:s");	
		// $ids = $json['id'];
		// $nombres = $json['nombre_ingrediente'];
		// $unidades = $json['unidad_ingre'];
		// $cantidades_s = $json['cantidadSucia'];
		// $cantidades_l = $json['cantidadLimpia'];
		// $precios = $json['costo'];
		// $i = 0;	
		// $array_id = array();
		// $array_nombres = array();
		// $array_unidades = array();
		// $array_cant_sucias = array();
		// $array_cant_limpias = array();
		// $array_costos = array();

		// if(!empty($idPlatillo)){
		// 	$updateReceta = DB::update('update recetas_platillo set nombre =?, unidad =?, cantidad =?, costo =?, fechaModifica =?, horaModifica =?, estado=?, tipoReceta =? where idPlatillo =?', [$nombre,$unidad,$cantidad,$costo,$fecha_actualizacion, $hora_modificacion,$estado,$tipo_receta,$idPlatillo]);
	
		// }	

		// if(!empty($updateReceta)){
		// 		foreach($ids as $id_json => $value_id){ $i++; array_push($array_id,$value_id);}
		// 		foreach($nombres as $id_json => $value_nom){ $i++; array_push($array_nombres,$value_nom);}
		// 		foreach($unidades as $unidad_json => $value_unidad){ array_push($array_unidades,$value_unidad);}
		// 		foreach($cantidades_s as $cantidadSucia_json=>$value_cs){array_push($array_cant_sucias ,$value_cs); }
		// 		foreach($cantidades_l as $cantidadLimpia_json=>$value_cl){array_push($array_cant_limpias ,$value_cl);}
		// 		foreach($precios as $precios_json=>$value_precio){array_push($array_costos ,$value_precio);}
		
		// 	}

		// $ids_ingredientes_ex = DB::delete('delete from recetas_platillo_ingrediente where idPlatillo = ?', [$idPlatillo]);
		
		// for($x=0;$x<count($array_id);$x++){
		// 	$sql2 = DB::table('recetas_platillo_ingrediente')->insert(['idPlatillo'=> $idPlatillo,'idIngrediente'=>$array_id[$x],'subReceta'=>$subreceta,'nombre'=>$array_nombres[$x],'unidad'=>$array_unidades[$x], 'cantidadSucia'=>$array_cant_sucias[$x], 'cantidadLimpia'=>$array_cant_limpias[$x], 'fechaCrea' => $fecha_actualizacion,'horaCrea' =>$hora_modificacion,'estado'=> $estado, 'costoInge' => $array_costos[$x]]);			
		// }

		
	}

	public function guardarReceta(Request $request){
		$json_response = response()->json($request);
		$content = $json_response->getContent();
		$json = json_decode($content, true);
		$json_strings = (json_decode($json_response->getContent()));
		$nombre = $json_strings->nombre_receta;
		$unidad = $json_strings->unidad_receta;
		$costo = $json_strings->precio_receta;
		$estado = "2"; //ESTADO DE LA RECETA SIEMPRE PENDIENTE CAMBIAR CUANDO HAYA REVISIÓN //
		$cantidad = $json_strings->cantidad_receta;
		$tipo_receta = $json_strings->tipo_receta;
		$fecha_creacion = date("Y-m-d");
		$fecha_actualizacion = date("Y-m-d");
		$hora_creacion = date("H:i:s");
		$hora_modificacion = date("H:i:s");	
		$subreceta = $json_strings->subreceta;
		$ids = $json['id'];
		$nombres = $json['nombre_ingrediente'];
		$unidades = $json['unidad_ingre'];
		$cantidades_s = $json['cantidadSucia'];
		$cantidades_l = $json['cantidadLimpia'];
		$precios = $json['costo'];
		$i = 0;	
		$array_id = array();
		$array_nombres = array();
		$array_unidades = array();
		$array_cant_sucias = array();
		$array_cant_limpias = array();
		$array_costos = array();

		if(!empty($nombre)){
			$receta_existente = DB::table('recetas_platillo')->select('nombre', 'idPlatillo')->where('nombre', '=', $nombre)->first();
		}
		if(!empty($receta_existente)){
			$n = "-N/V";
			if(!empty($nombre)&&!empty($unidad)&&!empty($costo)&&!empty($estado)&&!empty($tipo_receta &&!empty($fecha_creacion))){
			$sql_response = DB::table('recetas_platillo')->insertGetId(['nombre'=> $nombre.''.$n, 'unidad'=> $unidad,'cantidad'=> $cantidad ,'costo'=>$costo,'fechaCrea' => $fecha_creacion,'horaCrea' =>$hora_creacion, 'fechaModifica'=> $fecha_actualizacion, 'horaModifica'=> $hora_modificacion, 'estado'=> $estado,'tipoReceta' => $tipo_receta]);
			}

			if(!empty($sql_response)){
				foreach($ids as $id_json => $value_id){ $i++; array_push($array_id,$value_id);}
				foreach($nombres as $nombre_json => $value_nom){ $i++; array_push($array_nombres,$value_nom);}
				foreach($unidades as $unidad_json => $value_unidad){ array_push($array_unidades,$value_unidad);}
				foreach($cantidades_s as $cantidadSucia_json=>$value_cs){array_push($array_cant_sucias ,$value_cs); }
				foreach($cantidades_l as $cantidadLimpia_json=>$value_cl){array_push($array_cant_limpias ,$value_cl);}
				foreach($precios as $precios_json=>$value_precio){array_push($array_costos ,$value_precio);}

				for($x=0;$x<count($array_id);$x++){
					$sql = DB::table('recetas_platillo_ingrediente')->insert(['idPlatillo'=> $sql_response,'idIngrediente'=>$array_id[$x],'subReceta'=>$subreceta,'nombre'=>$array_nombres[$x],'unidad'=>$array_unidades[$x], 'cantidadSucia'=>$array_cant_sucias[$x], 'cantidadLimpia'=>$array_cant_limpias[$x], 'fechaCrea' => $fecha_creacion,'horaCrea' =>$hora_creacion,'estado'=> $estado, 'costoInge' => $array_costos[$x]]);							
				}
			
			}
		}else{
			if(!empty($nombre)&&!empty($unidad)&&!empty($costo)&&!empty($estado)&&!empty($tipo_receta &&!empty($fecha_creacion))){
				$sql_response = DB::table('recetas_platillo')->insertGetId(['nombre'=> $nombre, 'unidad'=> $unidad,'cantidad'=> $cantidad ,'costo'=>$costo,'fechaCrea' => $fecha_creacion,'horaCrea' =>$hora_creacion, 'fechaModifica'=> $fecha_actualizacion, 'horaModifica'=> $hora_modificacion, 'estado'=> $estado,'tipoReceta' => $tipo_receta]);
			}
			if(!empty($sql_response)){
				foreach($ids as $id_json => $value_id){ $i++; array_push($array_id,$value_id);}
				foreach($nombres as $id_json => $value_nom){ $i++; array_push($array_nombres,$value_nom);}
				foreach($unidades as $unidad_json => $value_unidad){ array_push($array_unidades,$value_unidad);}
				foreach($cantidades_s as $cantidadSucia_json=>$value_cs){array_push($array_cant_sucias ,$value_cs); }
				foreach($cantidades_l as $cantidadLimpia_json=>$value_cl){array_push($array_cant_limpias ,$value_cl);}
				foreach($precios as $precios_json=>$value_precio){array_push($array_costos ,$value_precio);}

				for($x=0;$x<count($array_id);$x++){
					$sql = DB::table('recetas_platillo_ingrediente')->insert(['idPlatillo'=> $sql_response,'idIngrediente'=>$array_id[$x],'subReceta'=>$subreceta,'nombre'=>$array_nombres[$x],'unidad'=>$array_unidades[$x], 'cantidadSucia'=>$array_cant_sucias[$x], 'cantidadLimpia'=>$array_cant_limpias[$x], 'fechaCrea' => $fecha_creacion,'horaCrea' =>$hora_creacion,'estado'=> $estado, 'costoInge' => $array_costos[$x]]);	
				}
			
			}

		}
	
	}

}
	
				
				
	
		
	
				
	
			
		