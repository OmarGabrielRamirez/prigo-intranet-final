<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SesionesController extends Controller
{
    public function __construct()
    {
       $this->middleware('guest');
    }

    public function guardarSesion(Request $request){
		$json_response = response()->json($request);
      $json_strings = (json_decode($json_response->getContent()));
      $email = $json_strings->email_sesion;
      $fecha = date("Y-m-d H:i:s");
      $busqueda = DB::table('users')->select('name')->where('email', '=', $email)->first();
      
      if($busqueda != null){
         foreach($busqueda as $b){
            if($b != null){
               $sql = DB::table('sesiones')->insert(['nombre'=> $b, 'fechaInicio' => $fecha]); 
               dd($sql);
            }
         }
		
	}

}

}