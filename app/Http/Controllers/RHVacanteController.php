<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class RHVacanteController extends Controller
{
    public function __construct()
    {
		$this->middleware('auth', ['except' => ['sendAuths','autRequest']]);
        $this->middleware(function ($request, $next) {
			$idUsuario = Auth::id();
			$user = Auth::user();
			$sql = "SELECT * FROM config_app_access WHERE idUsuario = ? AND idAplicacion = 3; ";
			$accesQuery = DB::select($sql, [$idUsuario]);
			if(!empty($accesQuery))
			{
				session(['RHRole' => $accesQuery[0]->idRole]);
			   if($accesQuery[0]->idRole != 1)
			   {
					$sql = "SELECT group_concat(`idSucursal` separator ',') as `sucursales` FROM rh_sucursal_usuario WHERE idUsuario = ? GROUP BY idUsuario;";
					$sucursales = DB::select($sql, [$idUsuario]);
					if(!empty($sucursales))
					{
						session(['sucursales' => $sucursales[0]->sucursales]);
					}
			   }
			}

            return $next($request);
        });

    }
	public function index()
    {		
		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		if($RHRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) "; 

		$sql = "SELECT SUM(autorizado.total)autorizados, SUM(empleados) empleados, SUM(autorizado.total) -SUM(empleados) vacantes, SUM(solicitudes.total) solicitudes, SUM(solicitudes.atraso) solicitudes_atraso, SUM(solicitudes.bien) solicitudes_bien FROM (SELECT 'vacante' tipo,of.nombre oficina, plazas.* FROM (SELECT idSucursal, SUM(cantidad) total FROM rh_plazas_autorizadas ".( $RHRole != 1 ? " WHERE idSucursal ".$strValSucursales : ""  )." GROUP BY idSucursal)plazas INNER JOIN rh_oficinas of ON of.id = plazas.idSucursal) autorizado LEFT JOIN (SELECT idSucursal, COUNT(estado) empleados FROM rh_empleado WHERE estado = 1 GROUP BY idSucursal) empleados ON autorizado.idSucursal = empleados.idSucursal LEFT JOIN (SELECT idSucursal, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM (SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF(NOW() > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (1,2,3)) data GROUP BY idSucursal,estado)solicitudes ON solicitudes.idSucursal = autorizado.idSucursal GROUP BY tipo;";
		$vacantes= DB::select($sql);
		$actuales = empty($vacantes[0]->empleados)?0:$vacantes[0]->empleados;
		$autorizados = empty($vacantes[0]->autorizados)?0:$vacantes[0]->autorizados;
		$diferencia = empty($vacantes[0]->vacantes)?0:$vacantes[0]->vacantes;
		$abiertas =empty($vacantes[0]->solicitudes)?0:$vacantes[0]->solicitudes;
		$retrasadas = empty($vacantes[0]->solicitudes_atraso)?0:$vacantes[0]->solicitudes_atraso;
		$entiempo = empty($vacantes[0]->solicitudes_bien)?0:$vacantes[0]->solicitudes_bien;
		
		$sql = "SELECT COUNT(estado) total FROM rh_vacante_solicitud_partida WHERE (MONTH(lastUpdateDate) = ".date("m")." AND YEAR(lastUpdateDate) = ".date("Y").") AND estado IN (4) ".( $RHRole != 1 ? " AND idSucursal ".$strValSucursales : ""  )." ;";
		$cerradas = DB::select($sql);
		
		
		$sql = "SELECT SUM(total) total, SUM(atraso) atraso, SUM(bien)bien , SUM(atraso)/SUM(total)*100 peratraso, SUM(bien)/SUM(total)*100 perbien   FROM (SELECT 2018 anio,idSucursal, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM (SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF(NOW() > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (1,2,3)) data GROUP BY idSucursal,estado UNION ALL SELECT 2018 anio,idSucursal, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM (SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF( partida.lastUpdateDate > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (4)) data GROUP BY idSucursal,estado) solicitudes GROUP BY anio;";
		$efectividad = DB::select($sql);

		//$sql = "SELECT base.*, actual.perbien actual, actual.bien bienActual, actual.atraso atrasoActual, anterior.perbien anterior FROM (SELECT solicitudes.idReclutador, rh_reclutador.nombre ,SUM(total) total, SUM(atraso) atraso, SUM(bien)bien , SUM(atraso)/SUM(total)*100 peratraso, SUM(bien)/SUM(total)*100 perbien FROM ( SELECT 2018 anio,idReclutador, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM (	SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF(NOW() > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (1,2,3) ) data INNER JOIN rh_reclutador_sucursal recsuc ON data.idSucursal = recsuc.idSucursal GROUP BY idReclutador,estado UNION ALL SELECT 2018 anio,idReclutador, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM ( SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF( partida.lastUpdateDate > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (4)	) data INNER JOIN rh_reclutador_sucursal recsuc ON data.idSucursal = recsuc.idSucursal GROUP BY idReclutador,estado) solicitudes INNER JOIN rh_reclutador ON solicitudes.idReclutador = rh_reclutador.idReclutador GROUP BY anio,solicitudes.idReclutador,rh_reclutador.nombre ) base LEFT JOIN (SELECT solicitudes.idReclutador,SUM(total) total, SUM(atraso) atraso, SUM(bien)bien , SUM(atraso)/SUM(total)*100 peratraso, SUM(bien)/SUM(total)*100 perbien FROM ( SELECT 2018 anio,idReclutador, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM (	SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF(NOW() > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (1,2,3) AND MONTH(sol.fechaCrea) = ".date("m")." ) data INNER JOIN rh_reclutador_sucursal recsuc ON data.idSucursal = recsuc.idSucursal GROUP BY idReclutador,estado UNION ALL SELECT 2018 anio,idReclutador, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM ( SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF( partida.lastUpdateDate > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (4) AND MONTH(sol.fechaCrea) = ".date("m")."	) data INNER JOIN rh_reclutador_sucursal recsuc ON data.idSucursal = recsuc.idSucursal GROUP BY idReclutador,estado) solicitudes INNER JOIN rh_reclutador ON solicitudes.idReclutador = rh_reclutador.idReclutador GROUP BY anio,solicitudes.idReclutador,rh_reclutador.nombre) actual ON actual.idReclutador = base.idReclutador LEFT JOIN (SELECT solicitudes.idReclutador,SUM(total) total, SUM(atraso) atraso, SUM(bien)bien , SUM(atraso)/SUM(total)*100 peratraso, SUM(bien)/SUM(total)*100 perbien FROM ( SELECT 2018 anio,idReclutador, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM (	SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF(NOW() > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (1,2,3) AND MONTH(sol.fechaCrea) = ".(date("m")-1)." ) data INNER JOIN rh_reclutador_sucursal recsuc ON data.idSucursal = recsuc.idSucursal GROUP BY idReclutador,estado UNION ALL SELECT 2018 anio,idReclutador, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM ( SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF( partida.lastUpdateDate > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (4) AND MONTH(sol.fechaCrea) = ".(date("m")-1)."	) data INNER JOIN rh_reclutador_sucursal recsuc ON data.idSucursal = recsuc.idSucursal GROUP BY idReclutador,estado) solicitudes INNER JOIN rh_reclutador ON solicitudes.idReclutador = rh_reclutador.idReclutador GROUP BY anio,solicitudes.idReclutador,rh_reclutador.nombre) anterior ON anterior.idReclutador = base.idReclutador ORDER BY base.perbien DESC;";
		$sql = "SELECT 	base.*, actual.perbien actual, actual.bien bienActual, actual.atraso atrasoActual, anterior.perbien anterior FROM 	( 		SELECT solicitudes.idReclutador, users.name nombre ,SUM(total) total, SUM(atraso) atraso, SUM(bien)bien , SUM(atraso)/SUM(total)*100 peratraso, SUM(bien)/SUM(total)*100 perbien FROM 		( 			SELECT 2018 anio, recsuc.idUsuario idReclutador, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM (	SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF(NOW() > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (1,2,3) ) data 			INNER JOIN rh_sucursal_usuario recsuc ON data.idSucursal = recsuc.idSucursal INNER JOIN config_app_access ON (recsuc.idUsuario = config_app_access.idUsuario AND config_app_access.idRole = 2  AND config_app_access.idAplicacion = 3)  GROUP BY recsuc.idUsuario,estado 			UNION ALL 			SELECT 2018 anio, recsuc.idUsuario idReclutador, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM ( SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF( partida.lastUpdateDate > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (4)	) data 			INNER JOIN rh_sucursal_usuario recsuc ON data.idSucursal = recsuc.idSucursal INNER JOIN config_app_access ON (recsuc.idUsuario = config_app_access.idUsuario AND config_app_access.idRole = 2  AND config_app_access.idAplicacion = 3)  GROUP BY recsuc.idUsuario,estado 		) solicitudes INNER JOIN users ON solicitudes.idReclutador = users.id GROUP BY anio,solicitudes.idReclutador,users.name 		 	) 	base LEFT JOIN 	(	 		SELECT solicitudes.idReclutador,SUM(total) total, SUM(atraso) atraso, SUM(bien)bien , SUM(atraso)/SUM(total)*100 peratraso, SUM(bien)/SUM(total)*100 perbien FROM 		( 			SELECT 2018 anio, recsuc.idUsuario idReclutador,  COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM (	SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF(NOW() > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (1,2,3) AND MONTH(sol.fechaCrea) = ".date("m")." ) data 		 			INNER JOIN rh_sucursal_usuario recsuc ON data.idSucursal = recsuc.idSucursal INNER JOIN config_app_access ON (recsuc.idUsuario = config_app_access.idUsuario AND config_app_access.idRole = 2  AND config_app_access.idAplicacion = 3)  GROUP BY recsuc.idUsuario,estado 			UNION ALL 			SELECT 2018 anio, recsuc.idUsuario idReclutador,  COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM ( SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF( partida.lastUpdateDate > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (4) AND MONTH(sol.fechaCrea) = ".date("m")."	) data 			 			INNER JOIN rh_sucursal_usuario recsuc ON data.idSucursal = recsuc.idSucursal INNER JOIN config_app_access ON (recsuc.idUsuario = config_app_access.idUsuario AND config_app_access.idRole = 2  AND config_app_access.idAplicacion = 3)  GROUP BY recsuc.idUsuario,estado 		) solicitudes INNER JOIN users ON solicitudes.idReclutador = users.id GROUP BY anio,solicitudes.idReclutador,users.name 		 	) 	actual ON actual.idReclutador = base.idReclutador LEFT JOIN 	( 		SELECT solicitudes.idReclutador,SUM(total) total, SUM(atraso) atraso, SUM(bien)bien , SUM(atraso)/SUM(total)*100 peratraso, SUM(bien)/SUM(total)*100 perbien FROM 		( 			SELECT 2018 anio, recsuc.idUsuario idReclutador, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM (	SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF(NOW() > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (1,2,3) AND MONTH(sol.fechaCrea) = ".(date("m")-1)." ) data 			INNER JOIN rh_sucursal_usuario recsuc ON data.idSucursal = recsuc.idSucursal INNER JOIN config_app_access ON (recsuc.idUsuario = config_app_access.idUsuario AND config_app_access.idRole = 2  AND config_app_access.idAplicacion = 3)  GROUP BY recsuc.idUsuario,estado 			UNION ALL 			SELECT 2018 anio, recsuc.idUsuario idReclutador, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM ( SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF( partida.lastUpdateDate > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (4) AND MONTH(sol.fechaCrea) = ".(date("m")-1)."	) data 			INNER JOIN rh_sucursal_usuario recsuc ON data.idSucursal = recsuc.idSucursal INNER JOIN config_app_access ON (recsuc.idUsuario = config_app_access.idUsuario AND config_app_access.idRole = 2  AND config_app_access.idAplicacion = 3)  GROUP BY recsuc.idUsuario,estado 		) solicitudes INNER JOIN users ON solicitudes.idReclutador = users.id GROUP BY anio,solicitudes.idReclutador,users.name 	) anterior ON anterior.idReclutador = base.idReclutador ORDER BY base.perbien DESC;";
		$efectividadReclutador = DB::select($sql);
		
					
		return view('vacantes.index', ['autorizados' => $autorizados, "cerradas" => (empty($cerradas)?0:$cerradas[0]->total), "abiertas" => $abiertas, "retrasadas" => $retrasadas, "entiempo" => $entiempo, 'actuales' => $actuales, 'diferencia' => $diferencia, 'efectividad' => (empty($efectividad)?0:$efectividad[0]), 'efectividadReclutador' => $efectividadReclutador, 'role' => session('RHRole')]);
	}
	public function detPlantillaTable(Request $request)
	{
		$sql = "SELECT aut.idPuesto, puesto.nombre puesto, puesto.orden, aut.cantidad FROM rh_plazas_autorizadas aut INNER JOIN rh_puesto puesto ON aut.idPuesto = puesto.idPuesto WHERE cantidad > 0 AND idSucursal = ".$request->input('ids')." ORDER BY puesto.orden, puesto.nombre;";
		$autorizados = DB::select($sql);
		$plantilla = array();
		$puestos = "";

		foreach($autorizados as $autorizado)
		{
			
			$sql = "SELECT rh_empleado.nombre, rh_puesto.nombre puesto, 0 excedente FROM rh_empleado INNER JOIN rh_puesto ON rh_puesto.idPuesto = rh_empleado.idPuesto WHERE rh_empleado.idPuesto = ".$autorizado->idPuesto." AND rh_empleado.idSucursal = ".$request->input('ids')." AND rh_empleado.estado =1 ORDER BY rh_puesto.orden ASC;";
			$empleados = DB::select($sql);
			$puesto = 0;
			foreach($empleados as $empleado)
			{		
				$puesto++;

				if($autorizado->cantidad < $puesto)
				{
					$empleado->excedente = 1;
				}

				$plantilla[] = $empleado;
				//$autorizado->idPuesto
			}
			
			$sql  ="SELECT CONCAT('Solicitud #',idSolicitud) nombre, rh_puesto.nombre puesto, 0 excedente FROM rh_vacante_solicitud_partida INNER JOIN rh_puesto ON rh_puesto.idPuesto = rh_vacante_solicitud_partida.idPuesto  WHERE rh_vacante_solicitud_partida.estado IN (1,2,3,4,9) AND rh_vacante_solicitud_partida.idPuesto = ".$autorizado->idPuesto." AND rh_vacante_solicitud_partida.idSucursal = ".$request->input('ids')." ;";
			$solicitudes = DB::select($sql);
			foreach($solicitudes AS $solicitud)
			{
				$puesto++;
				if($autorizado->cantidad < $puesto)
				{
					$solicitud->excedente = 1;
				}
				else
				{
					$solicitud->excedente = 2;
				}

				$plantilla[] = $solicitud;
				//$autorizado->idPuesto
				
			}

			if($autorizado->cantidad > $puesto)
			{
				for(;$puesto<$autorizado->cantidad; $puesto++)	
				{
					$disponible = new  \stdClass();
					@$disponible->nombre = "Disponible ". ($puesto+1);
					@$disponible->puesto = $autorizado->puesto;
					@$disponible->excedente = 3;
					$plantilla[] = $disponible;
					//$autorizado->idPuesto
				}
			}

			if(!empty($autorizado->idPuesto))
				$puestos .= ",";
			$puestos .= $autorizado->idPuesto;

		}

		return view('vacantes.detPlantillaTable', ['detalle' => $plantilla, 'role' => session('RHRole'), 'role' => session('RHRole') ]);
	}

	public function downloadPlantilla($idSucursal=0){
		if($idSucursal)
		{
			$sql = "SELECT * FROM rh_oficinas WHERE id = ?";
			$sucursal = DB::select($sql,[$idSucursal]);
/*
			$sql = "SELECT idPuesto, cantidad FROM rh_plazas_autorizadas WHERE idSucursal = ? AND cantidad >0";
			$plazas = DB::select($sql,[$idSucursal]);
			$puestos = array();				
			foreach($plazas AS $plaza)
			{
				$puestos[$plaza->idPuesto] = $plaza->cantidad;
			}
			
			$sql = "SELECT rh_empleado.idPuesto,rh_empleado.nombre, rh_puesto.nombre puesto FROM rh_empleado INNER JOIN rh_puesto ON rh_puesto.idPuesto = rh_empleado.idPuesto WHERE rh_empleado.idSucursal = ".$idSucursal." AND rh_empleado.estado =1 ORDER BY rh_puesto.orden ASC;";
			$detalle = DB::select($sql);
*/
			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->mergeCells('A1:D1');
			$sheet->mergeCells('A2:D2');

			$sheet->getColumnDimension('A')->setWidth(6);
			$sheet->getColumnDimension('B')->setWidth(4);
			$sheet->getColumnDimension('C')->setWidth(25);
			$sheet->getColumnDimension('E')->setWidth(25);
			$sheet->getColumnDimension('D')->setWidth(40);

			$sheet->setCellValue('A1', 'ERIC KAYSER MEXICO SAPI DE CV ');
			$sheet->setCellValue('A2', $sucursal[0]->nombre);
			$sheet->setCellValue('E1', 'PLANTILLA AUTORIZADA');
			$sheet->setCellValue('E2', 'PLANTILLA ACTUAL');

			$sheet->setCellValue('A5', "# AUT");
			$sheet->setCellValue('B5', "#");
			$sheet->setCellValue('C5', "PUESTO");
			$sheet->setCellValue('D5', "NOMBRE");
			$totEmp= 0;
			$totPuestos = 0;
			$i=6;
			//$puesto = "";

			$sql = "SELECT aut.idPuesto, puesto.nombre puesto, puesto.orden, aut.cantidad FROM rh_plazas_autorizadas aut INNER JOIN rh_puesto puesto ON aut.idPuesto = puesto.idPuesto WHERE cantidad > 0 AND idSucursal = $idSucursal ORDER BY puesto.orden, puesto.nombre;";
			$autorizados = DB::select($sql);


			foreach($autorizados as $autorizado)
			{
				$totPuestos += $autorizado->cantidad;
				$sql = "SELECT rh_empleado.nombre, rh_puesto.nombre puesto, 0 excedente FROM rh_empleado INNER JOIN rh_puesto ON rh_puesto.idPuesto = rh_empleado.idPuesto WHERE rh_empleado.idPuesto = ".$autorizado->idPuesto." AND rh_empleado.idSucursal = $idSucursal AND rh_empleado.estado =1 ORDER BY rh_puesto.orden ASC;";
				$empleados = DB::select($sql);
				$puesto = 0;
				
				$sheet->setCellValue('A'.$i, $autorizado->cantidad);

				foreach($empleados as $empleado)
				{		
					$puesto++;
					$i++;
					$totEmp++;

					$sheet->setCellValue('B'.$i, $puesto);
					$sheet->setCellValue('C'.$i, $empleado->puesto);
					$sheet->setCellValue('D'.$i, $empleado->nombre);

					if($autorizado->cantidad < $puesto)
					{
						$sheet->setCellValue('E'.$i, "Excedente");
					}

				}

				$sql  ="SELECT CONCAT('Solicitud #',idSolicitud) nombre, rh_puesto.nombre puesto, 0 excedente FROM rh_vacante_solicitud_partida INNER JOIN rh_puesto ON rh_puesto.idPuesto = rh_vacante_solicitud_partida.idPuesto  WHERE rh_vacante_solicitud_partida.estado IN (1,2,3,4,9) AND rh_vacante_solicitud_partida.idPuesto = ".$autorizado->idPuesto." AND rh_vacante_solicitud_partida.idSucursal = $idSucursal ;";
				$solicitudes = DB::select($sql);
				foreach($solicitudes AS $solicitud)
				{
					$puesto++;
					$i++;
					if($autorizado->cantidad < $puesto)
					{
						$sheet->setCellValue('E'.$i, "Excedente");
					}
	
					$sheet->setCellValue('B'.$i, $puesto);
					$sheet->setCellValue('C'.$i, $solicitud->puesto);
					$sheet->setCellValue('D'.$i, $solicitud->nombre);				
				}

				if($autorizado->cantidad > $puesto)
				{
					for(;$puesto<$autorizado->cantidad; $puesto++)	
					{
						$i++;
						$sheet->setCellValue('A'.$i, $autorizado->cantidad);
						$sheet->setCellValue('B'.$i, 1);
						$sheet->setCellValue('C'.$i, $autorizado->puesto);
						$sheet->setCellValue('D'.$i, "Disponible ". ($puesto+1));
					}
				}

			}

			$sheet->setCellValue('F2', $totEmp);
			$sheet->setCellValue('F1', $totPuestos);
			
			$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="plantilla_'.$sucursal[0]->idSap.'_'.date("Ymd").'.xlsx"');
			$writer->save("php://output");

			
		}

		//return $idSucursal;
	}

	public function showGlobalHeadcount()
	{

		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		if($RHRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) ";
		$sql = "SELECT autorizado.idSucursal, autorizado.oficina ,autorizado.total autorizado, empleados.empleados, solicitudes.total, solicitudes.atraso, solicitudes.bien FROM (SELECT of.nombre oficina, plazas.* FROM (SELECT idSucursal, SUM(cantidad) total FROM rh_plazas_autorizadas ".( $RHRole != 1 && $RHRole != 4 && $RHRole != 5  ? " WHERE idSucursal ".$strValSucursales : ""  )." GROUP BY idSucursal)plazas INNER JOIN rh_oficinas of ON of.id = plazas.idSucursal) autorizado
LEFT JOIN (SELECT idSucursal, COUNT(estado) empleados FROM rh_empleado WHERE estado = 1 ".( $RHRole != 1 && $RHRole != 4 && $RHRole != 5 ? " AND idSucursal ".$strValSucursales : ""  )." GROUP BY idSucursal) empleados ON autorizado.idSucursal = empleados.idSucursal
LEFT JOIN (SELECT idSucursal, COUNT(estado) total, SUM(IF(atraso=1,1,0)) atraso, SUM(IF(atraso=0,1,0)) bien FROM (SELECT partida.idSucursal ,sol.fechaCrea, ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, partida.lastUpdateDate ,IF(NOW() > ADDDATE(sol.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0)atraso , partida.estado FROM rh_vacante_solicitud sol INNER JOIN rh_vacante_solicitud_partida partida ON sol.idSolicitud = partida.idSolicitud INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = partida.idPuesto WHERE partida.estado IN (1,2,3) ".( $RHRole != 1 && $RHRole != 4 && $RHRole != 5  ? " AND idSucursal ".$strValSucursales : ""  )." ) data GROUP BY idSucursal )solicitudes 
ON solicitudes.idSucursal = autorizado.idSucursal ORDER BY autorizado.oficina ASC;";
		$plantilla = DB::select($sql);
		return view('vacantes.plantilla',['plantilla' => $plantilla, 'role' => session('RHRole')]);
	}
	public function showNewRequestForm()
	{
		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		if($RHRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) ";

		$sucursales = DB::select("SELECT id AS idSucursal, nombre FROM rh_oficinas WHERE estado = 1 ".($RHRole != 1 ? "AND id ".$strValSucursales : "" )." ORDER BY nombre;");
		$trsucursales = DB::select("SELECT id AS idSucursal, nombre FROM rh_oficinas WHERE estado = 1 ORDER BY nombre;");
		return view('vacantes.newRequest',['sucursales' => $sucursales, 'trsucursales' => $trsucursales, 'role' => session('RHRole')]);
	}


	public function validanuevoPuesto(Request $request)
	{
		$sql = 'SELECT CONCAT( "1|",rh_empleado.idEmpleado) as id, rh_empleado.nombre FROM rh_empleado LEFT JOIN rh_vacante_solicitud_partida rvsp ON  (rh_empleado.idEmpleado = rvsp.idEmpleado AND rvsp.estado !=6 ) WHERE rvsp.idEmpleado is null AND rh_empleado.estado = 1 AND rh_empleado.idPuesto = ? AND rh_empleado.idSucursal = ? UNION ALL ';
		$sql .= 'SELECT CONCAT( "2|",rvsp.idPartida) as id, rh_empleado.nombre FROM rh_empleado INNER JOIN rh_vacante_solicitud_partida rvsp ON rh_empleado.idEmpleado = rvsp.idEmpleado WHERE rvsp.idPuesto = ? AND rvsp.idSucursal = ? AND rh_empleado.estado = 1  AND NOT(rvsp.estado IN (4, 8, 9 ,6))  UNION ALL ';
		$sql .= 'SELECT CONCAT( "3|", rvsp.idPartida) as id, CONCAT("CUBRIR VACANTE - ", rvsp.idPartida) nombre  FROM rh_vacante_solicitud_partida rvsp WHERE rvsp.solicitud = 1 AND rvsp.idPuesto = ? AND rvsp.idSucursal = ? AND NOT(rvsp.estado IN (4, 8, 9 ,6)) ORDER BY nombre;';
		$empleados = DB::select($sql, [$request->input('idPuesto'), $request->input('idSucursal'), $request->input('idPuesto'), $request->input('idSucursal'), $request->input('idPuesto'), $request->input('idSucursal')]);
		return  response()->json([ 'empleados' => $empleados ]);
	}

	public function validaPuesto(Request $request)
	{
		
		$disponibles= DB::select("SELECT if(empleados.total is NULL, 0 , empleados.total) total, plazas.autorizados, if(sols.cantidad is NULL, 0 , sols.cantidad) solicitudes, (plazas.autorizados - if(empleados.total is NULL, 0 , empleados.total)- if(sols.cantidad is NULL, 0 , sols.cantidad)) AS diferencia  FROM ( SELECT estado, idPuesto, SUM(cantidad) autorizados FROM rh_plazas_autorizadas WHERE estado = 1 AND idPuesto = ".$request->input('idPuesto')." AND idSucursal = ".$request->input('idSucursal')." GROUP BY estado , idPuesto ) plazas LEFT JOIN ( SELECT rh_empleado.estado, idPuesto, COUNT(rh_empleado.estado) total FROM rh_empleado INNER JOIN sucursales ON rh_empleado.idSucursal = sucursales.id WHERE rh_empleado.estado = 1 AND idPuesto = ".$request->input('idPuesto')." AND idSucursal = ".$request->input('idSucursal')." GROUP BY rh_empleado.estado, idPuesto ) empleados ON empleados.estado = plazas.estado LEFT JOIN (SELECT idPuesto, COUNT(idPuesto) cantidad FROM  rh_vacante_solicitud_partida WHERE idPuesto = ".$request->input('idPuesto')." AND solicitud =1 AND idSucursal = ".$request->input('idSucursal')." AND estado = 1 GROUP BY idPuesto) sols ON sols.idPuesto = plazas.idPuesto;" );
		
		$cantidad = 0;
		
		if(!empty($disponibles[0]))
		{
			if(!empty($disponibles[0]->diferencia) && $disponibles[0]->diferencia > 0)
				$cantidad = $disponibles[0]->diferencia;
		}
		
		$sql = "SELECT rh_empleado.idEmpleado as id, rh_empleado.nombre FROM rh_empleado WHERE rh_empleado.estado = 1 AND idPuesto = ".$request->input('idPuesto')." AND idSucursal = ".$request->input('idSucursal').";";
		
		$empleados = DB::select($sql);
		
		return  response()->json([ 'disponibles' => $cantidad, 'empleados' => $empleados ]);
	}
	
	public function getPuestosGrowup(Request $request)
	{
		$puestos= DB::select("SELECT rh_puesto.idPuesto id, rh_puesto.nombre FROM rh_puesto_crecimiento INNER JOIN rh_puesto ON rh_puesto_crecimiento.idCrece = rh_puesto.idPuesto WHERE estado = 1 AND rh_puesto_crecimiento.idPuesto = " . $request->input('puesto'));
		
		if(!empty($puestos[0]))
		{
			return  response()->json([
			'data' => $puestos
			]);
		}
		else
		{
			return  response()->json([
			'tipo' => 0,
			'data' => []
			]);
		}
	}
	public function getPuestosList(Request $request)
	{
		$puestos= DB::select("SELECT rh_puesto.idPuesto id, rh_puesto.nombre FROM rh_puesto_area INNER JOIN rh_puesto ON rh_puesto.idPuesto = rh_puesto_area.idPuesto WHERE estado = 1 AND idArea = " . $request->input('id'));// AND idArea = " . $request->input('id'));
		if(!empty($puestos[0]))
		{
			return  response()->json([
			'data' => $puestos
			]);
		}
		else
		{
			return  response()->json([
			'tipo' => 0,
			'data' => []
			]);
		}
	}
	
	public function getDeptosList(Request $request)
	{
		$suc= DB::select("SELECT * FROM rh_oficinas WHERE id = " . $request->input('id'));
		if(!empty($suc[0]))
		{
			if($suc[0]->idTipo == 1)
			{
				$sql = "SELECT * FROM rh_departamento WHERE id = 13;";
				$sql2 = "SELECT * FROM rh_area WHERE id IN (1,2,3,6,8,9);";
			}
			else
			{
				$sql = "SELECT * FROM rh_departamento WHERE estado = 1";
				$sql2 = "SELECT * FROM rh_area;";
			}
			
			$dep= DB::select($sql);
			$areas = DB::select($sql2);
			return  response()->json([
			'tipo' => $suc[0]->idTipo,
			'data' => $dep,
			'areas' => $areas
			]);
		}
		else
		{
			return  response()->json([
			'tipo' => 0,
			'data' => []
			]);
		}			
	}
	public function saveRequest(Request $request)
	{
		DB::enableQueryLog();
		$user = Auth::user();
		$idUsuario = $user->id;
		$uemail = $user->email;
		$uname = $user->name;
		$ids = $request->input('id');
		$sucursales = $request->input('idSucursal');
		$trsucursales = $request->input('transucId');
		$areas = $request->input('idArea');
		$deptos = $request->input('idDepto');
		$acciones = $request->input('accion');
		$empleados = $request->input('empleado');
		$comentario = $request->input('comentario');
		$nvoPuesto = $request->input('nvoPuesto');
		$empleadoBaja = $request->input('empleadoBaja');
		
		if(!empty($ids) && !empty($sucursales) && !empty($areas) )
		{
			$comentario = empty($comentario)?"":$comentario;
			DB::insert('insert into rh_vacante_solicitud (idUsuario, fechaCrea, horaCrea, comentario) values (?, ?, ?, ?)', [$idUsuario, date("Y-m-d"),date("H:i:s"), $comentario]);
			$lid = DB::getPdo()->lastInsertId();
			$sql="";
			$sqlBajas = "";
			$sqlNvoPuesto = array();
			if(!empty($lid))
			{
				foreach($ids as $id=>$value)
				{
					if(!empty($sql))
						$sql .= ", ";
					

					if($acciones[$id] == 10 )
					{
						//$idPuesto = DB::select("SELECT idPuesto FROM rh_empleado WHERE idEmpleado = " . (empty($empleados[$id])?0:$empleados[$id]));
						if(!empty($sql))
							$sql .= ", ";	
						$sql .= "( ".$lid." ,".$value.", ".$sucursales[$id].", ".$trsucursales[$id].", ".$areas[$id].", ".$deptos[$id].", 1 ,  0 ,   1  ,'".date("Y-m-d")."','".date("H:i:s")."',".$idUsuario.", '".date("Y-m-d")."', 0)";
						$sql .= ",( ".$lid." ,".$nvoPuesto[$id].", ".$sucursales[$id].", ".$trsucursales[$id].", ".$areas[$id].", ".$deptos[$id].", ".$acciones[$id].", ".(empty($empleados[$id])?0:$empleados[$id]).",6,'".date("Y-m-d")."','".date("H:i:s")."',".$idUsuario.", '".date("Y-m-d")."', ".(empty($empleados[$id])?0:$empleados[$id])." )";

						$sqlNvoPuesto[] = "UPDATE rh_empleado SET idPuesto= ".$nvoPuesto[$id]." WHERE idEmpleado = ".(empty($empleados[$id])?0:$empleados[$id]);
						
						if(!empty($empleadoBaja[$id]))
						{
							$auxEmpBaja=explode( "|", $empleadoBaja[$id] );
							if($auxEmpBaja[0]==1 && !empty($auxEmpBaja[1]))
							{
								if(!empty($sqlBajas))
									$sqlBajas .= ", ";
								$sqlBajas .= $empleados[$id];
							}
							else if(!empty($auxEmpBaja[1]))
							{
								$sqlNvoPuesto[] = "UPDATE rh_vacante_solicitud_partida SET estado = 6 ,idContratado= ".$empleados[$id]." WHERE idPartida = ".$auxEmpBaja[1];
							}
						}

					}	
					else 
					{
						
						$sql .= "( ".$lid." ,".$value.", ".$sucursales[$id].", ".$trsucursales[$id].", ".$areas[$id].", ".$deptos[$id].", ".$acciones[$id].", ".(empty($empleados[$id])?0:$empleados[$id]).",1,'".date("Y-m-d")."','".date("H:i:s")."',".$idUsuario.", '".date("Y-m-d")."', 0)";

						if(($acciones[$id] == 6 || $acciones[$id] == 2 ) && !empty($empleados[$id]))
						{
							if(!empty($sqlBajas))
								$sqlBajas .= ", ";
							$sqlBajas .= $empleados[$id];
						}
						
					}

				}
				if(!empty($sql))
				{
					DB::insert('insert into rh_vacante_solicitud_partida (idSolicitud, idPuesto, idSucursal, idSucursalTrans, idArea, idDepartamento, solicitud , idEmpleado,estado,lastUpdateDate, lastUpdateTime, idUserUpdate, ingreso, idContratado) values '.$sql);
					
					if(!empty($sqlBajas))
						$affected = DB::update('update rh_empleado set estado = 2 , fechaSolBaja= "'.date('Y-m-d').'", horaSolBaja= "'.date('H:i:s').'" where idEmpleado IN ('.$sqlBajas.")");

					if(!empty($sqlNvoPuesto))
						foreach($sqlNvoPuesto as $evsql)
							$affected = DB::update($evsql);
					if(!empty($lid))
					{
						$sql = "SELECT RHR.email FROM users RHR INNER JOIN rh_sucursal_usuario RHRS ON RHR.id = RHRS.idUsuario INNER JOIN config_app_access RHAC ON RHAC.idUsuario = RHR.id INNER JOIN rh_vacante_solicitud_partida RHRP ON RHRP.idSucursal = RHRS.idSucursal WHERE RHAC.idAplicacion=3 AND RHAC.idRole = 2 and  RHRP.idSolicitud = ".$lid." GROUP BY RHR.email;";
						$reclutadores = DB::select($sql);
						$recArray = array();
						foreach($reclutadores as $rec){
							$recArray[] = $rec->email;
						}
						$recArray[] = "apiana@prigo.com.mx";
						$recArray[] = "emarin@prigo.com.mx";
						$recArray[] = "rgallardo@prigo.com.mx";
						$url = url('/detallevacante/'.$lid);

						$partidas = DB::select("SELECT tipo.idTipo, tipo.tipo solicitud, estado.estado, rh_vsp.idSolicitud, rh_vsp.idPartida, rh_vs.fechaCrea, rh_o.nombre AS sucursal, rh_o2.nombre AS transferencia,  rh_p.nombre AS puesto, rh_vs.comentario, rh_empleado.nombre FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_tipo tipo ON tipo.idTipo = rh_vsp.solicitud INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado LEFT JOIN rh_empleado ON rh_empleado.idEmpleado = rh_vsp.idEmpleado LEFT JOIN rh_oficinas AS rh_o2 ON rh_o2.id = rh_vsp.idSucursalTrans WHERE rh_vsp.idSolicitud = '".$lid."';");
						
						$sqlAut = "";
						
						foreach($partidas as $partida)
						{
							if($partida->idTipo == 4 || $partida->idTipo == 5 || $partida->idTipo == 3 )
							{
								if(!empty($sqlAut))
									$sqlAut .= ", ";
								$sqlAut .= "(".$lid.", ".$partida->idPartida.",0,".$partida->idTipo." )";
							}
						}

						if(!empty($sqlAut))
							DB::insert('insert into rh_vacante_solicitud_autorizacion (idSolicitud, idPartida, idAutoriza, tipoSolicitud ) values '.$sqlAut);

						Mail::send('vacantes.mailVacante', ['url' => $url,'name' => $uname, 'partidas' => $partidas, 'comentario' => $comentario], function ($message) use ($recArray)
						{
							$message->from('reportes@prigo.com.mx', 'Reportes PRIGO');
							$message->to($recArray);
							$message->subject("Nueva solicitud de reclutamiento recibida");
						});
					}					
					else
					{
						return "{ 'success': false, 'msg': 'Error al guardar los datos!'}";
					}
				}
			}				
			
		}
		
	}
	
	public function showEmployees(){
		return view('vacantes.empleados', ['role' => session('RHRole')]);
	}

	public function showRequests($tipo=1)
	{
		$tipos = array("Abiertas","En Tiempo","Atrasadas");
		return view('vacantes.showRequests', ["tipo" => $tipo, "titulo" => "Solicitudes ".$tipos[$tipo-1], 'role' => session('RHRole')]);
	}
	public function showClosedRequests()
	{
		return view('vacantes.showClosedRequests');
	}
	public function showPendingConfirmation(){
		$tipo=4;
		return view('vacantes.showRequests', ["tipo" => $tipo, "titulo" => "Contratados", 'role' => session('RHRole')]);
	}	
		
	public function showPendingFinalConfirmation(){
		$tipo=5;
		return view('vacantes.showRequests', ["tipo" => $tipo, "titulo" => "Confirmados Capacitación", 'role' => session('RHRole')]);
	}

	public function showPendingDismiss(){	
		$tipo=6;	
		return view('vacantes.showDismissRequests', ["tipo" => $tipo, "titulo" => "Bajas", 'role' => session('RHRole')]);
	}
	public function exportRequest(Request $request)
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Solicitud');
		$sheet->setCellValue('B1', 'Fecha');
		$sheet->setCellValue('C1', 'Sucursal');
		$sheet->setCellValue('D1', 'Solicita');
		$sheet->setCellValue('E1', 'Puesto');
		$sheet->setCellValue('F1', 'Solicitud');
		$sheet->setCellValue('G1', 'Reclutador');
		$sheet->setCellValue('H1', 'Estado');
		$sheet->setCellValue('I1', 'Referencia');
		$sheet->setCellValue('J1', 'Comentario');

		$idUsuario = Auth::id();
		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";

		if($RHRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) ";

		$puesto = !empty($request->input('findPuesto')) ? $request->input('findPuesto') : "" ;
		$sucursal = !empty($request->input('findSucursal')) ? $request->input('findSucursal') : "" ;
		$tipo = !empty($request->input('tipo')) ? $request->input('tipo') : 1 ;

		$strBusca =  "";
		$strAtraso = "";

		$busca = array();

		if(!empty($sucursal))
			$busca[] = " rh_o.nombre LIKE '%".$sucursal."%' ";
		if(!empty($puesto))
			$busca[] = " rh_p.nombre LIKE '%".$puesto."%' ";

		if(!empty($busca))
		{
			$strBusca = " AND ".implode(" AND ", $busca); 
		}

		if(!empty($tipo) && $tipo != 1)
		{
			$strAtraso .= " atraso = ". ($tipo==2?1:0);

		}

		$items =    DB::select("SELECT * FROM ( SELECT rh_vsp.idSolicitud, IF(rh_vsp.solicitud =1,'Cubrir Vacante','Reemplazo') AS solicitud , rh_vs.fechaCrea , ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, rh_vsp.lastUpdateDate ,IF(NOW() > ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0) AS atraso , rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, estado.estado, rh_rec.nombre AS reclutador FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado  LEFT JOIN rh_reclutador_sucursal rh_recsuc ON rh_recsuc.idSucursal = rh_vsp.idSucursal INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = rh_vsp.idPuesto LEFT JOIN rh_reclutador rh_rec ON rh_rec.idReclutador = rh_recsuc.idReclutador WHERE NOT(rh_vsp.estado IN(4,5) ) ".($RHRole != 1 ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." ".$strBusca.") AS datos ".(empty($strAtraso)?"":" WHERE $strAtraso").";");

		$row =2;
		foreach($items as $item)
		{
			$sheet->setCellValue('A'.$row, $item->idSolicitud );
			$sheet->setCellValue('B'.$row, $item->fechaCrea);
			$sheet->setCellValue('C'.$row, $item->sucursal);
			$sheet->setCellValue('D'.$row, "");
			$sheet->setCellValue('E'.$row, $item->puesto);
			$sheet->setCellValue('F'.$row, $item->solicitud);
			$sheet->setCellValue('G'.$row, $item->reclutador);
			$sheet->setCellValue('H'.$row, $item->estado);
			$sheet->setCellValue('I'.$row, "");
			$sheet->setCellValue('J'.$row, "");
			$row++;
		}

		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="vacantes_'.date("Ymd").'.xlsx"');
		$writer->save("php://output");

	}
	public function getDismissRequest(Request $request)
	{
		$strBusca = "";
		$draw = !empty($request->input('draw'))?$request->input('draw'):1;
		$start = !empty($request->input('start'))?$request->input('start'):0;
		$length = !empty($request->input('length'))?$request->input('length'):10;
		$queryarr = !empty($request->input('search'))?$request->input('search'):array();
		$query = !empty($queryarr["value"]) ? $queryarr["value"] : "" ;
		$allitems = null;

		if(!empty($request->input('columns')))
		{
			$cols = $request->input('columns');
			$busca = array();
			foreach($cols as $col)
			{
				if(!empty($col["search"]["value"]))
				{
					switch($col["data"]){
						case "puesto":
							$busca[] = " rh_p.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
						case "sucursal":
							$busca[] = " rh_o.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
					}
				}
			}
			if(!empty($busca))
			{
				$strBusca = " AND ".implode(" AND ", $busca); 
			}
			
		}

		$orden = " datos.fechaCrea DESC ";
		if(!empty($request->input('order')))
		{
			$ordena = $request->input('order');
			$orden = "";
			switch($ordena[0]["column"])
			{
				case 0:
					$orden = " datos.sucursal " . $ordena[0]["dir"];
				break;
				case 1:
				$orden = " datos.puesto  " . $ordena[0]["dir"];
				break;
				case 2:
				$orden = " datos.nombre  " . $ordena[0]["dir"];
				break;
			}
		}

		//TODO: Checar que pasa cuando no existe tiempo de contratacion para algun puesto, podemos poner default 8 dias  atodos los peustos nuevos
		//$allitems = DB::select("SELECT * FROM ( SELECT rh_vsp.idSolicitud, rh_vs.fechaCrea , ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, rh_vsp.lastUpdateDate ,IF(NOW() > ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0) AS atraso  , rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, rh_rec.nombre AS reclutador FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = rh_vsp.idPuesto LEFT JOIN rh_reclutador_sucursal rh_recsuc ON rh_recsuc.idSucursal = rh_vsp.idSucursal LEFT JOIN rh_reclutador rh_rec ON rh_rec.idReclutador = rh_recsuc.idReclutador WHERE ".$strBusca.") AS datos;");
		$allitems = DB::select("SELECT * FROM ( SELECT rh_e.idEmpleado, rh_e.nombre, rh_o.nombre sucursal, rh_p.nombre puesto FROM rh_empleado rh_e INNER JOIN rh_oficinas rh_o ON rh_e.idSucursal = rh_o.id INNER JOIN rh_puesto rh_p ON rh_e.idPuesto = rh_p.idPuesto WHERE rh_e.estado = 2 ".$strBusca." ) AS datos;");
		//$items =    DB::select("SELECT * FROM ( SELECT rh_vsp.idSolicitud, IF(rh_vsp.solicitud =1,'Cubrir Vacante','Reemplazo') AS solicitud , rh_vs.fechaCrea , ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, rh_vsp.lastUpdateDate ,IF(NOW() > ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0) AS atraso , rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, estado.estado, rh_rec.nombre AS reclutador FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado  LEFT JOIN rh_reclutador_sucursal rh_recsuc ON rh_recsuc.idSucursal = rh_vsp.idSucursal INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = rh_vsp.idPuesto LEFT JOIN rh_reclutador rh_rec ON rh_rec.idReclutador = rh_recsuc.idReclutador WHERE ". (empty($strStatus)?"NOT(rh_vsp.estado IN(4,5,6) )": " rh_vsp.estado = ".$strStatus)." ".($RHRole != 1 ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." ".$strBusca.") AS datos ".(empty($strAtraso)?"":" WHERE $strAtraso")."  ORDER BY $orden LIMIT ".$start.", ".$length.";");
		//$items = DB::select("SELECT * FROM ( SELECT rh_vsp.idSolicitud, IF(rh_vsp.solicitud =1,'Cubrir Vacante','Reemplazo') AS solicitud , rh_vs.fechaCrea , ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, rh_vsp.lastUpdateDate ,IF(NOW() > ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0) AS atraso , rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, estado.estado, rh_rec.name AS reclutador FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado   	 	LEFT JOIN rh_sucursal_usuario rh_recsuc ON rh_vsp.idSucursal = rh_recsuc.idSucursal INNER JOIN config_app_access ON (rh_recsuc.idUsuario = config_app_access.idUsuario AND config_app_access.idRole = 2  AND config_app_access.idAplicacion = 3) INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = rh_vsp.idPuesto LEFT JOIN users rh_rec ON rh_rec.id = rh_recsuc.idUsuario WHERE ". (empty($strStatus)?"NOT(rh_vsp.estado IN(4,5,6) )": " rh_vsp.estado = ".$strStatus)." ".($RHRole != 1 ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." ".$strBusca.") AS datos ".(empty($strAtraso)?"":" WHERE $strAtraso")."  ORDER BY $orden LIMIT ".$start.", ".$length.";");
		$items = DB::select("SELECT * FROM ( SELECT rh_e.idEmpleado, rh_e.nombre, rh_o.nombre sucursal, rh_p.nombre puesto, rh_e.fechaSolBaja fecha FROM rh_empleado rh_e INNER JOIN rh_oficinas rh_o ON rh_e.idSucursal = rh_o.id INNER JOIN rh_puesto rh_p ON rh_e.idPuesto = rh_p.idPuesto WHERE rh_e.estado = 2 ".$strBusca." ) AS datos ORDER BY $orden LIMIT ".$start.", ".$length.";");
		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allitems),
			'recordsFiltered' => count($allitems),
			'data' => $items
		]);

	}

	public function getEmployees(Request $request)
	{
		$idUsuario = Auth::id();
		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		$strStatus ="";
		if($RHRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) ";

		$draw = !empty($request->input('draw'))?$request->input('draw'):1;
		$start = !empty($request->input('start'))?$request->input('start'):0;
		$length = !empty($request->input('length'))?$request->input('length'):10;
		$queryarr = !empty($request->input('search'))?$request->input('search'):array();
		$query = !empty($queryarr["value"]) ? $queryarr["value"] : "" ;
		$allitems = null;
		$strBusca =  "";
		
		if(!empty($request->input('columns')))
		{
			$cols = $request->input('columns');
			$busca = array();
			foreach($cols as $col)
			{
				if(!empty($col["search"]["value"]))
				{
					switch($col["data"]){
						case "nombre":
							$busca[] = " rh_e.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
						case "puesto":
							$busca[] = " rh_p.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
						case "sucursal":
							$busca[] = " rh_o.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
					}
				}
			}
			if(!empty($busca))
			{
				$strBusca = " AND ".implode(" AND ", $busca); 
			}
			
		}
		
		$orden = " datos.nombre DESC ";
		if(!empty($request->input('order')))
		{
			$ordena = $request->input('order');
			$orden = "";
			switch($ordena[0]["column"])
			{
				case 0:
					$orden = " datos.idEmpleado " . $ordena[0]["dir"];
				break;
				case 1:
					$orden = " datos.nombre " . $ordena[0]["dir"];
				break;
				case 2:
					$orden = " datos.sucursal " . $ordena[0]["dir"];
				break;
				case 3:
				$orden = " datos.puesto  " . $ordena[0]["dir"];
				break;
			}
		}

		//TODO: Checar que pasa cuando no existe tiempo de contratacion para algun puesto, podemos poner default 8 dias  atodos los peustos nuevos
		//$allitems = DB::select("SELECT * FROM ( SELECT rh_vsp.idSolicitud, rh_vs.fechaCrea , ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, rh_vsp.lastUpdateDate ,IF(NOW() > ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0) AS atraso  , rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, rh_rec.nombre AS reclutador FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = rh_vsp.idPuesto LEFT JOIN rh_reclutador_sucursal rh_recsuc ON rh_recsuc.idSucursal = rh_vsp.idSucursal LEFT JOIN rh_reclutador rh_rec ON rh_rec.idReclutador = rh_recsuc.idReclutador WHERE ". (empty($strStatus)?"NOT(rh_vsp.estado IN(4,5,6,9,8) )": " rh_vsp.estado = ".$strStatus)." ".(($RHRole != 1 && $RHRole !=4 ) ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." ".$strBusca.") AS datos ".(empty($strAtraso)?"":" WHERE $strAtraso")." ;");
		$allitems = DB::select("SELECT * FROM ( SELECT rh_e.idEmpleado FROM rh_empleado AS rh_e INNER JOIN rh_empleado_estado AS rh_e_e ON rh_e.estado = rh_e_e.idEstado INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_e.idSucursal INNER JOIN rh_puesto AS rh_p ON rh_e.idPuesto = rh_p.idPuesto  ".(($RHRole != 1 && $RHRole !=4 ) ?" WHERE rh_e.idSucursal ".$strValSucursales:"")." ".$strBusca.") AS datos");
		$items = DB::select("SELECT * FROM ( SELECT rh_e.idEmpleado, rh_e.nombre, rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_e_e.nombre AS estado FROM rh_empleado AS rh_e INNER JOIN rh_empleado_estado AS rh_e_e ON rh_e.estado = rh_e_e.idEstado INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_e.idSucursal INNER JOIN rh_puesto AS rh_p ON rh_e.idPuesto = rh_p.idPuesto ".(($RHRole != 1 && $RHRole !=4 ) ?" WHERE rh_e.idSucursal ".$strValSucursales:"")." ".$strBusca.") AS datos ORDER BY $orden LIMIT ".$start.", ".$length.";");

		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allitems),
			'recordsFiltered' => count($allitems),
			'data' => $items
		]);
	}

	public function getRequest(Request $request)
	{
		$idUsuario = Auth::id();
		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		$strStatus ="";
		if($RHRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) ";

		$draw = !empty($request->input('draw'))?$request->input('draw'):1;
		$start = !empty($request->input('start'))?$request->input('start'):0;
		$length = !empty($request->input('length'))?$request->input('length'):10;
		$queryarr = !empty($request->input('search'))?$request->input('search'):array();
		$query = !empty($queryarr["value"]) ? $queryarr["value"] : "" ;
		$allitems = null;
		$strBusca =  "";
		$strAtraso = "";
		
		if(!empty($request->input('columns')))
		{
			$cols = $request->input('columns');
			$busca = array();
			foreach($cols as $col)
			{
				if(!empty($col["search"]["value"]))
				{
					switch($col["data"]){
						case "puesto":
							$busca[] = " rh_p.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
						case "sucursal":
							$busca[] = " rh_o.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
					}
				}
			}
			if(!empty($busca))
			{
				$strBusca = " AND ".implode(" AND ", $busca); 
			}
			
		}
		
		if(!empty($request->input('tipo')) && ($request->input('tipo') == 2 || $request->input('tipo') == 3 ))
		{
			$strAtraso .= " atraso = ". ($request->input('tipo')==2?1:0);

		}
		else if(!empty($request->input('tipo')) && $request->input('tipo') == 4)
		{
			$strStatus = "4";
		}
		else if(!empty($request->input('tipo')) && $request->input('tipo') == 5)
		{
			$strStatus = "9";
		}

		$orden = " datos.fechaCrea DESC ";
		if(!empty($request->input('order')))
		{
			$ordena = $request->input('order');
			$orden = "";
			switch($ordena[0]["column"])
			{
				case 0:
					$orden = " datos.idSolicitud " . $ordena[0]["dir"];
				break;
				case 1:
					$orden = " datos.fechaCrea " . $ordena[0]["dir"];
				break;
				case 2:
					$orden = " datos.sucursal " . $ordena[0]["dir"];
				break;
				case 3:
				$orden = " datos.puesto  " . $ordena[0]["dir"];
				break;
				case 4:
				$orden = " datos.solicitud " . $ordena[0]["dir"];
				break;
				case 5:
				$orden = " datos.reclutador  " . $ordena[0]["dir"];
				break;
				case 6:
				$orden = " datos.estado  " . $ordena[0]["dir"];
				break;
				default:
				$orden = " datos.fechaCrea DESC ";
				break;
			}
		}

		//TODO: Checar que pasa cuando no existe tiempo de contratacion para algun puesto, podemos poner default 8 dias  atodos los peustos nuevos
		$allitems = DB::select("SELECT idSolicitud FROM ( SELECT rh_vsp.idSolicitud FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado  INNER JOIN rh_vacante_tipo ON rh_vsp.solicitud = rh_vacante_tipo.idTipo LEFT JOIN rh_sucursal_usuario rh_recsuc ON rh_vsp.idSucursal = rh_recsuc.idSucursal INNER JOIN config_app_access ON (rh_recsuc.idUsuario = config_app_access.idUsuario AND  config_app_access.idRole IN (1,2,3,4) AND config_app_access.idAplicacion = 3) INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = rh_vsp.idPuesto LEFT JOIN users rh_rec ON rh_rec.id = rh_recsuc.idUsuario WHERE ". (empty($strStatus)?"NOT(rh_vsp.estado IN(4,5,6,9,8) )": " rh_vsp.estado = ".$strStatus)." ".(($RHRole != 1  && $RHRole !=4 ) ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." ".$strBusca.") AS datos ".(empty($strAtraso)?"":" WHERE $strAtraso")." GROUP BY idSolicitud ;"); 
		//$items = DB::select("SELECT * FROM ( SELECT rh_vsp.idSolicitud, IF(rh_vsp.solicitud =1,'Cubrir Vacante','Reemplazo') AS solicitud , rh_vs.fechaCrea , ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, rh_vsp.lastUpdateDate ,IF(NOW() > ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0) AS atraso , rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, estado.estado, rh_rec.nombre AS reclutador FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado  LEFT JOIN rh_reclutador_sucursal rh_recsuc ON rh_recsuc.idSucursal = rh_vsp.idSucursal INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = rh_vsp.idPuesto LEFT JOIN rh_reclutador rh_rec ON rh_rec.idReclutador = rh_recsuc.idReclutador WHERE ". (empty($strStatus)?"NOT(rh_vsp.estado IN(4,5,6,9,8) )": " rh_vsp.estado = ".$strStatus)." ".(($RHRole != 1  && $RHRole !=4) ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." ".$strBusca.") AS datos ".(empty($strAtraso)?"":" WHERE $strAtraso")."  ORDER BY $orden LIMIT ".$start.", ".$length.";");
		$sql= "SELECT * FROM ( SELECT rh_vsp.idSolicitud FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado  INNER JOIN rh_vacante_tipo ON rh_vsp.solicitud = rh_vacante_tipo.idTipo LEFT JOIN rh_sucursal_usuario rh_recsuc ON rh_vsp.idSucursal = rh_recsuc.idSucursal INNER JOIN config_app_access ON (rh_recsuc.idUsuario = config_app_access.idUsuario AND  config_app_access.idRole IN (1,2,3,4) AND config_app_access.idAplicacion = 3) INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = rh_vsp.idPuesto LEFT JOIN users rh_rec ON rh_rec.id = rh_recsuc.idUsuario WHERE ". (empty($strStatus)?"NOT(rh_vsp.estado IN(4,5,6,9,8) )": " rh_vsp.estado = ".$strStatus)." ".(($RHRole != 1  && $RHRole !=4 ) ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." ".$strBusca.") AS datos ".(empty($strAtraso)?"":" WHERE $strAtraso")." ;";
		$items = DB::select("SELECT idSolicitud,  solicitud, sucursal, fechaCrea, limite, lastUpdateDate, atraso, puesto, comentario, estado, GROUP_CONCAT(reclutador) reclutador FROM ( SELECT rh_vsp.idSolicitud, rh_vacante_tipo.tipo AS solicitud , rh_vs.fechaCrea , ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ) limite, rh_vsp.lastUpdateDate ,IF(NOW() > ADDDATE(rh_vs.fechaCrea, INTERVAL tiempo.tiempo+1 DAY ),1,0) AS atraso , rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, estado.estado, rh_rec.name AS reclutador FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado  INNER JOIN rh_vacante_tipo ON rh_vsp.solicitud = rh_vacante_tipo.idTipo LEFT JOIN rh_sucursal_usuario rh_recsuc ON rh_vsp.idSucursal = rh_recsuc.idSucursal INNER JOIN config_app_access ON (rh_recsuc.idUsuario = config_app_access.idUsuario AND config_app_access.idRole = 2 AND config_app_access.idAplicacion = 3) INNER JOIN rh_tiempo_contrata tiempo ON tiempo.idPuesto = rh_vsp.idPuesto LEFT JOIN users rh_rec ON rh_rec.id = rh_recsuc.idUsuario WHERE ". (empty($strStatus)?"NOT(rh_vsp.estado IN(4,5,6,9,8) )": " rh_vsp.estado = ".$strStatus)." ".(($RHRole != 1  && $RHRole !=4 ) ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." ".$strBusca.") AS datos ".(empty($strAtraso)?"":" WHERE $strAtraso")." GROUP BY idSolicitud,  solicitud, sucursal,fechaCrea, limite, lastUpdateDate, atraso, puesto, comentario, estado ORDER BY $orden LIMIT ".$start.", ".$length.";");

		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allitems),
			'recordsFiltered' => count($allitems),
			'data' => $items,
			'sql' => $sql
		]);
	}
		
	public function getClosedRequests(Request $request)
	{
		$idUsuario = Auth::id();
		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		if($RHRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) ";

		$draw = !empty($request->input('draw'))?$request->input('draw'):1;
		$start = !empty($request->input('start'))?$request->input('start'):0;
		$length = !empty($request->input('length'))?$request->input('length'):10;
		$queryarr = !empty($request->input('search'))?$request->input('search'):array();
		$query = !empty($queryarr["value"]) ? $queryarr["value"] : "" ;
		$allitems = null;
		$strBusca =  "";
		
		if(!empty($request->input('columns')))
		{
			$cols = $request->input('columns');
			$busca = array();
			foreach($cols as $col)
			{
				if(!empty($col["search"]["value"]))
				{
					switch($col["data"]){
						case "puesto":
							$busca[] = " rh_p.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
						case "sucursal":
							$busca[] = " rh_o.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
					}
				}
			}
			if(!empty($busca))
			{
				$strBusca = " AND ".implode(" AND ", $busca);
			}
			
		}
		
		if(!empty($strBusca))
		{
			$allitems = DB::select("SELECT rh_vsp.idSolicitud, rh_vs.fechaCrea, rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, rh_rec.nombre AS reclutador FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal  LEFT JOIN rh_reclutador_sucursal rh_recsuc ON rh_recsuc.idSucursal = rh_vsp.idSucursal LEFT JOIN rh_reclutador rh_rec ON rh_rec.idReclutador = rh_recsuc.idReclutador WHERE rh_vsp.estado IN(4) ".($RHRole != 1 ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." ".$strBusca.";");
			$items = DB::select("SELECT rh_vsp.idSolicitud, IF(rh_vsp.solicitud =1,'Cubrir Vacante','Reemplazo') AS solicitud , rh_vs.fechaCrea, rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, estado.estado, rh_rec.nombre AS reclutador FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado  LEFT JOIN rh_reclutador_sucursal rh_recsuc ON rh_recsuc.idSucursal = rh_vsp.idSucursal LEFT JOIN rh_reclutador rh_rec ON rh_rec.idReclutador = rh_recsuc.idReclutador  WHERE rh_vsp.estado IN(4) ".($RHRole != 1 ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." ".$strBusca." LIMIT ".$start.", ".$length.";");
		}
		else
		{
			$allitems = DB::select("SELECT rh_vsp.idSolicitud, rh_vs.fechaCrea, rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, rh_rec.nombre AS reclutador FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal  LEFT JOIN rh_reclutador_sucursal rh_recsuc ON rh_recsuc.idSucursal = rh_vsp.idSucursal LEFT JOIN rh_reclutador rh_rec ON rh_rec.idReclutador = rh_recsuc.idReclutador WHERE rh_vsp.estado IN(4) ".($RHRole != 1 ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." ;");
			$items = DB::select("SELECT rh_vsp.idSolicitud, IF(rh_vsp.solicitud =1,'Cubrir Vacante','Reemplazo') AS solicitud, rh_vs.fechaCrea, rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, estado.estado, rh_rec.nombre AS reclutador FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado  LEFT JOIN rh_reclutador_sucursal rh_recsuc ON rh_recsuc.idSucursal = rh_vsp.idSucursal LEFT JOIN rh_reclutador rh_rec ON rh_rec.idReclutador = rh_recsuc.idReclutador WHERE rh_vsp.estado IN(4) ".($RHRole != 1 ?" AND rh_vsp.idSucursal ".$strValSucursales:"")." LIMIT ".$start.", ".$length.";");
		}

		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allitems),
			'recordsFiltered' => count($allitems),
			'data' => $items
		]);
	}

	public function showRetrasadas()
	{
		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		$tipo =2 ;
		$tipos = array("Abiertas","En Tiempo","Atrasadas");
		return view('vacantes.showRequests', ["tipo" => $tipo, 'role' => session('RHRole'), 'sucursales'=>$sucursales, "titulo" => "Solicitudes ".$tipos[$tipo-1]]);
	}
	
	public function showAbiertas()
	{
		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		$tipo =1 ;
		$tipos = array("Abiertas","En Tiempo","Atrasadas");
		return view('vacantes.showRequests', ["tipo" => $tipo, 'role' => session('RHRole'), 'sucursales'=>$sucursales, "titulo" => "Solicitudes ".$tipos[$tipo-1]]);
	}

	public function showEnTiempo()
	{
		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		$tipo =3 ;
		$tipos = array("Abiertas","En Tiempo","Atrasadas");
		return view('vacantes.showRequests', ["tipo" => $tipo, 'role' => session('RHRole'), 'sucursales'=>$sucursales, "titulo" => "Solicitudes ".$tipos[$tipo-1]]);
	}

	public function employeeDetail($id)
	{
		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		
		$empleado = DB::select("SELECT rh_e.idEmpleado, rh_e.nombre, rh_e_e.nombre estado, rh_o.id AS idSucursal, rh_o.nombre sucursal, rh_p.idPuesto, rh_p.nombre puesto, rh_e.fechaCrea fecha FROM rh_empleado AS rh_e INNER JOIN rh_empleado_estado AS rh_e_e ON rh_e.estado = rh_e_e.idEstado INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_e.idSucursal INNER JOIN rh_puesto AS rh_p ON rh_e.idPuesto = rh_p.idPuesto WHERE  rh_e.idEmpleado = '".$id."';");
		$partidas = DB::select("SELECT * FROM (	SELECT rh_v_s.idSolicitud, rh_p.nombre puesto, rh_o.nombre sucursal, CONCAT(rh_v_t.tipo, IF(rh_o_t.id IS NULL,'', CONCAT(' a ',rh_o_t.nombre ))) solicitud, rh_v_s.fechaCrea fecha,rh_v_s.horaCrea hora, rh_u.name usuario FROM rh_vacante_solicitud AS rh_v_s INNER JOIN rh_vacante_solicitud_partida AS rh_v_p ON rh_v_s.idSolicitud = rh_v_p.idSolicitud INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_v_p.idSucursal INNER JOIN rh_puesto AS rh_p ON rh_v_p.idPuesto = rh_p.idPuesto INNER JOIN rh_vacante_tipo AS rh_v_t ON rh_v_p.solicitud = rh_v_t.idTipo INNER JOIN users rh_u ON rh_v_s.idUsuario = rh_u.id LEFT JOIN rh_oficinas AS rh_o_t ON rh_v_p.idSucursalTrans = rh_o_t.id WHERE rh_v_p.idEmpleado =".$id." UNION ALL SELECT rh_v_s.idSolicitud, rh_p.nombre puesto, rh_o.nombre sucursal, 'Solicita contratacion' solicitud, rh_v_s.fechaCrea fecha, rh_v_s.horaCrea hora, rh_u.name usuario FROM rh_vacante_solicitud AS rh_v_s INNER JOIN rh_vacante_solicitud_partida AS rh_v_p ON rh_v_s.idSolicitud = rh_v_p.idSolicitud INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_v_p.idSucursal INNER JOIN rh_puesto AS rh_p ON rh_v_p.idPuesto = rh_p.idPuesto INNER JOIN rh_vacante_tipo AS rh_v_t ON rh_v_p.solicitud = rh_v_t.idTipo INNER JOIN users rh_u ON rh_v_s.idUsuario = rh_u.id WHERE rh_v_p.idContratado = ".$id." UNION ALL SELECT idBaja, comentario puesto, '' sucursal, 'Baja Confirmada' solicitud, fecha, hora, rh_u.name usuario  FROM rh_empleado_baja rh_b INNER JOIN users rh_u ON rh_b.idUsuario = rh_u.id WHERE rh_b.idEmpleado = ".$id." UNION ALL SELECT idEmpLog, valorAnterior puesto, '' sucursal, accion solicitud, fechaCrea fecha, horaCrea hora, rh_u.name usuario  FROM rh_vacante_log rh_l INNER JOIN rh_vacante_accion rh_a on rh_l.idAccion=rh_a.idAccion INNER JOIN users rh_u ON rh_l.idUsuario = rh_u.id WHERE rh_l.idEmpleado =".$id.") data ORDER BY fecha, hora");
		$puestos = DB::select("SELECT * FROM rh_puesto") ;
		$sucursales = DB::select("SELECT id AS idSucursal, nombre FROM rh_oficinas WHERE estado = 1 ORDER BY nombre;");

		return view('vacantes.empleado', ['empleado' => $empleado[0], 'partidas' => $partidas, 'role' => session('RHRole'), 'sucursales'=>$sucursales, 'puestos'=>$puestos]);
	}

	public function requestDetail($id)
	{
		$RHRole = session('RHRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		if($RHRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) ";
		
		$solicitud = DB::select("SELECT rh_vacante_solicitud.idSolicitud, users.name nombre, rh_vacante_solicitud.fechaCrea AS fecha, rh_vacante_solicitud.estado, rh_vacante_solicitud.comentario FROM rh_vacante_solicitud INNER JOIN users ON users.id = rh_vacante_solicitud.idUsuario WHERE rh_vacante_solicitud.idSolicitud = '".$id."';");		
		$partidas = DB::select("SELECT tipo.tipo solicitud, estado.estado, estado.idEstado, rh_vsp.idSolicitud, rh_vsp.idPartida, rh_vs.fechaCrea, rh_o.nombre AS sucursal, rh_p.nombre AS puesto, rh_vs.comentario, rh_empleado.nombre , contratado.nombre as contratado FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_tipo tipo ON tipo.idTipo = rh_vsp.solicitud INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado LEFT JOIN rh_empleado ON rh_empleado.idEmpleado = rh_vsp.idEmpleado  LEFT JOIN rh_empleado as contratado ON contratado.idEmpleado = rh_vsp.idContratado WHERE rh_vsp.idSolicitud = '".$id."' ".(($RHRole != 1 && $RHRole != 4) ? " AND rh_vsp.idSucursal ".$strValSucursales:"").";");
		
		return view('vacantes.detalle', ['solicitud' => $solicitud[0], 'partidas' => $partidas, 'role' => session('RHRole')]);
	}


	public function actualizaEmpleado(Request $request)
	{
		if( !empty($request->input('idEmpleado')))
		{
			DB::enableQueryLog();
			$user = Auth::user();
			$idUsuario = $user->id;
			$valor=$request->input('valor'); 

			$empleado = DB::select('select rh_empleado.nombre, rh_oficinas.nombre sucursal, rh_puesto.nombre puesto  from rh_empleado INNER JOIN rh_oficinas ON rh_empleado.idSucursal = rh_oficinas.id INNER JOIN rh_puesto ON rh_puesto.idPuesto = rh_empleado.idPuesto WHERE idEmpleado = ?', [$request->input('idEmpleado')]);

			switch($request->input('dato')){
				case 1:
					$campo="nombre";
					$valorAnterior = $empleado[0]->nombre;
				break;
				case 2:
					$campo="idPuesto";
					$valorAnterior = $empleado[0]->puesto;
				break;
				case 3:
					$campo="idSucursal";
					$valorAnterior = $empleado[0]->sucursal;
				break;
			}

			DB::update('update rh_empleado set '.$campo.' = ? where idEmpleado = ?', [$valor,$request->input('idEmpleado')]);

			DB::insert('insert into rh_vacante_log (idEmpleado, valorAnterior, idUsuario, fechaCrea, horaCrea, idAccion) values (?, ?, ?, ?, ?, ?)', [$request->input('idEmpleado'),$valorAnterior, $idUsuario ,date("Y-m-d"),date("H:i:s"),$request->input('dato')]);
			
			return "{ \"success\": true, \"$campo\": \"$valor\" }";
		}
		else 
		{
			return "{ \"success\": false }";
		}
	}

	public function saveBaja(Request $request){
		if( !empty($request->input('id')))
		{
			DB::enableQueryLog();
			$user = Auth::user();
			$idUsuario = $user->id;
			$isValid = true;

			if($request->input('accion') == 1)
			{
				DB::table('rh_empleado')
				->where('idEmpleado', $request->input('id'))
				->update(['estado' => 0]);
			}
			else if($request->input('accion') == 2){
				DB::table('rh_empleado')
				->where('idEmpleado', $request->input('id'))
				->update(['estado' => 1]);
			}
			else
			{
				return "{ \"success\": false }";
			}

			DB::insert('insert into rh_empleado_baja (idEmpleado, comentario, idUsuario, fecha, hora) values (?, ?, ?, ?, ?)', [$request->input('id'),$request->input('comentario'), $idUsuario ,date("Y-m-d"),date("H:i:s")]);
			$lid = DB::getPdo()->lastInsertId();
			
			return "{ \"success\": true }";
		}
		else 
		{
			return "{ \"success\": false }";
		}
	}
	
	public function updateVacante(Request $request)
	{
		if( !empty($request->input('partida')) && !empty($request->input('accion')) )
		{
			DB::enableQueryLog();
			$user = Auth::user();
			$idUsuario = $user->id;
			$isValid = true;

			$partida = DB::select("SELECT rh_vsp.idEmpleado, rh_vsp.solicitud, rh_vsp.idPartida, rh_vsp.idPuesto, rh_vsp.idSucursal, rh_p.nombre puesto, rh_vsp.idContratado FROM rh_vacante_solicitud_partida rh_vsp INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto WHERE idPartida =".$request->input('partida').";");

			if($request->input('accion') == 4)
			{
				$emps = DB::select("SELECT * FROM rh_empleado WHERE nombre = '".$request->input('empleado')."'");
				if(count($emps)>0)
				{
					$isValid = false;
				}
				else
				{
					
					if(!empty($partida)){
						if($partida[0]->solicitud == 1 ||$partida[0]->solicitud == 2 || $partida[0]->solicitud == 3)
						{
							DB::insert('insert into rh_empleado (idSucursal, idPuesto, puesto, nombre, idUsuario, fechaCrea, horaCrea,estado) values (?, ?, ?, ?, ?, ?, ?, ?)', [$partida[0]->idSucursal,$partida[0]->idPuesto,$partida[0]->puesto, $request->input('empleado'), $idUsuario, date("Y-m-d"),date("H:i:s"),3]);
							$lid = DB::getPdo()->lastInsertId();
							if(!empty($lid))
							{

								DB::table('rh_vacante_solicitud_partida')
								->where('idPartida', $request->input('partida'))
								->update(['idContratado' => $lid]);
								
								if(!empty($partida[0]->idEmpleado))
								{
									DB::table('rh_empleado')
									->where('idEmpleado', $partida[0]->idEmpleado)
									->update(['estado' => 2, 'fechaSolBaja' => date('Y-m-d'), 'horaSolBaja' => date('H:i:s')]);
								}

							}
							else {
								$isValid = false;
							}
							
						}
					}
					else
					{
						$isValid = false;
					}
				}
			}
			
			if($request->input('accion') == 7)
			{
				DB::table('rh_empleado')
				->where('idEmpleado', $partida[0]->idContratado)
				->update(['estado' => 2]);
			}
			if($request->input('accion') == 6)
			{
				DB::table('rh_empleado')
				->where('idEmpleado', $partida[0]->idContratado)
				->update(['estado' => 1]);
			}
			
			if($isValid)
			{
				DB::table('rh_vacante_solicitud_partida')
				->where('idPartida', $request->input('partida'))
				->update(['estado' => $request->input('accion'), 'lastUpdateDate' => date("Y-m-d"), 'lastUpdateTime' => date("H:i:s"), 'idUserUpdate' => $idUsuario]);
				if($request->input('accion') == 9)
				{
					DB::table('rh_vacante_solicitud_partida')
					->where('idPartida', $request->input('partida'))
					->update(['ingreso' => $request->input('fingreso')]);

				}
				return "{ success: true }";
			}			
			else
			{
				return "{ success: false }";
			}
		}
		else
		{
			return "{ success: false }";
		}
	}

	public function autRequest($idSolicitud, $idAutoriza, $accion)
	{
		
		if(!empty($idSolicitud) && !empty($idAutoriza))
		{
			$sql = "SELECT rh_vacante_solicitud_autorizacion.idAutorizacion, rh_vacante_solicitud_autorizacion.autoriza, rh_vacante_solicitud_partida.*, rh_empleado.idPuesto FROM rh_vacante_solicitud_autorizacion INNER JOIN rh_vacante_solicitud_partida ON rh_vacante_solicitud_partida.idPartida = rh_vacante_solicitud_autorizacion.idPartida LEFT JOIN rh_empleado ON rh_vacante_solicitud_partida.idEmpleado = rh_empleado.idEmpleado WHERE rh_vacante_solicitud_autorizacion.idPartida = ?;";
			$conaut = DB::select($sql,[$idSolicitud]);
			if(!empty($conaut)){
				if($conaut[0]->autoriza == 2)
				{
					$sql = "UPDATE rh_vacante_solicitud_autorizacion SET idAutoriza= ?, fecha=?, hora=?, autoriza=?  WHERE idPartida=?;";
					$accesQuery = DB::update($sql, [$idAutoriza, date("Y-m-d"), date("H:i:s"), $accion,$idSolicitud]);

					$sql = "SELECT users.name solicita, tipo.tipo solicitud, estado.estado, rh_vsp.idSolicitud, rh_vsp.idSucursalTrans, rh_vsp.idEmpleado, rh_vsp.idPartida, rh_vs.fechaCrea, rh_o.nombre AS sucursal, rh_o2.nombre AS transferencia,  rh_p.nombre AS puesto, rh_vs.comentario, rh_empleado.nombre FROM rh_vacante_solicitud_partida AS rh_vsp INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN users ON users.id = rh_vs.idUsuario INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_tipo tipo ON tipo.idTipo = rh_vsp.solicitud INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado LEFT JOIN rh_empleado ON rh_empleado.idEmpleado = rh_vsp.idEmpleado LEFT JOIN rh_oficinas AS rh_o2 ON rh_o2.id = rh_vsp.idSucursalTrans WHERE rh_vsp.idPartida = ?;";
					$sol = DB::select($sql, [$idSolicitud]);

					if($conaut[0]->solicitud == 4 || $conaut[0]->solicitud ==  5)
					{
						if($accion == 1)
						{
							if(!empty($sol))
							{
								
								$sql = "UPDATE rh_empleado SET idSucursal = ? WHERE idEmpleado = ?;";
								$accesQuery = DB::update($sql,[$sol[0]->idSucursalTrans,$sol[0]->idEmpleado]);
								
								$sql = "UPDATE rh_vacante_solicitud_partida SET estado = ? WHERE idPartida = ?;";
								$accesQuery = DB::update($sql,[6,$idSolicitud]);

								$sql = "INSERT INTO rh_vacante_solicitud_partida (idSolicitud,idPuesto, idSucursal, idSucursalTrans, idArea, idDepartamento, solicitud, idEmpleado, lastUpdateDate, lastUpdateTime, idUserUpdate, ingreso, idContratado, estado) VALUES ( ? , ?, ? , 0, ?, ? , 1 , 0 , ?, ? , ? , ?, 0, 1);";
								$regSol = DB::insert($sql, [$conaut[0]->idSolicitud, $conaut[0]->idPuesto, $conaut[0]->idSucursal, $conaut[0]->idArea, $conaut[0]->idDepartamento, date("Y-m-d"), date("H:i:s"), $idAutoriza, date("Y-m-d")]);

							}
							return "<h2>Datos guardados correctamente</h2> <br>Se <strong>AUTORIZO</strong> la solicitud: <br><strong>".$sol[0]->solicitud."</strong> para ".$sol[0]->nombre." de ".$sol[0]->sucursal." a ".$sol[0]->transferencia."<br>Creada por : <br><strong>". $sol[0]->solicita."</strong>";
						}
						else 
						{
							$sql = "UPDATE rh_vacante_solicitud_partida SET estado = ? WHERE idPartida = ?;";
							$accesQuery = DB::update($sql,[5,$idSolicitud]);

							return "<h2>Datos guardados correctamente</h2> <br><br> Se <strong>RECHAZO</strong> la solicitud: <br><strong>".$sol[0]->solicitud."</strong> para ".$sol[0]->nombre." de ".$sol[0]->sucursal." a ".$sol[0]->transferencia."<br>Creada por : <br><strong>". $sol[0]->solicita."</strong>";
						}
					}
					else if($conaut[0]->solicitud == 3)
					{
						if($accion == 1)
						{
							if(!empty($sol))
							{
								$sql = "UPDATE rh_vacante_solicitud_partida SET estado = ? WHERE idPartida = ?;";
								$accesQuery = DB::update($sql,[1,$idSolicitud]);
								$sql = "UPDATE rh_plazas_autorizadas SET cantidad = cantidad+1 WHERE idPuesto = ? AND idSucursal=?;";
								$accesQuery = DB::update($sql,[$conaut[0]->idPuesto ,$conaut[0]->idSucursal ]);
							}
							return "<h2>Datos guardados correctamente</h2> <br>Se <strong>AUTORIZO</strong> la solicitud: <br><strong>".$sol[0]->solicitud."</strong> para ".$sol[0]->nombre." de ".$sol[0]->sucursal." a ".$sol[0]->transferencia."<br>Creada por : <br><strong>". $sol[0]->solicita."</strong>";
						}
						else 
						{
							# code...
						}
					}
					else {
						return "<h2>Solicitud de autorizacion inexistente, por favor consulte al administrador</h2>";
					}
				} else {
					return "<h2>Respuesta enviada con anterioridad</h2>";
				}
			}
			else {
				return "<h2>Solicitud de autorizacion inexistente, por favor consulte al administrador</h2>";
			}	
		}
		else 
		{
		 	return "Error, intentelo mas tarde....";
		}
	}

	public function sendAuths()
	{
		$recArray = array();
		$recArray[] = "ml@maison-kayser.com.mx";
		
		$partidas = DB::select("SELECT users.name solicita, tipo.tipo solicitud, estado.estado, rh_vsp.idSolicitud, rh_vsp.idPartida, rh_vs.fechaCrea, rh_o.nombre AS sucursal, rh_o2.nombre AS transferencia,  rh_p.nombre AS puesto, rh_vs.comentario, rh_empleado.nombre FROM rh_vacante_solicitud_autorizacion AS rh_aut INNER JOIN rh_vacante_solicitud_partida AS rh_vsp ON rh_aut.idPartida = rh_vsp.idPartida INNER JOIN rh_vacante_solicitud AS rh_vs ON rh_vsp.idSolicitud = rh_vs.idSolicitud INNER JOIN users ON users.id = rh_vs.idUsuario INNER JOIN rh_puesto AS rh_p ON rh_p.idPuesto = rh_vsp.idPuesto INNER JOIN rh_oficinas AS rh_o ON rh_o.id = rh_vsp.idSucursal INNER JOIN rh_vacante_tipo tipo ON tipo.idTipo = rh_vsp.solicitud INNER JOIN rh_vacante_estado estado ON estado.idEstado = rh_vsp.estado LEFT JOIN rh_empleado ON rh_empleado.idEmpleado = rh_vsp.idEmpleado LEFT JOIN rh_oficinas AS rh_o2 ON rh_o2.id = rh_vsp.idSucursalTrans WHERE rh_aut.enviado =0;");

		foreach($partidas as $partida)
		{
			$url = url('/detallevacante/'.$partida->idSolicitud );

			Mail::send('vacantes.mailAuth', ['url' => $url, 'partida' => $partida], function ($message) use ($recArray)
			{
				$message->from('reportes@prigo.com.mx', 'Reportes PRIGO');
				$message->to($recArray);
				$message->bcc("rgallardo@prigo.com.mx");
				$message->subject("Nueva solicitud de autorizacion recibida");
			});
			$sql = "UPDATE rh_vacante_solicitud_autorizacion SET enviado= 1, fechaEnvio=?, horaEnvio=? WHERE idPartida=?;";
			$accesQuery = DB::update($sql, [date("Y-m-d"), date("H:i:s"), $partida->idPartida]);
		}

	}
}