<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UsuariosController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        return view('usuarios.usuarios', []);
    }

    public function getUsers(Request $request)
    {
		$idUsuario = Auth::id();
		$draw = !empty($request->input('draw'))?$request->input('draw'):1;
        $allusers = DB::select("SELECT id, name, email  FROM users ;");
	
		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allusers),
			'recordsFiltered' => count($allusers),
            'data' => $allusers
            ]);
    }
    
	public function getDetalleUser($id)
	{
        $users = DB::select("SELECT * FROM users WHERE id =  '".$id."'");
		return view('usuarios.editar', ['usuarios' => $users]);
	}
	
	public function editUser(request $request)
	{
		$json_response = response()->json($request);
		$json_strings = (json_decode($json_response->getContent()));
		$updatedBy = strval(Auth::id());
		$idUsuario = $json_strings->id_usuario;
		$nombreUsuario = $json_strings->nombre_usuario;
		$correoUsuario = $json_strings->correo_usuario;
		$estadoUsuario = $json_strings->estado_usuario;
		$updateDate = date("Y-m-d H:i:s");

		$sql = DB::table('users')
				->where('id', $idUsuario)
				->update(['name' => $nombreUsuario, 'email' => $correoUsuario,'estado'=> $estadoUsuario, 'updated_at' => $updateDate, 'estado'=> $estadoUsuario,'updated_by' => $updatedBy
				]);
	}

	public function changuePass(request $request){
		$json_response = response()->json($request);
		$json_strings = (json_decode($json_response->getContent()));
		$updatedBy = strval(Auth::id());
		$updateDate = date("Y-m-d H:i:s");
		$idUsuario = $json_strings->id_usuario;
		$nuePass = $json_strings->nue_pass;
		$correoUsuario = $json_strings->correo_usuario;
		
		$sql = DB::table('users')
				->where('id', $idUsuario)
				->update(['password' => bcrypt($nuePass), 'updated_at' => $updateDate,'updated_by' => $updatedBy]);

		//--- Not working in locale ----//
		// Mail::send('mails.send', ['pass' => $nuePass], function ($message){
		// 	$message->from('reportes@prigo.com.mx', 'Reportes PRIGO');
		// 	$message->to('ogabriel@prigo.com.mx);	 //----email static for test, change to dynamic -----//			
		// 	$message->subject("Contraseña PRIGO");
		// });


        // return response()->json(['message' => 'Request completed']);

	
	}

	public function getFormNewUser(){
		return view('usuarios.nuevousuario');
	}

	public function guardarSesion(Request $request){
		$json_response = response()->json($request);
		$json_strings = (json_decode($json_response->getContent()));

		return $json_strings;
		
	}



}

