<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SesionAuthController extends Controller
{

    public function __construct()
    {
       $this->middleware('auth');
    }

    public function getIndexSesion()
    {

        return view('usuarios.sesionesUsuarios', []);
    }

    public function getSesiones(Request $request)
    {
		$idUsuario = Auth::id();
		$draw = !empty($request->input('draw'))?$request->input('draw'):1;

		$allUsers = DB::table('sesiones')->orderBy('fechaInicio', 'desc')->get();

		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allUsers),
			'recordsFiltered' => count($allUsers),
            'data' => $allUsers
            ]);
    }
}