<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MusicaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['setStatus']]);
    }
	
	public function index(){
		
		$equipos = DB::select("SELECT idEquipo, estado, fechaEstado, horaEstado, volumen, cancion FROM musica_equipo ORDER BY fechaEstado DESC, horaEstado DESC;");
		
		return view("musica.index", ['equipos' => $equipos]);
	}
	
    public function setStatus(Request $request){

        $body = json_decode($request->getContent());
        DB::update("UPDATE musica_equipo SET estado=?, fechaEstado=?, horaEstado=?, cancion=?, volumen=? WHERE idEquipo=? ", [$body->volumio->status,date("Y-m-d"), date("H:i:s"), $body->volumio->title, $body->volumio->volume ,$body->storeId]);

        if( $body->storeId == "KAYSELK")
        {
			echo "echo \"SUCCESS\"";
		//echo "/volumio/app/plugins/system_controller/volumio_command_line_client/volumio.sh volume 70";
            //$body->storeId == "KAYSARTZ" ||
			//echo "/volumio/app/plugins/system_controller/volumio_command_line_client/volumio.sh vrestart > /home/volumio/cron.log";
	    //echo "/volumio/app/plugins/system_controller/volumio_command_line_client/volumio.sh play";
            //echo "/sbin/shutdown -r now > /home/volumio/cron.log";
            //echo "echo \"SUCCESS\" > /home/volumio/cron.log";
        }
        else
        {
            echo "echo \"SUCCESS\"";
        }
  /*      if( $body->storeId == "KAYSTVA")
        {
            echo "/volumio/app/plugins/system_controller/volumio_command_line_client/volumio.sh volume 65 > /home/volumio/cron.log";
        }
        else
        {
            */
            
            /*
        }*/
        //echo "/volumio/app/plugins/system_controller/volumio_command_line_client/volumio.sh volume 70";
        // /volumio/app/plugins/system_controller/volumio_command_line_client/volumio.sh vrestart
    }
}
