<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class UberController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth', ['except' => ['getNotification','getUberRequest']]);
    }

    public function getMenu()
    {
        $client = new \GuzzleHttp\Client();
        /************************************************** DATA TO GET ACCESS TOKEN
        $url = "https://login.uber.com/oauth/v2/token";
        $client_id ="Tdd_ecwCorlu3-09e2tGX4CprMIwbbFk";
        $client_secret ="JzQ0IdKCjXzoCyttVt_IzC0eoOtaSQLavnLe2UxK";       
        $grant_type ="client_credentials";
        $scope ="eats.store";
        $response = $client->request('POST', $url, [
            'form_params' => [
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'grant_type' => $grant_type,
                'scope' => $scope,
            ]
        ]);
        $response = $response->getBody()->getContents();
        echo '<pre>';
        print_r($response);
        echo '</pre>';
        */

        $acces_token="JA.VUNmGAAAAAAAEgASAAAABwAIAAwAAAAAAAAAEgAAAAAAAAHQAAAAFAAAAAAADgAQAAQAAAAIAAwAAAAOAAAApAAAABwAAAAEAAAAEAAAAKIhMwl3tCIUPdJ8pBk-KK2AAAAA3ep0I4K6uaxdUvnWS459cGZ39aynJXLvSETHZ3evWMefFp6MW3Bz7ONNIMTDncOuuLSSG54bSwoXcICFm2WlOxgMNzDJVy2fwSkDtnkTjGm_RbQfyTKNChcjCJOK-nHlWvx6Elu6TF3RswWB6WFJtIhLBEofvVehSxSiQzj1TykMAAAAxrvvCVrCVAdhdRcoJAAAAGIwZDg1ODAzLTM4YTAtNDJiMy04MDZlLTdhNGNmOGUxOTZlZQ";
        $token_type="Bearer";
        $expires_in=2592000;
        $scope ="eats.store";
        /*********** NECESITAMOS OBTENER TODOS LOS IDS DE LAS STORES DE KAYSER 
         * TODO: PREGUNTAR COMO LOS VOY A OBTENER EN UN FUTURO
        */
        $eats_store_id = "1afb2aa8-89b2-4e6f-aea3-ccf99ef6e3f1";

        $url= "https://api.uber.com/v1/eats/stores/$eats_store_id";

        $res = $client->request('GET', $url, [
            'headers' => [
                'Authorization' => $token_type.' '.$acces_token,
                'Content-Type'  => 'application/json'
            ]
            ]);
        $status = $res->getStatusCode();
        // "200"
        echo $res->getHeader('content-type')[0];
        // 'application/json; charset=utf8'
        if($status == 200){
            $oUber = json_decode($res->getBody());
            echo "status: ".$oUber->status."<br>";
            echo "name: ".$oUber->status."<br>";
            echo "store_id: ".$oUber->status."<br>";
            if($oUber->status == "active")
            {
                $url= "https://api.uber.com/v1/eats/stores/".$eats_store_id."/menu";

                $resMenu = $client->request('GET', $url, [
                    'headers' => [
                        'Authorization' => $token_type.' '.$acces_token,
                        'Content-Type'  => 'application/json'
                    ]
                ]);
                $status = $resMenu->getStatusCode();
                if($status == 200)
                {
                    //$oMenuUber = json_decode($resMenu->getBody());
                    echo "<br>";
                    echo $resMenu->getBody();
                }
            }

        }        

        return "";
    }
    public function putMenu()
    {
        return view("uber.index",[]);
    }

    public function menu()
    {
        $sql = "SELECT idSeccion AS id, title AS seccion FROM uber_section WHERE estado = 1;";
        $secs = DB::select($sql);
        return view("uber.menu",[ "secciones"=> $secs]);
    }

    public function getSubseccion(Request $request)
    {
        if($request->input('seccion'))
        {
            $sql = "SELECT idSubSection AS id, title AS subseccion FROM uber_subsection WHERE idSection = ?;";
            $ssecs = DB::select($sql,[$request->input('seccion')]);
            if(!empty($ssecs[0]))
            {
                return  response()->json(['data' => $ssecs]);
            }
            else 
            {
                return  response()->json(['data' => []]);
            }
        }

        return  response()->json(['error' => 'VAcio', 'data' => []]);
    }

    public function getNotification(Request $request)
    {
        Log::info('Uber content Type: '.$request->headers->get('Content-Type'));
        if($request->headers->get('Content-Type')=='application/json')
        {
            $bodyContent = json_decode($request->getContent());
        }
        else 
        {
            $bodyContent = $request->json();
        }

        Log::info('Uber info receipt: '.json_encode($bodyContent));
        DB::insert("INSERT INTO uber_prerequest VALUES (NULL,?,?,?,?,?,?,?,?,?,?,0)",[$bodyContent->event_id,$bodyContent->resource_href,$bodyContent->meta->status,$bodyContent->meta->rider_id,$bodyContent->meta->user_id,$bodyContent->meta->resource_id,$bodyContent->event_type,$bodyContent->event_time,date("Y-m-d"), date("H:i:s")]);
        
        return response('', 200);
    }

    public function getUberRequest(Request $request)
    {
        Log::info('Uber content Type: '.$request->headers->get('Content-Type'));
        if($request->headers->get('Content-Type')=='application/x-www-form-urlencoded')
        {
            $store = $request->input("store_id");
        }
        else 
        {
            $store = $request->input("store_id");
        }

        $pendientes = DB::select("SELECT * FROM uber_prerequest WHERE user_id = ? ORDER BY idRequest DESC;",[$store]);
        //$pendientes[0]->resource_id
        return response($pendientes[0]->resource_id,200);
    }

    public function index()
    {
        $client = new \GuzzleHttp\Client();

        $url = "https://login.uber.com/oauth/v2/token";
        $client_id ="Tdd_ecwCorlu3-09e2tGX4CprMIwbbFk";
        $client_secret ="JzQ0IdKCjXzoCyttVt_IzC0eoOtaSQLavnLe2UxK";       
        $grant_type ="client_credentials";
        $scope ="eats.store";
        $response = $client->request('POST', $url, [
            'form_params' => [
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'grant_type' => $grant_type,
                'scope' => $scope,
            ]
        ]);
        
        $token = json_decode($response->getBody()->getContents());
        
        $acces_token = $token->access_token;
        

        $sql = "SELECT * FROM uber_section WHERE idSeccion in (1000) ;";
        $sec = DB::select($sql);

        $ubrTxt= "";
        $ubrTxt.= '{
                "sections": [ ';
                foreach($sec as $idSec=>$osec){
                    if($idSec>0)
                        $ubrTxt.= ', ';
                    $ubrTxt.= '{ ';
                    $ubrTxt.= '"title": "'.$osec->title.'", ';
                    $ubrTxt.= '"subtitle": "'.$osec->subtitle.'", ';
                    $ubrTxt.= '"service_availability": ['; 
                    $sql = "SELECT * FROM uber_service_availability WHERE idSeccion = ?;";
                    $sav = DB::select($sql,[$osec->idSeccion]);
                    foreach($sav as $idSav => $osav)
                    {
                        if($idSav>0)
                            $ubrTxt.= ', ';
                        $ubrTxt.= '{
                                "enabled": '.($osav->enabled?"true":"false").', '; 
                        $ubrTxt.=   '"day_of_week": "'.$osav->day_of_week.'", '; 
                        $ubrTxt.=   '"time_periods": [';
                        $sql = "SELECT * FROM uber_time_periods WHERE idServiceAv = ?;";
                        $tpers = DB::select($sql,[$osav->idServiceAv]);
                        foreach($tpers as $idTpr => $otpers)
                        {
                            if($idTpr>0)
                                $ubrTxt.= ', ';
                            $ubrTxt.= '{ "start_time": "'.substr ($otpers->start_time,0,5).'", ';
                            $ubrTxt.= '"end_time": "'. substr ($otpers->end_time,0,5).'" } ';
                            
                        }
                        $ubrTxt.=   ']';
                        $ubrTxt.= '}';
                    }
                    $ubrTxt.= '],';
                    $ubrTxt.= '"subsections": ['; 

                    $sql = "SELECT * FROM uber_subsection WHERE idSection = ?;";
                    $ssec = DB::select($sql,[$osec->idSeccion]);
                    foreach($ssec as $idSSec => $ossec)
                    {
                        if($idSSec>0)
                            $ubrTxt.= ', ';
                        
                        $ubrTxt.= '{ '; 
                        $ubrTxt.=   '"title": "'.$ossec->title.'", '; 
                        $ubrTxt.=   '"items": [';
                        $sql = "SELECT * FROM uber_items WHERE idSubSection = ?;";
                        $itms = DB::select($sql,[$ossec->idSubSection]);
                        foreach($itms as $idItm => $oitms)
                        {
                            if($idItm>0)
                                $ubrTxt.= ', ';
                            $ubrTxt.= '{ "suspend_until": '.($oitms->suspend_until?$oitms->suspend_until:'null').', ';
                            $ubrTxt.= '"title": "'.$oitms->title.'", ';
                            $ubrTxt.= '"disable_instructions": '.($oitms->disable_instructions?"true":"false").', ';
                            $ubrTxt.= '"price": '.$oitms->price.', ';
                            $ubrTxt.= '"var_rate_percentage": '.(empty($oitms->var_rate_percentage)?"null":$oitms->var_rate_percentage).', ';
                            $ubrTxt.= '"image_url": '.(empty($oitms->image_url)?"null":'"'.$oitms->image_url.'"').', ';
                            $ubrTxt.= '"currency_code": '.(empty($oitms->currency_code)?"MXN":'"'.$oitms->currency_code.'"').', ';
                            $ubrTxt.= '"item_id": "'.$oitms->item_id.'", ';
                            $ubrTxt.= '"tax_rate": '.(empty($oitms->tax_rate)?"0":$oitms->tax_rate).', ';
                            $ubrTxt.= '"external_id": '.(empty($oitms->external_id)?"null":'"'.$oitms->external_id.'"').', ';
                            $ubrTxt.= '"nutritional_info": {}, ';
                            $ubrTxt.= '"item_description": '.(empty($oitms->item_description)?"null":'"'.$oitms->item_description.'"').', ';
                            $ubrTxt.= '"is_alcohol": '.(empty($oitms->is_alcohol)?"false":"true").', ';
                            $ubrTxt.= '"customizations": [';

                            $sql = "SELECT * FROM uber_customizations WHERE idItem = ?;";
                            $custs = DB::select($sql,[$oitms->idItem]);
                            foreach($custs as $idCus => $ocust)
                            {
                                if($idCus>0)
                                    $ubrTxt.= ', ';
                                $ubrTxt.= '{';
                                $ubrTxt.= '"max_permitted": '.$ocust->max_permitted.', ';
                                $ubrTxt.= '"min_permitted": '.$ocust->min_permitted.', ';
                                $ubrTxt.= '"title": "'.$ocust->title.'", ';
                                $ubrTxt.= '"customization_options": [';
                                    $sql = "SELECT * FROM uber_customization_options WHERE idCustomization = ?;";
                                    $ctypes = DB::select($sql,[$ocust->idCustomization]);
                                    foreach($ctypes as $idCty => $octype)
                                    {
                                        if($idCty>0)
                                            $ubrTxt.= ', ';
                                        $ubrTxt.= '{';
                                        $ubrTxt.= '"suspend_until":'.(empty($octype->suspend_until)? "null":$octype->suspend_until).', ';
                                        $ubrTxt.= '"title":'.(empty($octype->title)? "null":'"'.$octype->title.'"' ).', ';
                                        $ubrTxt.= '"price":'.$octype->price.', ';
                                        $ubrTxt.= '"external_id": "'.$octype->external_id.'", ';
                                        $ubrTxt.= '"nutritional_info": {}';
                                        $ubrTxt.= '}';
                                    }
                                $ubrTxt.= '] ';
                                $ubrTxt.= '} ';
                            }
                            $ubrTxt.= '] ';
                            $ubrTxt.= ' }';
                            
                        }
                        $ubrTxt.= ' ] ';                         
                        $ubrTxt.= '} ';                        
                        
                    }
                    $ubrTxt.= '] ';
                    $ubrTxt.=' } ';
                }
            $ubrTxt.=    '] 
            }';
            //echo $ubrTxt;
           $client = new \GuzzleHttp\Client();
//            $acces_token="JA.VUNmGAAAAAAAEgASAAAABwAIAAwAAAAAAAAAEgAAAAAAAAHQAAAAFAAAAAAADgAQAAQAAAAIAAwAAAAOAAAApAAAABwAAAAEAAAAEAAAAKIhMwl3tCIUPdJ8pBk-KK2AAAAA3ep0I4K6uaxdUvnWS459cGZ39aynJXLvSETHZ3evWMefFp6MW3Bz7ONNIMTDncOuuLSSG54bSwoXcICFm2WlOxgMNzDJVy2fwSkDtnkTjGm_RbQfyTKNChcjCJOK-nHlWvx6Elu6TF3RswWB6WFJtIhLBEofvVehSxSiQzj1TykMAAAAxrvvCVrCVAdhdRcoJAAAAGIwZDg1ODAzLTM4YTAtNDJiMy04MDZlLTdhNGNmOGUxOTZlZQ";
            $token_type="Bearer";
            $expires_in=2592000;
            $scope ="eats.store";
    
            $eats_store_id = "1afb2aa8-89b2-4e6f-aea3-ccf99ef6e3f1";
    
            $url= "https://api.uber.com/v1/eats/stores/$eats_store_id/menu";
            $res = $client->request('PUT', $url, [
                'headers' => [
                    'Authorization' => $token_type.' '.$acces_token,
                    'Content-Type'  => 'application/json'
                ],
                'body' => $ubrTxt
                ]);
            $status = $res->getStatusCode();
            // "200"
            echo $status . "<br>";
            echo $res->getHeader('content-type')[0]. "<br>";
            echo $ubrTxt;
            
    }
}
