<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EncuestaController extends Controller
{
    public function index($id, $nt = null)
    {
        if($nt == null)
            return view('experiencia.index')->with('id_sucursal', $id);
        else
            return view('experiencia.index_du')->with('id_sucursal', $id)->with('num_ticket', $nt);
   }

   public function savePoll(Request $request)
   {
    $json_response = response()->json($request);
    $json_strings = (json_decode($json_response->getContent()));
    
    $tipoE = $json_strings->tipoEncuesta;
    $numTicket = $json_strings->numTicket;
    $idSucursal = $json_strings->id_sucursal;
    $fecha = date("Y-m-d");

 
    if($tipoE == "1"){
        $p_v1 = $json_strings->pv1;
        $p_v2 = $json_strings->pv2;
        
        if(!empty($numTicket)&&!empty($idSucursal)&&!empty($p_v1)&&!empty($p_v2)&&!empty($fecha)){
            $sql = DB::table('encuestas_vitrina')->insert(['idSuc'=> $idSucursal, 'numTicket' => $numTicket, 'p1' => $p_v1,'p2' => $p_v2, 'fechaReg'=> $fecha]);
            dd($sql);
        }
    }else if($tipoE == "2"){
        $p_1 = $json_strings->p1;
        $p_2 = $json_strings->p2;
        $p_3 = $json_strings->p3;
        $p_4 = $json_strings->p4;
        $p_5 = $json_strings->p5;
        $p_6 = $json_strings->p6;
        $p_7 = $json_strings->p7;
        $p_8 = $json_strings->p8;
        if(!empty($numTicket)&&!empty($idSucursal)&&!empty($p_1)&&!empty($p_2)&&!empty($p_3)&&!empty($p_4)&&!empty($p_5)&&!empty($p_6)&&!empty($p_7)&&!empty($p_8)){
            $sql = DB::table('encuestas_rest')->insert(['idSuc'=> $idSucursal, 'numTicket' => $numTicket, 'p1' => $p_1,'p2' => $p_2,'p3' => $p_3,'p4' => $p_4,'p5' => $p_5, 'p6' => $p_6, 'p7' => $p_7, 'p8' => $p_8, 'fechaReg'=> $fecha]);
        }
        

    }

   }
   





}
