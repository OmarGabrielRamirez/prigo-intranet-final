<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class QRGeneratorController extends Controller
{
	
	public function __construct()
    {
		$this->middleware('auth');
		
	}
	
	public function index()
    {
        return view('qrgen.index');
    }
	
	public function download(Request $request) 
	{
		$nombre = empty($request->input("nombre"))?"":$request->input("nombre");
		$apellidos = empty($request->input("nombre"))?"":$request->input("apellidos");
		$tel = empty($request->input("nombre"))?"":$request->input("tel");
		$logo = empty($request->input("logo"))?0:1;
		$mail = empty($request->input("nombre"))?"":$request->input("mail");
		$org = empty($request->input("nombre"))?"Maison Kayser México":$request->input("org");
		$puesto = empty($request->input("nombre"))?"":$request->input("puesto");
		$url = empty($request->input("nombre"))?"https://wwww.maison-kayser.com.mx":$request->input("url");
		
		$codigo = 'BEGIN:VCARD'."\n".'VERSION:3.0';
		if(!empty($nombre))
		{
			$codigo .= "\n".'N:'.$apellidos.';'.$nombre.';;';
			$codigo .= "\n".'FN:'.$nombre. ' '.$apellidos;		
		}
		
		if(!empty($org))
		{
			$codigo .= "\n".'ORG:'.$org;
		}
		
		if(!empty($mail))
		{
			$codigo .= "\n".'EMAIL:'.$mail;
		}
		
		if(!empty($tel))
		{
			$codigo .= "\n".'TEL;WORK;VOICE:'.$tel; //(55) 4341-8330 
		}
		
		if(!empty($puesto))
		{
			$codigo .= "\n".'TITLE:'.$puesto;
		}
		
		if(!empty($url))
		{
			$codigo .= "\n".'URL:'.$url;
		}
		
		$codigo .= "\n".'END:VCARD';
		
		$codigo = utf8_encode($codigo);
		
		if(!empty($logo))
		{
			$image = \QrCode::format('png')
			->merge('http://intranet.prigo.com.mx/downloads/logo_MK.png', 0.2, true)
			->size(400)->errorCorrection('H')
			->generate($codigo);			
		}
		else
		{
			$image = \QrCode::format('png')
			->size(400)->errorCorrection('H')
			->generate($codigo);
		}
		
		return response($image)->header('Content-type','image/png');
		
	}
	
}