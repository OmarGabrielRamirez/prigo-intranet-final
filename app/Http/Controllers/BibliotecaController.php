<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BibliotecaController extends Controller
{
    public function __construct()
    {
       //$this->middleware('auth', ['except' => []]);
    }
    
    public function index()
    {
        return view('biblioteca.index', []);
    }
}