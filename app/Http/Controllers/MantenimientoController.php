<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Storage;

class MantenimientoController extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');
        $this->middleware(function ($request, $next) {
			$idUsuario = Auth::id();
			$user = Auth::user();
			$sql = "SELECT * FROM config_app_access WHERE idAplicacion=5 AND idUsuario = ? ";
			$accesQuery = DB::select($sql, [$idUsuario]);
			if(!empty($accesQuery))
			{
			   session(['MantoRole' => $accesQuery[0]->idRole]);
			   if($accesQuery[0]->idRole != 1)
			   {
					$sql = "SELECT group_concat(`idSucursal` separator ',') as `sucursales` FROM manto_sucursal_usuario WHERE idUsuario = ? GROUP BY idUsuario;";
					$sucursales = DB::select($sql, [$idUsuario]);
					if(!empty($sucursales))
					{
						session(['sucursales' => $sucursales[0]->sucursales]);
					}
			   }
			}

            return $next($request);
        });

    }
	
	public function index()
    {	
		$MantoRole = session('MantoRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		if($MantoRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) "; 
		$resueltos = 0;
		$abiertos= 0;
		$atrasados = 0;
		
		$sql = "SELECT SUM(atraso) n FROM (SELECT IF(HOUR(TIMEDIFF(NOW(),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0) atraso FROM manto_solicitud solicitud WHERE HOUR(TIMEDIFF(NOW(),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea))) > tiempo AND tiempo <> 0 AND estado IN (1,2) ".($MantoRole==1?"":" AND solicitud.idSucursal ". $strValSucursales)." ) atrasados GROUP BY atraso;";
		$tickets = DB::select($sql);
		
		if(!empty($tickets[0]))
		{
			$atrasados = $tickets[0]->n;
		}
		
		
		$sql = "SELECT SUM(tiempo) n FROM (SELECT IF(HOUR(TIMEDIFF(NOW(),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0) tiempo FROM manto_solicitud solicitud WHERE HOUR(TIMEDIFF(NOW(),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea))) < tiempo AND tiempo <> 0 AND estado IN (1,2)  ".($MantoRole==1?"":" AND solicitud.idSucursal ". $strValSucursales).") atrasados GROUP BY tiempo;";
		$tickets = DB::select($sql);
		
		if(!empty($tickets[0]))
		{
			$abiertos= $tickets[0]->n;
		}
		
		
		$sql = "SELECT COUNT(estado) n FROM manto_solicitud solicitud WHERE estado IN (3) ".($MantoRole==1?"":" AND solicitud.idSucursal ". $strValSucursales)." GROUP BY estado;";
		$tickets = DB::select($sql);
		
		if(!empty($tickets[0]))
		{
			$resueltos = $tickets[0]->n;
		}
		

		$sql = "SELECT sol.idTipProblema id, tipo.tipoProblema nombre ,COUNT(sol.idTipProblema) n FROM manto_solicitud sol INNER JOIN manto_tipo_problema tipo ON tipo.idTipProblema = sol.idTipProblema WHERE sol.estado != 4 ".($MantoRole==1?"":" AND sol.idSucursal ". $strValSucursales)." GROUP BY sol.idTipProblema, tipo.tipoProblema ORDER BY n DESC limit 0,5;";		
		$topProblema = DB::select($sql);

		$sql = "SELECT sol.idEquipo id, equipo.equipo nombre ,COUNT(sol.idEquipo) n FROM manto_solicitud sol INNER JOIN manto_equipo equipo ON equipo.idEquipo = sol.idEquipo WHERE sol.estado != 4 ".($MantoRole==1?"":" AND sol.idSucursal ". $strValSucursales)." GROUP BY sol.idEquipo, equipo.equipo ORDER BY n DESC limit 0,5;";		
		$topEquipo = DB::select($sql);
		

		$sql = "SELECT oficina.id, oficina.nombre , COUNT(sol.idSucursal) n FROM rh_oficinas oficina LEFT JOIN manto_solicitud sol ON oficina.id = sol.idSucursal WHERE ((sol.estado != 4 AND sol.estado != 3) OR sol.idSolicitud IS NULL) ".($MantoRole==1?"":" AND oficina.id ". $strValSucursales)." GROUP BY oficina.id, oficina.nombre ORDER BY n DESC;";
		$topSucursal = DB::select($sql);

		$sql = "SELECT tiempo, total, abiertos*100/total perAbierto, resueltoAtrasado*100/total perResueltoAtrasado, resueltos*100/total perResuelto,atrasados*100/total perAtraso  FROM (SELECT tiempo, COUNT(tiempo) total, SUM(IF(HOUR(TIMEDIFF(NOW(),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0)) atrasados, SUM(IF(HOUR(TIMEDIFF(NOW(),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))<solicitud.tiempo,1,0)) entiempo, SUM(IF(solicitud.estado=3,1,0)) resueltos, SUM(IF(solicitud.estado = 3, IF(HOUR(TIMEDIFF(CONCAT(solicitud.fechaTermina, ' ', solicitud.horaTermina),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo,1,0),0)) resueltoAtrasado, SUM(IF(NOT(solicitud.estado IN (3,4)),1,0)) abiertos FROM manto_solicitud solicitud WHERE estado != 4 ".($MantoRole==1?"":" AND solicitud.idSucursal ". $strValSucursales)." GROUP BY tiempo) tiempos;";
		$perTicket = DB::select($sql);

		return view('mantenimiento.index', ["abiertos" => $abiertos+$atrasados, "entiempo" => $abiertos,"atrasados"=> $atrasados, "resueltos"=> $resueltos, "topProblema"=> $topProblema, "topSucursal"=> $topSucursal, "perTicket" => $perTicket, "topEquipo" => $topEquipo]);
	}
	
	public function showNewRequestForm()
    {
		$MantoRole = session('MantoRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		if($MantoRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) "; 
		$sucursales = DB::select("SELECT id AS idSucursal, nombre FROM rh_oficinas WHERE ".( $MantoRole != 1 ? " id ".$strValSucursales . " AND " : ""  )." estado = 1  ORDER BY nombre;");	
		return view('mantenimiento.nuevo', ['sucursales' => $sucursales]);
	}

	public function showRequests()
    {		
		return view('mantenimiento.lista', ['estado' => 1]);
	}

	public function showLateRequests()
    {		
		return view('mantenimiento.lista', ['estado' => 2]);
	}

	public function showEndRequests()
    {		
		return view('mantenimiento.lista', ['estado' => 3]);
	}
	public function img()
	{
		return view('mantenimiento.img');
	}
	public function putImage(Request $request)
	{
		$sql = "SELECT NOW() hora;";
		$hora = DB::select($sql);
		return " php: ".date("H:i:s") . " - mysql :" . $hora[0]->hora;
		/*
		$lid = 7;
		$sucursal = 13;
		$uname  = "K Satelite";
		$sql = "SELECT T0.idUsuario, T2.name, T2.email FROM config_app_access T0 INNER JOIN users T2 ON T2.id = T0.idUsuario LEFT JOIN manto_sucursal_usuario T1 ON T1.idUsuario = T0.idUsuario  WHERE idAplicacion = 5 AND ( idRole = 1 OR (idRole IN (2,3) AND T1.idSucursal = $sucursal))";
							
		$tecnicos = DB::select($sql);
		
		$solicitud = DB::select("SELECT solicitud.idSolicitud, solicitud.fechaCrea fecha, IF(solicitud.tiempo = 24, '24 Hrs.', IF(solicitud.tiempo = 48, '48 Hrs.', IF(solicitud.tiempo=72,'72 Hrs.', IF(solicitud.tiempo=100,'Mas de 72 Hrs.','Indefinido')))) tiempo, HOUR(TIMEDIFF(NOW(),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea))) transcurrido, solicitud.comentario, users.name nombre ,rh_oficinas.nombre sucursal ,tipo.tipoProblema tipo, area.area, equipo.equipo, problema.problema, 'Pendiente' estado FROM manto_solicitud AS solicitud INNER JOIN rh_oficinas ON rh_oficinas.id = solicitud.idSucursal INNER JOIN manto_tipo_problema AS tipo ON tipo.idTipProblema = solicitud.idTipProblema INNER JOIN manto_area AS area ON area.idArea = solicitud.idArea INNER JOIN manto_equipo AS equipo ON equipo.idEquipo=solicitud.idEquipo INNER JOIN manto_problema AS problema ON problema.idProblema = solicitud.idProblema INNER JOIN users ON users.id = solicitud.idUsuario WHERE solicitud.idSolicitud = ?;",[$lid]);

		$tecArray = array();
		
		foreach($tecnicos as $tec)
		{
			if($tec->idUsuario != 9 || ($tec->idUsuario == 9 && $solicitud[0]->tiempo == "24 Hrs."))
				$tecArray[] = $tec->email;
		}
		
		$url = url('/mantenimiento/detalle/'.$lid);

		Mail::send('mantenimiento.mailRequest', ['url' => $url,'name' => $uname, 'comentario' => $solicitud[0]->comentario, 'solicitud' => $solicitud[0]], function ($message) use ($tecArray)
		{
			$message->from('reportes@prigo.com.mx', 'Reportes PRIGO');
			$message->to($tecArray);					
			$message->subject("Nueva solicitud de mantenimiento recibida");
		});

		return "<b>Enviado</b>";

		
		$directory  ="mantenimiento/imagenes/64";
		$files = Storage::disk('public')->files($directory);
		$url = Storage::url($files[0]);
        return "<img src='$url'>";	*/
	}
	public function requestDetail($id)
	{
		$MantoRole = session('MantoRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		if($MantoRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) "; 	

		$solicitud = DB::select("SELECT solicitud.idSolicitud, solicitud.fechaCrea fecha, IF(solicitud.tiempo = 24, '24 Hrs.', IF(solicitud.tiempo = 48, '48 Hrs.', IF(solicitud.tiempo=72,'72 Hrs.', IF(solicitud.tiempo=100,'Mas de 72 Hrs.','Indefinido')))) tiempo, HOUR(TIMEDIFF(NOW(),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea))) transcurrido, solicitud.comentario, users.name nombre ,rh_oficinas.nombre sucursal ,tipo.tipoProblema tipo, area.area, equipo.equipo, problema.problema, 'Pendiente' estado FROM manto_solicitud AS solicitud INNER JOIN rh_oficinas ON rh_oficinas.id = solicitud.idSucursal INNER JOIN manto_tipo_problema AS tipo ON tipo.idTipProblema = solicitud.idTipProblema INNER JOIN manto_area AS area ON area.idArea = solicitud.idArea INNER JOIN manto_equipo AS equipo ON equipo.idEquipo=solicitud.idEquipo INNER JOIN manto_problema AS problema ON problema.idProblema = solicitud.idProblema INNER JOIN users ON users.id = solicitud.idUsuario WHERE solicitud.idSolicitud = ?;",[$id]);
		$directory  ="mantenimiento/imagenes/$id";
		$files = Storage::disk('public')->files($directory);
		return view('mantenimiento.detalle', ['solicitud' => $solicitud[0], 'files'=> $files, 'role' => session('MantoRole')]);
	}

	public function getTipos(Request $request)
	{
		$sql = "SELECT tipo.idTipProblema id, tipo.tipoProblema nombre FROM manto_sucursal_area INNER JOIN manto_problema_clasificacion ON manto_problema_clasificacion.idArea = manto_sucursal_area.idArea INNER JOIN manto_tipo_problema tipo ON tipo.idTipProblema = manto_problema_clasificacion.idTipProblema WHERE manto_problema_clasificacion.idEquipo = ? AND manto_sucursal_area.idSucursal =? AND manto_problema_clasificacion.idArea = ? AND tipo.estado=1 GROUP BY tipo.idTipProblema, tipo.tipoProblema;";
		
		$tipos = DB::select($sql,[$request->input('id'),$request->input('idSuc'),$request->input('idArea')]);

		return  response()->json([
			'data' => $tipos
		]);

	}

	public function getAreas(Request $request)
	{
		$sql = "SELECT areas.idArea id, areas.area nombre FROM manto_sucursal_area AS suc INNER JOIN manto_problema_clasificacion AS clasifica ON suc.idArea = clasifica.idArea INNER JOIN manto_area AS areas ON areas.idArea = clasifica.idArea WHERE suc.idSucursal = ? and  areas.estado = 1 GROUP BY areas.idArea, areas.area ORDER BY areas.area;";
		
		$areas = DB::select($sql,[$request->input('id')]);
		/*
		clasifica.idTipProblema= ? and
	}*/
		return  response()->json([
			'data' => $areas
		]);
	}

	public function getEquipos(Request $request)
	{
		$sql = "SELECT eqp.idEquipo id, eqp.equipo nombre FROM manto_sucursal_area AS suc INNER JOIN manto_problema_clasificacion AS clasifica ON suc.idArea = clasifica.idArea INNER JOIN manto_equipo AS eqp ON eqp.idEquipo = clasifica.idEquipo WHERE suc.idSucursal = ? AND clasifica.idArea= ? and eqp.estado = 1 GROUP BY eqp.idEquipo, eqp.equipo ORDER BY eqp.equipo;";
		//$sql = "SELECT eqp.idEquipo id, eqp.equipo nombre FROM manto_sucursal_area AS suc INNER JOIN manto_problema_clasificacion AS clasifica ON suc.idArea = clasifica.idArea INNER JOIN manto_equipo AS eqp ON eqp.idEquipo = clasifica.idEquipo WHERE suc.idSucursal = ? and clasifica.idTipProblema= ? and clasifica.idArea= ? and eqp.estado = 1 GROUP BY eqp.idEquipo, eqp.equipo;";
		
		$equipos = DB::select($sql,[$request->input('idSuc'),$request->input('id')]);

		return  response()->json([
			'data' => $equipos
		]);
	}

	public function getProblemas(Request $request)
	{
		$sql = "SELECT problema.idProblema id, problema.problema nombre FROM manto_sucursal_area AS suc INNER JOIN manto_problema_clasificacion AS clasifica ON suc.idArea = clasifica.idArea INNER JOIN manto_problema AS problema ON problema.idProblema = clasifica.idProblema WHERE suc.idSucursal = ? and clasifica.idTipProblema= ? and clasifica.idArea= ? and clasifica.idEquipo= ? and problema.estado = 1 GROUP BY problema.idProblema, problema.problema ORDER BY problema.problema;";
		
		$problemas = DB::select($sql,[$request->input('idSuc'),$request->input('idTip'),$request->input('idArea'),$request->input('id')]);

		return  response()->json([
			'data' => $problemas
		]);
	}

	public function updateRequest(Request $request)
	{
		DB::enableQueryLog();
		$user = Auth::user();
		$idUsuario = $user->id;
		$uemail = $user->email;
		$uname = $user->name;
		
		$solicitud = $request->input('solicitud');
		$estado = $request->input('accion');
		
		if(!empty($solicitud) && !empty($estado))
		{
			if($estado != 3)
				DB::update("UPDATE manto_solicitud SET estado = ?, fechaModifica= ? , horaModifica = ? WHERE idSolicitud = ?",[$estado, date("Y-m-d"),date("H:i:s"),$solicitud]);
			else
				DB::update("UPDATE manto_solicitud SET estado = ?, fechaModifica= ? , horaModifica = ?, fechaTermina= ? , horaTermina = ? WHERE idSolicitud = ?",[$estado, date("Y-m-d"),date("H:i:s"), date("Y-m-d"),date("H:i:s"),$solicitud]);

			DB::insert('INSERT INTO manto_solicitud_log (idSolicitud,idUsuario, fechaCrea, horaCrea, estado ) values (?,?,?,?,?)', [$solicitud , $idUsuario, date("Y-m-d"),date("H:i:s"), $estado]);
			return "{ 'success': true, 'msg': 'Datos actualizados correctamente!'}";
		}
		else
		{
			return "{ 'success': false, 'msg': 'Error al guardar los datos!'}";
		}

	}

	public function saveRequest(Request $request)
	{
		DB::enableQueryLog();
		$user = Auth::user();

		$path ="";
		$idUsuario = $user->id;
		$uemail = $user->email;
		$uname = $user->name;

		$equipo = $request->input('equipo');
		$sucursal = $request->input('sucursal');
		$area = $request->input('area');
		$tipo = $request->input('tipos');
		$problema = $request->input('problema');
		$comentario = $request->input('comentario');
		
		if(!empty($equipo) && !empty($sucursal) && !empty($area) && !empty($problema) && !empty($tipo))
		{
			$comentario = empty($comentario)?"":$comentario;
			$sql = "SELECT tiempo FROM manto_problema_clasificacion WHERE idTipProblema= ? AND idArea = ? AND idEquipo = ? AND idProblema = ?;";
			$tiempo = DB::select($sql, [$tipo, $area, $equipo, $problema]);
			$horas = empty($tiempo[0]->tiempo)?72:$tiempo[0]->tiempo;

			DB::insert('insert into manto_solicitud (idUsuario, fechaCrea, horaCrea, comentario, idSucursal, idTipProblema, idArea, idEquipo, idProblema,tiempo ) values (?, ?, ?, ?, ?, ?, ?, ?, ?,?)', [$idUsuario, date("Y-m-d"),date("H:i:s"), $comentario, $sucursal,$tipo,$area,$equipo,$problema,$horas]);
			$lid = DB::getPdo()->lastInsertId();
			$sql="";
			if(!empty($lid))
			{
				if(!empty($request->hasFile('imagen')))
				{
					//$path = $request->file('imagen')->store("mantenimiento/imagenes/$lid");
					$path = $request->file('imagen')->store("mantenimiento/imagenes/$lid", 'public');
				}		
				//T2.email
				
				$sql = "SELECT T0.idUsuario, T2.name, T2.email FROM config_app_access T0 INNER JOIN users T2 ON T2.id = T0.idUsuario LEFT JOIN manto_sucursal_usuario T1 ON T1.idUsuario = T0.idUsuario  WHERE idAplicacion = 5 AND ( T0.idUsuario =16 OR (idRole IN (2,3) AND T1.idSucursal = $sucursal))";
							
				$tecnicos = DB::select($sql);
				
				$solicitud = DB::select("SELECT solicitud.idSolicitud, solicitud.fechaCrea fecha, IF(solicitud.tiempo = 24, '24 Hrs.', IF(solicitud.tiempo = 48, '48 Hrs.', IF(solicitud.tiempo=72,'72 Hrs.', IF(solicitud.tiempo=100,'Mas de 72 Hrs.','Indefinido')))) tiempo, HOUR(TIMEDIFF(NOW(),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea))) transcurrido, solicitud.comentario, users.name nombre ,rh_oficinas.nombre sucursal ,tipo.tipoProblema tipo, area.area, equipo.equipo, problema.problema, 'Pendiente' estado FROM manto_solicitud AS solicitud INNER JOIN rh_oficinas ON rh_oficinas.id = solicitud.idSucursal INNER JOIN manto_tipo_problema AS tipo ON tipo.idTipProblema = solicitud.idTipProblema INNER JOIN manto_area AS area ON area.idArea = solicitud.idArea INNER JOIN manto_equipo AS equipo ON equipo.idEquipo=solicitud.idEquipo INNER JOIN manto_problema AS problema ON problema.idProblema = solicitud.idProblema INNER JOIN users ON users.id = solicitud.idUsuario WHERE solicitud.idSolicitud = ?;",[$lid]);

				$tecArray = array();
				
				foreach($tecnicos as $tec)
				{
					if($tec->idUsuario != 9 && $tec->idUsuario != 1 && $tec->idUsuario != 6 )
						$tecArray[] = $tec->email;
				}
				
				$url = url('/mantenimiento/detalle/'.$lid);

				Mail::send('mantenimiento.mailRequest', ['url' => $url,'name' => $uname, 'comentario' => $comentario, 'solicitud' => $solicitud[0]], function ($message) use ($tecArray)
				{
					$message->from('reportes@prigo.com.mx', 'Reportes PRIGO');
					$message->to($tecArray);					
					$message->subject("Nueva solicitud de mantenimiento recibida");
				});
				
				return "{ \"success\": true, \"resultType\": \"success\", \"msg\": \"Solicitud enviada correctamente!\"}";

			}
			else
			{
				return "{ \"success\": false,  \"resultType\": \"error\", \"msg\": \"Error al guardar los datos!, intentelo nuevamente mas tarde\"}";
			}
		}
		else 
		{
			return "{ \"success\": false, \"resultType\": \"error\", \"msg\": \"Error al guardar los datos, falta información!\"}";
		}
		
	}
	public function getRequest(Request $request)
	{
		$idUsuario = Auth::id();
		$MantoRole = session('MantoRole');
		$sucursales = session('sucursales');
		$strValSucursales = "";
		$estado = !empty($request->input('estatus'))?$request->input('estatus'):1;
		if($MantoRole != 1)
			if(!empty($sucursales))
				$strValSucursales = " IN (".$sucursales.") ";
			else
				$strValSucursales = " IN (0) "; 

		$draw = !empty($request->input('draw'))?$request->input('draw'):1;
		$start = !empty($request->input('start'))?$request->input('start'):0;
		$length = !empty($request->input('length'))?$request->input('length'):10;
		$queryarr = !empty($request->input('search'))?$request->input('search'):array();
		$query = !empty($queryarr["value"]) ? $queryarr["value"] : "" ;
		$allitems = null;
		$strBusca =  "";
		$strAtraso = "";
		
		

		if(!empty($request->input('columns')))
		{
			$cols = $request->input('columns');
			$busca = array();
			foreach($cols as $col)
			{
				if(!empty($col["search"]["value"]))
				{
					switch($col["data"]){
						case "tipo":
							$busca[] = " problema.problema LIKE '%".$col["search"]["value"]."%' ";
						break;
						case "sucursal":
							$busca[] = " rh_oficinas.nombre LIKE '%".$col["search"]["value"]."%' ";
						break;
					}
				}
			}
			if(!empty($busca))
			{
				$strBusca = " AND ".implode(" AND ", $busca); 
			}
			
		}
		$orden = " datos.fechaCrea DESC ";
		if(!empty($request->input('order')))
		{
			$ordena = $request->input('order');
			$orden = "";
			switch($ordena[0]["column"])
			{
				case 0:
					$orden = " datos.idSolicitud " . $ordena[0]["dir"];
				break;
				case 1:
					$orden = " datos.fechaCrea " . $ordena[0]["dir"];
				break;
				case 2:
					$orden = " datos.sucursal " . $ordena[0]["dir"];
				break;
				case 3:
				$orden = " datos.tipo  " . $ordena[0]["dir"];
				break;
				case 4:
				$orden = " datos.problema " . $ordena[0]["dir"];
				break;
				case 5:
				$orden = " datos.equipo  " . $ordena[0]["dir"];
				break;
				case 6:
				$orden = " datos.estado  " . $ordena[0]["dir"];
				break;
				default:
				$orden = " datos.fechaCrea DESC ";
				break;			
			}
		}


		$allitems = DB::select("SELECT * FROM ( SELECT solicitud.idSolicitud, tipo.tipoProblema, area.area, equipo.equipo, problema.problema FROM manto_solicitud AS solicitud INNER JOIN manto_solicitud_estado AS estado ON estado.idEstado = solicitud.estado INNER JOIN rh_oficinas ON rh_oficinas.id = solicitud.idSucursal INNER JOIN manto_tipo_problema AS tipo ON tipo.idTipProblema = solicitud.idTipProblema INNER JOIN manto_area AS area ON area.idArea = solicitud.idArea INNER JOIN manto_equipo AS equipo ON equipo.idEquipo=solicitud.idEquipo INNER JOIN manto_problema AS problema ON problema.idProblema = solicitud.idProblema WHERE ".( $MantoRole != 1 ? " solicitud.idSucursal ".$strValSucursales . " AND " : ""  )." solicitud.estado IN(".($estado==1 || $estado==2 ?"1,2":($estado==3?"3":"4")).") ".( $estado!=2? " ": " AND HOUR(TIMEDIFF(NOW(),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo AND solicitud.tiempo <> 0 ")." ".$strBusca.") AS datos;");
		$items =    DB::select("SELECT * FROM ( SELECT solicitud.idSolicitud, solicitud.fechaCrea, rh_oficinas.nombre sucursal ,tipo.tipoProblema tipo, area.area, equipo.equipo, problema.problema, estado.estado FROM manto_solicitud AS solicitud INNER JOIN manto_solicitud_estado AS estado ON estado.idEstado = solicitud.estado INNER JOIN rh_oficinas ON rh_oficinas.id = solicitud.idSucursal INNER JOIN manto_tipo_problema AS tipo ON tipo.idTipProblema = solicitud.idTipProblema INNER JOIN manto_area AS area ON area.idArea = solicitud.idArea INNER JOIN manto_equipo AS equipo ON equipo.idEquipo=solicitud.idEquipo INNER JOIN manto_problema AS problema ON problema.idProblema = solicitud.idProblema WHERE ".( $MantoRole != 1 ? " solicitud.idSucursal ".$strValSucursales . " AND " : ""  )." solicitud.estado IN(".($estado==1 || $estado==2 ?"1,2":($estado==3?"3":"4")).") ".( $estado!=2? " ": " AND HOUR(TIMEDIFF(NOW(),CONCAT(solicitud.fechaCrea, ' ', solicitud.horaCrea)))>solicitud.tiempo  AND solicitud.tiempo <> 0 ")." ".$strBusca.") AS datos ORDER BY $orden LIMIT ".$start.", ".$length.";");

		return  response()->json([
			'draw' => $draw,
			'recordsTotal' => count($allitems),
			'recordsFiltered' => count($allitems),
			'data' => $items
		]);
	}

}