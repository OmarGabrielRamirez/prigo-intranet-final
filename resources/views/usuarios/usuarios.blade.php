@extends('layouts.app')
@include('menu.usuarios', ['seccion' => 'usuarios'])
@section('content')
    @include('usuarios.indexusuario')
@endsection
@section('aditionalScripts')
  <script type="text/javascript">

$(document).ready(function() {
    $('#datatables').DataTable({
        "responsive": true,	
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "{{ route('getusuarios') }}",
            "type": "GET",
			"data": function ( d ) {
                d._token = "{{ csrf_token() }}";
            }
        },
        "columns": [
            { "data": "id" },
            { "data": "name" },                      
            { "data": "email" },
			{ "render": function (data, type, row, meta) {
				return "<a href=\"{{ route('detalleusuario') }}/"+row.id+"\" class=\"btn btn-link btn-info btn-just-icon like\"><i class=\"material-icons\">open_in_new</i></a>";
				}
			}
        ]
    });

    var table = $('#datatables').DataTable();

	$('#findUserbtn').on('click', function () {
        var value = $("#findUser").val().toLowerCase();
        $("#datatables tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
        $("#findUser").val("");
    });

    $('#addUsuario').on('click', function(){
        window.location.replace('/usuarios/nuevo');
    });

    
      
});

</script>
<style>
    html,body { 
      overflow:hidden; 
    }
    
    .dataTables_filter {
        display: none;
    } 
</style>

@endsection