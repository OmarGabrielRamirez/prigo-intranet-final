@extends('layouts.app')
@include('menu.sesiones', ['seccion' => 'sesiones'])
@section('content')
    @include('usuarios.sesionesindex')
@endsection
@section('aditionalScripts')
  <script type="text/javascript">

$(document).ready(function() {
    $('#datatables').DataTable({
        "responsive": true,	
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "{{ route('getsesiones') }}",
            "type": "GET",
			"data": function ( d ) {
                d._token = "{{ csrf_token() }}";
            }
        },
        "columns": [
            { "data": "nombre" },
            { "data": "fechaInicio" }
        ]
    });

    var table = $('#datatables').DataTable();

	$('#findUserbtn').on('click', function () {
        $('#btnReturn').prop('disabled', false);
        var value = $("#findUser").val().toLowerCase();
        $("#datatables tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $('#btnReturn').on('click', function(){
        $("#findUser").val("");
        table.draw();
    });

    
});

</script>
<style>
    html,body { 
      overflow:hidden; 
    }
    
    .dataTables_filter {
        display: none;
    } 
</style>

@endsection