<div class="row">

      <div class="col-md-12">
          <div class="card ">
              <div class="card-header card-header-orange card-header-text">
                  <div class="card-text">
                      <h4 class="card-title">Nuevo Usuario</h4>
                  </div>
              </div>
              <div class="card-body ">
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Nombre:</label>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Correo:</label>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Estado:</label>
                        <div class="col-sm-6">
                            <div class="form-group bmd-form-group">
                                <select id="slctEstado" name="status" class="form-control" style="width: 65%" required>
                                    <option selected>Selecciona un estado</option>
                                    <option value="1">Activo</option>
                                    <option value="2">No activo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="contenedorPass" class="row">
                        <label class="col-sm-2 col-form-label">Contraseña:</label>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <input id="password" type="password" class="form-control" name="password" required>
                            <i class="fa fa-eye" id="mostrar"></i>
                        </div>
                    </div>
                    </div>
                    <div id="contenedorPass" class="row">
                        <label class="col-sm-2 col-form-label">Confirmar Contraseña:</label>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            <i class="fa fa-eye" id="mostrar2"></i>
                        </div>
                    </div>
                    </div>
                      <div class="row">
                          <div class="col-sm-6">
                              <div class="form-group bmd-form-group">

                                  <button id="btnGuardar"type="submit" class="btn btn-success" >Registrar</button>
                                  <button id="btnCancelar"type="button" class="btn btn-danger" >Cancelar</button>
                                  <button id="btnPass" type="button" class="btn btn-danger" >Generar contraseña</button>
                              </div>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
</div>

<script>
    $('#mostrar').click(function(){
      if($(this).hasClass('fa-eye')){
        $('#password').removeAttr('type');
        $('#mostrar').addClass('fa-eye-slash').removeClass('fa-eye');
      }else{
        $('#password').attr('type','password');
        $('#mostrar').addClass('fa-eye').removeClass('fa-eye-slash');
      }
    });
    
    $('#mostrar2').click(function(){
      if($(this).hasClass('fa-eye')){
      $('#password-confirm').removeAttr('type');
      $('#mostrar2').addClass('fa-eye-slash').removeClass('fa-eye');
      }else{
      $('#password-confirm').attr('type','password');
      $('#mostrar2').addClass('fa-eye').removeClass('fa-eye-slash');
      }
    });

    $('#btnPass').click(function(){
        var nuevaContra = rand_code(caracteres, longitud);
        $('#password').val(nuevaContra);
        $('#password-confirm').val(nuevaContra);
        
    });

    function rand_code(chars, lon){
        code = "";
        for (x=0; x < lon; x++){
        rand = Math.floor(Math.random()*chars.length);
        code += chars.substr(rand, 1);
        }
            return code;
    }

    caracteres = "0123456789abcdefghijklmnñopkrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ?¿¡!:;()[].,$#><-_+*";
    longitud = 10;

</script>