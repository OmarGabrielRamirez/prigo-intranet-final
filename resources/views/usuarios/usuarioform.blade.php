<div class="row">
    @foreach($usuarios AS $usuario)
      <div class="col-md-12">
          <div class="card ">
              <div class="card-header card-header-orange card-header-text">
                  <div class="card-text">
                      <h4 class="card-title">Editar Usuario</h4>
                  </div>
              </div>
              <div class="card-body ">
                  <form method="get" action="/" class="form-horizontal" id="formUsuario">
                      {!! csrf_field() !!}
                    <div class="row">
                        <label class="col-sm-2 col-form-label">ID:</label>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <input type="text" value="{{$usuario->id}}" id="idUsuario" name="idUsuario" class="form-control" disabled> 
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Nombre:</label>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <input type="text" value="{{$usuario->name}}" id="nombreUsuario" name="nombreUsuario" class="form-control" disabled> 
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Correo:</label>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <input type="text" value="{{$usuario->email}}" id="correoUsuario" name="correoUsuario" class="form-control" disabled> 
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Fecha de creación:</label>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <label>{{$usuario->created_at}}</label>
                        </div>
                    </div>
                    </div> 
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Ultima actualización:</label>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <label>{{$usuario->updated_at}}</label>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Estado:</label>
                        <div class="col-sm-6">
                            <div class="form-group bmd-form-group">
                                <select id="slctEstado" name="slctEstado" class="form-control" style="width: 65%" disabled>
                                    <option {{ $usuario->estado == '1' ? 'selected' : '' }} value="1">Activo</option>
                                    <option {{ $usuario->estado == '2' ? 'selected' : '' }} value="2">No activo</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div id="contenedorPass" class="row"  style="display: none">
                        <label class="col-sm-2 col-form-label">Nueva contraseña:</label>
                    <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                            <input type="text" value="" id="nuePassUsuario" name="nuePassUsuario" class="form-control" disabled> 
                        </div>
                    </div>
                    </div>
                      <div class="row">
                          <div class="col-sm-6">
                              <div class="form-group bmd-form-group">
                                  <button id="btnEditar" type="button" class="btn btn-warning">Editar</button>
                                  <button id="btnGuardar"type="button" class="btn btn-success" disabled>Guardar cambios</button>
                                  <button id="btnPass"type="button" class="btn btn-danger" disabled>Cambiar contraseña</button>
                              </div>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  @endforeach
</div>
@section('aditionalScripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        var idUsuario = "";
        var nombreUsuario = "";
        var correoUsuario = "";
        var estadoUsuario = "";
        var nuevaContra = "";

        $("#btnEditar").click(function(e){
            $('#nombreUsuario').prop('disabled', false);
            $('#correoUsuario').prop('disabled', false);
            $('#slctEstado').prop('disabled', false);
            $('#btnGuardar').prop('disabled', false); 
            $('#btnPass').prop('disabled', false);    
            $('#btnEditar').prop('disabled', true);   
            swal({
			type: 'success',
            title: 'Edición habilitada',
            text: 'Se habilitaron los campos para realizar la edición del usuario',
            icon: 'success',
            showConfirmButton: false,
            timer: 1000,
            });	 
        });

        $('#btnRevertir').click(function(){
        swal({
                  title: 'Revirtiendo cambios...',
				  allowEscapeKey: false,
				  allowOutsideClick: false,
				  showCancelButton: false,
				  showConfirmButton: false,
				  text: 'Espere un momento...'     
                });
            setTimeout(function() { window.location.href = "/usuarios";}, 1000);
    });

    $('#btnGuardar').click(function(){
      
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{!! csrf_token() !!}"
            }
        });

        idUsuario = $('#idUsuario').val();
        nombreUsuario = $('#nombreUsuario').val();
        correoUsuario  = $('#correoUsuario').val();
        estadoUsuario = $('#slctEstado').val();

        swal({
			title: "Estas segur@?",
			text: "Se guardaran permanentemente los cambios realizados.",
			type: "warning",
			showCancelButton: true,
            confirmButtonColor: "#DD6B55",
			allowOutsideClick: false,
			confirmButtonText: 'Guardar',
			cancelButtonText: 'Cancelar'
			}).then(
                value =>{
                    $.ajax({
                        type:'post',
                        url:'/usuarios/editarusuario/',
                        data: {id_usuario:idUsuario, nombre_usuario:nombreUsuario, correo_usuario:correoUsuario, estado_usuario:estadoUsuario},
                        success:function(data){
                            swal({
			                    type: 'success',
                                title: 'Se guardaron los cambios',
                                text: 'Se actualizaron los datos en la BD',
                                icon: 'success',
                                showConfirmButton: false,
                                timer: 1000,
                            });
                        }
                    });
                    
                },
                dismiss =>{
                    swal({
						  type: 'error',
						  title: 'Cambios no guardados',
				    });
                }
            ).catch(swal.noop);
    });

    $('#btnPass').click(function(e){
        idUsuario = $('#idUsuario').val();
        correoUsuario  = $('#correoUsuario').val();
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{!! csrf_token() !!}"
            }
        });

    function rand_code(chars, lon){
        code = "";
        for (x=0; x < lon; x++){
            rand = Math.floor(Math.random()*chars.length);
            code += chars.substr(rand, 1);
        }
        return code;
    }

    caracteres = "0123456789abcdefghijklmnñopkrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ?¿¡!:;()[].,$#><-_+*";
    longitud = 10;

            nuevaContra = rand_code(caracteres, longitud);

            swal({
			title: "Estas segur@?",
			text: "Se cambiara la contraseña actual.",
			type: "warning",
			showCancelButton: true,
            confirmButtonColor: "#DD6B55",
			allowOutsideClick: false,
			confirmButtonText: 'Guardar',
			cancelButtonText: 'Cancelar'
			}).then(
                value =>{
                    $.ajax({
                        type:'post',
                        url:'/usuarios/editarpass/',
                        data: {id_usuario:idUsuario, nue_pass: nuevaContra, correo_usuario:correoUsuario},
                        success:function(data){
                            $('#nuePassUsuario').val(nuevaContra);
                            $("#contenedorPass").removeAttr("style");
                            swal({
			                    type: 'success',
                                title: 'Se cambio la contraseña',
                                text: 'Se actualizaron los datos en la BD',
                                icon: 'success',
                                showConfirmButton: false,
                                timer: 1000,
                            });
                        },
                        error:function(data){
                            swal({
						  type: 'error',
						  title: 'Error al actualizar la contraseña',
				    });
                        }
                    });
                    
                },
                dismiss =>{
                    swal({
						  type: 'error',
						  title: 'No se cambio la contraseña',
				    });
                }
            ).catch(swal.noop);
    });

    </script>
@endsection