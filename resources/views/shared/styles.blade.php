<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>

<link rel="stylesheet" href="{{ asset('MaterialBS/css/material-dashboard.min.css') }}"/>

<!-- Documentation extras -->
<!-- CSS Just for demo purpose, don't include it in your project 
<link href="{{ asset('MaterialBS/assets-for-demo/demo.css') }}" rel="stylesheet">-->