@extends('layouts.newlay')
@include('menu.musica', ['seccion' => 'index'])
@section('content')
<div class="row" style="margin-bottom: 10px;">
    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info card-header-icon" data-background-color="orange">
				<h5 class="card-title">Equipos</h5>
            </div>
            <div class="card-body">
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Ultimo Reporte</th>
                            <th>Volumen</th>
							<th>Estado</th>
                        </tr>
                    </head>
                    <tbody>
                        @foreach($equipos AS $dato)
                            <tr><td>{{ $dato->idEquipo }}</td><td>{{ $dato->fechaEstado . " - " . $dato->horaEstado }} </td><td>{{ $dato->volumen }} </td><td>{{ $dato->estado }}<i class="pull-right material-icons @if($dato->estado == 'stop' || $dato->horaEstado < date('H:i:s') ) text-danger">highlight_off @else text-success"> check_circle_outline @endif </td></tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('jsimports')
<script src="{{ asset('MaterialBS/js/plugins/bootstrap-selectpicker.js') }}"></script>
<script src="{{ asset('MaterialBS/js/plugins/jquery.select-bootstrap.js') }}"></script>
<script src="{{ asset('MaterialBS/js/plugins/bootstrap-tagsinput.js') }}"></script>
<script src="{{ asset('MaterialBS/assets-for-demo/js/modernizr.js') }}"></script>
@endsection
@section('aditionalScripts')
  
  <script type="text/javascript">

  </script>
@endsection