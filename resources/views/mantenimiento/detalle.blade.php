@extends('layouts.app')
@include('menu.mantenimiento', ['seccion' => 'consultaManto'])
@section('content')
<div class="row">
	<div class="card ">
		<div class="card-header ">
			<h4 class="card-title">Solicitud # {{ $solicitud->idSolicitud }} - {{ $solicitud->sucursal }} </h4>
			<div class="row">
				<label class="col-sm-2 col-form-label"><b>Solicita</b></label>
				<div class="col-sm-2 col-form-label">{{ $solicitud->nombre }}</div>			
				<label class="col-sm-2 col-form-label"><b>Fecha</b></label>				
				<div class="col-sm-2 col-form-label">{{ $solicitud->fecha }}</div>		
            </div>
			<div class="row">
				<label class="col-sm-2 col-form-label"><b>Tipo de Problema</b></label>
				<div class="col-sm-2 col-form-label">{{ $solicitud->tipo }}</div>			
				<label class="col-sm-2 col-form-label"><b>Area</b></label>				
				<div class="col-sm-2 col-form-label">{{ $solicitud->area }}</div>		
			</div>
			<div class="row">
				<label class="col-sm-2 col-form-label"><b>Equipo</b></label>
				<div class="col-sm-2 col-form-label">{{ $solicitud->equipo }}</div>			
				<label class="col-sm-2 col-form-label"><b>Problema</b></label>				
				<div class="col-sm-2 col-form-label">{{ $solicitud->problema }}</div>		
            </div>
			<div class="row">
				<label class="col-sm-2 col-form-label"><b>Tiempo de Atención</b></label>
				<div class="col-sm-2 col-form-label">{{ $solicitud->tiempo }}</div>			
				<label class="col-sm-2 col-form-label"><b>Tiempo Transcurrido</b></label>				
				<div class="col-sm-2 col-form-label">{{ $solicitud->transcurrido }} Hrs.</div>		
            </div>            
			<div class="row">
				<label class="col-sm-2 col-form-label"><b>Comentario</b></label>
				<div class="col-sm-4 col-form-label">{{ $solicitud->comentario }}</div>	
			</div>
			@if($role <= 3)
			<div class="row">
				<label class="col-sm-2 col-form-label">Actualizar</label>
				<div class="col-sm-4">
					<select id="accion_{{ $solicitud->idSolicitud }}" onchange="validAccion({{ $solicitud->idSolicitud }},this.value)"  name="accion_{{ $solicitud->idSolicitud }}" class="selectpicker" data-style="btn select-with-transition" title="Seleccione un estado" data-size="7" tabindex="-98">
						<option value="1">Pendiente</option>
						<option value="2">Programado</option>
						<option value="3">Resuelto</option>
						<option value="4">Cancelado</option>
					</select>
				</div>
				<div class="col-sm-4">
				</div>
				<div class="col-sm-2">
					<button type="button" onclick="guardaEstado({{ $solicitud->idSolicitud }});" id="saveBtn_{{ $solicitud->idSolicitud}}" class="btn btn-info btn-sm">Guardar</button>
				</div>
			</div>
			@endif
			<div class="row">
				@foreach($files as $file)
				<div class="col-sm-2"><a href="/storage/{{ $file }}" target="_blank"><img src="/storage/{{ $file }}" width=100 height='100' ></a></div>
				@endforeach
			</div>		
        </div>
	</div>
</div>
@endsection
@section('aditionalScripts')
<script type="text/javascript">

function guardaEstado(partida){
	var accion = $("#accion_"+partida).val();
	var solicitud = "{{ $solicitud->idSolicitud }}";
	var _token ="{{ csrf_token() }}";
	if(accion==''){
		swal({
		  type: 'error',
		  title: 'Oops...',
		  text: 'Seleccione un estado a aplicar!',
		  footer: 'Problemas? sit@prigo.com.mx',
		});
	}
	else
	{
		swal({
			title: "Estas segur@?",
			text: "Se aplicara el estado a esta solicitud!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			allowOutsideClick: false,
			confirmButtonText: 'Si, actualizar!',
			cancelButtonText: 'No, cancelar!'
		}).then((result) => {
		  if (result) {
			  $('.button').prop('disabled', true);
			swal({
			  title: 'Guardando...',
			  allowEscapeKey: false,
			  allowOutsideClick: false,
			  showCancelButton: false,
			  showConfirmButton: false,
			  text: 'Espere un momento...'
			});
			
			$.ajax({
				type: "POST",
				url: "{{ route('updatesolicitudm') }}",
				data: { "_token": _token ,"accion": accion, "solicitud": solicitud },
				success: function(msg){
					swal({
						type: 'success',
						title: 'Tu Solicitud se ha actualizado!'
					});
					
					$('.button').prop('disabled', false);
					
				},
				error: function(){
					swal({
					  type: 'error',
					  title: 'Oops...',
					  text: 'Algo ha salido mal!',
					  footer: 'Problemas? sit@prigo.com.mx	',
					});
					$('.button').prop('disabled', false);
				}
			});	
			
		  }
		});
	}
}

function validAccion(partida,valor)
{

	$("#reem-"+partida+" .form-control").val('');
	
	if(valor == 4)
	{
		$("#lbl-reem-"+partida).show();
		$("#reem-"+partida).show();
	}
	else
	{
		$("#lbl-reem-"+partida).hide();
		$("#reem-"+partida).hide();
	}
}
</script>
@endsection