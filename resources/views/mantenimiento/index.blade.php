@extends('layouts.app')
@include('menu.mantenimiento', ['seccion' => 'main'])
@section('content')
<div class="row" style="margin-bottom: 10px;">
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card">
            <div class="card-header card-header-info card-header-icon">
            <div class="card-icon">
                <i class="material-icons">assignment</i>
            </div>
            <h5 class="card-title">Tickets Abiertos</h5>
            </div>
            <div class="card-body">
            <h3><a href="{{ route('consultaManto') }}">{{ $abiertos }}</a></h3>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="card">
                <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment_late</i>
                </div>
                <h5 class="card-title">Tickets Atrasados</h5>
                </div>
                <div class="card-body">
                <h3><a href="{{ route('mntoAtrasados') }}">{{ $atrasados }}</a></h3>
                </div>
            </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
    <div class="card">
        <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
            <i class="material-icons">assignment_turned_in</i>
        </div>
        <h5 class="card-title">Tickets Resueltos</h5>
        </div>
        <div class="card-body">
        <h3><a href="{{ route('mntoResueltos') }}">{{ $resueltos }}</a></h3>
        </div>
    </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info card-header-icon">
            <div class="card-icon">
                <i class="material-icons">assignment_turned_in</i>
            </div>
            <h5 class="card-title">Top 5 Problemas</h5>
            </div>
            <div class="card-body">
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Problema</th>							
                            <th># Tickets</th>					
                        </tr>
                    </head>
                    <tbody>
                        @foreach($topProblema AS $dato)
                            <tr><td>{{ $dato->nombre }}</td><td class="text-right">{{ $dato->n }}</td><td class="text-right"></td><td class="text-right"></td><td class="text-right"></td><td class="text-right"></td><td class="text-right"></td></tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card">
                <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment_turned_in</i>
                </div>
                <h5 class="card-title">Top 5 Equipos</h5>
                </div>
                <div class="card-body">
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th>Equipo</th>							
                                <th># Tickets</th>					
                            </tr>
                        </head>
                        <tbody>
                            @foreach($topEquipo AS $dato)
                                <tr><td>{{ $dato->nombre }}</td><td class="text-right">{{ $dato->n }}</td><td class="text-right"></td><td class="text-right"></td><td class="text-right"></td><td class="text-right"></td><td class="text-right"></td></tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @foreach($perTicket as $per)
    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info card-header-icon">
            <div class="card-icon">
                <b>
                    @if($per->tiempo > 0 && $per->tiempo <100)
                    {{ $per->tiempo}} hrs.
                    @elseif($per->tiempo == 100)
                    Mas de 72 hrs.
                    @else
                    Indefinido
                    @endif
                </b>
            </div>
            <h5 class="card-title">Porcentaje Resuelto</h5>
            </div>
            <div class="card-body">
                <h2>{{ number_format($per->perResuelto, 2 ) }}%</h2>
            </div>                
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-danger">warning</i> {{ number_format($per->perResueltoAtrasado, 2) }}% Resuelto con atraso
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info card-header-icon">
            <div class="card-icon">
                <i class="material-icons">store_mall_directory</i>
            </div>
            <h5 class="card-title">Sucursales</h5>
            </div>
            <div class="card-body">
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Estado</th>
                            <th># Tickets Abiertos</th>					
                        </tr>
                    </head>
                    <tbody>
                        @foreach($topSucursal AS $dato)
                            <tr><td>{{ $dato->nombre }}</td><td><i class="pull-right material-icons @if($dato->n > 0) text-danger">highlight_off @else text-success"> check_circle_outline @endif </td><td class="text-right">{{ $dato->n }} </td></tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>	
</div>
@endsection