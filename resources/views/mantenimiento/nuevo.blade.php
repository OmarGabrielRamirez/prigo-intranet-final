@extends('layouts.app')
@include('menu.mantenimiento', ['seccion' => 'solicitudManto'])
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-orange card-header-text">
				<div class="card-text">
                  <h4 class="card-title">Nueva Solicitud</h4>
                </div>
            </div>
            <div class="card-body ">
                <form id="NewFormManto" action="{{ route('guardasolicitudmanto') }}" method="POST" enctype="multipart/form-data">
                    <input name="_token" value= "{{ csrf_token() }}" type="hidden">
                    <div class="row">
                        <div id="divMessages" class="col-sm-12">                        
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Sucursal/Oficina</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <select onchange="getAreas()" id="idSucursal" name="sucursal" class="selectpicker" data-style="btn select-with-transition" title="Seleccione una sucursal" data-size="7" tabindex="-98">
                                    @foreach($sucursales AS $sucursal)
                                    <option value="{{ $sucursal->idSucursal }}">{{ $sucursal->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <label class="col-sm-2 col-form-label">Area</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <select onchange="getEquipo()" id="area" name="area" class="selectpicker" data-style="btn select-with-transition" title="Seleccione area" data-size="7" tabindex="-98">
                                    <option>Seleccione Area</option>
                                </select>
                            </div>
                        </div>
                        <label class="col-sm-2 col-form-label">Equipo</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <select onchange="getTipos()" id="equipo" name="equipo" class="selectpicker" data-style="btn select-with-transition" title="Seleccione Equipo" data-size="7" tabindex="-98">
                                    <option>Seleccione Equipo</option>
                                </select>
                            </div>
                        </div>
                        <label class="col-sm-2 col-form-label">Tipo Problema</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <select onchange="getProblema()" id="tipos" name="tipos" class="selectpicker" data-style="btn select-with-transition" title="Seleccione un tipo" data-size="7" tabindex="-98">
                                    <option>Seleccione Tipo</option>
                                </select>
                            </div>
                        </div>
                        <label class="col-sm-2 col-form-label">Problema</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <select id="problema" name="problema" class="selectpicker" data-style="btn select-with-transition" title="Seleccione un Problema" data-size="7" tabindex="-98">
                                    <option>Seleccione Problema</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Comentarios</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <textarea id="comentario" name="comentario" class="form-control"></textarea>
                                <span class="bmd-help">Detalles o excepciones del problema</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Archivo</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-file btn-sm"><span class="fileinput-new">Seleccione Imagen</span><span class="fileinput-exists">Cambiar</span><input type="file" name="imagen"  accept="image/*"></span>
                                <a href="#" class="input-group-addon btn btn-sm fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group bmd-form-group">
                                <button id="sendRequest" type="button" class="btn btn-info btn-round">Guardar Solicitud</button>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            &nbsp;
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>    
</div>
@endsection
@section('aditionalScripts')
<script>
function getTipos()
{
	$("#divMessages").empty();
	$("#tipos").empty();
    $("#problema").empty();
	
	$("#tipos").selectpicker('refresh');
    $("#problema").selectpicker('refresh');

	$.ajax({
		type: "POST",
		url: "{{ route('getTipos') }}",
        data: { "id": $("#equipo").val(),"idArea": $("#area").val(), "idSuc": $("#idSucursal").val(), "_token": "{{ csrf_token() }}"},
		success: function(msg){
			var len = msg.data.length;
			var l = 0;
			if(len > 0)
			{
				$("#tipos").empty();
				
				while(l<len)
				{
					$("#tipos").append("<option value='"+msg.data[l].id+"'>"+msg.data[l].nombre+"</option>");
					l++;
				}
				$("#tipos").selectpicker('refresh');
			}
		},
		error: function(){
			swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Algo ha salido mal!',
			  footer: 'Problemas? sit@prigo.com.mx	',
			});
			$('.button').prop('disabled', false);
		}
	});	
}
function getAreas()
{
	$("#divMessages").empty();
    $("#area").empty();
    $("#equipo").empty();
    $("#problema").empty();
	
    $("#area").selectpicker('refresh');
    $("#equipo").selectpicker('refresh');
    $("#problema").selectpicker('refresh');

	$.ajax({
		type: "POST",
		url: "{{ route('getAreas') }}",
		data: { "id":  $("#idSucursal").val(), "_token": "{{ csrf_token() }}"},
        //data: { "id": $("#tipos").val(), "idSuc": $("#idSucursal").val(), "_token": "{{ csrf_token() }}"},
		success: function(msg){
			var len = msg.data.length;
			var l = 0;
			if(len > 0)
			{
				$("#area").empty();
				
				while(l<len)
				{
					$("#area").append("<option value='"+msg.data[l].id+"'>"+msg.data[l].nombre+"</option>");
					l++;
				}
				$("#area").selectpicker('refresh');
			}
		},
		error: function(){
			swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Algo ha salido mal!',
			  footer: 'Problemas? sit@prigo.com.mx	',
			});
			$('.button').prop('disabled', false);
		}
	});	
}
function getEquipo()
{
    $("#divMessages").empty();
    $("#equipo").empty();
    $("#tipos").empty();
    $("#problema").empty();
	
    $("#equipo").selectpicker('refresh');
    $("#tipos").selectpicker('refresh');
    $("#problema").selectpicker('refresh');

	$.ajax({
		type: "POST",
		url: "{{ route('getEquipos') }}",
		data: { "id": $("#area").val(), "idSuc": $("#idSucursal").val(), "_token": "{{ csrf_token() }}"},
		success: function(msg){
			var len = msg.data.length;
			var l = 0;
			if(len > 0)
			{
				$("#equipo").empty();
				
				while(l<len)
				{
					$("#equipo").append("<option value='"+msg.data[l].id+"'>"+msg.data[l].nombre+"</option>");
					l++;
				}
				$("#equipo").selectpicker('refresh');
			}
		},
		error: function(){
			swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Algo ha salido mal!',
			  footer: 'Problemas? sit@prigo.com.mx	',
			});
			$('.button').prop('disabled', false);
		}
	});
}

function getProblema()
{
    $("#divMessages").empty();

    $("#problema").empty();
    $("#problema").selectpicker('refresh');

	$.ajax({
		type: "POST",
		url: "{{ route('getProblemas') }}",
		data: { "id": $("#equipo").val(),"idArea": $("#area").val(),"idTip": $("#tipos").val(), "idSuc": $("#idSucursal").val(), "_token": "{{ csrf_token() }}"},
		success: function(msg){
			var len = msg.data.length;
			var l = 0;
			if(len > 0)
			{
				$("#problema").empty();
				
				while(l<len)
				{
					$("#problema").append("<option value='"+msg.data[l].id+"'>"+msg.data[l].nombre+"</option>");
					l++;
				}
				$("#problema").selectpicker('refresh');
			}
		},
		error: function(){
			swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Algo ha salido mal!',
			  footer: 'Problemas? sit@prigo.com.mx	',
			});
			$('.button').prop('disabled', false);
		}
	});
}

$("#sendRequest").click(function(){
    if($("#idSucursal").val() && $("#tipos").val() && $("#area").val() && $("#equipo").val() && $("#problema").val())
    {

        swal({
            title: "Estas segur@?",
            text: "La solicitud sera enviada directamente al area de mantenimiento!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            allowOutsideClick: false,
            confirmButtonText: 'Si, enviar solicitud!',
            cancelButtonText: 'No, cancelar!'
        }).then((result) => {
            if (result) {
                $('.button').prop('disabled', true);
            swal({
                title: 'Guardando...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: false,
                showConfirmButton: false,
                text: 'Espere un momento...'
            });
            
            $('.button').prop('disabled', false);
            myformdata = new FormData($("#NewFormManto")[0]);
            $.ajax({
                type: "POST",
                url: "{{ route('guardasolicitudmanto') }}",
                data: myformdata,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function(msg){
                    obj = $.parseJSON(msg);

                    if(obj.success)
                    {
                        $("#NewFormManto")[0].reset();
                        $("#equipo").selectpicker('refresh');
                        $("#tipos").selectpicker('refresh');
                        $("#problema").selectpicker('refresh');
                        $("#area").selectpicker('refresh');
                        $("#idSucursal").selectpicker('refresh');
                    }

                    swal({
                        type: obj.resultType,
                        title: obj.msg
                    });

                    $('.button').prop('disabled', false);
                },
                error: function(){
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Algo ha salido mal!',
                        footer: 'Problemas? sit@prigo.com.mx	',
                    });
                    $('.button').prop('disabled', false);
                }
            });	
            
            }
        });
    } else {
        swal({
            type: 'error',
            title: 'Oops...',
            text: 'Agregue al menos un elemento a la lista!',
            footer: 'Problemas? sit@prigo.com.mx',
        });
    }
});
</script>
@endsection