<table class="table table-condensed table-striped">
	<thead>
		<tr>
			<th>#</th>							
			<th>Empleado</th>							
			<th>Puesto</th>					
		</tr>
	</head>
	<tbody>
		@foreach($detalle AS $dato)
			<tr @if($dato->excedente ==1) style="background: #cc0000;" @elseif($dato->excedente ==2) style="background: #0099ff;" @elseif($dato->excedente ==3) style="background: #33cc33;" @endif><td class="text-left">{{ $loop->iteration }}</td><td class="text-left">{{ $dato->nombre }}</td><td  class="text-right">{{ $dato->puesto }}</td></tr>
		@endforeach
	</tbody>
</table>