@extends('layouts.newlay')
@include('menu.vacantes', ['seccion' => 'getBajas'])
@section('content')
<form id="formExporta" action="{{ route('exportavacantes') }}" method="POST" target="_blank" class="form-horizontal">
<div class="row">

		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="tipo" value="{{ $tipo }}">
		<label class="col-sm-1 col-form-label">Puesto</label>
		<div class="col-sm-3">
			<div class="form-group bmd-form-group">
				<input id="findPuesto" name="findPuesto"  type="text" class="form-control" >
			</div>
		</div>
		<label class="col-sm-1 col-form-label">Sucursal</label>
		<div class="col-sm-3">
			<div class="form-group bmd-form-group">
				<input id="findSucursal" name="findSucursal" type="text" class="form-control">
			</div>
		</div>
		<div class="col-sm-1">
			<button id="findVacantebtn" class="btn btn-white btn-round btn-just-icon">
			<i class="material-icons">search</i>
			<div class="ripple-container"></div>
			</button>
		</div>
		<div class="col-sm-1">
			<button id="exportVacantebtn" class="btn btn-white btn-round btn-just-icon">
				<i class="material-icons">cloud_download</i>
				<div class="ripple-container"></div>
			</button>
		</div>
</div>
</form>
<div class="row">
<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
	<thead>
	  <tr>
		  <th>Sucursal</th>		  
          <th>Puesto</th>
					<th>Nombre</th>
					<th>Fecha</th>
		  <th>Acci&oacute;n</th>
	  </tr>
	</thead>
	<tfoot>
	  <tr>
		  <th>Sucursal</th>		  
		  <th>Puesto</th>
			<th>Nombre</th>
			<th>Fecha</th>
		  <th>Acci&oacute;n</th>
	  </tr>
	</tfoot>
	<tbody>
	</tbody>
</table>
</div>
@endsection
@section('jsimports')
  <script src="{{ asset('MaterialBS/js/plugins/bootstrap-selectpicker.js') }}"></script>
  <script src="{{ asset('MaterialBS/js/plugins/jquery.select-bootstrap.js') }}"></script>
  <script src="{{ asset('MaterialBS/js/plugins/bootstrap-tagsinput.js') }}"></script>
  <script src="{{ asset('MaterialBS/assets-for-demo/js/modernizr.js') }}"></script>

  <script src="{{ asset('MaterialBS/js/plugins/jquery.datatables.js') }}"></script>
@endsection
@section('aditionalScripts')
  <script type="text/javascript">

function cancelaBaja(id, nom){

	swal({
			title: "Estas segur@?",
			text: "Se regresara al empleado "+nom+" a la plantilla!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			allowOutsideClick: false,
			confirmButtonText: 'Si, actualizar!',
			cancelButtonText: 'No, cancelar!'
		}).then((result) => { 
			swal({
				title: "Puedes agregar un comentario",
				input: 'textarea',
				inputPlaceholder: 'Type your message here...',
				showCancelButton: true
			}).then((value) => {
					swal({
						title: 'Guardando...',
						allowEscapeKey: false,
						allowOutsideClick: false,
						showCancelButton: false,
						showConfirmButton: false,
						text: 'Espere un momento...'
					});
					$.ajax({
						type: "POST",
						url: "{{ route('guardabaja') }}",
						data: { "_token": "{{ csrf_token() }}" ,"accion": 2, "id": id, "comentario": value},
						success: function(msg){
							swal({
								type: 'success',
								title: 'Tu vacante se ha actualizado!'
							});
							
							$('.button').prop('disabled', false);
							
						},
						error: function(){
							swal({
								type: 'error',
								title: 'Oops...',
								text: 'Algo ha salido mal!',
								footer: 'Problemas? sit@prigo.com.mx	',
							});
							$('.button').prop('disabled', false);
						}
					});

			});
		});
}

function confirmaBaja(id, nom){

	swal({
			title: "Estas segur@?",
			text: "Se aplicara la baja al empleado: "+nom+"!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			allowOutsideClick: false,
			confirmButtonText: 'Si, actualizar!',
			cancelButtonText: 'No, cancelar!'
		}).then((result) => { 
			swal({
				title: "Puedes agregar un comentario",
				input: 'textarea',
				inputPlaceholder: 'Type your message here...',
				showCancelButton: true
			}).then((value) => {
					swal({
						title: 'Guardando...',
						allowEscapeKey: false,
						allowOutsideClick: false,
						showCancelButton: false,
						showConfirmButton: false,
						text: 'Espere un momento...'
					});
					$.ajax({
						type: "POST",
						url: "{{ route('guardabaja') }}",
						data: { "_token": "{{ csrf_token() }}" ,"accion": 1, "id": id, "comentario": value},
						success: function(msg){
							swal({
								type: 'success',
								title: 'Tu vacante se ha actualizado!'
							});
							
							$('.button').prop('disabled', false);
							
						},
						error: function(){
							swal({
								type: 'error',
								title: 'Oops...',
								text: 'Algo ha salido mal!',
								footer: 'Problemas? sit@prigo.com.mx	',
							});
							$('.button').prop('disabled', false);
						}
					});

			});
		});
	}



$(document).ready(function() {

    $('#datatables').DataTable({
        "responsive": true,	
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "{{ route('getSolicitudesBaja') }}",
            "type": "POST",
			"data": function ( d ) {
                d._token = "{{ csrf_token() }}";
            }
        },
        "columns": [
            { "data": "sucursal" },
            { "data": "puesto" },
            { "data": "nombre" },
						{ "data": "fecha" },
			{ "render": function (data, type, row, meta) {
				return "<a href=\"#\" onclick=\"confirmaBaja('"+row.idEmpleado+"','"+row.nombre+"')\" class=\"btn btn-link btn-danger btn-just-icon like\"><i class=\"material-icons\">remove_circle_outline</i></a> <a href=\"#\" onclick=\"cancelaBaja('"+row.idEmpleado+"','"+row.nombre+"')\" class=\"btn btn-link btn-success btn-just-icon like\"><i class=\"material-icons\">open_in_new</i></a>";
				}
			}
        ]
    });

    var table = $('#datatables').DataTable();
 
	$('#findVacantebtn').on( 'click', function (event) {
		event.preventDefault();
		if($("#findSucursal").val() != "")
			table.column(2).search( $("#findSucursal").val() );
		else
			table.column(2).search("");
		if($("#findPuesto").val() != "")
			table.column(3).search( $("#findPuesto").val() );
		else
			table.column(3).search("");
		if($("#findSucursal").val() != "" || $("#findPuesto").val() != "")
			table.draw();
	} );
	$("#datatables_filter").hide();
	$('#exportVacantebtn').on( 'click', function () {
		if($("#findSucursal").val() != "")
			table.column(2).search( $("#findSucursal").val() );
		else
			table.column(2).search("");
		if($("#findPuesto").val() != "")
			table.column(3).search( $("#findPuesto").val() );
		else
			table.column(3).search("");
		if($("#findSucursal").val() != "" || $("#findPuesto").val() != "")
			table.draw();
		$('form#formExporta').submit();
	} );
});

</script>

@endsection