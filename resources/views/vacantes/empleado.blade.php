@extends('layouts.newlay')
@include('menu.vacantes', ['seccion' => 'empleados'])
@section('content')
<div class="row">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $empleado->nombre }} - {{$empleado->idEmpleado}}</h4>
        </div>
        <div class="card-content">
            <div class="row">
                <div class="col-md-12">
                    <h4>Detalle</h4>
                </div>
                <div class="col-md-3">
                    <i class="material-icons">work</i><p>{{ $empleado->puesto }}</p>
                </div>
                <div class="col-md-3">
                    <i class="material-icons">store</i><p>{{ $empleado->sucursal }}</p>
                </div>
                <div class="col-md-3">
                    <i class="material-icons">info</i><p>{{ $empleado->estado }}</p>
                </div>
                <div class="col-md-3">
                    <i class="material-icons">calendar_today</i><p>@if($empleado->fecha == '0000-00-00' || empty($empleado->fecha)) Plantilla @else {{$empleado->fecha}} @endif</p>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-info btn-round" onclick="editPuesto()"><span class="btn-label">
                            <i class="material-icons">edit</i>
                        </span>Puesto<div class="ripple-container"></div></button>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-info btn-round" onclick="editSucursal()"><span class="btn-label">
                            <i class="material-icons">edit</i>
                        </span>Sucursal<div class="ripple-container"></div></button>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-info btn-round" onclick="editNombre()"><span class="btn-label">
                            <i class="material-icons">edit</i>
                        </span>Nombre<div class="ripple-container"></div></button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4>Historial</h4>
                    <ul class="timeline timeline-simple">
                        @foreach($partidas AS $partida)
                        <li class="timeline-inverted">
                            <div class="timeline-badge @if( $partida->solicitud == "Solicita contratacion") success @elseif($partida->solicitud == "Baja Confirmada") danger @else info @endif">
                                <i class="material-icons">@if( $partida->solicitud == "Solicita contratacion") card_travel @elseif($partida->solicitud == "Baja Confirmada") trash @else info @endif</i>
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <span class="label label-default">{{$partida->fecha}}</span>
                                </div>
                                <div class="timeline-body">
                                    <p>{{$partida->solicitud}} @if( $partida->solicitud == "Cambio de Nombre" || $partida->solicitud == "Cambio de Sucursal"  || $partida->solicitud ==  "Cambio de Puesto") - {{$partida->puesto }} @endif</p>
                                </div>
                                <h6>
                                    <i class="ti-time"></i>
                                    {{ $partida->usuario}}
                                </h6>
                            </div>
                        </li>
                        @endforeach
                    <ul>
                </div>                
            </div>
        </div>
    </div>
</div>
@endsection
@section('jsimports')
  <script src="{{ asset('MaterialBS/js/plugins/bootstrap-selectpicker.js') }}"></script>
  <script src="{{ asset('MaterialBS/js/plugins/jquery.select-bootstrap.js') }}"></script>
  <script src="{{ asset('MaterialBS/js/plugins/bootstrap-tagsinput.js') }}"></script>
  <script src="{{ asset('MaterialBS/assets-for-demo/js/modernizr.js') }}"></script>
  <script src="{{ asset('MaterialBS/js/plugins/jquery.datatables.js') }}"></script>
@endsection
@section('aditionalScripts')
<script>
    function editNombre(){
        swal({
            title: 'Modificación de Nombre',
            html: '<div class="form-group">' +
                        '<input id="modNombre" type="text" class="form-control" value="{{ $empleado->nombre}}" />' +
                    '</div>',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            $.ajax({
                type: "POST",
                url: "{{ route('actualizaEmpleado') }}",
                data: { "idEmpleado": {{ $empleado->idEmpleado }}, "dato": "1", "valor": $("#modNombre").val(), "_token": "{{ csrf_token() }}" },
                success: function(msg){
                    console.log(msg);
                    obj = JSON.parse(msg);
                    if(obj.success)
                    {
                        swal({
                            type: 'success',
                            title: 'Los datos han sido actualizados!'
                        });
                    }
                    else
                    {
                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Algo ha salido mal!',
                            footer: 'Problemas? sit@prigo.com.mx	',
                        });
                    }
                }
            });
        }).catch(swal.noop);
    }

    function editPuesto(){
        swal({
            title: 'Modificación de Puesto',
            html: '<div class="form-group">' +
                        '<select id="modPuesto" type="text" class="form-control">'+
                    @foreach($puestos as $puesto)
                    '<option value="{{ $puesto->idPuesto }}" @if($puesto->idPuesto==$empleado->idPuesto) SELECTED @endif>{{ $puesto->nombre }}</option>'+
                    @endforeach
                    '</select>' +
                    '</div>',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {

            $.ajax({
                type: "POST",
                url: "{{ route('actualizaEmpleado') }}",
                data: { "idEmpleado": {{ $empleado->idEmpleado }}, "dato": "2", "valor": $("#modPuesto").val(), "_token": "{{ csrf_token() }}" },
                success: function(msg){
                    obj = JSON.parse(msg);
                    if(obj.success)
                    {
                        swal({
                            type: 'success',
                            title: 'Los datos han sido actualizados!'
                        });
                    }
                    else
                    {
                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Algo ha salido mal!',
                            footer: 'Problemas? sit@prigo.com.mx	',
                        });
                    }
                }
            });
        }).catch(swal.noop);
    }
    
    function editSucursal(){
        swal({
            title: 'Modificación de Sucursal',
            html: '<div class="form-group">' +
                    '<select id="modSucursal" type="text" class="form-control">'+
                    @foreach($sucursales as $sucursal)
                    '<option value="{{ $sucursal->idSucursal }}" @if($sucursal->idSucursal==$empleado->idSucursal) SELECTED @endif>{{ $sucursal->nombre }}</option>'+
                    @endforeach
                    '</select>' +
                  '</div>',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            $.ajax({
                type: "POST",
                url: "{{ route('actualizaEmpleado') }}",
                data: { "idEmpleado": {{ $empleado->idEmpleado }}, "dato": "3", "valor": $("#modSucursal").val(), "_token": "{{ csrf_token() }}" },
                success: function(msg){
                    obj = JSON.parse(msg);
                    if(obj.success)
                    {
                        swal({
                            type: 'success',
                            title: 'Los datos han sido actualizados!'
                        });
                    }
                    else
                    {
                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Algo ha salido mal!',
                            footer: 'Problemas? sit@prigo.com.mx	',
                        });
                    }
                }
            });
        }).catch(swal.noop);
    }
</script>
@endsection
