@extends('layouts.newlay')
@include('menu.biblioteca', ['seccion' => 'index'])
@section('content')
<div class="row" style="z-index: 1050!important;">
      <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-tabs" data-background-color="orange">
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <span class="nav-tabs-title">Areas:</span>
                        <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="#mkt" data-toggle="tab">
                                    <i class="material-icons">language</i>
                                    Marketing
                                <div class="ripple-container"></div></a>                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#rh" data-toggle="tab">
                                    <i class="material-icons">assignment_ind</i>
                                    RH
                                <div class="ripple-container"></div></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#ubereats" data-toggle="tab">
                                    <i class="material-icons">shopping_basket</i>
                                    UberEats
                                <div class="ripple-container"></div></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#cocina" data-toggle="tab">
                                    <i class="material-icons">store</i>
                                    Cocina
                                <div class="ripple-container"></div></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#barra" data-toggle="tab">
                                    <i class="material-icons">local_cafe</i>
                                    Barra
                                <div class="ripple-container"></div></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#vitrina" data-toggle="tab">
                                    <i class="material-icons">fastfood</i>
                                    Vitrina
                                <div class="ripple-container"></div></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#salon" data-toggle="tab">
                                    <i class="material-icons">local_dining</i>
                                    Salon
                                <div class="ripple-container"></div></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#gerencial" data-toggle="tab">
                                    <i class="material-icons">work</i>
                                    Gerencial
                                <div class="ripple-container"></div></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#general" data-toggle="tab">
                                    <i class="material-icons">folder</i>
                                    General
                                <div class="ripple-container"></div></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
			<div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="mkt">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Etiquetas AICM</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/marketing/etiquetas vitrina_ aeropuerto_17052019.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Etiquetas Conservas</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/marketing/etiquetasconservas2.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="rh">
                            <div class="row">
                                    <div class="col-md-3">
                                        <div class="card card-pricing card-raised">
                                            <div class="content">
                                                <div class="icon">
                                                    <i class="material-icons">picture_as_pdf</i>
                                                </div>
                                                <p class="card-description">VACACIONES PRIGO.pdf</p>
                                                <a target="_blank" href="{{ Storage::url('biblioteca/rh/VACACIONES PRIGO.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-3">
                                        <div class="card card-pricing card-raised">
                                            <div class="content">
                                                <div class="icon">
                                                    <i class="material-icons">picture_as_pdf</i>
                                                </div>
                                                <p class="card-description">Politica de comanda.pdf</p>
                                                <a target="_blank" href="{{ Storage::url('biblioteca/rh/Politica de comanda.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-3">
                                        <div class="card card-pricing card-raised">
                                            <div class="content">
                                                <div class="icon">
                                                    <i class="material-icons">picture_as_pdf</i>
                                                </div>
                                                <p class="card-description">Manual Trámite de TIA.pdf</p>
                                                <a target="_blank" href="{{ Storage::url('biblioteca/rh/Manual Trámite de TIA.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                            </div>
                                        </div>
                                    </div>      
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Mapeo Talento.pdf</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/rh/DRH-CAP-F-11 Mapeo Talento.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>                                
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Entrenamiento de área.pdf</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/rh/DRH-CAP-F-10 Entrenamiento de área.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>
						
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Protocolo de Bienvenida.pdf</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/rh/DRH-CAP-F-02 Protocolo de Bienvenida.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>
                                
                                    <!--div class="col-md-3">
                                        <div class="card card-pricing card-raised">
                                            <div class="content">
                                                <div class="icon">
                                                    <i class="material-icons">picture_as_pdf</i>
                                                </div>
                                                <p class="card-description">Manual de Inducción.pdf</p>
                                                <a target="_blank" href="{{ Storage::url('biblioteca/rh/Manual de Inducción.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                            </div>
                                        </div>
                                    </div-->                                    
                                    <div class="col-md-3">
                                        <div class="card card-pricing card-raised">
                                            <div class="content">
                                                <div class="icon">
                                                    <i class="material-icons">picture_as_pdf</i>
                                                </div>
                                                <p class="card-description">Guía de Ejecución Entrenamiento</p>
                                                <a target="_blank" href="{{ Storage::url('biblioteca/rh/Guía de Ejecución del Entrenamiento.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                            </div>
                                        </div>
                                    </div>
                                                                
                                    <div class="col-md-3">
                                        <div class="card card-pricing card-raised">
                                            <div class="content">
                                                <div class="icon">
                                                    <i class="material-icons">picture_as_pdf</i>
                                                </div>
                                                <p class="card-description">Política de salud e higiene...</p>
                                                <a target="_blank" href="{{ Storage::url('biblioteca/rh/DRH-RL-POL-05 Política de salud e higiene en la elaboración de producto.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                            </div>
                                        </div>
                                    </div>                                
                                </div>

                    </div>
                    <div class="tab-pane" id="ubereats">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Politicas Uber.pdf</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/ubereats/politicas Uber.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Empaque</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/ubereats/Manual empaque uber eats.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Ayudas Visuales</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/ubereats/AYUDAS VISUALES.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manejo de quejas</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/ubereats/Manejo de quejas.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Entrega servicio a domicilio</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/ubereats/Entrega servicio a domicilio.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                    <div class="card card-pricing card-raised">
                                        <div class="content">
                                            <div class="icon">
                                                <i class="material-icons">picture_as_pdf</i>
                                            </div>
                                            <p class="card-description">CHECKLIST Supervision</p>
                                            <a target="_blank" href="{{ Storage::url('biblioteca/ubereats/CHECK_LIST DE SUPERVISION.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                        </div>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                    <div class="tab-pane" id="cocina">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Temporalidad Septiembre</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/ManualTemporalidadSeptiembre.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Tiempos de Vida</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/TiemposdeVida300419.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Desayunos</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/ManualDesayunos2019EK.pdf?r=5345345tgfrt') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Comidas</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/ManualComidas2019EK.pdf?r=4534rf34re') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Pan Sorpresa</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/ManualPanSorpresa2019.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Sandwiches</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/ManualSandwichesVersionAgo19.pdf?r=435353534dd') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Sandwiches Vitrina</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/ManualSandwichesVitrina.pdf?r=421353534dd') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Ensaladas Vitrina</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/ManualEnsaladasVitrina.pdf?r=421353534dd') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Catering Mini Cr.</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/ManualCateringMiniCroissant.pdf?r=435353534dd') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Limpieza Equipos</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/ManualLimpiezaEquipos.pdf?r=435353534dd') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="barra">
                        <div class="row">
							<div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Vinos</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/DRH-CAP-MA-31-ManualVinos.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Manual de Tés</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/barra/DRH-CAP-MA-36 Manual de Tes.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Manual de Bebidas.pdf</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/rh/Manual de Bebidas.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Manual Bebidas Thermo</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/barra/ManualBebidasThermo.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				    <div class="tab-pane" id="vitrina">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Horneado pan dulce</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/vitrina/ManualPanDulce.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Horneado Galette</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/vitrina/ManualHorneadoGalette.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Pan Dulce Caja</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/vitrina/ManualPanDulceCaja.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Helados</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/cocina/ManualHelados.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
							<div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Jugos e Infusiones</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/vitrina/ManualJugosInfusiones.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Manual Empaques</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/rh/ManualEmpaques.pdf?r=3453453ff') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>
                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual Merchandising</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/marketing/ManualMerchandising.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>				
                    <div class="tab-pane" id="salon">
                        <div class="row">

                            <div class="col-md-3">
                                <div class="card card-pricing card-raised">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="material-icons">picture_as_pdf</i>
                                        </div>
                                        <p class="card-description">Manual de Salon</p>
                                        <a target="_blank" href="{{ Storage::url('biblioteca/salon/ManualSalon.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
                    <div class="tab-pane" id="gerencial">
                        <div class="row">							
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Manual de Pedidos.pdf</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/rh/ManualPedidos.pdf?r=343254654r') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Manual Equipos</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/rh/ManualEquipos.pdf?r=343254654r') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>
                                                                 <div class="col-md-3">
                                        <div class="card card-pricing card-raised">
                                            <div class="content">
                                                <div class="icon">
                                                    <i class="material-icons">picture_as_pdf</i>
                                                </div>
                                                <p class="card-description">Manual Manejo de Crisis.pdf</p>
                                                <a target="_blank" href="{{ Storage::url('biblioteca/rh/Manual Manejo de Crisis.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                            </div>
                                        </div>
                                    </div>                              
                                    <div class="col-md-3">
                                        <div class="card card-pricing card-raised">
                                            <div class="content">
                                                <div class="icon">
                                                    <i class="material-icons">picture_as_pdf</i>
                                                </div>
                                                <p class="card-description">Manual de Inventario...</p>
                                                <a target="_blank" href="{{ Storage::url('biblioteca/rh/Manual de Inventario en Sucursal.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                            </div>
                                        </div>
                                    </div>                            
                                    <div class="col-md-3">
                                        <div class="card card-pricing card-raised">
                                            <div class="content">
                                                <div class="icon">
                                                    <i class="material-icons">picture_as_pdf</i>
                                                </div>
                                                <p class="card-description">Manual Envio de Información al Corporativo</p>
                                                <a target="_blank" href="{{ Storage::url('biblioteca/rh/ManualEnvioInformacion.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
                                            </div>
                                        </div>
                                    </div>
						</div>
					</div>
                    <div class="tab-pane" id="general">
                        <div class="row">
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Manual de Producto</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/general/ManualProducto.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Manual Calidad y Servicio</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/general/ManualCalidadServicio.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Manual Apertura-Cierre Sucursal</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/general/ManualAperturaCierre.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>                                
							<div class="col-md-3">
								<div class="card card-pricing card-raised">
									<div class="content">
										<div class="icon">
											<i class="material-icons">picture_as_pdf</i>
										</div>
										<p class="card-description">Manual Micros.pdf</p>
										<a target="_blank" href="{{ Storage::url('biblioteca/rh/Manual Micros.pdf') }}" class="btn btn-warning btn-round">Descargar</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('jsimports')
<script src="{{ asset('MaterialBS/js/plugins/bootstrap-selectpicker.js') }}"></script>
  <script src="{{ asset('MaterialBS/js/plugins/jquery.select-bootstrap.js') }}"></script>
  <script src="{{ asset('MaterialBS/js/plugins/bootstrap-tagsinput.js') }}"></script>
  <script src="{{ asset('MaterialBS/assets-for-demo/js/modernizr.js') }}"></script>
@endsection
@section('aditionalScripts')
  
  <script type="text/javascript">

  </script>
@endsection