<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
    <title>Experiencia - Maison Kayser</title>
	<div id="principal" class="container h-100">
		<div class="d-flex justify-content-center h-100">
			<div class="user_card">
				<div class="d-flex justify-content-center">
					<div class="brand_logo_container">
						<img src="http://maison-kayser.com.mx/img/KAY_Logo_blanco_Square.png" class="brand_logo" alt="Logo">
					</div>
				</div>
				<div class="d-flex justify-content-center form_container">
					<form method="POST">
            <label for="sel1">Número de ticket:</label>
						<div class="input-group mb-3">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input name="num_ticket" id="num_ticket" class="form-control"  placeholder="" 
                            oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            type = "number"
                            maxlength = "5" style="font-size: 80%;"
                            />
                            <input type="hidden" name="id_sucursal" class="form-control" id="id_sucursal" value='{{$id_sucursal}}'>
                        </div>
                          <label for="sel1">¡Compártenos tu experiencia en Maison Kayser!</label>
						<div class="select_p">
							<select class="form-control" id="sel1">
								<option  selected>Selecciona una opción</option>
								<option  value="1">Vitrina</option>
								<option  value="2">Restaurante</option>
							  </select> 
						</div>
					<div class="d-flex justify-content-center mt-3 login_container">
				 	    	<button type="button"  id="btnIniciar" class="btn iniciar_btn">Iniciar</button>
                    </div>
                    
                    <br>
					</form>
				</div>
            </div>
		</div>
		</div>
	</div>
</body>
</html>