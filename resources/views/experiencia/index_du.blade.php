@extends('layouts.encuesta')
@section('content')
    @include('experiencia.index_empty')
    @include('experiencia.modal_inicio_r')
    @include('experiencia.modal_inicio_v')
    @include('experiencia.restaurante_e')
    @include('experiencia.vitrina_e')
    @include('experiencia.final')
@stop
@section('addScripts')
    <script src="{{ asset('js/funcionalidad_nt.js') }}"></script>
@stop
