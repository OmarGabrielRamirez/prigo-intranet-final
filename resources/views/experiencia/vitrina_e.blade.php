<!-- Modal Encuesta Vitrina 1-->
<div class="modal fade" id="encuestaV1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="content_f">
                        <img align="left" src="https://image.flaticon.com/icons/svg/1632/1632670.svg" width="30px" height="30px">
                        <h4 class="titulo_v" align="left">Vitrina - <span class="span_e">Experiencia del cliente </span></h4>
                    </div>        
                </div>
                <div class="modal-body">
                    <div class="progress" style="height:12px">
                        <div  class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%"></div>
                    </div>
                    <div class="texto_v">
                        <h4 class="pregunta_1">1/2 ¿Cómo estuvo la experiencia?</h4>
                        <h4 class="pregunta_1_1">1/2 Plase rate your experince.</h4>
                    </div>
                    <div class="icons_v" align="center">
                    <li><input type="radio" name="type" id="cb1" value="Bien"/>
                        <label class="ck" for="cb1"><img src="https://image.flaticon.com/icons/svg/456/456115.svg" /></label>
                      </li>
                      <li><input type="radio" name="type" id="cb2" value="Mal"/>
                        <label class="ck" for="cb2"><img src="https://image.flaticon.com/icons/svg/1455/1455915.svg" /></label>
                      </li>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSiguienteP1" class="btn siguiente_v" disabled>Siguiente</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Encuesta Vitrina 1-->
<!-- Modal Encuesta Vitrina 2-->
<div class="modal fade" id="encuestaV2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="content_f">
                        <img align="left" src="https://image.flaticon.com/icons/svg/1632/1632670.svg" width="30px" height="30px">
                        <h4 class="titulo_v" align="left">Vitrina - <span class="span_e">Experiencia del cliente </span></h4>
                    </div>        
                </div>
                <div class="modal-body">
                    <div class="progress" style="height:12px">
                        <div  class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
                    </div>
                    <div class="texto_v">
                        <h4 class="pregunta_1">2/2 ¿Qué tan probable es que recomiendes Maison Kayser a tus familiares o amigos?</h4>
                        <h4 class="pregunta_1_1">2/2 How likely are you to recommend Maison Kayser to a friend or relative?</h4>
                    </div>
                    <div class="icons_v" align="center">
                        <p class="clasificacion">
                            <input id="radio1.1" type="radio" name="estrellas-v" value="10">
                            <label for="radio1.1">10</label>
                            <input id="radio2.2" type="radio" name="estrellas-v" value="9">
                            <label for="radio2.2">9</label>
                            <input id="radio3.3" type="radio" name="estrellas-v" value="8">
                            <label for="radio3.3">8</label>
                            <input id="radio4.4" type="radio" name="estrellas-v" value="7">
                            <label for="radio4.4">7</label>
                            <input id="radio5.5" type="radio" name="estrellas-v" value="6">
                            <label for="radio5.5">6</label>
                            <input id="radio6.6" type="radio" name="estrellas-v" value="5">
                            <label for="radio6.6">5</label>
                            <input id="radio7.7" type="radio" name="estrellas-v" value="4">
                            <label for="radio7.7">4</label>
                            <input id="radio8.8" type="radio" name="estrellas-v" value="3">
                            <label for="radio8.8">3</label>
                            <input id="radio9.9" type="radio" name="estrellas-v" value="2">
                            <label for="radio9.9">2</label>
                            <input id="radio10.10" type="radio" name="estrellas-v" value="1">
                            <label for="radio10.10">1</label>
                            <input id="radio11.11" type="radio" name="estrellas-v" value="0">
                            <label for="radio11.11">0</label>
                          </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnFinal" class="btn siguiente_v" disabled>FInalizar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Encuesta Vitrina 2-->