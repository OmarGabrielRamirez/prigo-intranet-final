<!-- Modal Encuesta Restaurante 1-->
<div class="modal fade" id="encuestaR1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="content_f">
                        <img align="left" src="https://image.flaticon.com/icons/svg/1632/1632670.svg" width="30px" height="30px">
                        
                        <span class="titulo_v"> Restaurante -</span>  <span class="span_e">Experiencia del cliente </span>
                    </div>        
                </div>
                <div class="modal-body">
                    <div class="progress" style="height:12px">
                        <div  class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="12.5" aria-valuemin="0" aria-valuemax="100" style="width:12.5%"></div>
                    </div>
                    <div class="texto_v">
                        <span class="pregunta_1">1/8 Califique la atención y amabilidad del personal.</span>
                        <br>
                        <span class="pregunta_1_1">1/8 Plase rate our staff members' kindness and attentiveness</span>
                    </div>
                    <div class="icons_v" align="center">
                        <p class="clasificacion">
                            <input id="radio1" type="radio" name="estrellas" value="5">
                            <label for="radio1">★</label>
                            <input id="radio2" type="radio" name="estrellas" value="4">
                            <label for="radio2">★</label>
                            <input id="radio3" type="radio" name="estrellas" value="3">
                            <label for="radio3">★</label>
                            <input id="radio4" type="radio" name="estrellas" value="2">
                            <label for="radio4">★</label>
                            <input id="radio5" type="radio" name="estrellas" value="1">
                            <label for="radio5">★</label>
                          </p>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSig1" class="btn siguiente_v" disabled>Siguiente</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Encuesta Restaurante 1-->
<!-- Modal Encuesta Restaurante 2-->
<div class="modal fade" id="encuestaR2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="content_f">
                        <img align="left" src="https://image.flaticon.com/icons/svg/1632/1632670.svg" width="30px" height="30px">
                        <span class="titulo_v"> Restaurante -</span>  <span class="span_e">Experiencia del cliente </span>
                    </div>        
                </div>
                <div class="modal-body">
                    <div class="progress" style="height:12px">
                        <div  class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:25%"></div>
                    </div>
                    <div class="texto_v">
                        <h4 class="pregunta_1">2/8 Califique la calidad de los alimentos.</h4>
                        <h4 class="pregunta_1_1">2/8 Plase rate the quality of our products.</h4>
                    </div>
                    <div class="icons_v" align="center">
                        <p class="clasificacion">
                            <input id="radio6" type="radio" name="estrellas-2" value="5">
                            <label for="radio6">★</label>
                            <input id="radio7" type="radio" name="estrellas-2" value="4">
                            <label for="radio7">★</label>
                            <input id="radio8" type="radio" name="estrellas-2" value="3">
                            <label for="radio8">★</label>
                            <input id="radio9" type="radio" name="estrellas-2" value="2">
                            <label for="radio9">★</label>
                            <input id="radio10" type="radio" name="estrellas-2" value="1">
                            <label for="radio10">★</label>
                          </p>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSig2" class="btn siguiente_v" disabled>Siguiente</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Encuesta Restaurante 2-->
<!-- Modal Encuesta Restaurante 3-->
<div class="modal fade" id="encuestaR3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="content_f">
                        <img align="left" src="https://image.flaticon.com/icons/svg/1632/1632670.svg" width="30px" height="30px">
                        <span class="titulo_v"> Restaurante -</span>  <span class="span_e">Experiencia del cliente </span>
                    </div>        
                </div>
                <div class="modal-body">
                    <div class="progress" style="height:12px">
                        <div  class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow=37.5 aria-valuemin="0" aria-valuemax="100" style="width:37.5%"></div>
                    </div>
                    <div class="texto_v">
                        <h4 class="pregunta_1">3/8 Califique la calidad de nuestro servicio.</h4>
                        <h4 class="pregunta_1_1">3/8 Plase rate our service.</h4>
                    </div>
                    <div class="icons_v" align="center">
                        <p class="clasificacion">
                            <input id="radio11" type="radio" name="estrellas-3" value="5">
                            <label for="radio11">★</label>
                            <input id="radio12" type="radio" name="estrellas-3" value="4">
                            <label for="radio12">★</label>
                            <input id="radio13" type="radio" name="estrellas-3" value="3">
                            <label for="radio13">★</label>
                            <input id="radio14" type="radio" name="estrellas-3" value="2">
                            <label for="radio14">★</label>
                            <input id="radio15" type="radio" name="estrellas-3" value="1">
                            <label for="radio15">★</label>
                          </p>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSig3" class="btn siguiente_v" disabled>Siguiente</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Encuesta Restaurante 3-->
<!-- Modal Encuesta Restaurante 4-->
<div class="modal fade" id="encuestaR4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="content_f">
                        <img align="left" src="https://image.flaticon.com/icons/svg/1632/1632670.svg" width="30px" height="30px">
                        <span class="titulo_v"> Restaurante -</span>  <span class="span_e">Experiencia del cliente </span>
                    </div>        
                </div>
                <div class="modal-body">
                    <div class="progress" style="height:12px">
                        <div  class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%"></div>
                    </div>
                    <div class="texto_v">
                        <h4 class="pregunta_1">4/8 ¿Le ofrecieron el platillo del mes?</h4>
                        <h4 class="pregunta_1_1">4/8 Were you offered  this month's special.</h4>
                    </div>
                    <div class="contenedor">
                        <select id="select_platomes"class="browser-default custom-select">
                            <option selected>Selecciona una opción</option>
                            <option value="S">Sí / Yes</option>
                            <option value="N">No</option>
                          </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSig4" class="btn siguiente_v" disabled>Siguiente</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Encuesta Restaurante 4-->
<!-- Modal Encuesta Restaurante 5-->
<div class="modal fade" id="encuestaR5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="content_f">
                        <img align="left" src="https://image.flaticon.com/icons/svg/1632/1632670.svg" width="30px" height="30px">
                        <span class="titulo_v"> Restaurante -</span>  <span class="span_e">Experiencia del cliente </span>
                    </div>        
                </div>
                <div class="modal-body">
                    <div class="progress" style="height:12px">
                        <div  class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="62.5" aria-valuemin="0" aria-valuemax="100" style="width:62.5%"></div>
                    </div>
                    <div class="texto_v">
                        <h4 class="pregunta_1">5/8 ¿Qué tan probable es que recomiendes Maison Kayser a tus familiares o amigos?</h4>
                        <h4 class="pregunta_1_1">5/8 How likely are you to recommend Maison Kayser to a friend or relative?</h4>
                    </div>
                    <div class="icons_v" align="center">
                        <p class="clasificacion">
                            <input id="radio16" type="radio" name="estrellas-5" value="10">
                            <label for="radio16">10</label>
                            <input id="radio17" type="radio" name="estrellas-5" value="9">
                            <label for="radio17">9</label>
                            <input id="radio18" type="radio" name="estrellas-5" value="8">
                            <label for="radio18">8</label>
                            <input id="radio19" type="radio" name="estrellas-5" value="7">
                            <label for="radio19">7</label>
                            <input id="radio20" type="radio" name="estrellas-5" value="6">
                            <label for="radio20">6</label>
                            <input id="radio21" type="radio" name="estrellas-5" value="5">
                            <label for="radio21">5</label>
                            <input id="radio22" type="radio" name="estrellas-5" value="4">
                            <label for="radio22">4</label>
                            <input id="radio23" type="radio" name="estrellas-5" value="3">
                            <label for="radio23">3</label>
                            <input id="radio24" type="radio" name="estrellas-5" value="2">
                            <label for="radio24">2</label>
                            <input id="radio25" type="radio" name="estrellas-5" value="1">
                            <label for="radio25">1</label>
                            <input id="radio26" type="radio" name="estrellas-5" value="0">
                            <label for="radio26">0</label>
                          </p>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSig5" class="btn siguiente_v" disabled>Siguiente</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Encuesta Restaurante 5-->
<!-- Modal Encuesta Restaurante 6-->
<div class="modal fade" id="encuestaR6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="content_f">
                        <img align="left" src="https://image.flaticon.com/icons/svg/1632/1632670.svg" width="30px" height="30px">
                        <span class="titulo_v"> Restaurante -</span>  <span class="span_e">Experiencia del cliente </span>
                    </div>        
                </div>
                <div class="modal-body">
                    <div class="progress" style="height:12px">
                        <div  class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:75%"></div>
                    </div>
                    <div class="texto_v">
                        <h4 class="pregunta_1">6/8 ¿Qué tan probable es que recomiendes Maison Kayser a tus familiares o amigos?</h4>
                        <h4 class="pregunta_1_1">6/8 How likely are you to recommend Maison Kayser to a friend or relative?</h4>
                    </div>
                    <select id="select_frecuencia"class="browser-default custom-select">
                        <option selected>Selecciona una opción</option>
                        <option value="DIA">Diaria / Dally</option>
                        <option value="SEM">Semanal / Once a week</option>
                        <option value="MEN">Mensual / Monthly</option>
                        <option value="SMS">Semestral / Every 5 months</option>
                        <option value="AN">Anual / Once year</option>
                      </select>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSig6" class="btn siguiente_v" disabled>Siguiente</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Encuesta Restaurante 6-->
<!-- Modal Encuesta Restaurante 7-->
<div class="modal fade" id="encuestaR7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="content_f">
                        <img align="left" src="https://image.flaticon.com/icons/svg/1632/1632670.svg" width="30px" height="30px">
                        <span class="titulo_v"> Restaurante -</span>  <span class="span_e">Experiencia del cliente </span>
                    </div>        
                </div>
                <div class="modal-body">
                    <div class="progress" style="height:12px">
                        <div  class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="87.5" aria-valuemin="0" aria-valuemax="100" style="width:87.5%"></div>
                    </div>
                    <div class="texto_v">
                        <h4 class="pregunta_1">7/8 ¡Queremos seguir en contacto! Por favor escriba su correo electronico.</h4>
                        <h4 class="pregunta_1_1">7/8 We want to keep in touch! Please  wirte down your e-mail adress.</h4>
                    </div>
                    <div class="icons_v" align="center">
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="ejemplo@ejemplo.com" required>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSig7" class="btn siguiente_v" disabled>Siguiente</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Encuesta Restaurante 7-->
<!-- Modal Encuesta Restaurante 8-->
<div class="modal fade" id="encuestaR8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="content_f">
                        <img align="left" src="https://image.flaticon.com/icons/svg/1632/1632670.svg" width="30px" height="30px">
                        <span class="titulo_v"> Restaurante -</span>  <span class="span_e">Experiencia del cliente </span>
                    </div>        
                </div>
                <div class="modal-body">
                    <div class="progress" style="height:12px">
                        <div  class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
                    </div>
                    <div class="texto_v">
                        <h4 class="pregunta_1">8/8 Si tiene algún comentario o sugerencia, escribalo aquí.</h4>
                        <h4 class="pregunta_1_1">8/8 Let us know about any comment you have or suggestion you'd like to experience</h4>
                    </div>
                    <div class="icons_v" align="center">
                        <div class="md-form">
                            <textarea id="form7" class="md-textarea form-control" rows="3"></textarea>
                          </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btnFinal2" class="btn siguiente_v" disabled>Finalizar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Encuesta Restaurante 8-->