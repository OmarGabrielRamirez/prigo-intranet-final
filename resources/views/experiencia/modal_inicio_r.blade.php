<!-- Modal inicio Restaurante-->
<div class="modal fade" id="bienvenidaR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-body">
                    <div align="center">
                        <span class="titulo">¡Hola!</span>
                    </div>
                    <div align="center">
                        <span class="sub">¡Comparténos tu experiencia en Maison Kayser!</span>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnIniciarR" class="btn iniciar_btn">Continuar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal inicio Restaurante-->
