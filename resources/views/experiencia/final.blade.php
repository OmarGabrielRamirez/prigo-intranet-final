<!-- Modal Final-->
<div class="modal fade" id="encuestaFinal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">      
                </div>
                <div class="modal-body">
                    <div class="icons_v" align="center">
                        <img class="logo_final" src="https://image.flaticon.com/icons/svg/725/725107.svg" height="100px" width="100px" >

                    </div>
                    
                    <div align="center">
                        <span class="sub">Tus comentarios son importantes.</span>
                    </div>
                    <div align="center">
                        <span class="titulo">¡Gracias!</span>
                    </div>
                    <div align="center">
                        <span class="sub_2">- Maison Kayser México -</span>
                    </div>
                </div>
            
                <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<!-- Modal Final-->
