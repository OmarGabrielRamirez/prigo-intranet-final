@extends('layouts.newlay')
@section('content')
@if(!empty($apps))
<div class="row">
	@foreach($apps as $app)
    <div class="col-lg-3 col-md-6 col-sm-6">		
		<button id="btnPedidos" href="{{ route($app->url) }}" style="width: 100%" class="btn btn-warning btn-lg btn-app-link">
            <i class="material-icons">{{$app->icono}}</i>
			<br><br>{{$app->nombre}}
            <div class="ripple-container"></div>
		</button>
    </div>
	@endforeach
</div>
@endif
@endsection