@extends('layouts.pro')
@include('menu.pedidos')
@section('content')
<div class="row">
		<label class="col-sm-1 col-form-label">Fecha</label>
		<div class="col-sm-3">
			<div class="form-group bmd-form-group">
				<input id="findFecha" name="findFecha"  type="text" class="form-control" >
			</div>
		</div>
		<label class="col-sm-1 col-form-label">Almacen</label>
		<div class="col-sm-3">
			<div class="form-group bmd-form-group">
				<input id="findSucursal" name="findSucursal" type="text" class="form-control">
			</div>
		</div>
		<div class="col-sm-1">
			<button id="findPedidosbtn" class="btn btn-white btn-round btn-just-icon">
			<i class="material-icons">search</i>
			<div class="ripple-container"></div>
			</button>
		</div>
</div>
<div class="row">
<div class="col-12">
<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%;">
	<thead>
	  <tr>
		  <th>#</th>
		  <th>Almacen</th>
		  <th>Fecha</th>
		  <th>Hora</th>
		  <th>Estado</th>
		  <th>Comentario</th>
		  <th>Codigo SAP</th>
		  <th>Acciones</th>
	  </tr>
	</thead>
	<tfoot>
	  <tr>
		  <th>#</th>
		  <th>Almacen</th>
		  <th>Fecha</th>
		  <th>Hora</th>
		  <th>Estado</th>
		  <th>Comentario</th>
		  <th>Codigo SAP</th>
		  <th>Acciones</th>
	  </tr>
	</tfoot>
	<tbody>
	</tbody>
</table>
</div>
</div>
@endsection
@section('jsimports')
<script src="{{ asset('material_pro_2_1_0/assets/js/plugins/jquery.dataTables.min.js') }}"></script>
@endsection
@section('aditionalScripts')
  <script type="text/javascript">

$(document).ready(function() {
    $('#datatables').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "{{ route('getTraslados') }}",
            "type": "POST",
			"data": function ( d ) {
                d._token = "{{ csrf_token() }}";
            }
        },
        "columns": [
            { "data": "idTraslado" },
            { "data": "almacen" },
            { "data": "fecha" },
            { "data": "hora" },
            { "data": "estado" },
            { "data": "comentario" },
            { "data": "codsap" },
			{ "render": function (data, type, row, meta) {
				return "<a href=\"{{ route('detalletraslado') }}/"+row.idTraslado+"\" class=\"btn btn-link btn-info btn-just-icon like\"><i class=\"material-icons\">open_in_new</i></a>"+"<a href=\"{{ route('recibirtraslado') }}/"+row.idTraslado+"\" class=\"btn btn-link btn-warning btn-just-icon edit\"><i class=\"material-icons\">assignment_returned</i></a>";
				}
			}
        ]
    });


    var table = $('#datatables').DataTable();
 
	$('#findPedidosbtn').on( 'click', function (event) {
		event.preventDefault();
		if($("#findSucursal").val() != "")
			table.column(1).search( $("#findSucursal").val() );
		else
			table.column(1).search("");
		if($("#findFecha").val() != "")
			table.column(2).search( $("#findFecha").val() );
		else
			table.column(2).search("");
		if($("#findSucursal").val() != "" || $("#findFecha").val() != "")
			table.draw();
	} );
    //$('.card .material-datatables label').addClass('form-group');
});

</script>
@endsection