@extends('layouts.pro')
@include('menu.pedidos')
@section('content')
<script>
function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode != 46 && charCode > 31 
	&& (charCode < 48 || charCode > 57))
	return false;
	return true;
}  
</script>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-orange card-header-text">
				<div class="card-text">
				  <h4 class="card-title">Nuevo Traslado</h4>
					<input type="hidden" value="0" id="artId" />
					<input type="hidden" value="" id="artCod" />
					<input type="hidden" value="" id="artName" />
					<input type="hidden" value="" id="artUnit" />
					<input type="hidden" value="1" id="artConv" />
					<input type="hidden" value="" id="artUnitFood" />
				</div>
			</div>
			<div class="card-body ">
				@if(!empty($items))
				<div class="alert alert-success" role="alert">
				Se agregaron correctamente {{ $nitems }} articulos del archivo excel
				</div>
				@endif
				@if(!empty($noitems))
				<div class="alert alert-danger" role="alert">
				No se pudieron agregar {{ $nnoitems }} articulos del archivo excel
				</div>
				@endif
				@if(!empty($nitemsDesabasto))
				<div class="alert alert-danger" role="alert">
				No se pudieron agregar {{ $nnitemsDesabasto }} articulos del archivo excel
				</div>
				@endif
				<form method="get" action="/" class="form-horizontal">
					<div class="row">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<label class="col-sm-2 col-form-label">Almacen Destino</label>
						<div class="col-sm-6">
						<div class="form-group bmd-form-group">
							<select  class="select2-1" style="width: 100%" id = "almacen" name="almacen" >
								@foreach($almacenes as $almacen)
								<option value="{{ $almacen->idAlmacen }}">{{ $almacen->almacen }}</option>
								@endforeach
							</select>
						</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-8">
							<div class="form-group bmd-form-group">
								<select id="slctArticulo" class="getItemSelect" style="width: 100%"><option>Seleccione un articulo</option></select>
							</div>
						</div>
						<div class="col-sm-4" style="display: inline-block;vertical-align: middle;float: none;">
							<button type="button" id="addArtBtn" class="btn btn-info btn-round"><i class="material-icons">add_shopping_cart</i> Agregar Articulo</button>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">Fecha de Entrega Requerida</div>
						<div class="col-sm-2">
						<input id="fechaRequerida" name="fechaRequerida" type="text" placeholder="" data-date-format="yyyy-mm-dd" class="form-control datepicker-here" value="{{ $suggestedDate }}"></div>
						<div class="col-sm-7"></div>
					</div>
					<div class="row">
						<div class="col-sm-12 table-wrapper-2">
						  <table id="tblArts" class="table table-striped">
							<thead>
							  <tr>
								<th class='col-xs-1'>Codigo</th>
								<th class='col-xs-6'>Nombre</th>
								<th class='col-xs-1'>Cantidad</th>
								<th class='col-xs-1'>UM</th>
								<th class='col-xs-1'></th>
							  </tr>
							</thead>
							<tbody>
								@if(!empty($items))
								@foreach($items as $item)
								<tr id='trArt{{ $item->id }}' class='item-row form-group'>
									<td class='col-xs-1'><input type='hidden' class='item-added' value='{{ $item->id }}' name='id[]'>{{ $item->cod }}</td>
									<td class='col-xs-6'>{{ $item->name }}</td>
									<td class='col-xs-1'><input type='text' value='{{ $item->cant }}' class='item-added-qty form-control' style='width: 70px !important;' id='CantArt{{ $item->id }}' name='cantidad[]' onkeypress='return isNumberKey(event)'></td>
									<td class='col-xs-1'>{{ $item->unit }}</td>
									<td class='td-actions col-sd-1'><button type='button' class='btn btn-link remove-btn'><i class='material-icons'>close</i></button></td>
								</tr>
								@endforeach
								@endif
								@if(!empty($noitems))
								<tr class='item-row'><td colspan=5><h3>Los siguientes productos no fueron encontrados en la base de datos actual</h3></td></tr>
								@foreach($noitems as $noitem)
								<tr class='item-row'>
									<td>{{ $noitem->cod }}</td>
									<td>{{ $noitem->name }}</td>
									<td>{{ $noitem->cant }}</td>
									<td>{{ $noitem->unit }}</td>
									<td>{{ $noitem->cantprg }}</td>
									<td></td>
									<td class='td-actions'><button type='button' title='' class='btn btn-link remove-btn'><i class='material-icons'>close</i></button></td>
								</tr>
								@endforeach
								@endif
								@if(!empty($dsitems))
								<tr class='item-row'><td colspan=5><h3>Los siguientes productos se encuentran en DESABASTO</h3></td></tr>
								@foreach($dsitems as $itemsDesabasto)
								<tr class='item-row'>
									<td>{{ $itemsDesabasto->cod }}</td>
									<td>{{ $itemsDesabasto->name }}</td>
									<td>{{ $itemsDesabasto->cant }}</td>
									<td>{{ $itemsDesabasto->unit }}</td>
									<td>{{ $itemsDesabasto->cantprg }}</td>
									<td></td>
									<td class='td-actions'><button type='button' title='' class='btn btn-link remove-btn'><i class='material-icons'>close</i></button></td>
								</tr>
								@endforeach
								@endif
							</tbody>
						  </table>
						</div>
					</div>
					<div class="row">
						<label class="col-sm-2 col-form-label">Comentarios</label>
						<div class="col-sm-10">
							<div class="form-group bmd-form-group">
								<textarea name="comentario" maxlength="250" class="form-control"></textarea>
								<span class="bmd-help">Detalles o excepciones del pedido Maximo 250 Caracteres</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group bmd-form-group">
								<button id="sendRequest" type="button" class="btn btn-info btn-round">Guardar Pedido</button>
							</div>
						</div>
						<div class="col-sm-4">
							<div id="requestTemplate" class="form-group bmd-form-group">

							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group bmd-form-group">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#xlsModalCenter">
								  Cargar Excel
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Modals -->
<div class="modal fade" id="xlsModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form method="post" action="{{ route('upPedido') }}" class="form-horizontal" enctype="multipart/form-data">
		  <input type="hidden" name="sucursal" id="sucursalXls" value="0">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Carga de archivo Excel</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
				<h4>Seleccione el archivo a cargar:</h4>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="file" name="xlsPedido" >
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			<!--button type="button" class="btn btn-primary">Cargar archivo</button-->
			<input type="submit" id="upXlsBtn" class="btn btn-info btn-round" value="Cargar Xls">						
		  </div>
		</form>
    </div>
  </div>
</div>
<!-- Modals -->
@endsection
@section('aditionalScripts')
<style>
.table-wrapper-2 {
    display: block;
    max-height: 300px;
    overflow-y: auto;
    -ms-overflow-style: -ms-autohiding-scrollbar;
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
<link href="{{ asset('css/datepicker.min.css')}}" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ asset('MaterialBSFull/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/datepicker.min.js')}}"></script>
<script src="{{ asset('js/datepicker.es.js')}}"></script>
<!--script src="{{ asset('js/plugin/waitingfor.jquery.js') }}"></script-->
<script>
	$(".remove-btn").click(function(){
		$(this).parents(".item-row").remove();
	});
	$( "[name='cantidad[]']" ).keyup(function() {
		
		var val = $( this ).val()?$( this ).val():0;
		var conv = $("#"+$( this ).attr("id")+"Conv").val()?$("#"+$( this ).attr("id")+"Conv").val():1;

		$("#"+$( this ).attr("id")+"Prg").empty();
		$("#"+$( this ).attr("id")+"Prg").text(val*conv);
	});
	$('#addArtBtn').click(function(){
		//e.preventDefault();
		if($("#artId").val() != 0 && $("#artId").val() != "" && $("#artId").val() != "0" && $("#artId").val() != "Seleccione un articulo")
		{
			var id= $("#artId").val();
			var cod= $("#artCod").val();
			var name= $("#artName").val();
			var unit= $("#artUnit").val();
			var conv= $("#artConv").val();
			var CantPrg = conv ? conv:1;
			var unitfood= $("#artUnitFood").val();
			
			if(document.getElementById("trArt"+id) == null)
			{
				$('#tblArts tbody').append("<tr id='trArt"+id+"' class='item-row form-group'><td class='col-xs-1'><input type='hidden' class='item-added' value='"+id+"' name='id[]'>"+cod+"</td><td class='col-xs-6'>"+name+"</td><td class='col-xs-1'><input type='text' value='1' class='item-added-qty form-control' id='CantArt"+id+"' style='width: 70px !important;' name='cantidad[]' onkeypress='return isNumberKey(event)'></td><td class='col-xs-1'>"+unit+"</td><td class='td-actions col-xs-1'><button type='button' rel='tooltip' data-placement='left' title='' class='btn btn-link remove-btn' data-original-title='Remove item'><i class='material-icons'>close</i></button></td></tr>");
				
				$("#artId").val(0);	
				$("#artCod").val("");
				$("#artName").val("");
				$("#artUnit").val("");
				
				$(".remove-btn").click(function(){
					$(this).parents(".item-row").remove();
				});
				
			
			}
			else
			{
				$("#CantArt"+id).focus();
				
				swal({
				  type: 'error',
				  title: 'Articulo duplicado',
				  text: 'Este item ya se encuentra en tu pedido!',
				  footer: 'Problemas? sit@prigo.com.mx',
				});
			}
		}
		else
		{
			swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Seleccione un articulo de la lista!',
			  footer: 'Problemas? sit@prigo.com.mx',
			});
		}
	});
	$("#sendRequest").click(function(e){
		
		 e.preventDefault();
		if($('.item-added').length > 0 ){
			if($("[name=sucursal]").val() != "")
			{
				var bqtys = 0;
				$('.item-added-qty').each(function() {
					if($( this ).val() == "" || $( this ).val() <=0)
						bqtys++;
				});
				if(bqtys == 0)
				{
					swal({
						title: "Estas segur@?",
						text: "El pedido sera enviado directo al proveedor!",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						allowOutsideClick: false,
						confirmButtonText: 'Si, crear pedido!',
						cancelButtonText: 'No, cancelar!'
					}).then((result) => {
					  if (result) {
						  $('.button').prop('disabled', true);
						swal({
						  title: 'Guardando...',
						  allowEscapeKey: false,
						  allowOutsideClick: false,
						  showCancelButton: false,
						  showConfirmButton: false,
						  text: 'Espere un momento...'
						});
						
						$.ajax({
							type: "POST",
							url: "{{ route('guardapedidotrans') }}",
							data: $('form.form-horizontal').serialize(),
							success: function(msg){
								$('#tblArts tbody').empty();
								$('form.form-horizontal')[0].reset();
								swal({
									type: 'success',
									title: 'Tu pedido ha quedado registrado, '+msg.msg+'!'
								});
								
								$('.button').prop('disabled', false);

							},
							error: function(){
								swal({
								  type: 'error',
								  title: 'Oops...',
								  text: 'Algo ha salido mal!',
								  footer: 'Problemas? sit@prigo.com.mx	',
								});
								$('.button').prop('disabled', false);
								
							}
						});	
						
					  }
					});
				} else {
					swal({
					  type: 'error',
					  title: 'Oops...',
					  text: 'Se detectaron cantidades no validas en la lista de articulos!',
					  footer: 'Problemas? sit@prigo.com.mx	',
					});
				}
			} else {
				swal({
				  type: 'error',
				  title: 'Oops...',
				  text: 'Debes seleccionar una sucursal!',
				  footer: 'Problemas? sit@prigo.com.mx',
				});
			}
		} else {
			swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Agregue al menos un elemento a la lista!',
			  footer: 'Problemas? sit@prigo.com.mx',
			});
		}
		return false;
	});
	
	
	$('.getItemSelect').select2({
		theme: "bootstrap",
		ajax: {
		url: "{{ route('getarticulotrans') }}",
		dataType: 'json',
		delay: 250,
		data: function (params) {
		  return {
			q: params.term, // search term
			page: params.page
		  };
		},
		processResults: function (data, params) {
			// parse the results into the format expected by Select2
			// since we are using custom formatting functions we do not need to
			// alter the remote JSON data, except to indicate that infinite
			// scrolling can be used
			params.page = params.page || 1;

			return {
				results: data.items,
				pagination: {
				  more: (params.page * 30) < data.total_count
				}
			};
		},
		cache: true
		},
		minimumInputLength: 3,
		placeholder: 'Search for a repository',
		escapeMarkup: function (markup) { return markup; },
		templateResult: formatRepo,
		templateSelection: formatRepoSelection
	});
	
	$('.select2-1').select2();
	
	function formatRepo (repo) {
  if (repo.loading) {
    return repo.text;
  }

  var markup = "<div class='select2-result-repository clearfix' "+(repo.Estado==2?"style='background-color: #FFD700;'":"")+">" +
    "<div class='select2-result-repository__meta'>" +
      "<div class='select2-result-repository__title'>" + repo.name + "</div>";

   markup += "<div class='select2-result-repository__description'>" + repo.codfs + " "+(repo.Estado==2?"DESABASTO":"")+"</div>";

   markup += "</div></div>";

   return markup;
}
function setSuc(){
	$("#sucursalXls").val($("#sucursal").val());
	swal({
		type: 'info',
		title: 'Espere un mometo'
	});
	$('.button').prop('disabled', true);
	$.ajax({
		type: "POST",
		url: "{{ route('getExcelOpts') }}",
		data: { idSucursal: $("#sucursal").val(), _token: "{{ csrf_token() }}" },
		success: function(msg){
			$("#requestTemplate").empty();
			var html = '<a class="btn btn-warning" href="'+msg.url+'" target="_blank">';
				html += 'Descargar Plantilla</a>';
			$("#requestTemplate").append(html);
			$('.button').prop('disabled', false);
			swal.close();
		},
		error: function(){
			swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Algo ha salido mal!',
			  footer: 'Problemas? sit@prigo.com.mx	',
			});
			$('.button').prop('disabled', false);
		}
	});
}
function formatRepoSelection (repo) {
	$("#artId").val(repo.id);	
	$("#artCod").val(repo.cod);	
	$("#artName").val(repo.name);
	$("#artUnit").val(repo.unit);
	$("#artConv").val(repo.conv);
	$("#artUnitFood").val(repo.unitfood);
	return repo.name || repo.id;
}

$(document).ready(function() 
{
    var start = new Date();
	$('#fechaRequerida').datepicker({
	language: 'es',
	minDate: new Date(),
	startDate: start,
	onSelect: function onSelect(fd, date) {
	$('#fechaRequerida').data('datepicker').hide();
	}
	})
	$('#fechaRequerida').val("{{ $suggestedDate }}");
});
	

@if(!empty($sucursal))
$("#sucursal").val({{$sucursal}});
$("#sucursalXls").val({{$sucursal}});
@endif
</script>
@if(!empty($selected))
<script>
	setSuc();
</script>
@endif
@endsection