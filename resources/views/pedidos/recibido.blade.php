@if(!empty($flagUser) && empty($partidas))
@section('content')
	<div class="row">
		<div class="col-md-12">
			<h2>Pedido sin registro de entrega</h2>
		</div>
	</div>
@endsection
@else
@extends('layouts.app')
@section('appmenu')
<li class="nav-item ">
	<a class="nav-link" data-toggle="collapse" href="#pagesExamples" aria-expanded="true">
		<i class="material-icons">store</i>
		<p> Pedidos <b class="caret"></b> </p>
    </a>
	<div class="collapse show" id="pagesExamples" style="">
		<ul class="nav">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('nuevopedido') }}">
					<i class="material-icons">shopping_basket</i>
					<p> Solicitud de Pedido </p>
				</a>
			</li>
			<li class="nav-item active">
				<a class="nav-link" href="{{ route('consultapedido') }}">
					<i class="material-icons">view_list</i>
					<p> Consulta de Pedido </p>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('consultarticulos') }}">
					<i class="material-icons">image_search</i>
					<p> Consulta de Articulos </p>
				</a>
			</li>
		</ul>
	</div>
</li>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card card-body printableArea">
			<h3><b>RECEPCION PEDIDO <span>#{{ $entradas[0]->idPedido }}</span></b></h3>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="pull-left">
						<address>
							<h3> &nbsp;<b class="text-danger">Eric Kayser Mexico</b></h3>
							<p class="text-muted m-l-5">{{ $entradas[0]->fecha }}</p>
						</address>
					</div>
					<div class="pull-right text-right">
						<address>
							<h3>De,</h3>
							<h4 class="font-bold">Food Services,</h4>
							<p class="m-t-30"><b>{{ $entradas[0]->fecha }} , {{ $entradas[0]->hora }}</b></p>
						</address>
					</div>
				</div>
				<div class="col-md-12">
					<div class="table-responsive m-t-40" style="clear: both;">
						<table class="table table-hover">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Cod Prigo</th>
									<th class="text-center">Cod Food Services</th>
									<th>Articulo</th>
									<th>U de M</th>
									<th class="text-right">Cantidad</th>
									<th class="text-right">Accion</th>
								</tr>
							</thead>
							<tbody>
							@foreach($partidas as $partida)
								<tr>
									<td class="text-center">{{ $loop->index + 1 }}</td>
									<td class="text-center">{{ $partida->CodPrigo }}</td>
									<td class="text-center">{{ $partida->CodFood }}</td>
									<td class="text-left">{{ $partida->Descripcion }}</td>	
									<td class="text-left">{{ $partida->UnidadFood }}</td>	
									<td class="text-right">{{ $partida->cantidad }}</td>
									<td class="text-right">{{ $partida->accion }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-12">
					<div class="pull-right m-t-30 text-right">
						<p>{{ $entradas[0]->comentario }}</p>
					</div>
					<div class="clearfix"></div>
					<hr>
					<!--div class="text-right">
						<button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
					</div-->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@endif