@extends('layouts.app')
@include('menu.pedidos')
@section('content')
<div class="row">
<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
	<thead>
	  <tr>
		  <th>Prigo</th>
		  <th>Categoria</th>
		  <th>Descripcion</th>
		  <th>Unidad Medida</th>
	  </tr>
	</thead>
	<tfoot>
	  <tr>
		  <th>Prigo</th>
		  <th>Categoria</th>
		  <th>Descripcion</th>
		  <th>Unidad Medida</th>
	  </tr>
	</tfoot>
	<tbody>
	</tbody>
</table>
</div>
@endsection
@section('aditionalScripts')
  <script type="text/javascript">

$(document).ready(function() {
    $('#datatables').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "{{ route('gridArticulos') }}",
            "type": "POST",
			"data": function ( d ) {
                d._token = "{{ csrf_token() }}";
            }
        },
        "columns": [
            { "data": "CodPrigo" },
            { "data": "cat" },
            { "data": "Descripcion" },
            { "data": "UnidadFood" }
        ]
    });


    var table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');

        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
    });

    // Delete a record
    table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function() {
        alert('You clicked on Like button');
    });

    //$('.card .material-datatables label').addClass('form-group');
});

</script>

@endsection