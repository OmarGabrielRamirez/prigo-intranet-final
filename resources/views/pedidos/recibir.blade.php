@extends('layouts.app')

@section('appmenu')
<li class="nav-item ">
	<a class="nav-link" data-toggle="collapse" href="#pagesExamples" aria-expanded="true">
		<i class="material-icons">store</i>
		<p> Entrega pedido <b class="caret"></b> </p>
    </a>
	<div class="collapse show" id="pagesExamples" style="">
		<ul class="nav">
			<li class="nav-item active">
				<a class="nav-link" href="{{ route('nuevopedido') }}">
					<i class="material-icons">shopping_basket</i>
					<p> Solicitud de Pedido </p>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('consultapedido') }}">
					<i class="material-icons">view_list</i>
					<p> Consulta de Pedido </p>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('consultarticulos') }}">
					<i class="material-icons">image_search</i>
					<p> Consulta de Articulos </p>
				</a>
			</li>
		</ul>
	</div>
</li>
@endsection

@section('content')
<script>
function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode != 46 && charCode > 31 
	&& (charCode < 48 || charCode > 57))
	return false;
	return true;
}  
</script>
<style>
	.inrequest {
		background-color: #4CAF50;
		color: #fff;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-orange card-header-text">
				<div class="card-text">
				  <h4 class="card-title">Recibir pedido #{{ $idPedido }}</h4>					
					<input type="hidden" value="0" id="artId" />
					<input type="hidden" value="" id="artCod" />
					<input type="hidden" value="" id="artName" />
					<input type="hidden" value="0" id="artReq" />
					<input type="hidden" value="" id="artUnitFood" />
				</div>
			</div>
			<div class="card-body ">
				@if(!empty($items))
				<div class="alert alert-success" role="alert">
				Se agregaron correctamente {{ $nitems }} articulos del archivo excel
				</div>
				@endif
				@if(!empty($noitems))
				<div class="alert alert-danger" role="alert">
				No se pudieron agregar {{ $nnoitems }} articulos del archivo excel
				</div>
				@endif
				<form method="get" action="/" class="form-horizontal">
					<div class="row">
						<input type="hidden" value="{{$idPedido}}" id="idPedido"  name="idPedido" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<label class="col-sm-1 col-form-label">Pedido #:</label>
						<label class="col-sm-1 col-form-label">{{$idPedido}}</label>
						<label class="col-sm-1 col-form-label">Sucursal:</label>
						<label class="col-sm-2 col-form-label">{{$pedido[0]->sucursal}}</label>
						<label class="col-sm-1 col-form-label">Fecha:</label>
						<label class="col-sm-2 col-form-label">{{$pedido[0]->fecha}}</label>
					</div>
					<div class="row">
						<div class="col-sm-8">
							<div class="form-group bmd-form-group">
								<label for="slctArticulo">
									Seleccione un Articulo
								</label>
								<select id="slctArticulo" class="getItemSelect" style="width: 100%"><option>nothing selected</option></select>
							</div>
						</div>
						<div class="col-sm-4" style="display: inline-block;vertical-align: middle;float: none;">
							<button type="button" id="addArtBtn" class="btn btn-info btn-round"><i class="material-icons">add_shopping_cart</i> Agregar Articulo</button>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 table-wrapper-2">
						  <table id="tblArts" class="table table-striped">
							<thead>
							  <tr>
								<th class='col-xs-1'>Codigo</th>
								<th class='col-xs-5'>Nombre</th>
								<th class='col-xs-1'>Pedido</th>
								<th class='col-xs-1'>Recibido</th>
								<th class='col-xs-1'>UM FS</th>
								<th class='col-xs-1'>Acciones</th>
								<th class='col-xs-1'></th>
							  </tr>
							</thead>
							<tbody>
								@if(!empty($items))
								@foreach($items as $item)
								<tr id='trArt{{ $item->id }}' class='item-row form-group'>
									<td class='col-xs-1'><input type='hidden' class='item-added' value='{{ $item->id }}' name='id[]'>{{ $item->cod }}</td>
									<td class='col-xs-5'>{{ $item->name }}</td>
									<td class='col-xs-1'></td>
									<td class='col-xs-1'><input type='text' value='{{ $item->cant }}' class='item-added-qty form-control' style='width: 70px !important;' id='CantArt{{ $item->id }}' name='cantidad[]' onkeypress='return isNumberKey(event)'></td>
									<td class='col-xs-1'>{{ $item->unitfood }}</td>
									<td class='col-xs-1'></td>
									<td class='td-actions col-sd-1'><button type='button' class='btn btn-link remove-btn'><i class='material-icons'>close</i></button></td>
								</tr>
								@endforeach
								@endif
								@if(!empty($noitems))
								<tr class='item-row'><td colspan=5><h3>Los siguientes productos no fueron encontrados en la base de datos actual</h3></td></tr>
								@foreach($noitems as $noitem)
								<tr class='item-row'>
									<td>{{ $noitem->cod }}</td>
									<td>{{ $noitem->name }}</td>
									<td></td>
									<td>{{ $noitem->cant }}</td>
									<td></td>
									<td></td>
									<td class='td-actions'><button type='button' title='' class='btn btn-link remove-btn'><i class='material-icons'>close</i></button></td>
								</tr>
								@endforeach
								@endif
							</tbody>
						  </table>
						</div>
					</div>
					<div class="row">
						<label class="col-sm-2 col-form-label">Comentarios</label>
						<div class="col-sm-10">
							<div class="form-group bmd-form-group">
								<textarea name="comentario" class="form-control"></textarea>
								<span class="bmd-help">Detalles o excepciones de la entrega</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-2">
							<div class="form-group bmd-form-group">
								<button id="sendRequest" type="button" class="btn btn-info btn-round">Recibir Pedido</button>
							</div>
						</div>
						<div class="col-sm-8">
							&nbsp;
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Modals -->
<div class="modal fade" id="xlsModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form method="post" action="{{ route('upPedido') }}" class="form-horizontal" enctype="multipart/form-data">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Carga de archivo Excel</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
				<h4>Seleccione el archivo a cargar:</h4>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="file" name="xlsPedido" >
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			<!--button type="button" class="btn btn-primary">Cargar archivo</button-->
			<input type="submit" id="upXlsBtn" class="btn btn-info btn-round" value="Cargar Xls">						
		  </div>
		</form>
    </div>
  </div>
</div>
<!-- Modals -->
@endsection
@section('aditionalScripts')
<style>
.table-wrapper-2 {
    display: block;
    max-height: 300px;
    overflow-y: auto;
    -ms-overflow-style: -ms-autohiding-scrollbar;
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<!--script src="{{ asset('js/plugin/waitingfor.jquery.js') }}"></script-->
<script>
	$(".remove-btn").click(function(){
		$(this).parents(".item-row").remove();
	});
	$('#addArtBtn').click(function(){
		//e.preventDefault();
		if($("#artId").val() != 0)
		{
			var id= $("#artId").val();
			var cod= $("#artCod").val();
			var name= $("#artName").val();
			var requested= $("#artReq").val();
			var unitfood= $("#artUnitFood").val();
			
			$('#tblArts tbody').append("<tr id='trArt"+id+"' class='item-row form-group'><td class='col-xs-1'><input type='hidden' class='item-added' value='"+id+"' name='id[]'>"+cod+"</td><td class='col-xs-6'>"+name+"</td><td class='col-xs-1'>"+requested+"</td><td class='col-xs-1'><input type='text' value='1' class='item-added-qty form-control' id='CantArt"+id+"' style='width: 70px !important;' name='cantidad[]' onkeypress='return isNumberKey(event)'></td><td class='col-xs-1'>"+unitfood+"</td><td class='col-xs-1'><select class='form-control' name='accion[]'><option value='1'>Aceptar</option><option value='2'>Producto Faltante</option><option value='3'>Mermar</option><option value='4'>Devolver</option></select></td><td class='td-actions col-xs-1'><button type='button' rel='tooltip' data-placement='left' title='' class='btn btn-link remove-btn' data-original-title='Remove item'><i class='material-icons'>close</i></button></td></tr>");
			
			$("#artId").val(0);
			$("#artReq").val(0);
			$("#artCod").val("");
			$("#artName").val("");
			$("#artUnitFood").val("");
			
			$(".remove-btn").click(function(){
				$(this).parents(".item-row").remove();
			});
					
			$("#CantArt"+id).focus();
			$("#CantArt"+id).select();
		}
		else
		{
			swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Seleccione un articulo de la lista!',
			  footer: 'Problemas? sit@prigo.com.mx',
			});
		}
	});
	$("#sendRequest").click(function(){
		//if($('.item-added').length > 0 ){
			if($("[name=sucursal]").val() != "")
			{
				var bqtys = 0;
			/*	$('.item-added-qty').each(function() {
					if($( this ).val() == "" || $( this ).val() < 0)
						bqtys++;
				});
				if(bqtys == 0)
				{*/
					swal({
						title: "Estas segur@?",
						text: "La entrega sera registrada y no podra modificarse despues!",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						allowOutsideClick: false,
						confirmButtonText: 'Si, recibir pedido!',
						cancelButtonText: 'No, cancelar!'
					}).then((result) => {
					  if (result) {
						  $('.button').prop('disabled', true);
						swal({
						  title: 'Guardando...',
						  allowEscapeKey: false,
						  allowOutsideClick: false,
						  showCancelButton: false,
						  showConfirmButton: false,
						  text: 'Espere un momento...'
						});
						
						$.ajax({
							type: "POST",
							url: "{{ route('recibepedido') }}",
							data: $('form.form-horizontal').serialize(),
							success: function(msg){
								$('#tblArts tbody').empty();
								$('form.form-horizontal')[0].reset();
								swal({
									type: 'success',
									title: 'Tu entrega ha quedado registrada!'
								});
								console.log(msg);
								
								$('.button').prop('disabled', false);
								
							},
							error: function(){
								swal({
								  type: 'error',
								  title: 'Oops...',
								  text: 'Algo ha salido mal!',
								  footer: 'Problemas? sit@prigo.com.mx	',
								});
								$('.button').prop('disabled', false);
							}
						});	
						
					  }
					});
				/*} else {
					swal({
					  type: 'error',
					  title: 'Oops...',
					  text: 'Se detectaron cantidades no validas en la lista de articulos!',
					  footer: 'Problemas? sit@prigo.com.mx	',
					});
				}*/
			} else {
				swal({
				  type: 'error',
				  title: 'Oops...',
				  text: 'Debes seleccionar una sucursal!',
				  footer: 'Problemas? sit@prigo.com.mx',
				});
			}
		/*} else {
			swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Agregue al menos un elemento a la lista!',
			  footer: 'Problemas? sit@prigo.com.mx',
			});
		}*/
	});
	
	$('.getItemSelect').select2({
		theme: "bootstrap",
		ajax: {
		url: "{{ route('getarticulorecibir') }}",
		dataType: 'json',
		delay: 250,
		data: function (params) {
		  return {
			q: params.term, // search term
			id: {{ $idPedido }},
			page: params.page
		  };
		},
		processResults: function (data, params) {
			// parse the results into the format expected by Select2
			// since we are using custom formatting functions we do not need to
			// alter the remote JSON data, except to indicate that infinite
			// scrolling can be used
			params.page = params.page || 1;

			return {
				results: data.items,
				pagination: {
				  more: (params.page * 30) < data.total_count
				}
			};
		},
		cache: true
		},
		minimumInputLength: 3,
		placeholder: 'Search for a repository',
		escapeMarkup: function (markup) { return markup; },
		templateResult: formatRepo,
		templateSelection: formatRepoSelection
	});
	
	function formatRepo (repo) {
  if (repo.loading) {
    return repo.text;
  }

  var markup = "<div class='select2-result-repository clearfix"+(repo.inRequest=="0"?"":" inrequest")+"'>" +
    "<div class='select2-result-repository__meta'>" +
      "<div class='select2-result-repository__title"+(repo.inRequest=="0"?"":" inrequest")+"'> " + repo.name + "</div>";

  if (repo.cod) {
    markup += "<div class='select2-result-repository__description'>" + repo.cod + "</div>";
  }

  markup += "</div></div>";

  return markup;
}

function formatRepoSelection (repo) {
	$("#artId").val(repo.id);	
	$("#artReq").val(repo.requested);	
	$("#artCod").val(repo.cod);	
	$("#artName").val(repo.name);
	$("#artUnitFood").val(repo.unitfood);
	return repo.name || repo.id;
}
</script>
@endsection