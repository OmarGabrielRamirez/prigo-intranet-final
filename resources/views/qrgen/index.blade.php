@extends('layouts.app')
@section('appmenu')
<li class="nav-item active">
	<a class="nav-link" href="#" aria-expanded="true">
		<i class="material-icons">center_focus_strong</i>
		<p> QR Genetor <b class="caret"></b> </p>
    </a>
</li>
@endsection
@section('content')
<div class="row" >
    <div class="col-md-12">
        <div class="card" >
            <div class="card-header">
                <h4 class="card-title">Generación de codigos QR</h4>
            </div>
			<div class="card-content" style="padding: 10px;">
				<form id="frmRedes" target="_blank" method="POST" action="{{ route('qrgendwn')}}" class="form-horizontal">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<label class="col-sm-2 label-on-left">Nombre</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <input  id="nombre" name="nombre" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
					</div>
					<div class="row">
						<label class="col-sm-2 label-on-left">Apellidos</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <input  id="apellidos" name="apellidos" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
					</div>
					<div class="row">
						<label class="col-sm-2 label-on-left">E-mail</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <input  id="mail" name="mail" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
					</div>
					<div class="row">
						<label class="col-sm-2 label-on-left">Telefono</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <input  id="tel" name="tel" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
					</div>
					<div class="row">
						<label class="col-sm-2 label-on-left">Puesto</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <input  id="puesto" name="puesto" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
					</div>
					<div class="row">
						<label class="col-sm-2 label-on-left">Empresa</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <input  id="org" name="org" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
					</div>
					<div class="row">
						<label class="col-sm-2 label-on-left">Pagina Web</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <input  id="url" name="url" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
					</div>
					<div class="row">
						<label class="col-sm-2 label-on-left">Logo</label>
                        <div class="col-sm-8">
                            <div class="form-check">
								<label class="form-check-label">
									<input  id="logo" name="logo" type="checkbox" class="form-check-input">Incluir logo
									<span class="form-check-sign">
									  <span class="check"></span>
									</span>
								</label>
                            </div>
                        </div>
					</div>
                    <div class="row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-info">Generar</a>
                        </div>
                    </div>
				</form>
			</div>
        </div>
      </div>
</div>
@endsection
