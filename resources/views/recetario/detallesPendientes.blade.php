@extends('layouts.app')
@include('menu.recetario', ['seccion' => 'autorizar'])
@section('content')

<div class="row">
      @foreach($receta AS $dato)
        @foreach($ingredientes AS $ingrediente)
            @php
                if($ingrediente->subReceta == "NO"){
                    $subReceta = "NO";
                }else if($ingrediente->subReceta == "SÍ"){
                    $subReceta = "Sí";
                }else if(empty($ingrediente->subReceta)){
                    $subReceta = "";
                }
            @endphp
        @endforeach
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header card-header-orange card-header-text">
                    <div class="card-text">
                        <h4 class="card-title">Editar receta</h4>
                    </div>
                </div>
                <div class="card-body ">
                    <form method="get" action="/" class="form-horizontal" id="formReceta">
                        {!! csrf_field() !!}
                        <input type="hidden" value="0" id="ingId">
                        <input type="hidden" value="0" id="ingCosto">                        
                        <input type="hidden" value="" id="ingCod">
                        <input type="hidden" value="" id="ingTipo">
                        <input type="hidden" value="" id="ingName">
                        <input type="hidden" value="" id="ingUnit">
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Platillo</label>
                            <input type="hidden" value="{{$dato->idPlatillo}}" id="idPlatillo" name="idPlatillo"> 
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                <input id="nombrePlatilloInput" type="text" class="form-control" name="nombre_receta" style="text-transform:uppercase;"  onkeyup="mayus(this);" value="{{$dato->nombre}}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Tipo de receta</label>
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                    <select id="tipoReceta" name="tipo_receta" class="tipoReceta" style="width: 100%" disabled>
                                        <option selected="selected">Seleccione un tipo de receta</option>
                                        <option {{ $dato->tipoReceta == 'PRIGO (PRODUCCIÓN)' ? 'selected' : '' }}>PRIGO (PRODUCCIÓN)</option>
                                        <option {{ $dato->tipoReceta == 'KAYSER (TIENDA)' ? 'selected' : '' }}>KAYSER (TIENDA)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Unidad</label>
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                    <select id="slctUnidad" class="slctUnidadReceta" name="unidad_receta" style="width: 100%" disabled>
                                        <option selected="selected">Seleccione una opción</option>
                                        <option {{ $dato->unidad == 'ORD' ? 'selected' : '' }}>ORD</option>
                                        <option {{ $dato->unidad == 'PZA' ? 'selected' : '' }}>PZA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Cantidad</label>
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                    <input id="cantidadReceta" type="number" class="form-control" name="cantidad_receta" min="0" style="text-transform:uppercase;"disabled  value="{{$dato->cantidad}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Subreceta</label>
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                    <select id="slctSub" name="subreceta" class="slctSubcreceta" style="width: 100%" disabled>
                                        <option {{ $subReceta == '' ? 'selected' : '' }} selected="selected">Seleccione una opción</option>
                                        <option {{ $subReceta == 'SÍ' ? 'selected' : '' }}>SÍ</option>
                                        <option {{ $subReceta == 'NO' ? 'selected' : '' }}>NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Estado</label>
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                    <select id="slctEdo" name="estado" class="slctEdo" style="width: 100%" disabled>
                                        <option value="2" >En revisión</option>
                                        <option value="1">Autorización</option>
                                        <option value="3">Cambios no autorizados</option>
                                        <option value="4">Baja</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group bmd-form-group">
                                    <select id="slctArticulo" class="getItemSelect" style="width: 100%" disabled><option>Seleccione un Ingrediente</option></select>
                                </div>
                            </div>
                            <div class="col-sm-4" style="display: inline-block;vertical-align: middle;float: none;">
                                <button type="button" id="addArtBtn" class="btn btn-success btn-round btn-just-icon" disabled><i class="material-icons">control_point</i></button> 
                                <button type="button" id="addIngreNoExistente" data-toggle="modal" data-target="#modalIngrediente" class="btn btn-info btn-round"  disabled><i class="material-icons" >playlist_add</i> Ingrediente no existente</button>                 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 ml-auto">
                                <table id="tblIngre" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class='col-xs-1'>Codigo</th>	
                                            <th class='col-xs-1'>Ingrediente</th>							
                                            <th class='col-xs-1'>Unidad</th>
                                            <th class='col-xs-1'>Cantidad Sucia</th>
                                            <th class='col-xs-1'>Cantidad Limpia</th>
                                            <th class='col-xs-1'>Costo Unitario</th>
                                            <th class='col-xs-1'>Costo Final</th>
                                            <th class='col-xs-1'>Acciones</th>
                                        </tr>
                                    </thead>
                                        <tbody> 
                                        @foreach($ingredientes AS $ingrediente)
                                            <tr class='item-row form-group'><td class='col-xs-1'><input name="id[]" type='hidden' value="{{ $ingrediente->idIngrediente }}">{{ $ingrediente->idIngrediente }}</td><td class='col-xs-1'><input name="nombre_ingrediente[]" type='hidden' value="{{ $ingrediente->nombre }}">{{ $ingrediente->nombre }}</td> <td class='col-xs-1'><select name="unidad_ingre[]" id="slctUniIngreGet"class='custom-select' disabled><option {{ $ingrediente->unidad == 'Kilos' ? 'selected' : '' }} selected="selected">Kilos</option><option {{ $ingrediente->unidad == 'Litros' ? 'selected' : '' }}>Litros</option></select></td><td class='col-xs-1'><input type='text' value='{{ $ingrediente->cantidadSucia }}'  id='CostoIng{{ $ingrediente->idIngrediente }}' style='width: 80px !important; border-radius: 5px;' name='cantidadSucia[]' class='item-added-qty form-control' onkeyup='recalc(this)' onkeypress='return isNumberKey(event, this)' disabled></td><td class='col-xs-1'><input type='text' value='{{ $ingrediente->cantidadLimpia }}'  id='CantidadLimpia"+id+"Cant' style='width: 80px !important; border-radius: 5px;' name='cantidadLimpia[]' class='item-added-qty form-control' focus='recalc(this)' onkeypress='return isNumberKey(event, this)' disabled></td><td class='col-xs-1' id='lblCostoIng{{ $ingrediente->idIngrediente }}'>{{ $ingrediente->costoInge }}<input name='costo[]' type='hidden' value="{{ $ingrediente->costoInge }}"><td class='col-xs-1' id='lblFinal{{ $ingrediente->idIngrediente }}'><input id='costoF{{ $ingrediente->idIngrediente }}' name='costoF[]' type='text'></td><td class='td-actions col-xs-1'><button type='button' rel='tooltip' data-placement='left' title='' class='btn btn-link remove-btn' disabled><i class='material-icons'>close</i></button></td></tr>
                                        @endforeach 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-12" id="costo_receta_div">
                            Costo de la receta:  $ <span id="lblCostoReceta" name="costo_receta" >{{ $dato->costo }}</span>
                            <input id="lblCostoReceta_hidden" type="hidden" class="form-control" name="precio_receta" value=" {{ $dato->costo }}">
                            <input id="lblCostoRecetaA_hidden" type="hidden"  name="precio_receta_d" value="">
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                    <button id="btnEditar" type="button" class="btn btn-warning">Editar</button>
                                    <button id="btnGuardar"type="button" class="btn btn-success" disabled>Guardar cambios</button>
                                    <button id="btnRevertir"type="button" class="btn btn-danger" disabled>Revertir cambios</button>
                                </div>
                            </div>
                        </div>
                         
                    </form>
                </div>
            </div>
        </div>
    @endforeach
</div>
<!-- Modal agregar ingrediente no existente  -->
<div class="modal fade" id="modalIngrediente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Agregar Ingrediente no existente</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-4">
        <div class="col-sm-9">
          <input type="text" id="nombreIngrediente" class="form-control"  style="text-transform:uppercase;" style="width: 400px !important;" onkeyup="mayus(this);">
          <label data-error="wrong" data-success="right" for="nombreIngrediente">Nombre Ingrediente</label>
        </div>
        <div class="col-sm-9">
            <select id="slcUM" class="form-control" style="width: 40%">
                <option>Kilos</option>
                <option>Litros</option>
            </select>
            <label data-error="wrong" data-success="right" for="cantidadSucia">UM</label>
        </div>
        <div class="col-sm-9">
          <input type="text" id="cantidadSucia" class="form-control" style="width: 110px !important;">
          <label data-error="wrong" data-success="right" for="cantidadSucia">Cantidad Sucia</label>
        </div>
        <div class="col-sm-9">
            <input type="text" id="cantidadLimpia" class="form-control" style="width: 110px !important;">
            <label data-error="wrong" data-success="right" for="cantidadLimpia">Cantidad Limpia</label>
        </div>
        <div class="col-sm-9">
            <input type="text" id="costo" class="form-control" style="width: 110px !important;" placeholder="40.00">
            <label data-error="wrong" data-success="center" for="costo">Costo</label>
        </div>
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button id="ingredienteSave" type="button" class="btn btn-info btn-round" >Guardar</button>
      </div>
    </div>
  </div>
</div>
  <!-- Modals -->
@endsection
@section('aditionalScripts')
<style>
    .table-wrapper-2 {
        display: block;
        max-height: 300px;
        overflow-y: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }
    .custom-select {
        position: relative;
        font-family: Arial;
    }

    .custom-select select {
        display: none;
    }

    .select-selected {
        background-color: DodgerBlue;
    }

    .select-selected:after {
        position: absolute;
        content: "";
        top: 14px;
        right: 10px;
        width: 0;
        height: 0;
        border: 1px solid transparent;
        border-color: #fff transparent transparent transparent;
    }
    .select-selected.select-arrow-active:after {
        border-color: transparent transparent #fff transparent;
        top: 7px;
    }

    .select-items div,.select-selected {
        color: #ffffff;
        padding: 8px 16px;
        border: 1px solid transparent;
        border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
        cursor: pointer;
    }

    .select-items {
        position: absolute;
        background-color: DodgerBlue;
        top: 100%;
        left: 0;
        right: 0;
        z-index: 99;
    }

    .select-hide {
        display: none;
    }

    .select-items div:hover, .same-as-selected {
        background-color: rgba(0, 0, 0, 0.1);
    }

    html,body { 
      overflow:hidden; 
    }

</style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>

$.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
     }
 }); 

    var valorUnidadReceta = "";
    var valorSubreceta = "";
    var tipoRecetaSlct = "";
    var nombrePlatillo = "";
    var costoTotal = 0.0;
    var arrayT = new Array();

    $('.tipoReceta').select2({ theme: "bootstrap" });
    $('.slctSubcreceta' ).select2({ theme: "bootstrap "});
    $('.slctUnidadReceta').select2({ theme: "bootstrap" });
    $('.slctEdo').select2({ theme: "bootstrap" });

    $("#btnEditar").click(function(e){
        $('#nombrePlatilloInput').prop('disabled', false);
        $('#tipoReceta').prop('disabled', false);
        $('#slctUnidad').prop('disabled', false);
        $('#cantidadReceta').prop('disabled', false);
        $('#slctSub').prop('disabled', false);
        $('#slctEdo').prop('disabled', false);
        $('#btnEditar').prop('disabled', false);
        $('#slctArticulo').prop('disabled', false);
        $('#addArtBtn').prop('disabled', false);
        $('#addIngreNoExistente').prop('disabled', false);
        $('#btnGuardar').prop('disabled', false); 
        $('#btnRevertir').prop('disabled', false);    
        $('#btnEditar').prop('disabled', true);   
        $('.custom-select').prop('disabled', false); 
        $('.item-added-qty').prop('disabled', false);
        $(".remove-btn").prop('disabled',false);
        swal({
			type: 'success',
            title: 'Edición habilitada',
            text: 'Se habilitaron los campos para realizar la edición de la receta.',
            icon: 'success',
            showConfirmButton: false,
            timer: 2000,
            });	 
    });

    $(document).on('change', '#slctSub', function(event) { valorSubreceta = $(this).val(); });      

    $(document).on('change', '#slctUnidad', function (event){ valorUnidadReceta = $(this).val(); });

    $('.getItemSelect').select2({
		theme: "bootstrap",
		ajax: {
		url: "{{ route('getIngredienteFormat') }}",
		dataType: 'json',
		delay: 250,
		data: function (params) {
		  return {
			q: params.term, 
			page: params.page
		  };
		},
		processResults: function (data, params) {
			params.page = params.page || 1;
			return {
				results: data.items,
				pagination: {
				  more: (params.page * 30) < data.total_count
				}
			};
		},
		cache: true
		},
		minimumInputLength: 3,
		placeholder: 'Buscar ingrediente',
		escapeMarkup: function (markup) { return markup; },
		templateResult: formatRepo,
		templateSelection: formatRepoSelection
	});
	
	function formatRepo (repo) {
        if (repo.loading) {
                return repo.text;
        }

        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'>" + repo.nombre + "</div>";
        markup += "<div class='select2-result-repository__description'>" + (repo.tipo ==1?"Ingrediente":"Subreceta") + "</div>";
        markup += "</div></div>";
        return markup;
    }

    function formatRepoSelection (repo) {
	        $("#ingId").val(repo.id);	
	        $("#ingCod").val(repo.cod);	
	        $("#ingTipo").val(repo.tipo);
            $("#ingName").val(repo.nombre);
            $("#ingCosto").val(repo.precio);
	        $("#ingUnit").val(repo.unidad);
	return repo.nombre || repo.id;
    }

    $('#addArtBtn').click(function(){ 
        var idValue = ($("#ingId").val());
        var idString = String(idValue);
        var idIngredienteNoSeleccionado = "Seleccione un Ingrediente";

    if(idString == idIngredienteNoSeleccionado){
        swal({
        type: 'error',
        title: 'Oops...',
        text: 'Seleccione un articulo de la lista!',
        footer: 'Problemas? sit@prigo.com.mx',
        });			
    }else{
        var id = $("#ingId").val();
        var cod = $("#ingCod").val();
        var name = $("#ingName").val();
        var unit = $("#ingUnit").val();
        var costo = parseFloat($("#ingCosto").val()); 
        var conv = $("#ingTipo").val();
        var nombreIngre = null;
        nombreIngre = $("#tblIngre #nombreIngre").text();

    if( nombreIngre.indexOf(name) > -1){
        swal({
          type: 'error',
          title: 'Oops...',
          text: 'Ya seleccionaste este ingrediente!',
          footer: 'Problemas? sit@prigo.com.mx',
        });
        
    }else{
        $('#tblIngre tbody').append("<tr id='trIngrediente "+id+"' class='item-row form-group'><td class='col-xs-1'><input name='id[]' type='hidden' value="+id+">"+id+"<td class='col-xs-1'><input type='hidden' name='nombre_ingrediente[]' id='NombreIng"+id+"' value='"+name+"'>"+name+"</td><td class='col-xs-1'><select name='unidad_ingre[]'class='custom-select'><option value='Kilos'>Kilos</option><option value='Litros'>Litros</option></select></td><td class='col-xs-1'><input type='text' value='1' class='item-added-qty form-control' id='CantidadSucia"+id+"Cant' style='width: 70px !important;' name='cantidadSucia[]' onkeyup='recalc(this)' onkeypress='return isNumberKey(event, this)'></td><td class='col-xs-1'><input type='text' value='1' class='item-added-qty form-control' id='CostoIngLimpia"+id+"Cant' style='width: 70px !important;' name='cantidadLimpia[]' onkeyup='recalc(this)' onkeypress='return isNumberKey(event, this)'></td><td class='col-xs-1' id='lblCostoIng'>"+costo+"<input name='costo[]' type='hidden' value="+costo+"></td><td class='col-xs-1' id='lblCostoIng'>"+costo+"<input name='costo[]' type='hidden' value="+costo+"></td><td class='td-actions col-xs-1'><button type='button' rel='tooltip' data-placement='left' title='' class='btn btn-link remove-btn' data-original-title='Remove item'><i class='material-icons'>close</i></button></td></tr>");
        var total = 0.0;
        $("#tblIngre tr").find('td:eq(5)').each(function () {
            costo = $(this).html();
            total += parseFloat(costo);
        });
        $("#lblCostoReceta").text(total.toFixed(2));
        $("#lblCostoReceta_hidden").val(total.toFixed(2));
        }
    } 

    $(".remove-btn").click(function(){
        swal({
				    title: "Estas segur@?",
				    text: "Se eliminará el ingrediente seleccionado",
				    type: "warning",
				    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
					allowOutsideClick: false,
					confirmButtonText: 'Confirmar',
					cancelButtonText: 'Cancelar'
			}).then((result)=>{
                if(result == true){
                    var total8 = 0.0
                    $(this).parents(".item-row").remove();
                    $("#tblIngre tr").find('td:eq(5)').each(function () {
                    costo = $(this).html();
                    total8 += parseFloat(costo);
                });
                $("#lblCostoReceta").text(total8.toFixed(2));
                $("#lblCostoReceta_hidden").val(total8.toFixed(2));
            }
        });

    });

    });

    $(".remove-btn").click(function(){
        swal({
				    title: "Estas segur@?",
				    text: "Se eliminará el ingrediente seleccionado",
				    type: "warning",
				    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
					allowOutsideClick: false,
					confirmButtonText: 'Confirmar',
					cancelButtonText: 'Cancelar'
			}).then((result)=>{
                if(result == true){
                    var total3 = 0.0
                    $(this).parents(".item-row").remove();
                    $("#tblIngre tr").find('td:eq(5)').each(function () {
                    costo = $(this).html();
                    total3 += parseFloat(costo);
                });
                $("#lblCostoReceta").text(total3.toFixed(2));
                $("#lblCostoReceta_hidden").val(total3.toFixed(2));
            }
        });
    
          
    });

    $('#btnRevertir').click(function(){
        swal({
                  title: 'Revirtiendo cambios...',
				  allowEscapeKey: false,
				  allowOutsideClick: false,
				  showCancelButton: false,
				  showConfirmButton: false,
				  text: 'Espere un momento...'     
                });
        setTimeout(function() {
                            window.location.href = "/recetas/autorizar";
                        }, 1000);
    });
    

    $('#ingredienteSave').click(function(){ 
        var hoy = new Date();
        var dd = hoy.getDate();
        var mm = hoy.getMonth()+1;
        var yyyy = hoy.getFullYear();
        
        function addZero (i){ if(i<10){ i = '0' + 1; } return i;}
    
        dd=addZero(dd);
        mm=addZero(mm);
    
        var id =  "PENDI-" + dd + mm;
        var nombreIngre = $("#nombreIngrediente").val();
        var unidadMedida = $("#slcUM").val();
        var cantidadSucia = $("#cantidadSucia").val();
        var cantidadLimpia = $("#cantidadLimpia").val();
        var costo = parseFloat( $("#costo").val());
    
        if( nombreIngre && unidadMedida && cantidadSucia && cantidadLimpia && costo != ''){
            $('#tblIngre tbody').append("<tr id='trIngrediente "+id+"' class='item-row form-group'><td class='col-xs-1'><input name='id[]' type='hidden' value="+id+">"+id+"<td class='col-xs-1'><input type='hidden' name='nombre_ingrediente[]' id='NombreIng"+id+"' value='"+nombreIngre+"'>"+nombreIngre+"</td><td class='col-xs-1'><select name='unidad_ingre[]'class='custom-select'><option value='Kilos'>Kilos</option><option value='Litros'>Litros</option></select></td><td class='col-xs-1'><input type='text' value='"+cantidadSucia+"' class='item-added-qty form-control' id='CantidadSucia"+id+"Cant' style='width: 70px !important;' name='cantidadSucia[]' onkeyup='recalc(this)' onkeypress='return isNumberKey(event, this)'></td><td class='col-xs-1'><input type='text' value='"+cantidadLimpia+"' class='item-added-qty form-control' id='CostoIngLimpia"+id+"Cant' style='width: 70px !important;' name='cantidadLimpia[]' onkeyup='recalc(this)' onkeypress='return isNumberKey(event, this)'></td><td class='col-xs-1' id='lblCostoIng'>"+costo+"<input name='costo[]' type='hidden' value="+costo+"></td><td class='td-actions col-xs-1'><button type='button' rel='tooltip' data-placement='left' title='' class='btn btn-link remove-btn' data-original-title='Remove item'><i class='material-icons'>close</i></button></td></tr>");
            var total2 = 0.0
            $("#tblIngre tr").find('td:eq(5)').each(function () {
                costo = $(this).html();
                total2 += parseFloat(costo);
            });

            $("#lblCostoReceta").text(total2.toFixed(2));
                $("#nombreIngrediente").val("");
                $("#cantidadSucia").val("");
                $("#cantidadLimpia").val("");
                $("#costo").val("");
                $('#modalIngrediente').modal('hide');
             
            $(".remove-btn").click(function(){ 
                swal({
				    title: "Estas segur@?",
				    text: "Se eliminará el ingrediente seleccionado",
				    type: "warning",
				    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
					allowOutsideClick: false,
					confirmButtonText: 'Confirmar',
					cancelButtonText: 'Cancelar'
			}).then((result)=>{
                if(result == true){
                    var total3 = 0.0
                    $(this).parents(".item-row").remove();
                    $("#tblIngre tr").find('td:eq(5)').each(function () {
                    costo = $(this).html();
                    total3 += parseFloat(costo);
                });
                $("#lblCostoReceta").text(total3.toFixed(2));
                $("#lblCostoReceta_hidden").val(total3.toFixed(2));
            }
        });
            });
                
        }else{
            swal({
                  type: 'error',
                  title: 'Oops...',
                  text: 'Ingrese todos los datos',
                  footer: 'Problemas? sit@prigo.com.mx',
                });
        }
    });


    function mayus(e) {
    e.value = e.value.toUpperCase();
    }

    function recalc(el){
        var id = el.id.toString();
        var idS = id.substring(8);
        var cantidad = el.value;
        var costoIngre = $("#lbl"+id).text();
        $("#costoHidden"+idS).val(costoIngre);
        const costoFijo =  $("#costoHidden"+idS).val();

        if(!isNaN(cantidad)){    
            if( cantidad > 0){
                var costoTotal = cantidad * costoIngre;
                $("#costoF"+idS).text(costoTotal);
                $("#lblFinal"+idS).empty();
                $("#lblFinal"+idS).text(costoTotal);
                $("#costoF"+idS).val("hola");
            }else if( cantidad == 1){
                var costoTotal = costoFijo;
                $("#costoF"+idS).text(costoTotal);
                $("#lblFinal"+idS).empty(); 
                $("#lblFinal"+idS).text(costoTotal);
            }else if( cantidad == 0){
                var costoTotal = 0;
                $("#costoF"+idS).text(costoTotal);
                $("#lblFinal"+idS).empty(); 
                $("#lblFinal"+idS).text(costoTotal);

            }
        }else{
            swal({
                  type: 'error',
                  title: 'Oops...',
                  text: 'Ingresa un número valido'
                });
        }   
            
    }

    $("#btnGuardar").click(function(e){  {} 
        nombrePlatillo = $("#nombrePlatilloInput").val();
			swal({
				    title: "Estas segur@?",
				    text: "Se guardaran permanentemente los cambios realizados.",
				    type: "warning",
				    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
					allowOutsideClick: false,
					confirmButtonText: 'Enviar receta',
					cancelButtonText: 'Cancelar'
			}).then((result) =>{
                if (result == true) {
				$('.button').prop('disabled', true);
                swal({
                  title: 'Guardando...',
				  allowEscapeKey: false,
				  allowOutsideClick: false,
				  showCancelButton: false,
				  showConfirmButton: false,
				  text: 'Espere un momento...'     
                });
                $.ajax({
                    type: "POST",
                    url: "{{route('actualizarReceta')}}",
                    data: $('form.form-horizontal').serialize(),
                    // success: function(msg){
                    //     swal({
					// 		type: 'success',
                    //         title: 'Los cambios fueron enviados!',
                    //         icon: 'success',
                    //         showConfirmButton: false,
                    //         timer: 9000,
                    //     });	
                    //     setTimeout(function() {
                    //         window.location.href = "/recetas/autorizar";
                    //     }, 2000);
                    //     },
                    error: function(){
                        swal({
						  type: 'error',
						  title: 'Oops...',
						  text: 'Algo ha salido mal!',
						  footer: 'Problemas? sit@prigo.com.mx	',
						});
                    }
                    
                });
            }else{
                swal({
						  type: 'error',
						  title: 'Cancelaste el envio de la receta',
				});
            }
            });
       

    });

</script>
@endsection