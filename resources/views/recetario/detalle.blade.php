@extends('layouts.app')
@include('menu.recetario', ['seccion' => 'receta'])
@section('content')
@foreach($receta AS $dato)
<div class="row">
        <div class="col-md-12 ml-auto">
         {{ $dato->nombre }}
        </div>
</div>
<div class="row">
    <div class="col-md-12 ml-auto">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>Ingrediente</th>							
                    <th>Unidad</th>
                    <th>Cantidad Sucia</th>
                    <th>Cantidad Limpia</th>
                    <th>Costo</th>
                </tr>
            </head>
            <tbody> 
                
                @foreach($ingredientes AS $ingrediente)
                    <tr><td class="text-center">{{ $ingrediente->nombre }}</td><td class="text-center">{{ $ingrediente->unidad }}</td><td class="text-center">{{ $ingrediente->cantidadSucia }} </td><td class="text-center">{{ $ingrediente->cantidadLimpia }}</td><td class="text-center">{{ $ingrediente->costoInge }} </td> </tr>
                @endforeach                 
                 <tr><td></td><td></td><td></td><td>Costo Total:</td><td class="text-center">{{ $dato->costo}}</td></tr>
            </tbody>
        </table>
    </div>
</div>
@endforeach
@endsection
