@extends('layouts.app')
@include('menu.recetario', ['seccion' => 'localizar'])
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-orange card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Buscar Articulo en Recetas</h4>
					<input type="hidden" value="0" id="artId" />
					<input type="hidden" value="" id="artCod" />
					<input type="hidden" value="" id="artName" />
                </div>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group bmd-form-group">
                            <label for="slctArticulo">
                                Seleccione un Articulo
                            </label>
                            <select id="slctArticulo" class="getItemSelect" style="width: 100%"><option>nothing selected</option></select>
                            <button type="button" id="addArtBtn" class="btn btn-info btn-round"><i class="material-icons">search</i> Buscar</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 table-wrapper-2">
                        <table id="tblArts" class="table table-striped">
                        <thead>
                            <tr>
                                <th class='col-xs-1'>Receta</th>
                                <th class='col-xs-6'>Unidad</th>
                                <th class='col-xs-1'>Cant Receta</th>
                                <th class='col-xs-1'>U Ingrediente</th>
                                <th class='col-xs-1'>Cantidad</th>
                                <th class='col-xs-1'></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('aditionalScripts')
<style>
.table-wrapper-2 {
    display: block;
    max-height: 1300px;
    overflow-y: auto;
    -ms-overflow-style: -ms-autohiding-scrollbar;
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
$('.getItemSelect').select2({
		theme: "bootstrap",
		ajax: {
		url: "{{ route('getIngrediente') }}",
		dataType: 'json',
		delay: 250,
		data: function (params) {
		  return {
			q: params.term, // search term
			page: params.page
		  };
		},
		processResults: function (data, params) {
			// parse the results into the format expected by Select2
			// since we are using custom formatting functions we do not need to
			// alter the remote JSON data, except to indicate that infinite
			// scrolling can be used
			params.page = params.page || 1;

			return {
				results: data.items,
				pagination: {
				  more: (params.page * 30) < data.total_count
				}
			};
		},
		cache: true
		},
		minimumInputLength: 3,
		placeholder: 'Search for a repository',
		escapeMarkup: function (markup) { return markup; },
		templateResult: formatRepo,
		templateSelection: formatRepoSelection
	});
	function formatRepo (repo) {
  if (repo.loading) {
    return repo.text;
  }

  var markup = "<div class='select2-result-repository clearfix'>" +
    "<div class='select2-result-repository__meta'>" +
      "<div class='select2-result-repository__title'>" + repo.nombre + "</div>";

  if (repo.cod) {
    markup += "<div class='select2-result-repository__description'>" + repo.cod + "</div>";
  }

  markup += "</div></div>";

  return markup;
}

function formatRepoSelection (repo) {
	$("#artId").val(repo.id);	
	$("#artCod").val(repo.cod);	
	$("#artName").val(repo.nombre);
	return repo.nombre || repo.id;
}

$('#addArtBtn').click(function(){
		//e.preventDefault();
		if($("#artId").val() != 0)
		{
			var id= $("#artId").val();
			var cod= $("#artCod").val();
			var name= $("#artName").val();
            $('#tblArts tbody').empty();
            $.ajax({
                type: "POST",
                url: "{{ route('findIngrediente') }}",
                data: { "ingrediente": id, "_token": "{{ csrf_token() }}"},
                success: function(msg){
                    $('#tblArts tbody').empty();
                    var len = msg.data.length;
                    var l = 0;
                    if(len > 0)
                    {
                        while(l<len)
                        {
                            $('#tblArts tbody').append("<tr class='item-row form-group'><td class='col-xs-1'>"+msg.data[l].receta+"</td><td class='col-xs-6'>"+msg.data[l].ureceta+"</td><td class='col-xs-1'>"+msg.data[l].creceta+"</td><td class='col-xs-1'>"+msg.data[l].uing+"</td><td class='col-xs-1'>"+msg.data[l].cing+"</td><td class='td-actions col-xs-1'><a href=\"{{ route('detallereceta') }}/"+msg.data[l].irec+"\" class=\"btn btn-link btn-info btn-just-icon like\"><i class=\"material-icons\">open_in_new</i></a></td></tr>");
                            l++;
                        }
                    }
                },
                error: function(){
                    swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Algo ha salido mal!',
                    footer: 'Problemas? sit@prigo.com.mx	',
                    });
                    $('.button').prop('disabled', false);
                }
            });
        }
});
</script>
@endsection
