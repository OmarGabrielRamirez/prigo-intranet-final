@section('appmenu')
<li class="nav-item active">
	<a class="nav-link" href="{{ route('manto') }}" aria-expanded="true">
		<i class="material-icons">build</i>
		<p> Mantenimiento <b class="caret"></b> </p>
    </a>
	<div class="collapse show" id="pagesExamples" style="">
		<ul class="nav">
			<li class="nav-item @if($seccion == 'solicitudManto') active @endif ">
				<a class="nav-link" href="{{ route('solicitudManto') }}">
					<i class="material-icons">assignment</i>
					<p> Agregar</p>
				</a>
			</li>
			<li class="nav-item @if($seccion == 'consultaManto') active @endif">
				<a class="nav-link" href="{{ route('consultaManto') }}">
					<i class="material-icons">view_list</i>
					<p> Consultar </p>
				</a>
			</li>
		</ul>
	</div>
</li>
@endsection