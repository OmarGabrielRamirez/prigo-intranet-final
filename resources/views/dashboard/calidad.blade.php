@extends('layouts.app')
@include('menu.dashalex', ['seccion' => 'dash'])
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Calidad, Costo</h4>
            </div>
            <div class="card-content" style="padding: 10px;">
                <form class="form-horizontal">
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Sucursal</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="sucursal" name="sucursal" class="selectpicker">
                                    <option selected disabled> Seleccione una sucursal</option>
                                    @foreach($sucursales as $sucursal)
                                        <option value="{{ $sucursal->id }}">{{ $sucursal->nombre}}</option>
                                    @endforeach
                                </select>
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Periodo</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <select id="periodo" name="periodo" class="selectpicker">
                                    <option selected disabled> Seleccione un Periodo</option>
                                    <option value="201904">04-2019</option>
                                    <option value="201905">05-2019</option>
                                    <option value="201906">06-2019</option>
                                    <option value="201907">07-2019</option>
                                    <option value="201908">08-2019</option>
                                    <option value="201909">09-2019</option>
                                    <option value="201910">10-2019</option>
                                    <option value="201911">11-2019</option>
                                    <option value="201912">12-2019</option>
                                </select>
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Calidad</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="calidad" name="calidad" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Costo</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="costo" name="costo" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <a href="#" onclick="guardar()" class="btn btn-info">Guardar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
</div>

<!-- Modal -->
<div class="modal fade" id="detModal" tabindex="-1" role="dialog" aria-labelledby="detModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="detModalLabel"></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div id="detModalTabContent" class="col-md-12 ml-auto mr-auto"  style="height: 300px !important; overflow-y: scroll;">
                    
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- Modal -->
@endsection
@section('aditionalScripts')
<style>
        .loader {
          border: 16px solid #f3f3f3;
          border-radius: 50%;
          border-top: 16px solid #3498db;
          width: 120px;
          height: 120px;
          -webkit-animation: spin 2s linear infinite; /* Safari */
          animation: spin 2s linear infinite;
        }
        
        /* Safari */
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }
        
        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }

        .table-sm{
          font-size: 12px;
        }
            
        .table-sm th{
          width: 120px;
          padding: 4px;
        }
        .table-sm td{
          padding: 4px;
        }
        .tdnumber{
            text-align: right !important;
        }
        </style>
<script>
function getDet(periodo,nom,idp){
	$( ".loader" ).remove();
	$("#detModalLabel").text(nom);
	$('#detModal').modal('show');
    $("#detModalTabContent").append("<div class='loader'></div>");
    var params = { "periodo": periodo, "idp": idp, "_token": "{{ csrf_token() }}" };
	$.ajax({ 
		type: "POST",
		url: "{{ route('detAlexTable') }}",
		data: params,
		success: function(msg){
			$("#detModalTabContent").empty();
			$("#detModalTabContent").append(msg);
			$( ".loader" ).remove();
		},
		error: function(){
			console.log("error");
		}
	});
}

function guardar()
{
    if($("#costo").val() && $("#calidad").val() && $("#periodo").val()  && $("#sucursal").val())
    {
        swal({
            title: "Estas segur@?",
            text: "Los datos seran guardados permanentemente!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            allowOutsideClick: false,
            confirmButtonText: 'Si, enviar datos!',
            cancelButtonText: 'No, cancelar!'
        }).then((result) => {
            if (result) {
                $('.button').prop('disabled', true);
            swal({
                title: 'Guardando...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: false,
                showConfirmButton: false,
                text: 'Espere un momento...'
            });
            
            $.ajax({
                type: "POST",
                url: "{{ route('guardacalidad') }}",
                data: $('form.form-horizontal').serialize(),
                success: function(msg){
                    $('#tblReq tbody').empty();
                    $('form.form-horizontal')[0].reset();
                    swal({
                        type: 'success',
                        title: 'Los datos han sido guardados!'
                    });						
                    $('.button').prop('disabled', false);
                    
                },
                error: function(){
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Algo ha salido mal!',
                        footer: 'Problemas? sit@prigo.com.mx	',
                    });
                    $('.button').prop('disabled', false);
                }
            });	
            
            }
        });
    } else {
        swal({
            type: 'error',
            title: 'Oops...',
            text: 'Todos los datos son obligatorios!',
            footer: 'Problemas? sit@prigo.com.mx',
        });
    }
}
</script>
@endsection