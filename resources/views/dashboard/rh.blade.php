@extends('layouts.app')
@section('appmenu')
<li class="nav-item active">
	<a class="nav-link" data-toggle="collapse" href="#pagesExamples" aria-expanded="true">
		<i class="material-icons">assessment</i>
		<p> Dashboard <b class="caret"></b> </p>
    </a>
</li>
@endsection
@section('content')
<div class="row" style="margin-bottom: 10px;">
	<div class="col-md-7 ml-auto">
		<ul class="nav nav-pills nav-pills-warning justify-content-right" role="tablist">
			<li class="nav-item">
			<a class="nav-link" href="{{ route('panel') }}" role="tablist">
			  Dashboard
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashfin') }}" role="tablist">
			  Finanzas
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashop') }}" role="tablist">
			  Operaciones
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashcom') }}" role="tablist">
			  Compras
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link active" href="{{ route('dashrh') }}" role="tablist">
			  RH
			</a>
			</li>
		</ul>
	</div>
</div>
<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-6">
		<div class="card">
		  <div class="card-header card-header-success card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">assignment_turned_in</i>
			</div>
			<h5 class="card-title">Plantilla Autorizada</h5>
		  </div>
		  <div class="card-body">
			<h3>{{ $autorizados }}</h3>
		  </div>
		</div>
	</div>
	<div class="col-lg-4 col-md-6 col-sm-6">
		<div class="card">
		  <div class="card-header card-header-warning card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">assignment_late</i>
			</div>
			<h5 class="card-title">Vacantes #</h5>
		  </div>
		  <div class="card-body">
			<h3>{{ $diferencia }}</h3>
		  </div>
		</div>
	</div>
	<div class="col-lg-4 col-md-6 col-sm-6">
		<div class="card">
		  <div class="card-header card-header-info card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">assignment_ind</i>
			</div>
			<h5 class="card-title">Plantilla Actual</h5>
		  </div>
		  <div class="card-body">
			<h3>{{ $actuales }}</h3>
		  </div>
		</div>
	</div>
	<div class="col-lg-4 col-md-6 col-sm-6">
		<div class="card">
		  <div class="card-header card-header-info card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">assignment</i>
			</div>
			<h5 class="card-title">Solicitudes abiertas</h5>
		  </div>
		  <div class="card-body">
			<h3>{{ $abiertas }}</h3>
		  </div>
		</div>
	</div>
	<div class="col-lg-4 col-md-6 col-sm-6">
		<div class="card">
		  <div class="card-header card-header-warning card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">assignment_late</i>
			</div>
			<h5 class="card-title">Solicitudes retrasadas</h5>
		  </div>
		  <div class="card-body">
			<h3>{{ $retrasadas }}</h3>
		  </div>
		</div>
	</div>
	<div class="col-lg-4 col-md-6 col-sm-6">
		<div class="card">
		  <div class="card-header card-header-success card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">assignment_turned_in</i>
			</div>
			<h5 class="card-title">Solicitudes en tiempo</h5>
		  </div>
		  <div class="card-body">
			<h3>{{ $entiempo }}</h3>
		  </div>
		</div>
	</div>
	<div class="col-lg-4 col-md-6 col-sm-6">
		<div class="card">
		  <div class="card-header card-header-success card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">assignment_turned_in</i>
			</div>
			<h5 class="card-title">Contrataciones del mes</h5>
		  </div>
		  <div class="card-body">
			<h3>{{ $cerradas }}</h3>
		  </div>
		</div>
	</div>
</div>

@endsection
@section('aditionalScripts')
<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.ct-chart-bar .ct-series line {
  cursor:pointer;
}
.ct-series-a {
  .ct-line {
    animation: dash 5s linear forwards;
    animation-delay: 1s;
  }
  .ct-line, .ct-point, .ct-bar {
    stroke: #5b9bd5;
  }
}
.ct-series-b {
  .ct-line {
    animation: dash 5s linear forwards;
    animation-delay: 2s;
  }
  .ct-line, .ct-point, .ct-bar {
    stroke: #ed7d31;
  }
}
.ct-series-c {
  .ct-line {
    animation: dash 5s linear forwards;
    animation-delay: 3s;
  }
  .ct-line, .ct-point, .ct-bar {
    stroke: #a5a5a5;
  }
}
.key {
  font-size: 0.7rem;
  color: #555;
  padding: 0 30px 0 90px;
  .key-element {
    width: 32%;
    display: inline-block;
    padding-left: 22px;
    box-sizing: border-box;
    position: relative;
    &:before {
      content: "";
      display: block;
      width: 16px;
      height: 16px;
      position: absolute;
      top: -2px;
      left: 0;
    }
    &.key-series-a {
      &:before {
        background-color: #5b9bd5;
      }
    }
    &.key-series-b {
      &:before {
        background-color: #ed7d31;
      }
    }
  }
}
</style>
<script>
	
</script>
<style>
.ct-chart-bar .ct-label.ct-horizontal.ct-end {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  transform: rotate(-65deg);
  transform-origin: 50% 90%;
  text-align: right;
  max-height: 4em;
  min-width: 40px;
  max-width: 40px;
}
</style>
@endsection