<table class="table table-condensed table-striped cctt">
	<tbody>
	<tr>
		<td></td>
		<td>{{ $mes }}</td>
		<td>Cambio %</td>
		<td></td>
	</tr>
	<tr>
		<td><select id="mesKpiFinanzas" name="mes" class="form-control" onchange="loadKpiFinanzas()">
		<option value="0" selected="selected">Grupo PRIGO</option>
		<option value="1">Enero</option>
		<option value="2">Febrero</option>
		<option value="3">Marzo</option>
		<option value="4">Abril</option>
		<option value="5">Mayo</option>
		<option value="6">Junio</option>
		<option value="7">Julio</option>
		<option value="8">Agosto</option>
		<option value="9">Septiembre</option>
		<option value="10">Octubre</option>
		<option value="11">Noviembre</option>
		<option value="12">Diciembre</option>
		</select></td>
		<td>Actual</td>
		<td>Budget</td>
		<td>Forecast</td>
	</tr>
	  <tr class='cctt-collapsed'>
		<td class='cctt-control text-left'>Ingreso</td>
		<td class="text-right">{{$venta}}</td>
		<td class="text-right">{{ $venta_budget }} 
		@if( $alarmaVenta >= 100 ) 
			<i class="material-icons text-success">check</i>
		@elseif( $alarmaVenta >= 90 ) 
			<i class="material-icons text-warning">warning</i></td>
		@else 
			<i class="material-icons text-danger">error</i></td> 
		@endif
		<td class="text-right">Forecast</td>
	  </tr>
	  @if( !empty($venta_mes_det) )
		  @foreach($venta_mes_det as $ventam)
		<tr style='display:none;'><td class="text-left">{{ $ventam->idSucursal }}</td><td class="text-right">{{ number_format($ventam->total,0,'',',') }}</td><td>--</td><td>--</td></tr>
		  @endforeach
	  @endif
	  <tr class='cctt-collapsed'>
		<td class='cctt-control text-left'>Costo de Venta</td>
		<td class="text-right">{{ $costo }}</td>
		<td class="text-right">{{ $costo_budget }} 						
		@if( $alarmaCosto >= 100 ) 
			<i class="material-icons text-danger">error</i></td> 
		@elseif( $alarmaCosto >= 90 ) 
			<i class="material-icons text-warning">warning</i></td>
		@else 
			<i class="material-icons text-success">check</i>
		@endif</td>
		<td class="text-right">Forecast</td>
	  </tr>
	  <tr class='cctt-collapsed'>
		<td class='cctt-control text-left'>Utilidad Neta</td>
		<td class="text-right">{{ $uneta }}</td>
		<td class="text-right">{{ $uneta_budget }}
		@if( $alarmaUneta >= 100 ) 
			<i class="material-icons text-success">check</i>
		@elseif( $alarmaUneta >= 90 ) 
			<i class="material-icons text-warning">warning</i></td>
		@else 
			<i class="material-icons text-danger">error</i></td> 
		@endif
		</td>
		<td class="text-right">Forecast</td>
	  </tr>
	  <tr class='cctt-collapsed'>
		<td class='cctt-control text-left'>Gastos</td>
		<td class="text-right">{{ $gasto }}</td>
		<td class="text-right">{{ $gasto_budget }}
		@if( $alarmaGasto >= 100 ) 
			<i class="material-icons text-danger">error</i></td> 
		@elseif( $alarmaGasto >= 90 ) 
			<i class="material-icons text-warning">warning</i></td>
		@else 
			<i class="material-icons text-success">check</i>
		@endif</td>
		</td>
		<td class="text-right">Forecast</td>
	  </tr>
	  @if( !empty($gasto_mes_det) )
		  @foreach($gasto_mes_det as $gastom)
		<tr style='display:none;'><td class="text-left">{{ ucfirst(strtolower($gastom->categoria)) }}</td><td class="text-right">{{ number_format($gastom->total,0,'',',') }}</td><td>--</td><td>--</td></tr>
		  @endforeach
	  @endif
	  <tr class='cctt-collapsed'>
		<td class='cctt-control text-left'>Ebidta</td>
		<td class="text-right">{{ $ebidta }}</td>
		<td class="text-right stats">{{ $ebidta_budget }}
		@if( $alarmaEbidta >= 100 ) 
			<i class="material-icons text-success">check</i>
		@elseif( $alarmaEbidta >= 90 ) 
			<i class="material-icons text-warning">warning</i></td>
		@else 
			<i class="material-icons text-danger">error</i></td> 
		@endif</td>
		<td class="text-right">Forecast</td>
	  </tr>
	</tbody>
</table>