@if(!empty($acciones))
    @foreach($acciones as $pendiente)
        @if($tipo==1)
<tr><td>{{$pendiente->tipo}}</td><td>{{ $pendiente->fecha}}</td><td id="td_{{ $pendiente->id}}"><span id="lbl_{{ $pendiente->id}}">
    @if($pendiente->prioridad < 3)
        <i class="material-icons 
            @if($pendiente->prioridad < 2) 
            text-danger
            @else 
            text-warning
            @endif">flag</i>
        @endif {{ $pendiente->accion}}</span></td><td class="td-actions text-right"> <a href="#" onclick="editaPendiente({{ $pendiente->id  }}, {{ empty($pendiente->prioridad) ? 3 :$pendiente->prioridad }})" class="btn btn-success btn-simple"><i class="material-icons">edit</i></a>
        <a href="#" onclick="muestraMensajes({{ $pendiente->id  }})" class="btn btn-info btn-simple"><i class="material-icons">open_in_new</i></a>
</td><td style="text-align: center;"><input type="hidden" name="ida[]" value="{{$pendiente->id}}"><input type="checkbox" name="accion[{{$pendiente->id}}]"></td></tr>
        @else
            <tr><td>{{$pendiente->tipo}}</td><td>{{ $pendiente->accion}}</td></tr>
        @endif
    @endforeach 
@else
    <tr><td colspan=3>NO DATA</td></tr>
@endif