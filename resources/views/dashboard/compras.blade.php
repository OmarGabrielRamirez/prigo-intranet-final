@extends('layouts.app')
@section('appmenu')
<li class="nav-item active">
	<a class="nav-link" data-toggle="collapse" href="#pagesExamples" aria-expanded="true">
		<i class="material-icons">assessment</i>
		<p> Dashboard <b class="caret"></b> </p>
    </a>
</li>
@endsection
@section('content')
<div class="row" style="margin-bottom: 10px;">
	<div class="col-md-7 ml-auto">
		<ul class="nav nav-pills nav-pills-warning justify-content-right" role="tablist">
			<li class="nav-item">
			<a class="nav-link" href="{{ route('panel') }}" role="tablist">
			  Dashboard
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashfin') }}" role="tablist">
			  Finanzas
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashop') }}" role="tablist">
			  Operaciones
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link active" href="{{ route('dashcom') }}" role="tablist">
			  Compras
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashrh') }}" role="tablist">
			  RH
			</a>
			</li>
		</ul>
	</div>
</div>
<div class="row">
	<div id="dashop-panel-7" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Proveedores Global</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="proveedorTabContent" class="col-md-12 ml-auto mr-auto"  style="height: 300px !important; overflow-y: scroll;">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>#</th>							
									<th>Proveedor</th>							
									<th>Compra Total ($)</th>								
									<th>% Compra</th>								
								</tr>
							</head>
							<tbody>
								@foreach($compraTotalProv AS $dato)
									<tr onclick="getDetArt('{{ date('Y') }}','0','{{ $dato->idProveedor }}','{{ $dato->proveedorName }}')"><td>{{ $loop->iteration }}</td><td class="text-left">{{ $dato->proveedorName }}</td><td class="text-right">{{ number_format($dato->compraTotal,0,"",",") }}</td><td class="text-right">{{ number_format($dato->porcentaje,2,".",",") }}</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="row hidden" id="kpiventasHoraTab">
					<div id="kpiventasHoraTabContent" class="col-md-12 ml-auto mr-auto">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="dashop-panel-7" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Productos Global</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="proveedorTabContent" class="col-md-12 ml-auto mr-auto"  style="height: 300px !important; overflow-y: scroll;">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>#</th>							
									<th>Producto</th>							
									<!--th>Precio Acumulado</th>
									<th>Precio Anterior</th>								
									<th>Precio Actual</th-->
									<th>Compra Total ($)</th>
									<th>% Compra</th>
								</tr>
							</head>
							<tbody>
								@foreach($compraTotalItemProv AS $dato)
									<tr><td>{{ $loop->iteration }}</td><td class="text-left">{{ $dato->itemName }}</td><!--td class="text-right"></td><td class="text-right"></td><td class="text-right"></td--><td class="text-right">{{ number_format($dato->compraTotal,0,"",",") }}</td><td class="text-right">{{ number_format($dato->porcentaje,2,".",",") }}</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="row hidden" id="kpiventasHoraTab">
					<div id="kpiventasHoraTabContent" class="col-md-12 ml-auto mr-auto">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="dashop-panel-1" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Proveedores Eric Kayser</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="proveedorTabContent" class="col-md-12 ml-auto mr-auto"  style="height: 300px !important; overflow-y: scroll;">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>#</th>							
									<th>Proveedor</th>							
									<th>Compra Total ($)</th>								
									<th>% Compra</th>								
								</tr>
							</head>
							<tbody>
								@foreach($compraProv AS $dato)
									<tr onclick="getDetArt('{{ date('Y') }}','{{ $dato->idCompania }}','{{ $dato->idProveedor }}','{{ $dato->proveedorName }}')"><td>{{ $loop->iteration }}</td><td class="text-left">{{ $dato->proveedorName }}</td><td class="text-right">{{ number_format($dato->compraTotal,0,"",",") }}</td><td class="text-right">{{ number_format($dato->porcentaje,2,".",",") }}</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="row hidden" id="kpiventasHoraTab">
					<div id="kpiventasHoraTabContent" class="col-md-12 ml-auto mr-auto">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="dashop-panel-2" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Productos Eric Kayser</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="prodTabContent" class="col-md-12 ml-auto mr-auto" style="height: 300px !important; overflow-y: scroll;">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>#</th>							
									<th>Producto</th>							
									<th>Precio Acumulado</th>
									<th>Precio Anterior</th>								
									<th>Precio Actual</th>								
									<th>Compra Total ($)</th>								
									<th>% Compra</th>							
								</tr>
							</head>
							<tbody>
								@foreach($compraItem AS $dato)
									<tr onclick="getDetProv('{{ date('Y') }}','{{ $dato->idCompania }}','{{ $dato->itemCode }}','{{ $dato->itemName }}')"><td>{{ $loop->iteration }}</td><td class="text-left">{{ $dato->itemName }}</td><td class="text-right">{{ number_format($dato->acumulado,2,".",",") }}</td><td class="text-right">{{ number_format($dato->anterior,2,".",",") }}</td><td class="text-right">{{ number_format($dato->actual,2,".",",") }}</td><td class="text-right">{{ number_format($dato->compraTotal,0,"",",") }}</td><td class="text-right">{{ number_format($dato->porcentaje,2,".",",") }}</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="row hidden" id="kpiventasHoraTab">
					<div id="kpiventasHoraTabContent" class="col-md-12 ml-auto mr-auto">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="dashop-panel-3" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Compras hechas a Prigo</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="primosgoPromvTabContent" class="col-md-12 ml-auto mr-auto" style="height: 300px !important; overflow-y: scroll;">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>#</th>							
									<th>Producto</th>
									<th>Precio Acumulado</th>
									<th>Precio Anterior</th>								
									<th>Precio Actual</th>	
									<th>Compra Total ($)</th>							
								</tr>
							</head>
							<tbody>
								@foreach($primosItem AS $dato)
									<tr><td>{{ $loop->iteration }}</td><td class="text-left">{{ $dato->itemName }}</td><td class="text-right">{{ number_format($dato->acumulado,2,".",",") }}</td><td class="text-right">{{ number_format($dato->anterior,2,".",",") }}</td><td class="text-right">{{ number_format($dato->actual,2,".",",") }}</td><td class="text-right">{{ number_format($dato->total,0,"",",") }}</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="row hidden" id="kpiventasHoraTab">
					<div id="kpiventasHoraTabContent" class="col-md-12 ml-auto mr-auto">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="dashop-panel-4" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Proveedores Prigo</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="proveedorTabContent" class="col-md-12 ml-auto mr-auto"  style="height: 300px !important; overflow-y: scroll;">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>#</th>							
									<th>Proveedor</th>							
									<th>Compra Total ($)</th>								
									<th>% Compra</th>								
								</tr>
							</head>
							<tbody>
								@foreach($compraProvPrigo AS $dato)
									<tr onclick="getDetArt('{{ date('Y') }}','{{ $dato->idCompania }}','{{ $dato->idProveedor }}','{{ $dato->proveedorName }}')"><td>{{ $loop->iteration }}</td><td class="text-left">{{ $dato->proveedorName }}</td><td class="text-right">{{ number_format($dato->compraTotal,0,"",",") }}</td><td class="text-right">{{ number_format($dato->porcentaje,2,".",",") }}</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="row hidden" id="kpiventasHoraTab">
					<div id="kpiventasHoraTabContent" class="col-md-12 ml-auto mr-auto">
						
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div id="dashop-panel-5" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Productos Prigo</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="prodTabContent" class="col-md-12 ml-auto mr-auto" style="height: 300px !important; overflow-y: scroll;">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>#</th>							
									<th>Producto</th>							
									<th>Precio Acumulado</th>								
									<th>Precio Anterior</th>								
									<th>Precio Actual</th>								
									<th>Compra Total ($)</th>								
									<th>% Compra</th>						
								</tr>
							</head>
							<tbody>
								@foreach($compraItemPrigo AS $dato)
									<tr onclick="getDetProv('{{ date('Y') }}','{{ $dato->idCompania }}','{{ $dato->itemCode }}','{{ $dato->itemName }}')"><td>{{ $loop->iteration }}</td><td class="text-left">{{ $dato->itemName }}</td><td class="text-right">{{ number_format($dato->acumulado,2,".",",") }}</td><td class="text-right">{{ number_format($dato->anterior,2,".",",") }}</td><td class="text-right">{{ number_format($dato->actual,2,".",",") }}</td><td class="text-right">{{ number_format($dato->compraTotal,0,"",",") }}</td><td class="text-right">{{ number_format($dato->porcentaje,2,".",",") }}</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="row hidden" id="kpiventasHoraTab">
					<div id="kpiventasHoraTabContent" class="col-md-12 ml-auto mr-auto">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="detModal" tabindex="-1" role="dialog" aria-labelledby="detModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="detModalLabel"></h4>
      </div>
      <div class="modal-body">
		<div class="row">
			<div id="detModalTabContent" class="col-md-12 ml-auto mr-auto"  style="height: 300px !important; overflow-y: scroll;">
				
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
@endsection
@section('aditionalScripts')
<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.ct-chart-bar .ct-series line {
  cursor:pointer;
}
.ct-series-a {
  .ct-line {
    animation: dash 5s linear forwards;
    animation-delay: 1s;
  }
  .ct-line, .ct-point, .ct-bar {
    stroke: #5b9bd5;
  }
}
.ct-series-b {
  .ct-line {
    animation: dash 5s linear forwards;
    animation-delay: 2s;
  }
  .ct-line, .ct-point, .ct-bar {
    stroke: #ed7d31;
  }
}
.ct-series-c {
  .ct-line {
    animation: dash 5s linear forwards;
    animation-delay: 3s;
  }
  .ct-line, .ct-point, .ct-bar {
    stroke: #a5a5a5;
  }
}
.key {
  font-size: 0.7rem;
  color: #555;
  padding: 0 30px 0 90px;
  .key-element {
    width: 32%;
    display: inline-block;
    padding-left: 22px;
    box-sizing: border-box;
    position: relative;
    &:before {
      content: "";
      display: block;
      width: 16px;
      height: 16px;
      position: absolute;
      top: -2px;
      left: 0;
    }
    &.key-series-a {
      &:before {
        background-color: #5b9bd5;
      }
    }
    &.key-series-b {
      &:before {
        background-color: #ed7d31;
      }
    }
  }
}
</style>
<script>
$('#kpiventasHora').on('click', '.ct-chart-bar .ct-labels foreignobject', function(evt) {
  var index = $(this).index();
  var label = $(this).find('span.ct-label').text();
});

function getDetProv(anio, idc, idi, nom ){
	$( ".loader" ).remove();
	$("#detModalLabel").text(nom);
	$('#detModal').modal('show');
	$("#detModalTabContent").append("<div class='loader'></div>");
	
	var params = { "anio": anio, "idc": idc, "idi": idi, "_token": "{{ csrf_token() }}" };
	$.ajax({ 
		type: "POST",
		url: "{{ route('detArtTable') }}",
		data: params,
		success: function(msg){
			$("#detModalTabContent").empty();
			$("#detModalTabContent").append(msg);
			$( ".loader" ).remove();
		},
		error: function(){
			console.log("error");
		}
	});
}


function getDetArt(anio, idc, idp, nom ){
	$( ".loader" ).remove();
	$("#detModalLabel").text(nom);
	$('#detModal').modal('show');
	$("#detModalTabContent").append("<div class='loader'></div>");
	
	var params = { "anio": anio, "idc": idc, "idp": idp, "_token": "{{ csrf_token() }}" };
	$.ajax({ 
		type: "POST",
		url: "{{ route('detProvTable') }}",
		data: params,
		success: function(msg){
			$("#detModalTabContent").empty();
			$("#detModalTabContent").append(msg);
			$( ".loader" ).remove();
		},
		error: function(){
			console.log("error");
		}
	});
}

function getcolor(context) {
	if(context.type == "bar")
	{
		var stroke = "";
		switch(context.seriesIndex)
		{
			case 0:
				stroke = "220";
			break;
			case 1:
				stroke = "3";
			break;
			case 2:
				stroke = "44";
			break;
			case 3:
				stroke = "113";
			break;
			case 4:
				stroke = "200";
			break;
			case 5:
				stroke = "250";
			break;
		}
		context.element.attr({ style: 'stroke: hsl('+stroke+', 100%, 65%);'});
	}
}
$('#kpiventasHora').on('click', '.ct-chart-bar .ct-series-a line, .ct-chart-bar .ct-series-b line, .ct-chart-bar .ct-series-c line', function(evt) {
	
  var index = $(this).index();
  var label = $(this).closest('.ct-chart-bar').find('.ct-labels foreignObject:nth-child('+(index+1)+') span').text();
  var value = $(this).attr('ct:value');
  var metadata = $(this).attr('ct:meta');
  var meta = metadata.split(";");
  
  $("#kpiventasHoraTab").removeClass("hidden");
  $("#kpiventasHoraTab").addClass("animated fadeIn");
  $("#kpiventasHoraTabContent").empty();
  $("#kpiventasHoraTabContent").append("<div class='loader'></div>");
  loadTable("#kpiventasHoraTabContent", {"anio": meta[0], "mes": ({{ $tiempo }} != 4 ? "":meta[1]), "filtro": ({{ $tiempo }} != 4 ? meta[1]:meta[2]), "tiempo": "{{ $tiempo }}","_token": "{{ csrf_token() }}" } );
  
});
function loadTable(idDiv, params)
{
	$.ajax({ 
		type: "POST",
		url: "{{ route('kpiOpsTable') }}",
		data: params,
		success: function(msg){
			$(idDiv).empty();
			$(idDiv).append(msg);
		},
		error: function(){
			console.log("error");
		}
	});

}
	
</script>
<style>
.ct-chart-bar .ct-label.ct-horizontal.ct-end {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  transform: rotate(-65deg);
  transform-origin: 50% 90%;
  text-align: right;
  max-height: 4em;
  min-width: 40px;
  max-width: 40px;
}
</style>
@endsection