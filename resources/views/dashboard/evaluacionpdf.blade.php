@if(!empty($isPdf))
	    <table class="table-sm table-bordered" border="1" >
        <thead>
            <tr>
                <th rowspan=2>Tienda/Periodo</th>
                @foreach($periodos as $periodo)
            <th colspan=5>{{ $meses[$periodo->mes] }} - {{ $periodo->anio }}</th>
                @endforeach            
            </tr>
            <tr>
                @foreach($periodos as $periodo)
                <th style="text-align: center;">Evaluacion</th><th  style="text-align: center;">Bono</th><th  style="text-align: center;">Supervisor</th><th  style="text-align: center;">Gerente</th><th  style="text-align: center;">Chef</th><th  style="text-align: center;">Venta</th>
                @endforeach 
            </tr>
        </thead>
        <tbody>
            @foreach($bonos as $bono)
            <tr>
                <td >{{ $bono->nombre }}</td>
                <td  style="text-align: right;">{{ number_format($bono->evaluacion,2,".",",") }}</td>
                <td style="text-align: right;">{{ number_format($bono->bono,0,"",",") }}</td>
                <td style="text-align: right;">{{ number_format($bono->bonoSupervisor,0,"",",") }}</td>
                <td style="text-align: right;">{{ number_format($bono->bonoGerente,0,"",",") }}</td>
                <td style="text-align: right;">{{ number_format($bono->bonoChef,0,"",",") }}</td>
                <td style="text-align: right;">{{ number_format($bono->venta,0,"",",") }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endif	