@extends('layouts.app')
@include('menu.dashalex', ['seccion' => 'dash'])
@section('content')
<div class="row" style="z-index: 1050!important;">
    <div class="col-md-12">
    <table class="table-sm table-bordered">
        <thead>
            <tr>
                <th rowspan=2>Tienda/Periodo</th>
            <th colspan=7>{{ $meses[$mes] }} - {{ $anio }}  <a target="_blank" class="btn btn-info btn-sm btn-simple" href="{{ route('compara', 1) }}">
              <i class="material-icons">get_app</i>
          </a></th>
            </tr>
            <tr>
            <th class="text-center"><a href="{{ route('compara', ['isPdf'=>0,'order' => ($order=='desc'?'asc':'desc'),'col'=> 'Nomina']) }}">Nomina</a></th><th class="text-center"><a href="{{ route('compara', ['isPdf'=>0,'order' => ($order=='desc'?'asc':'desc'),'col'=> 'Costo']) }}">Costo</a></th><th class="text-center"><a href="{{ route('compara', ['isPdf'=>0,'order' => ($order=='desc'?'asc':'desc'),'col'=> 'Gasto']) }}">Gasto</a></th><th class="text-center"><a href="{{ route('compara', ['isPdf'=>0,'order' => ($order=='desc'?'asc':'desc'),'col'=> 'Renta']) }}">Renta</a></th>
            <th class="text-center"><a href="{{ route('compara', ['isPdf'=>0,'order' => ($order=='desc'?'asc':'desc'),'col'=> 'EbitM']) }}">Ebit $</a></th>
            <th class="text-center"><a href="{{ route('compara', ['isPdf'=>0,'order' => ($order=='desc'?'asc':'desc'),'col'=> 'EbitP']) }}">Ebit %</a></th>
            <th class="text-center"><a href="{{ route('compara', ['isPdf'=>0,'order' => ($order=='desc'?'asc':'desc'),'col'=> 'supervisor']) }}">Supervisa</a></th>
            </tr>
        </thead>
        <tbody>
            @foreach($gastos as $bono)
            <tr>
                <td >{{ $bono->nombre }}</td>
                <td class="text-center">{{ number_format($bono->Nomina,2,".",",") }} %</td>
                <td class="text-center" @if($bono->Costo<=29.5)style="color:green;font-weight:bold;" @else style="color:red;font-weight:bold;"@endif>{{ number_format($bono->Costo,2,".",",") }} %</td>
                <td class="text-center">{{ number_format($bono->Gasto,2,".",",") }} %</td>
                <td class="text-center">{{ number_format($bono->Renta,2,".",",") }} %</td>
                <td class="text-center">{{ number_format($bono->EbitM,2,".",",") }} </td>
                <td class="text-center">{{ number_format($bono->EbitP,2,".",",") }} %</td>
                <td class="text-center">{{ $bono->supervisor }} </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</div>
@endsection
@section('aditionalScripts')
<style>
        .loader {
          border: 16px solid #f3f3f3;
          border-radius: 50%;
          border-top: 16px solid #3498db;
          width: 120px;
          height: 120px;
          -webkit-animation: spin 2s linear infinite; /* Safari */
          animation: spin 2s linear infinite;
        }
        
        /* Safari */
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }
        
        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }

        .table-sm{
          font-size: 12px;
        }
            
        .table-sm th{
          width: 120px;
          padding: 4px;
        }
        .table-sm td{
          padding: 4px;
        }
        .tdnumber{
            text-align: right !important;
        }
        </style>
@endsection