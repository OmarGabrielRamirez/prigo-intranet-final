<h4>
Productos de {{ $seccion }} en 
@IF($tipo==1)
	Monto ($) <a href="#">ver por Cantidad</a>
@ELSEIF($tipo==2)
	Cantidad (#) <a href="#">ver por Monto</a>
@ENDIF
</h4>
<table class="table table-condensed table-striped">
@IF($tipo==1)
	<thead>
		<tr>
			<th>Articulo</th>							
			<th>Venta Total ($)</th>
		</tr>
	</head>
	<tbody>
		@foreach($detArt AS $dato)
			<tr><td class="text-left">{{ $dato->menuItem }}</td><td  class="text-right">{{ number_format($dato->total,0,"",",") }}</td></tr>
		@endforeach
	</tbody>
@ELSEIF($tipo==2)
	<thead>
		<tr>
			<th>Articulo</th>							
			<th>En Combo</th>
			<th>Cantidad Total</th>
		</tr>
	</head>
	<tbody>
		@foreach($detArt AS $dato)
			<tr><td class="text-left">{{ $dato->menuItem }}</td><td  class="text-right">{{ $dato->combo }}</td><td  class="text-right">{{ $dato->total }}</td></tr>
		@endforeach
	</tbody>
@ENDIF
</table>