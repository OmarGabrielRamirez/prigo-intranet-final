@extends('layouts.app')
@section('appmenu')
<li class="nav-item active">
	<a class="nav-link" data-toggle="collapse" href="#pagesExamples" aria-expanded="true">
		<i class="material-icons">assessment</i>
		<p> Dashboard <b class="caret"></b> </p>
    </a>
</li>
@endsection
@section('content')
<div class="row" style="margin-bottom: 10px;">
	<div class="col-md-7 ml-auto">
		<ul class="nav nav-pills nav-pills-warning justify-content-right" role="tablist">
			<li class="nav-item">
			<a class="nav-link" href="{{ route('panel') }}" role="tablist">
			  Dashboard
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashfin') }}" role="tablist">
			  Finanzas
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link active" href="{{ route('dashop') }}" role="tablist">
			  Operaciones
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashcom') }}" role="tablist">
			  Compras
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashrh') }}" role="tablist">
			  RH
			</a>
			</li>
		</ul>
	</div>
</div>
<div class="row">
	<div id="dashop-panel-2" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Venta por RVC {{ $mes }}</h4>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto"  id="dashop-panel-2-body">
						<div class="row" id="rvc-level-1">
							<div class="col-md-7" style="overflow-x: auto;overflow-y: hidden;">
								<div class="ct-chart" id="kpiRVCChart" style="height: 250px!important;"></div>
							</div>
							<div class="col-md-5 ml-auto mr-auto" style="overflow-x: auto;overflow-y: hidden;">
								<table class="table table-condensed table-striped">
									<tr><td>RVC</td><td>{{ $mes }}</td><td>Anterior</td><td></td></tr>
								@foreach($ventasRVC as $rvc)
									<tr>
										<td class="text-left"><span style='background-color: @if($loop->index == 0) #00BCD4 @elseif($loop->index == 1) #F44336 @elseif($loop->index == 2) #FF9800 @elseif($loop->index == 3) #00CC44 @else #c68c53 @endif;' >&nbsp;&nbsp;&nbsp;&nbsp;</span> {{ $rvc->RVC }} </td><td class="text-right"> $ {{ number_format($rvc->total,0,"",",") }} </td><td class="text-right"> $ {{ number_format($rvc->past,0,"",",")}}</td><td class="text-right"><i class="material-icons">@if($rvc->total >= $rvc->past) trending_up @else trending_down @endif </i></td>
									</tr>
								@endforeach
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div id="dashop-panel-2-table" class="col-md-12 ml-auto mr-auto">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="dashop-panel-1" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Ventas por Horario ($K) {{ $mes }}</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="tabchequePromvTabContent" class="col-md-12 ml-auto mr-auto" style="height: 300px !important; overflow-y: scroll;">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>Horario</th>
									<th>CDMX</th>								
									<th>CDMX Alta Gama</th>								
									<th>AICM</th>								
									<th>OMA</th>								
									<th>Convenio</th>								
								</tr>
							</head>
							<tbody>
								@foreach($ventasHora AS $dato)
									<tr><td class="text-left">De {{ $dato->horaInicio }} @IF($dato->horaInicio <24 )  a {{ $dato->horaInicio +1 }} @ENDIF</td><td class="text-right">{{ number_format($dato->cat_1,0,"",",") }}</td><td class="text-right">{{ number_format($dato->cat_2,0,"",",") }}</td><td class="text-right">{{ number_format($dato->cat_3,0,"",",") }}</td><td class="text-right">{{ number_format($dato->cat_4,0,"",",") }}</td><td class="text-right">{{ number_format($dato->cat_5,0,"",",") }}</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="row hidden" id="kpiventasHoraTab">
					<div id="kpiventasHoraTabContent" class="col-md-12 ml-auto mr-auto">
						
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div id="dashop-panel-3" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Cheque promedio Salon {{ $mes }}</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="chPromSalonTabContent" class="col-md-12 ml-auto mr-auto">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>Gama</th>
									<th>Acumulado</th>								
									<th>Mes Anterior</th>								
									<th>{{ $mes }}</th>								
									<th>D. Acumulado</th>								
									<th>D. Anterior</th>								
								</tr>
							</head>
							<tbody>
								@foreach($chequeSalon AS $dato)
									<tr><td class="text-left">{{ $dato->categoria }} </td><td class="text-right">{{ number_format($dato->Cheque_Acumulado,0,"",",") }}</td><td class="text-right">{{ number_format($dato->Cheque_Anterior,0,"",",") }}</td><td class="text-right">{{ number_format($dato->Cheque_Actual,0,"",",") }}</td><td class="text-right">{{ number_format(($dato->Cheque_Actual - $dato->Cheque_Acumulado)/$dato->Cheque_Acumulado*100,0,"",",") }} %</td><td class="text-right">{{ number_format(($dato->Cheque_Actual - $dato->Cheque_Anterior)/(empty($dato->Cheque_Anterior)?1:$dato->Cheque_Anterior)*100,0,"",",") }} %</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>		
	<div id="dashop-panel-4" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Cheque promedio Vitrina {{ $mes }}</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="chPromVitrinaTabContent" class="col-md-12 ml-auto mr-auto">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>Gama</th>
									<th>Acumulado</th>								
									<th>Mes Anterior</th>								
									<th>{{ $mes }}</th>								
									<th>D. Acumulado</th>								
									<th>D. Anterior</th>								
								</tr>
							</head>
							<tbody>
								@foreach($chequeVitrina AS $dato)
									<tr><td class="text-left">{{ $dato->categoria }} </td><td class="text-right">{{ number_format($dato->Cheque_Acumulado,0,"",",") }}</td><td class="text-right">{{ number_format($dato->Cheque_Anterior,0,"",",") }}</td><td class="text-right">{{ number_format($dato->Cheque_Actual,0,"",",") }}</td><td class="text-right">{{ number_format(($dato->Cheque_Actual - $dato->Cheque_Acumulado)/$dato->Cheque_Acumulado*100,0,"",",") }} %</td><td class="text-right">{{ number_format(($dato->Cheque_Actual - $dato->Cheque_Anterior)/(empty($dato->Cheque_Anterior)?1:$dato->Cheque_Anterior)*100,0,"",",") }} %</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div id="dashop-panel-5" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Clientes promedio Salon {{ $mes }}</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="chPromVitrinaTabContent" class="col-md-12 ml-auto mr-auto">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>Gama</th>
									<th>Acumulado</th>								
									<th>Mes Anterior</th>								
									<th>{{ $mes }}</th>								
									<th>D. Acumulado</th>								
									<th>D. Anterior</th>								
								</tr>
							</head>
							<tbody>
								@foreach($chequeSalon AS $dato)
									<tr><td class="text-left">{{ $dato->categoria }} </td><td class="text-right">{{ number_format($dato->Clientes_Acumulado,0,"",",") }}</td><td class="text-right">{{ number_format($dato->Clientes_Anterior,0,"",",") }}</td><td class="text-right">{{ number_format($dato->Clientes_Actual,0,"",",") }}</td><td class="text-right">{{ number_format(($dato->Clientes_Actual - $dato->Clientes_Acumulado)/$dato->Clientes_Acumulado*100,0,"",",") }} %</td><td class="text-right">{{ number_format(($dato->Clientes_Actual - $dato->Clientes_Anterior)/(empty($dato->Clientes_Anterior)?1:$dato->Clientes_Anterior)*100,0,"",",") }} %</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>		
	<div id="dashop-panel-6" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Clientes promedio Vitrina {{ $mes }}</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="chPromVitrinaTabContent" class="col-md-12 ml-auto mr-auto">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>Gama</th>
									<th>Acumulado</th>								
									<th>Mes Anterior</th>								
									<th>{{ $mes }}</th>								
									<th>D. Acumulado</th>								
									<th>D. Anterior</th>								
								</tr>
							</head>
							<tbody>
								@foreach($chequeVitrina AS $dato)
									<tr><td class="text-left">{{ $dato->categoria }} </td><td class="text-right">{{ number_format($dato->Clientes_Acumulado,0,"",",") }}</td><td class="text-right">{{ number_format($dato->Clientes_Anterior,0,"",",") }}</td><td class="text-right">{{ number_format($dato->Clientes_Actual,0,"",",") }}</td><td class="text-right">{{ number_format(($dato->Clientes_Actual - $dato->Clientes_Acumulado)/$dato->Clientes_Acumulado*100,0,"",",") }} %</td><td class="text-right">{{ number_format(($dato->Clientes_Actual - $dato->Clientes_Anterior)/(empty($dato->Clientes_Anterior)?1:$dato->Clientes_Anterior)*100,0,"",",") }} %</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
@endsection
@section('aditionalScripts')
<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.ct-chart-bar .ct-series line {
  cursor:pointer;
}
</style>
<script>

$('#kpiRVCChart').on('click', '.ct-slice-pie', function(evt) {
  
  var index = $(this).index();
  var value = $(this).attr('ct:value');
  var metadata = $(this).attr('ct:meta');

  var meta = metadata.split("|");
  
  loadChart("dashop-panel-2-body","2", {"anio": meta[2], "mes": meta[1], "filtro": meta[0] ,"_token": "{{ csrf_token() }}" } );
 
});
var colors = new Array("#00BCD4","#F44336","#FF9800","#00CC44","#c68c53","#ffffff");

function loadChart(idDiv,level, params)
{
	$("#rvc-level-"+level).remove();
	$("#"+idDiv).append("<div class='row' id='rvc-level-"+level+"'><div class='col-md-12'><h4>"+params.filtro+"</h4></div><div class='loader'></div></div>");
	
    $.ajax({ 
		type: "POST",
		url: "{{ route('rvcDetMajor') }}",
		data: params,
		success: function(msg){
			$("#rvc-level-"+level+" .loader").remove();
			var data = msg.data;
			var series= new Array();
			var labels = new Array();
			data.forEach(function(element, index, array) {
				  series.push({value: element.porcentaje, meta: element.RVC+"|"+element.MajorGroup+"|"+element.mes+"|"+element.anio });
				  labels.push( "<tr><td class='text-left'><span style='background-color: "+colors[index]+"'>&nbsp;&nbsp;&nbsp;&nbsp;</span> "+element.MajorGroup + "</td><td class='text-right'>$ " + element.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") +"</td></tr>");
			});
			
			$("#rvc-level-"+level).append("<div id='rvc-level-"+level+"-chart' class='col-md-7' style='overflow-x: auto;overflow-y: hidden;'><div class='ct-chart' id='kpiRVCChart-"+level+"' style='height: 250px!important;'></div></div>");
			$("#rvc-level-"+level).append("<div  id='rvc-level-"+level+"-info' class='col-md-5 ml-auto mr-auto' style='overflow-x: auto;overflow-y: hidden;'><table class='table table-condensed table-striped'><tr><td>Major Group</td><td>Monto</td></tr>"+labels.join(" ")+"</table></div>");
			
			var dataPie = {
			  series: series
			};

			new Chartist.Pie("#kpiRVCChart-"+level, dataPie, {
			  labelOffset: 60,
			  chartPadding: 35,
			  labelInterpolationFnc: function(value) {
				return Math.round(value) + '%';
			  }
			});
			//rvcDetItem
			$("#kpiRVCChart-"+level).on('click', '.ct-slice-pie', function(evt) {
				  var index = $(this).index();
				  var value = $(this).attr('ct:value');
				  var metadata = $(this).attr('ct:meta');
				  var meta = metadata.split("|");
				  console.log(meta);
				  loadTable("#kpiRVCChart-"+level+"-table", { mes: meta[2], anio: meta[3], filtro: meta[0], filtro2: meta[1], tipo: 1, "_token": "{{ csrf_token() }}" });
			});
			
			loadTable(idDiv, { mes: params.mes, anio: params.anio, filtro: params.filtro, tipo: 1,"_token": "{{ csrf_token() }}" })
			
		},
		error: function(){
			console.log("error");
		}
	});
}

	var data = {
	  series: [@foreach($ventasRVC as $rvc) @if($loop->index > 0) , @endif { value: {{ $rvc->porcentaje }}, meta: '{{ $rvc->RVC }}|{{ $rvc->mes }}|{{ $rvc->anio }}' } @endforeach]
	};

	new Chartist.Pie('#kpiRVCChart', data, {
      labelOffset: 60,
	  chartPadding: 35,
	  labelInterpolationFnc: function(value) {
		return Math.round(value) + '%';
	  }
	});
	
	function loadTable(idDiv, params){
		$("#dashop-panel-2-table").empty();
		$("#dashop-panel-2-table").append("<div class='loader'></div>");
	    $.ajax({ 
			type: "POST",
			url: "{{ route('rvcDetItem') }}",
			data: params,
			success: function(msg){
				$("#dashop-panel-2-table").empty();
				$("#dashop-panel-2-table").append(msg);
			},
			error: function(){
				console.log("error");
			}
		});
	}
	
</script>
<style>
.ct-chart-bar .ct-label.ct-horizontal.ct-end {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  transform: rotate(-65deg);
  transform-origin: 50% 90%;
  text-align: right;
  max-height: 4em;
  min-width: 40px;
  max-width: 40px;
}
</style>
@endsection