@extends('layouts.app')
@section('appmenu')
<li class="nav-item active">
	<a class="nav-link" href="#" aria-expanded="true">
		<i class="material-icons">assessment</i>
		<p> Dashboard <b class="caret"></b> </p>
    </a>
	<div class="collapse show" id="pagesExamples" style="">
		<ul class="nav">
			<li class="nav-item">
				<a class="nav-link" href="{{route('bitacora')}}">
					<i class="material-icons">assignment_turned_in</i>
					<p> Bitacora </p>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{route('redes')}}">
					<i class="material-icons">thumb_up_alt</i>
					<p>Satisfaccion Cliente</p>
				</a>
			</li>
			<li class="nav-item">
                <a class="nav-link" href="{{route('auditi')}}">
					<i class="material-icons">assignment</i>
					<p> Auditoria I. </p>
				</a>
			</li>
			<li class="nav-item">
                <a class="nav-link" href="{{ route('ubercal')}}">
					<i class="material-icons">local_taxi</i>
					<p> Uber </p>
				</a>
			</li>
			<li class="nav-item">
                <a class="nav-link" href="{{ route('rhemps')}}">
					<i class="material-icons">transfer_within_a_station</i>
					<p> RH </p>
				</a>
			</li>
		</ul>
	</div>
</li>
@endsection
@section('content')
<div class="row" >
    <div class="col-md-12">
        <div class="card" >
            <div class="card-header">
                <h4 class="card-title">Registro de satisfaccion en redes sociales</h4>
            </div>
            <div class="card-content" style="padding: 10px;">
                <form id="frmRedes" class="form-horizontal">
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Sucursal</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="sucursal" name="sucursal" class="selectpicker">
                                    <option selected disabled> Seleccione una sucursal</option>
                                    @foreach($sucursales as $sucursal)
                                        <option value="{{ $sucursal->id }}">{{ $sucursal->nombre}}</option>
                                    @endforeach
                                </select>
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Red Social</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                    <select id="red" name="red" class="selectpicker">
                                        <option selected disabled> Seleccione una red</option>
                                        <option value="1">Tripadvisor</option>
                                        <option value="2">Google</option>
                                        <option value="3">Facebook</option>
                                        <option value="4">Yelps</option>
                                        <option value="5">Twitter</option>
                                        <option value="6">Foursquare</option>
                                        <option value="7">Ubereats</option>
                                    </select>
                                    <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Periodo</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                    <select id="periodo" name="periodo" class="selectpicker">
                                        <option selected disabled> Seleccione un Periodo</option>
                                        <option value="201904">04-2019</option>
                                        <option value="201905">05-2019</option>
                                        <option value="201906">06-2019</option>
                                        <option value="201907">07-2019</option>
                                        <option value="201908">08-2019</option>
                                        <option value="201909">09-2019</option>
                                        <option value="201910">10-2019</option>
                                        <option value="201911">11-2019</option>
                                        <option value="201912">12-2019</option>
                                    </select>
                                    <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Calificación</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input  id="califica" name="califica" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <a href="#" onclick="guardar()" class="btn btn-info">Guardar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Registro Encuesta</h4>
            </div>
            <div class="card-content" style="padding: 10px;">
                <form id="frmEncuesta" class="form-horizontal">
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Sucursal</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="essucursal" name="sucursal" class="selectpicker">
                                    <option selected disabled> Seleccione una sucursal</option>
                                    @foreach($sucursales as $sucursal)
                                        <option value="{{ $sucursal->id }}">{{ $sucursal->nombre}}</option>
                                    @endforeach
                                </select>
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Periodo</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <select id="esperiodo" name="periodo" class="selectpicker">
                                    <option selected disabled> Seleccione un Periodo</option>
                                        <option value="201904">04-2019</option>
                                        <option value="201905">05-2019</option>
                                        <option value="201906">06-2019</option>
                                        <option value="201907">07-2019</option>
                                        <option value="201908">08-2019</option>
                                        <option value="201909">09-2019</option>
                                        <option value="201910">10-2019</option>
                                        <option value="201911">11-2019</option>
                                        <option value="201912">12-2019</option>
                                </select>
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">E.R. atencion y amabilidad</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="eraa" name="eraa" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">E.R. calidad alimentos</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="erca" name="erca" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">E. R. calidad servicio</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="ercs" name="ercs" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">E. R. recomendación</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="err" name="err" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">E. V. atencion y amabilidad</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="evaa" name="evaa" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">E. V. rapidez</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="evrz" name="evrz" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">E. V. calidad servicio</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="evcs" name="evcs" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">E. V. recomendación</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="evr" name="evr" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <a href="#" onclick="guardarES()" class="btn btn-info">Guardar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Registro Capacitación</h4>
            </div>
            <div class="card-content" style="padding: 10px;">
                <form id="frmCapacita" class="form-horizontal">
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Sucursal</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="capsucursal" name="sucursal" class="selectpicker">
                                    <option selected disabled> Seleccione una sucursal</option>
                                    @foreach($sucursales as $sucursal)
                                        <option value="{{ $sucursal->id }}">{{ $sucursal->nombre}}</option>
                                    @endforeach
                                </select>
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Periodo</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <select id="capperiodo" name="periodo" class="selectpicker">
                                    <option selected disabled> Seleccione un Periodo</option>
                                    <option value="201904">04-2019</option>
                                    <option value="201905">05-2019</option>
                                    <option value="201906">06-2019</option>
                                    <option value="201907">07-2019</option>
                                    <option value="201908">08-2019</option>
                                    <option value="201909">09-2019</option>
                                    <option value="201910">10-2019</option>
                                    <option value="201911">11-2019</option>
                                    <option value="201912">12-2019</option>
                                </select>
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Capacitación</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="capacita" name="capacita" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                        
                        <label class="col-sm-2 label-on-left">Uso de la aplicacion</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <input id="uso" name="uso" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <a href="#" onclick="guardarCap()" class="btn btn-info">Guardar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="detModal" tabindex="-1" role="dialog" aria-labelledby="detModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="detModalLabel"></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div id="detModalTabContent" class="col-md-12 ml-auto mr-auto"  style="height: 300px !important; overflow-y: scroll;">
                    
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- Modal -->
@endsection
@section('aditionalScripts')
<style>
        .loader {
          border: 16px solid #f3f3f3;
          border-radius: 50%;
          border-top: 16px solid #3498db;
          width: 120px;
          height: 120px;
          -webkit-animation: spin 2s linear infinite; /* Safari */
          animation: spin 2s linear infinite;
        }
        
        /* Safari */
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }
        
        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }

        .table-sm{
          font-size: 12px;
        }
            
        .table-sm th{
          width: 120px;
          padding: 4px;
        }
        .table-sm td{
          padding: 4px;
        }
        .tdnumber{
            text-align: right !important;
        }
        </style>
<script>
function getDet(periodo,nom,idp){
	$( ".loader" ).remove();
	$("#detModalLabel").text(nom);
	$('#detModal').modal('show');
    $("#detModalTabContent").append("<div class='loader'></div>");
    var params = { "periodo": periodo, "idp": idp, "_token": "{{ csrf_token() }}" };
	$.ajax({ 
		type: "POST",
		url: "{{ route('detAlexTable') }}",
		data: params,
		success: function(msg){
			$("#detModalTabContent").empty();
			$("#detModalTabContent").append(msg);
			$( ".loader" ).remove();
		},
		error: function(){
			console.log("error");
		}
	});
}

function guardarCap()
{
    if( $("#capacita").val() && $("#capperiodo").val()  && $("#capsucursal").val())
    {
        swal({
            title: "Estas segur@?",
            text: "Los datos seran guardados permanentemente!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            allowOutsideClick: false,
            confirmButtonText: 'Si, enviar datos!',
            cancelButtonText: 'No, cancelar!'
        }).then((result) => {
            if (result) {
                $('.button').prop('disabled', true);
            swal({
                title: 'Guardando...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: false,
                showConfirmButton: false,
                text: 'Espere un momento...'
            });
            
            $.ajax({
                type: "POST",
                url: "{{ route('guardacapacita') }}",
                data: $('#frmCapacita').serialize(),
                success: function(msg){
                    $('#tblReq tbody').empty();
                    $('form.form-horizontal')[2].reset();
                    swal({
                        type: 'success',
                        title: 'Los datos han sido guardados!'
                    });						
                    $('.button').prop('disabled', false);
                    
                },
                error: function(){
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Algo ha salido mal!',
                        footer: 'Problemas? sit@prigo.com.mx	',
                    });
                    $('.button').prop('disabled', false);
                }
            });	
            
            }
        });
    } else {
        swal({
            type: 'error',
            title: 'Oops...',
            text: 'Todos los datos son obligatorios!',
            footer: 'Problemas? sit@prigo.com.mx',
        });
    }
}

function guardar()
{
    if($("#red").val() && $("#califica").val() && $("#periodo").val()  && $("#sucursal").val())
    {
        swal({
            title: "Estas segur@?",
            text: "Los datos seran guardados permanentemente!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            allowOutsideClick: false,
            confirmButtonText: 'Si, enviar datos!',
            cancelButtonText: 'No, cancelar!'
        }).then((result) => {
            if (result) {
                $('.button').prop('disabled', true);
            swal({
                title: 'Guardando...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: false,
                showConfirmButton: false,
                text: 'Espere un momento...'
            });
            
            $.ajax({
                type: "POST",
                url: "{{ route('guardaredes') }}",
                data: $('#frmRedes').serialize(),
                success: function(msg){
                    $('#tblReq tbody').empty();
                    $('form.form-horizontal')[0].reset();
                    swal({
                        type: 'success',
                        title: 'Los datos han sido guardados!'
                    });						
                    $('.button').prop('disabled', false);
                    
                },
                error: function(){
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Algo ha salido mal!',
                        footer: 'Problemas? sit@prigo.com.mx	',
                    });
                    $('.button').prop('disabled', false);
                }
            });	
            
            }
        });
    } else {
        swal({
            type: 'error',
            title: 'Oops...',
            text: 'Todos los datos son obligatorios!',
            footer: 'Problemas? sit@prigo.com.mx',
        });
    }
}

function guardarES()
{
    if($("#esperiodo").val()  && $("#essucursal").val() && $("#eraa").val()  && $("#erca").val()  && $("#ercs").val()  && $("#err").val()  && $("#evaa").val()  && $("#evrz").val() && $("#evcs").val() && $("#evr").val() )
    {
        swal({
            title: "Estas segur@?",
            text: "Los datos seran guardados permanentemente!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            allowOutsideClick: false,
            confirmButtonText: 'Si, enviar datos!',
            cancelButtonText: 'No, cancelar!'
        }).then((result) => {
            if (result) {
                $('.button').prop('disabled', true);
            swal({
                title: 'Guardando...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: false,
                showConfirmButton: false,
                text: 'Espere un momento...'
            });
            
            $.ajax({
                type: "POST",
                url: "{{ route('guardaencuesta') }}",
                data: $('#frmEncuesta').serialize(),
                success: function(msg){
                    $('#tblReq tbody').empty();
                    $('form.form-horizontal')[1].reset();
                    swal({
                        type: 'success',
                        title: 'Los datos han sido guardados!'
                    });						
                    $('.button').prop('disabled', false);
                    
                },
                error: function(){
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Algo ha salido mal!',
                        footer: 'Problemas? sit@prigo.com.mx	',
                    });
                    $('.button').prop('disabled', false);
                }
            });	
            
            }
        });
    } else {
        swal({
            type: 'error',
            title: 'Oops...',
            text: 'Todos los datos son obligatorios!',
            footer: 'Problemas? sit@prigo.com.mx',
        });
    }
}
</script>
@endsection