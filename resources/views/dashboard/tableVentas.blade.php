<H3>{{ $filtro }} {{ $anio }}</H3>
<table class="table table-condensed table-striped">
	<thead>
		<tr>
			<th>@if( $tiempo == 1 || $tiempo == 4)
					Mes
				@else
					{{ $tiempo }}
				@endif
				</th>
			<th>Monto</th>
		</tr>
	</head>
	<tbody>
		@foreach($datos AS $dato)
			<tr><td>{{ $dato->tiempo }}</td><td>{{ number_format($dato->monto,0,"",",") }}</td></tr>
		@endforeach
	</tbody>
</table>