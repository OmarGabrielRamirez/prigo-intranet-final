@extends('layouts.app')
@include('menu.dashalex', ['seccion' => 'dash'])
@section('content')
<div class="row" style="z-index: 1050!important;">
    <div class="col-md-12">
    <table class="table-sm table-bordered">
        <thead>
            <tr>
                <th rowspan=2>Tienda/Periodo</th>
                @foreach($periodos as $periodo)
            <th colspan=5>{{ $meses[$periodo->mes] }} - {{ $periodo->anio }}  <a target="_blank" class="btn btn-info btn-sm btn-simple" href="{{ route('evaluacion', 1) }}">
              <i class="material-icons">get_app</i>
          </a></th>
                @endforeach            
            </tr>
            <tr>
                @foreach($periodos as $periodo)
                <th class="text-center">Evaluacion</th><th class="text-center">Bono</th><th class="text-center">Supervisor</th><th class="text-center">Gerente</th><th class="text-center">Chef</th><th class="text-center">Venta</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($bonos as $bono)
            <tr>
                <td >{{ $bono->nombre }}</td>
                <td class="text-center">{{ number_format($bono->evaluacion,2,".",",") }}</td>
                <td class="text-center">{{ number_format($bono->bono,0,"",",") }}</td>
                <td class="text-center">{{ number_format($bono->bonoSupervisor,0,"",",") }}</td>
                <td class="text-center">{{ number_format($bono->bonoGerente,0,"",",") }}</td>
                <td class="text-center">{{ number_format($bono->bonoChef,0,"",",") }}</td>
                <td class="text-center">{{ number_format($bono->venta,0,"",",") }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</div>
@endsection
@section('aditionalScripts')
<style>
        .loader {
          border: 16px solid #f3f3f3;
          border-radius: 50%;
          border-top: 16px solid #3498db;
          width: 120px;
          height: 120px;
          -webkit-animation: spin 2s linear infinite; /* Safari */
          animation: spin 2s linear infinite;
        }
        
        /* Safari */
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }
        
        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }

        .table-sm{
          font-size: 12px;
        }
            
        .table-sm th{
          width: 120px;
          padding: 4px;
        }
        .table-sm td{
          padding: 4px;
        }
        .tdnumber{
            text-align: right !important;
        }
        </style>
@endsection