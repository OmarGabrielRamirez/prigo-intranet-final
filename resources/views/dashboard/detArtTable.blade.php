<table class="table table-condensed table-striped">
	<thead>
		<tr>
			<th>Proveedor</th>							
			<th>Compra Total ($)</th>					
		</tr>
	</head>
	<tbody>
		@foreach($detArt AS $dato)
			<tr><td class="text-left">{{ $dato->proveedorName }}</td><td  class="text-right">{{ number_format($dato->total,0,"",",") }}</td></tr>
		@endforeach
	</tbody>
</table>