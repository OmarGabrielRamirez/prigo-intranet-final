@extends('layouts.app')
@section('appmenu')
<li class="nav-item active">
	<a class="nav-link" data-toggle="collapse" href="#pagesExamples" aria-expanded="true">
		<i class="material-icons">assessment</i>
		<p> Dashboard <b class="caret"></b> </p>
    </a>
</li>
@endsection
@section('content')
<div class="row" style="margin-bottom: 10px;">
<div class="col-md-6 ml-auto">
	<ul class="nav nav-pills nav-pills-warning justify-content-right" role="tablist">
		<li class="nav-item">
		<a class="nav-link active show" href="{{ route('panel') }}" role="tablist">
		  Dashboard
		</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="{{ route('dashfin') }}" role="tablist">
		  Finanzas
		</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="{{ route('dashop') }}" role="tablist">
		  Operaciones
		</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="{{ route('dashrh') }}" role="tablist">
		  RH
		</a>
		</li>
	</ul>
</div>
</div>
<div class="row">
	<div id="kpiYTDFinanzas-panel-1" class="col-md-4">
		<div class="card ">
			<div class="card-header card-header-info card-header-icon">
				<div class="card-icon">
					<i class="material-icons">equalizer</i>
				</div>
				<h4 class="card-title">Ingreso al día</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						<div class="ct-chart" id="kpiVentasYTD"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						Diferencia: <span class="@if($ventasaYTDL - $ventasbYTDL >= 0) success @else danger @endif">{{number_format($ventasaYTDL - $ventasbYTDL, 0,".",",")}}</span><br>
						Venta: {{ number_format($ventasaYTDL, 0,".",",") }}<br>
						Budget: {{ number_format($ventasbYTDL, 0,".",",") }}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="kpigastoFinanzas-panel-1" class="col-md-4">
		<div class="card ">
			<div class="card-header @if( $difpergasto < 0 ) card-header-danger @else card-header-success @endif card-header-icon">
				<div class="card-icon">
					<i class="material-icons">@if( $difpergasto < 0 ) trending_up @else trending_down @endif</i>
				</div>
				<h4 class="card-title"><b>Gasto {{ number_format($difpergasto,2) }}%</b></h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						<div class="ct-chart" id="kpigasto"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						<div class="stats">
							Diferencia: {{ number_format($difpergasto,2) }}%<br>
							Gastos: {{number_format($pergasto,2)}}%<br>
							Budget: {{number_format($pergastoBudget,2)}}%
						</div>		
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="kpicostoFinanzas-panel-1" class="col-md-4">
		<div class="card ">
			<div class="card-header @if( $difpercosto < 0 ) card-header-danger @else card-header-success @endif card-header-icon">
				<div class="card-icon">
					<i class="material-icons">@if( $difpercosto < 0 ) trending_up @else trending_down @endif</i>
				</div>
				<h4 class="card-title"><b>Costo {{ number_format($difpercosto,2) }}%</b></h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						<div class="ct-chart" id="kpicosto"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						<div class="stats">
							Diferencia: {{ number_format($difpercosto,2) }}%<br>
							Costo: {{number_format($percosto,2)}}%<br>
							Budget: {{number_format($percostoBudget,2)}}%
						</div>		
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card ">
			<div class="card-header @if( $ventasaYTDML - $ventasbYTDML < 0 ) card-header-danger @else card-header-success @endif card-header-icon">
				<div class="card-icon">
					<i class="material-icons">attach_money</i>
				</div>
				<h4 class="card-title">Ingreso del mes</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						<h3>$ {{ number_format($ventasaYTDML, 0,".",",") }}</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						Diferencia: {{number_format($ventasaYTDML - $ventasbYTDML, 0,".",",")}}<br>
						Budget: {{ number_format($ventasbYTDML, 0,".",",") }}
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="col-lg-3 col-md-6 col-sm-6">
	<div class="card">
	  <div class="card-header card-header-success card-header-icon">
		<div class="card-icon">
		  <i class="material-icons">trending_up</i>
		</div>
		<h4 class="card-title">Anual</h4>
	  </div>
	  <div class="card-body"><h3>{{ number_format($crecimientoAnual,2) }} %</h3></div>
	  <div class="card-footer">
		<div class="stats">
		  CRECIMIENTO EN VENTAS RESPECTO AL AÑO PASADO 
		</div>
	  </div>
	</div>
</div>
	
<div class="col-lg-3 col-md-6 col-sm-6">
	<div class="card">
	  <div class="card-header card-header-danger card-header-icon">
		<div class="card-icon">
		  <i class="material-icons">trending_down</i>
		</div>
		<h4 class="card-title">Mensual</h4>
	  </div>
	  <div class="card-body">
		<h3>{{ number_format($crecimientoMes,2) }} %</h3>
	  </div>
	  <div class="card-footer">
		<div class="stats">
		  CRECIMIENTO EN VENTAS RESPECTO AL AÑO PASADO Mensual
		</div>
	  </div>
	</div>
</div>	
	
<div class="col-lg-3 col-md-6 col-sm-6">
	<div class="card">
	  <div class="card-header @if( $difebitda < 0 ) card-header-danger @else card-header-success @endif card-header-icon">
		<div class="card-icon">
		  <i class="material-icons">@if( $difebitda > 0 ) trending_up @else trending_down @endif</i>
		</div>
		<h4 class="card-title">EBIT %</h4>
	  </div>
	  <div class="card-body">
		<h3>{{ number_format($perebit,2) }} %</h3>
	  </div>
	  <div class="card-footer">
		<div class="stats">
		  Diferencia: {{$difebitda}} <br>
		  Budget: {{ number_format($perebitBudget,2) }}%
		</div>
	  </div>
	</div>
</div>
	
<div class="col-lg-3 col-md-6 col-sm-6">
	<div class="card">
	  <div class="card-header card-header-info card-header-icon">
		<div class="card-icon">
		  <i class="material-icons">receipt</i>
		</div>
		<h4 class="card-title">Cheque Prom</h4>
	  </div>
	  <div class="card-body"><h3>{{ number_format($cheque,2) }}</h3></div>
	  <div class="card-footer">
		<div class="stats">
		  Mes anterior: {{number_format($chequeant,2)}}
		</div>
	  </div>
	</div>
</div>	
<div class="col-lg-3 col-md-6 col-sm-6">
	<div class="card">
		<div class="card-header card-header-info card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">transfer_within_a_station</i>
			</div>
			<h4 class="card-title">Fuerza Laboral</h4>
		</div>
		<div class="card-body ">
			<div class="row">
				<div class="col-md-12 ml-auto mr-auto">
					<div class="ct-chart" id="kpiFuerzaLab"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 ml-auto mr-auto">
					Empleados: 1050 <br> Vacantes Disponibles: 236
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
@section('aditionalScripts')
<script>
$().ready(function(){
	
	var datakpiYTDFinanzas = {
		labels: ['Ventas', 'Budget'],
		series: [
			[{{ $ventasaYTD }}],
			[{{ $ventasbYTD }}]
		]
	};

	var optionskpiYTDFinanzas = {
		axisY: {
			offset: 80
		},
		legend: {
			display: true			
		},
		chartPadding: {
			top: 0,
			right: 0,
			bottom: 0,
			left: 10
		}
	};
	var kpiYTDFinanzasChart = new Chartist.Bar('#kpiVentasYTD', datakpiYTDFinanzas, optionskpiYTDFinanzas);
	// start animation for the Completed Tasks Chart - Line Chart
	md.startAnimationForLineChart(kpiYTDFinanzasChart);
	
	var chartCosto = new Chartist.Bar('#kpicosto',{labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago'],series: [ [{{	$kpiGastoMes }} ]]})
	md.startAnimationForLineChart(chartCosto);
	
	var chartGasto = new Chartist.Line('#kpigasto', {
	  labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago'],
	  series: [ [{{	$kpiGastoMes }} ]]
	}, {
		axisY: {
			offset: 100
		},
		axisX: {
			labelInterpolationFnc: function(value) {
				return value.split(/\s+/).map(function(word) {
					return word[0];
				}).join('');
			}
		},
		lineSmooth: Chartist.Interpolation.cardinal({
			tension: 0
		}),
		low: {{$minGastoMes - 1000 }},
		high: {{$maxGastoMes + 1000}},
		showArea: false,
		showPoint: false,
		fullWidth: true
	});

	md.startAnimationForLineChart(chartGasto);
	
	
/*	var kpiMTDFinanzasChart = new Chartist.Pie('#kpiVentasMTD', {
  series: [50,20,30]
}, {
  donut: true,
  donutWidth: 40,
  startAngle: 270,
  total: 200,
  showLabel: false
});
	
	md.startAnimationForLineChart(kpiMTDFinanzasChart);
	
	*/

var data = {
  series: [1050,236]
};

var sum = function(a, b) { return a + b };

new Chartist.Pie('#kpiFuerzaLab', data, {
  labelInterpolationFnc: function(value) {
	console.log(value);
	console.log(Math.round(value / 1286 * 100));
    return Math.round(value / 1286 * 100) + '%';
  }
});
	
	
});
</script>
@endsection