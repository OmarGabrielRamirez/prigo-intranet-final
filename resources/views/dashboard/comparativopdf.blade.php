@if(!empty($isPdf))
	    <table class="table-sm table-bordered" border="1" >
        <thead>
            <tr>
                <th rowspan=2>Tienda/Periodo</th>
                <th colspan=4>{{ $meses[$mes] }} - {{ $anio }}</tr>
            <tr>
                <th style="text-align: center;">Nomina</th><th style="text-align: center;">Costo</th><th style="text-align: center;">Gasto</th><th style="text-align: center;">Renta</th>
            </tr>
        </thead>
        <tbody>
            @foreach($gastos as $bono)
            <tr>
                <td >{{ $bono->nombre }}</td>
                <td style="text-align: right;">{{ number_format($bono->Nomina,2,".",",") }}</td>
                <td style="text-align: right;">{{ number_format($bono->Costo,2,".",",") }}</td>
                <td style="text-align: right;">{{ number_format($bono->Gasto,2,".",",") }}</td>
                <td style="text-align: right;">{{ number_format($bono->Renta,2,".",",") }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endif	