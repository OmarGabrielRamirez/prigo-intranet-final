@extends('layouts.app')
@include('menu.dashalex', ['seccion' => 'dash'])
@section('content')
@php
$vacios = "";
$evaluacion = 0;
@endphp
<div class="row" style="z-index: 1050!important;">
    <div class="col-md-12">
        <div class="card" style="z-index: 1050!important;">
            <div class="card-header">
                <h4 class="card-title">Consultar</h4>
            </div>
            <div class="card-content" style="padding: 10px;">
                <select onchange="cambia(this.value)" class="selectpicker">
                    <option value="" @if(empty($selSuc)) selected @endif> Todas las sucursales</option>
                    @foreach($sucursales as $sucursal)
                        <option @if(!empty($selSuc) && $selSuc==$sucursal->id) selected @endif value="{{ base64_encode($sucursal->id)}}">{{ $sucursal->nombre}}</option>
                    @endforeach
                </select>     
                <!--button onclick="opnBitacora()" class="btn btn-info">Crear Bitacora</button-->
            </div>
        </div>
      </div>
</div>
<div class="row">
        <div style="width: 100%; position: relative;padding-left:20px;padding-right:20px;padding-top:10px;">
                <div style="width: 205px;background-color: #fff; position: absolute; z-index: 1000; top: 10;">                    
                    <table class="table-sm table-bordered" style="width:205px !important;background-color: #fff;">
                        <thead>
                            <tr>
                                <!--th style="padding-top: 17px;padding-bottom: 17px;line-height: 12px !important; font-size: 12px !important;"></th-->
                                <th>@if(!empty($selSucName)){{$selSucName}} @else &nbsp; @endif</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($Role!=5)
                            <tr><td class="text-center"><strong>General</strong></td></tr>
                            <tr><td>Venta</td></tr>
                            <tr><td>TI</td></tr>@if(empty($selSuc))
                            <tr><td>TOT</td></tr>@endif
                            <tr><td>Costo</td></tr>
                            <tr><td>Nomina</td></tr>
                            <tr><td>Servicios (luz, agua, gaz)</td></tr>
                            <tr><td>Mantenimiento</td></tr>
                            <tr><td>Equipo menor</td></tr>
                            <tr><td>Suministros de operación</td></tr>
                            <tr><td>Otros gastos</td></tr>
                            <tr><td>Renta Locales</td></tr>
                            <!--tr><td>Empaques</td></tr-->
                            <tr><td>Productos limpieza</td></tr>
                            <tr><td>Papeleria</td></tr>
                            <tr><td>EBIDTA $</td></tr>
                            <tr><td>EBIDTA %</td></tr>
                            <tr><td class="text-center"><strong>Salon</strong></td></tr>
                            <tr><td>Venta Salon</td></tr>
                            <tr><td>Numero de comensales</td></tr>
                            <tr><td>Ticket promedio</td></tr>
                            <tr><td class="text-center"><strong>Vitrina</strong></td></tr>
                            <tr><td>Venta VITRINA</td></tr>
                            <tr><td>Numero de comensales</td></tr>
                            <tr><td>Ticket promedio</td></tr>
                            <tr><td class="text-center"><strong>UBER</strong></td></tr>
                            <tr><td>Venta UBER</td></tr>
                            <tr><td>Numero de comensales</td></tr>
                            <tr><td>Ticket promedio</td></tr>
                        @endif
                            <tr><td  class="text-center"><strong>RH</strong></td></tr>
                            <tr><td>Rotacion %</td></tr>
                            <tr><td>Numero de empleados</td></tr>
                        @if($Role!=5)
                            <tr><td class="text-center"><strong>Satisfacion</strong></td></tr>
                            <tr><td>Redes Sociales</td></tr>
                            <tr><td>Calificacion Ubereats</td></tr>
                            <tr><td>E.R. atencion y amabilidad</td></tr>
                            <tr><td>E.R. calidad alimentos</td></tr>
                            <tr><td>E. R. calidad servicio</td></tr>
                            <tr><td>E. R. recomendación</td></tr>
                            <tr><td>E. V. Experiencia cliente</td></tr>
                            <tr><td>E. V. recomendación</td></tr>
                            <tr><td  class="text-center"><strong>UBER EATS</strong></td></tr>
                            <tr><td>Tiempo de conexión %</td></tr>
                            <tr><td>Tiempo pausado %</td></tr>
                            <tr><td>Pedidos aceptados #</td></tr>
                            <tr><td>Segundos en aceptar (s)</td></tr>
                            <tr><td>Venta perdida $</td></tr>
                            <tr><td>Tiempo de preparacion (min)</td></tr>
                            <tr><td  class="text-center"><strong>Operación</strong></td></tr>
                            <tr><td>Mantenimiento Terminado %</td></tr>
                            <tr><td>Capacitacion</td></tr>
                            <tr><td>Uso de la app</td></tr> 
                            <tr><td>Cash management</td></tr>
                            <tr><td>Experiencia del cliente</td></tr>
							@endif
                            <tr><td>Calidad y higiene</td></tr>
                            <tr><td>Control de costo</td></tr>
							@if($Role!=5)
                            <tr><td>Procesos de pedidos</td></tr>
							@endif
                        </tbody>
                    </table>
                </div>
                <div style="width: 100%;overflow: auto;padding-left: 205px;padding-bottom: 28px;">     
                    <div style="position:fixed; top:0px;display: none;" class="topTH">
                        <table style="background:#fff; width: {{ ($nPeriodos +1) * 105 }}px !important;" class="table-sm table-bordered">
                            <thead>
                                <tr>
                                    @foreach ($periodos as $periodo)
                                    <th>{{ $periodo }}</th>
                                    @php
                                        $vacios .= "<td>&nbsp;</td>";
                                    @endphp
                                    @endforeach
									@if(!empty($selSuc))
									<th>EVALUACIÓN</th>
									@endif
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <table style="width: {{ ($nPeriodos +1) * 105 }}px !important;" class="table-sm table-bordered">
                        <thead>
                            <tr>
                                @foreach ($periodos as $periodo)
                                <th>{{ $periodo }}</th>
                                @endforeach
                                @if(!empty($selSuc))
                                <th>EVALUACIÓN</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if($Role!=5)
                                <tr>
                                {!! $vacios !!}
                                @if(!empty($selSuc))
                                    <td class="text-center"><span id="evaluacion"></span></td>
                                @endif
                                </tr>
                                <tr>
                                    @foreach($venta as $v)
                                    <td onclick="getDet({{ $v->periodo }}, 'Detalle Venta',1)" class="tdnumber {{ empty($ventaBudget[$v->periodo])? ' text-warning' : ($ventaBudget[$v->periodo]> $v->total ?' text-danger':' text-success') }}">{{ number_format($v->total*1.16,0,".",",") }} </td>
                                    @if(!empty($selSuc))
                                        @if($v->periodo==(date("Ym")-2))
                                        <td class="text-center">{{ empty($ventaBudget[$v->periodo])? "NA":($ventaBudget[$v->periodo]> $v->total ? "0%":"44%") }}</td>
                                        @php 
                                            $evaluacion += empty($ventaBudget[$v->periodo])? 0:($ventaBudget[$v->periodo]> $v->total ? 0: 44);
                                        @endphp
                                        @endif
                                    @endif
                                    @endforeach                                    
                                </tr>
                                <tr>
									@foreach($TIPer AS $TIDet)
										<td class="tdnumber {{ empty($TIDet)? ' text-warning' : ($TIDet<= 0 ?' text-danger':' text-success') }}">{{ number_format($TIDet,2,".",",") }} %</td>
									@endforeach
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>@if(empty($selSuc))
                                <tr>
									@foreach($TOTPer AS $TODet)
										<td class="tdnumber {{ empty($TODet)? ' text-warning' : ($TODet<= 0 ?' text-danger':' text-success') }}">{{ number_format($TODet,2,".",",") }} %</td>
									@endforeach
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>@endif
                                <tr>
                                    @foreach($costo as $v)
                                    <td class="tdnumber {{ empty($costoBudget[$v->periodo])? ' text-warning' : ($costoBudget[$v->periodo]< $v->total ?' text-danger':' text-success') }}">{{ number_format($v->total,0,".",",") }}</td>
                                    @if(!empty($selSuc))
                                        @if($v->periodo==(date("Ym")-2) )
                                        <td class="text-center">{{ empty($costoBudget[$v->periodo])? "NA":($costoBudget[$v->periodo]< $v->total ? "0%":(in_array($selSucId,$sinUber)?"23%":"20%")) }}</td>
                                        @php 
                                            $evaluacion += empty($costoBudget[$v->periodo])? 0:($costoBudget[$v->periodo]< $v->total ? 0: 20 + (in_array($selSucId,$sinUber)?3:0));
                                        @endphp
                                        @endif
                                    @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    @for ($i = $nPeriodos-2; $i >=0 ; $i--)
                                    <td class="tdnumber {{ empty($gastos[$i][1]->monto)? '' : ($gastos[$i][1]->total > $gastos[$i][1]->monto ?' text-danger':' text-success') }}">{{ number_format(empty($gastos[$i][1]->total)?0:$gastos[$i][1]->total,0,".",",") }}</td>
                                    @endfor
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                    @for ($i = $nPeriodos-2; $i >=0 ; $i--)
                                    <td class="tdnumber {{ empty($gastos[$i][5]->monto)? '' : ($gastos[$i][5]->total > $gastos[$i][5]->monto ?' text-danger':' text-success') }}">{{ number_format(empty($gastos[$i][5]->total)?0:$gastos[$i][5]->total,0,".",",") }}</td>
                                    @endfor
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                    @for ($i = $nPeriodos-2; $i >=0 ; $i--)
                                    <td class="tdnumber {{ empty($gastos[$i][2]->monto)? '' : ($gastos[$i][2]->total > $gastos[$i][2]->monto ?' text-danger':' text-success') }}">{{ number_format(empty($gastos[$i][2]->total)?0:$gastos[$i][2]->total,0,".",",") }}</td>
                                    @endfor
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                    @for ($i = $nPeriodos-2; $i >=0 ; $i--)
                                    <td class="tdnumber {{ empty($gastos[$i][0]->monto)? '' : ($gastos[$i][0]->total > $gastos[$i][0]->monto ?' text-danger':' text-success') }}">{{ number_format(empty($gastos[$i][0]->total)?0:$gastos[$i][0]->total,0,".",",") }}</td>
                                    @endfor
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                    @for ($i = $nPeriodos-2; $i >=0 ; $i--)
                                    <td class="tdnumber {{ empty($gastos[$i][6]->monto)? '' : ($gastos[$i][6]->total > $gastos[$i][6]->monto ?' text-danger':' text-success') }}">{{ number_format(empty($gastos[$i][6]->total)?0:$gastos[$i][6]->total,0,".",",") }}</td>
                                    @endfor
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                    @for ($i = $nPeriodos-2; $i >=0 ; $i--)
                                    <td class="tdnumber {{ empty($gastos[$i][3]->monto)? '' : ($gastos[$i][3]->total > $gastos[$i][3]->monto ?' text-danger':' text-success') }}">{{ number_format(empty($gastos[$i][3]->total)?0:$gastos[$i][3]->total,0,".",",") }}</td>
                                    @endfor
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                    @for ($i = $nPeriodos-2; $i >=0 ; $i--)
                                    <td class="tdnumber {{ empty($gastos[$i][4]->monto)? '' : ($gastos[$i][4]->total > $gastos[$i][4]->monto ?' text-danger':' text-success') }}">{{ number_format(empty($gastos[$i][4]->total)?0:$gastos[$i][4]->total,0,".",",") }}</td>
                                    @endfor
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                    @for ($i = 0; $i <= $nPeriodos-2 ; $i++)
                                    <td class="tdnumber">{{ number_format(empty($limpieza[$i]->total)?0:$limpieza[$i]->total,0,".",",") }}</td>
                                    @endfor
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                    @for ($i = 0; $i <= $nPeriodos-2 ; $i++)
                                    <td class="tdnumber">{{ number_format(empty($papeleria[$i]->total)?0:$papeleria[$i]->total,0,".",",") }}</td>
                                    @endfor
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                    @for ($i = 0; $i <= $nPeriodos-2 ; $i++)
                                    
                                    <td class="tdnumber">{{ number_format((empty($venta[$i]->total)?0:$venta[$i]->total) - (empty($costo[$i]->total)?0:$costo[$i]->total) - (empty($gasto[$i]->total)?0:$gasto[$i]->total) ,0,".",",") }}</td>
                                    @endfor
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                    @for ($i = 0; $i <= $nPeriodos-2 ; $i++)
                                    <td class="tdnumber">{{ number_format(((empty($venta[$i]->total)?0:$venta[$i]->total)-(empty($gasto[$i]->total)?0:$gasto[$i]->total)-(empty($costo[$i]->total)?0:$costo[$i]->total))*100/(empty($venta[$i]->total)?1:($venta[$i]->total)),2,".",",") }}</td>
                                    @endfor
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>{!! $vacios !!}@if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif</tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo)
                                    <td class="tdnumber">{{ number_format(empty($cheque[$periodo]->Salon)?0:$cheque[$periodo]->Salon,0,".",",") }}</td>
                                    @endforeach
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($cheque[$periodo]->ClientesSalon)?0:$cheque[$periodo]->ClientesSalon,0,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                    </tr>
                                    <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($cheque[$periodo]->ChequeSalon)?0:$cheque[$periodo]->ChequeSalon,0,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                    </tr>
                                    <tr>{!! $vacios !!}@if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif</tr>
                                    <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($cheque[$periodo]->vitrina)?0:$cheque[$periodo]->vitrina,0,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                    </tr>
                                    <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($cheque[$periodo]->ClientesVitrina)?0:$cheque[$periodo]->ClientesVitrina,0,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                    </tr>
                                    <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($cheque[$periodo]->ChequeVitrina)?0:$cheque[$periodo]->ChequeVitrina,0,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                    </tr>
                                    <tr>{!! $vacios !!}@if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif</tr>
                                    <tr>
                                        @foreach ($dperiodos as $periodo)                                    
                                        <td class="tdnumber">{{ number_format(empty($cheque[$periodo]->Domicilio)?0:$cheque[$periodo]->Domicilio,0,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                    </tr>
                                    <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($cheque[$periodo]->ClientesDomicilio)?0:$cheque[$periodo]->ClientesDomicilio,0,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                    </tr>
                                    <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($cheque[$periodo]->ChequeDomicilio)?0:$cheque[$periodo]->ChequeDomicilio,0,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                    </tr>
                            @endif
                                <tr>{!! $vacios !!}@if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif</tr>
                                <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($rh[$periodo]->rotacion)?0:$rh[$periodo]->rotacion,2,".",",") }}</td>
                                        @if(!empty($selSuc))
                                            @if ($loop->last)                                        
                                                <td class="text-center">{{ empty($rh[$periodo]->rotacion)?"5%":($rh[$periodo]->rotacion < 10 ? "5%" : "0%") }}</td>
                                                @php 
                                                    $evaluacion += empty($rh[$periodo]->rotacion)?5:($rh[$periodo]->rotacion < 10 ? 5 : 0);
                                                @endphp
                                            @endif
                                        @endif
                                        @endforeach
                                </tr>
                                <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($rh[$periodo]->empleados)?0:$rh[$periodo]->empleados,0,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                @if($Role!=5)
                                <tr>{!! $vacios !!}@if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif</tr>
                                <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber {{ empty($redes[$periodo])? ' text-warning' : ($redes[$periodo]< 4 ?' text-danger':' text-success') }}">{{ number_format(empty($redes[$periodo])?0:$redes[$periodo],2,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber {{ empty($redesUber[$periodo])? ' text-warning' : ($redesUber[$periodo]< 4 ?' text-danger':' text-success') }}">{{ number_format(empty($redesUber[$periodo])?0:$redesUber[$periodo],2,".",",") }}</td>
                                        @if(!empty($selSuc))
                                            @if ($loop->last)   
                                                <td class="text-center">{{ empty($redesUber[$periodo])?0:($redesUber[$periodo] > 4.5 ? "0.5%" : "0%") }}</td>
                                                @php 
                                                    $evaluacion += empty($redesUber[$periodo])?0:($redesUber[$periodo] > 4.5 ? 0.5 : 0);
                                                @endphp
                                            @endif
                                        @endif
                                        @endforeach
                                </tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo)                                    
                                    <td class="tdnumber">{{ number_format(empty($encuestas[$periodo]->eraa)?0:$encuestas[$periodo]->eraa,0,".",",") }}</td>                                    
                                    @if(!empty($selSuc))
                                        @if ($loop->last)   
                                            <td class="text-center">{{ empty($encuestas[$periodo]->eraa)?0:($encuestas[$periodo]->eraa > 80 ? "0.5%" : "0%") }}</td>
                                            @php 
                                                $evaluacion += empty($encuestas[$periodo]->eraa)?0:( $encuestas[$periodo]->eraa> 80 ? 0.5 : 0);
                                            @endphp
                                        @endif
                                    @endif                      
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo)                                    
                                    <td class="tdnumber">{{ number_format(empty($encuestas[$periodo]->erca)?0:$encuestas[$periodo]->erca,0,".",",") }}</td>                                    
                                    @if(!empty($selSuc))
                                        @if ($loop->last)   
                                            <td class="text-center">{{ empty($encuestas[$periodo]->erca)?0:( $encuestas[$periodo]->erca> 80 ? "0.5%" : "0%") }}</td>
                                            @php 
                                                $evaluacion += empty($encuestas[$periodo]->erca)?0:( $encuestas[$periodo]->erca> 80 ? 0.5 : 0);
                                            @endphp
                                        @endif
                                    @endif   
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo)                                    
                                    <td class="tdnumber">{{ number_format(empty($encuestas[$periodo]->ercs)?0:$encuestas[$periodo]->ercs,0,".",",") }}</td>                                    
                                    @if(!empty($selSuc))
                                    @if ($loop->last)   
                                    <td class="text-center">{{ empty($encuestas[$periodo]->ercs)?0:( $encuestas[$periodo]->ercs> 80 ? "0.5%" : "0%") }}</td>
                                    @php 
                                        $evaluacion += empty($encuestas[$periodo]->ercs)?0:( $encuestas[$periodo]->ercs> 80 ? 0.5 : 0);
                                    @endphp
                                @endif   
                                @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo)                                    
                                    <td class="tdnumber">{{ number_format(empty($encuestas[$periodo]->err)?0:$encuestas[$periodo]->err,0,".",",") }}</td>                                    
                                    @if(!empty($selSuc))
                                    @if ($loop->last)   
                                    <td class="text-center">{{ empty($encuestas[$periodo]->err)?0:( $encuestas[$periodo]->err> 60 ? "0.5%" : "0%") }}</td>
                                    @php 
                                        $evaluacion += empty($encuestas[$periodo]->err)?0:($encuestas[$periodo]->err> 60 ? 0.5 : 0);
                                    @endphp
                                @endif   
                                @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo)                                    
                                    <td class="tdnumber">{{ number_format(empty($encuestas[$periodo]->evaa)?0:$encuestas[$periodo]->evaa,0,".",",") }}</td>                                    
                                    @if(!empty($selSuc))
                                    @if ($loop->last)   
                                    <td class="text-center">{{ empty($encuestas[$periodo]->evaa)?0:( $encuestas[$periodo]->evaa> 80 ? "0.5%" : "0%") }}</td>
                                    @php 
                                        $evaluacion += empty($encuestas[$periodo]->evaa)?0:( $encuestas[$periodo]->evaa> 80 ? 0.5 : 0);
                                    @endphp
                                @endif   
                                @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo)                                    
                                    <td class="tdnumber">{{ number_format(empty($encuestas[$periodo]->evr)?0:$encuestas[$periodo]->evr,0,".",",") }}</td>                                    
                                    @if(!empty($selSuc))
                                    @if ($loop->last)   
                                    <td class="text-center">{{ empty($encuestas[$periodo]->evr)?0:( $encuestas[$periodo]->evr> 60 ? "0.5%" : "0%") }}</td>
                                    @php 
                                        $evaluacion += empty($encuestas[$periodo]->evr)?0:( $encuestas[$periodo]->evr> 60 ? 0.5 : 0);
                                    @endphp
                                @endif   
                            @endif
                                    @endforeach
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>{!! $vacios !!}@if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif</tr>
                                <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($uber[$periodo]->tiempoCon)?0:$uber[$periodo]->tiempoCon,0,".",",") }}</td>
                                        @if(!empty($selSuc))
                                        @if ($loop->last)
                                        <td class="text-center">{{ empty($uber[$periodo]->tiempoCon)?0:($uber[$periodo]->tiempoCon >= 95 ? "0.5%" : "0%") }}</td>
                                            @php 
                                                $evaluacion += empty($uber[$periodo]->tiempoCon)?0:($uber[$periodo]->tiempoCon >= 95 ? 0.5 : 0);
                                            @endphp
                                        @endif
                                        @endif
                                        @endforeach
                                </tr>
                                <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($uber[$periodo]->tiempoPausa)?0:$uber[$periodo]->tiempoPausa,0,".",",") }}</td>
                                        @if(!empty($selSuc))
                                        @if ($loop->last)
                                        <td class="text-center">{{ empty($uber[$periodo]->tiempoPausa)?0:($uber[$periodo]->tiempoPausa == 0 ? "0.5%" : "0%") }}</td>
                                        @php 
                                            $evaluacion += empty($uber[$periodo]->tiempoPausa)?0:($uber[$periodo]->tiempoPausa == 0 ? 0.5 : 0);
                                        @endphp
                                        @endif
                                        @endif
                                        @endforeach
                                </tr>
                                <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($uber[$periodo]->aceptados)?0:$uber[$periodo]->aceptados,0,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($uber[$periodo]->tiempoAcepta)?0:$uber[$periodo]->tiempoAcepta,0,".",",") }}</td>
                                        @if(!empty($selSuc))
                                        @if ($loop->last)
                                        <td class="text-center">{{ empty($uber[$periodo]->tiempoAcepta)?0:($uber[$periodo]->tiempoAcepta < 8 ? "0.5%" : "0%") }}</td>
                                        @php 
                                            $evaluacion += empty($uber[$periodo]->tiempoAcepta)?0:($uber[$periodo]->tiempoAcepta < 8 ? 0.5 : 0);
                                        @endphp
                                        @endif
                                        @endif
                                        @endforeach
                                </tr>
                                <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($uber[$periodo]->ventaPerdida)?0:$uber[$periodo]->ventaPerdida,0,".",",") }}</td>
                                        @endforeach
                                        @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                        @foreach ($dperiodos as $periodo)
                                        <td class="tdnumber">{{ number_format(empty($uber[$periodo]->tiempoPrepara)?0:$uber[$periodo]->tiempoPrepara,0,".",",") }}</td>
                                        @if(!empty($selSuc))
                                        @if ($loop->last)
                                        <td class="text-center">{{ empty($uber[$periodo]->tiempoPrepara)?0:($uber[$periodo]->tiempoPrepara < 8 ? "0.5%" : "0%") }}</td>
                                        @php 
                                            $evaluacion += empty($uber[$periodo]->tiempoPrepara)?0:($uber[$periodo]->tiempoPrepara < 8 ? 0.5 : 0);
                                        @endphp
                                        @endif
                                        @endif
                                        @endforeach
                                </tr>
                                <tr>{!! $vacios !!}@if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif</tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo)                                   
                                    <td class="tdnumber">{{ number_format(empty($manto[$periodo]->perResuelto)?0:$manto[$periodo]->perResuelto,2,".",",") }}</td>
                                    @endforeach
                                    @if(!empty($selSuc))<td class="tdnumber">&nbsp;</td>@endif
                                </tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo)                                    
                                    <td class="tdnumber">{{ number_format(empty($capacitaciones[$periodo]->capacita)?0:$capacitaciones[$periodo]->capacita,0,".",",") }}</td>
                                    @if(!empty($selSuc))
                                    @if ($loop->last)
                                    <td class="text-center">{{ empty($capacitaciones[$periodo]->capacita)?0:($capacitaciones[$periodo]->capacita >= 85 ? "5%" : "0%") }}</td>
                                    @php 
                                        $evaluacion += empty($capacitaciones[$periodo]->capacita)?0:($capacitaciones[$periodo]->capacita >= 85 ? 5 : 0);
                                    @endphp
                                    @endif
                                    @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo)                                    
                                    <td class="tdnumber">{{ number_format(empty($capacitaciones[$periodo]->uso)?0:$capacitaciones[$periodo]->uso,0,".",",") }}</td>    
                                    @if(!empty($selSuc))
                                    @if ($loop->last)
                                    <td class="text-center">{{ empty($capacitaciones[$periodo]->uso)?0:($capacitaciones[$periodo]->uso >= 90 ? "5%" : "0%") }}</td>
                                    @php 
                                        $evaluacion += empty($capacitaciones[$periodo]->uso)?0:($capacitaciones[$periodo]->uso >= 90 ? 5 : 0);
                                    @endphp
                                    @endif
                                    @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo)       
                                    <td class="tdnumber">{{ number_format(empty($audita[$periodo]->cashman)?0:$audita[$periodo]->cashman,1,".",",") }}</td>
                                    @if(!empty($selSuc))
                                    @if ($loop->last)
                                    <td class="text-center">{{ empty($audita[$periodo]->cashman)?0:($audita[$periodo]->cashman == 100 ? "2%" : "0%") }}</td>
                                    @php 
                                        $evaluacion += empty($audita[$periodo]->cashman)?0:($audita[$periodo]->cashman == 100 ? 2 : 0);
                                    @endphp
                                    @endif
                                    @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo) 
                                    <td class="tdnumber">{{ number_format(empty($audita[$periodo]->puntos)?0:$audita[$periodo]->puntos,1,".",",") }}</td>
                                    @if(!empty($selSuc))
                                    @if ($loop->last)
                                    <td class="text-center">{{ empty($audita[$periodo]->puntos)?0:($audita[$periodo]->puntos > 80 ? "2%" : "0%") }}</td>
                                    @php 
                                        $evaluacion += empty($audita[$periodo]->puntos)?0:($audita[$periodo]->puntos > 80 ? 2 : 0);
                                    @endphp
                                    @endif
                                    @endif
                                    @endforeach
                                </tr>
								@endif
                                <tr>
                                    @foreach ($dperiodos as $periodo) 
                                    <td class="tdnumber">{{ number_format(empty($calidad[$periodo]->calidad)?0:$calidad[$periodo]->calidad,1,".",",") }}</td>
                                    @if(!empty($selSuc))
                                    @if ($loop->last)
                                    <td class="text-center">{{ empty($calidad[$periodo]->calidad)?0:($calidad[$periodo]->calidad > 90 ? "5%" : "0%") }}</td>
                                    @php 
                                        $evaluacion += empty($calidad[$periodo]->calidad)?0:($calidad[$periodo]->calidad > 90 ? 5 : 0);
                                    @endphp
                                    @endif
                                    @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach ($dperiodos as $periodo) 
                                    <td class="tdnumber">{{ number_format(empty($calidad[$periodo]->costo)?0:$calidad[$periodo]->costo,1,".",",") }}</td>
                                    @if(!empty($selSuc))
                                    @if ($loop->last)
                                    <td class="text-center">{{ empty($calidad[$periodo]->costo)?0:($calidad[$periodo]->costo > 90 ? "5%" : "0%") }}</td>
                                    @php 
                                        $evaluacion += empty($calidad[$periodo]->costo)?0:($calidad[$periodo]->costo > 90 ? 5 : 0);
                                    @endphp
                                    @endif
                                    @endif
                                    @endforeach
                                </tr>
								@if($Role!=5)
                                <tr>
                                    @foreach ($dperiodos as $periodo) 
                                    <td class="tdnumber">{{ number_format(empty($ppedidos[$periodo]->pedido)?100:100-$ppedidos[$periodo]->pedido,1,".",",") }}</td> 
                                    @if(!empty($selSuc))
                                    @if ($loop->last)
                                    <td class="text-center">{{ empty($ppedidos[$periodo]->pedido)?1:($ppedidos[$periodo]->pedido < 10 ? "1%" : "0%") }}</td>
                                    @php 
                                        $evaluacion += empty($ppedidos[$periodo]->pedido)?1:($ppedidos[$periodo]->pedido < 10 ? 1 : 0);
                                    @endphp
                                    @endif
                                    @endif
                                    @endforeach
                                </tr>

                                @endif
                        </tbody>
                    </table>
                </div>
            </div>
        
</div>

<!-- Modal -->

<div class="modal fade" id="detModal" tabindex="-1" role="dialog" aria-labelledby="detModalLabel" style="z-index: 1059!important;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="detModalLabel"></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div id="detModalTabContent" class="col-md-12 ml-auto mr-auto"  style="height: 300px !important; overflow-y: scroll;">
                    
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- Modal -->
@endsection
@section('aditionalScripts')
<style>
        .loader {
          border: 16px solid #f3f3f3;
          border-radius: 50%;
          border-top: 16px solid #3498db;
          width: 120px;
          height: 120px;
          -webkit-animation: spin 2s linear infinite; /* Safari */
          animation: spin 2s linear infinite;
        }
        
        /* Safari */
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }
        
        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }

        .table-sm{
          font-size: 12px;
        }
            
        .table-sm th{
          width: 120px;
          padding: 4px;
        }
        .table-sm td{
          padding: 4px;
        }
        .tdnumber{
            text-align: right !important;
        }
        </style>
<script>
function opnBitacora(){
    $('#bitModal').modal('show');
}
function getDet(periodo,nom,idp){
	$( ".loader" ).remove();
	$("#detModalLabel").text(nom);
	$('#detModal').modal('show');
    $("#detModalTabContent").append("<div class='loader'></div>");
    var params = { "periodo": periodo, "idp": idp, "_token": "{{ csrf_token() }}" };
	$.ajax({ 
		type: "POST",
		url: "{{ route('detAlexTable') }}",
		data: params,
		success: function(msg){
			$("#detModalTabContent").empty();
			$("#detModalTabContent").append(msg);
			$( ".loader" ).remove();
		},
		error: function(){
			console.log("error");
		}
	});
}
function cambia(page)
{
    $( ".loader" ).remove();
	$("#detModalLabel").text("Cargando espere un momento...");
	$('#detModal').modal('show');
    $("#detModalTabContent").append("<div class='loader'></div>");
    document.location.href = "http://intranet.prigo.com.mx/finanzas/alex/"+page;
}
function addAction()
{
    $("#bitModalTabContent").append("<div class='row'><div class='col-sm-1'>"+ "<button type='button' class='close'><span aria-hidden='true'>&times;</span></button>"+"</div><div class='col-sm-10'><input name='accion[]' class='form-control'></div><div class='col-sm-1'></div></div>");
}
//$("form").onsubmit(function(){e.preventDefault();});
$("form").submit(function(e){
    e.preventDefault();
    return false;
});
$('#closeBtn').click(function(e){e.preventDefault();});
$('#saveBtn').click(function(e){e.preventDefault();

    $.ajax({
        type: "POST",
        url: "{{ route('guardaBitacora') }}",
        data: $('#frmBitacora').serialize(),
        success: function(msg){
            obj = JSON.parse(msg);
            
            if($("#idBit").val() == 0 && obj.lid)
                $("#idBit").val(obj.lid);

            if(obj.success)
                swal({
                    type: 'success',
                    title: 'Tu solicitud a quedado registrada!'
                });
            else
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Algo ha salido mal!',
                    footer: 'Problemas? sit@prigo.com.mx	',
                });         
        },
        error: function(){
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'Algo ha salido mal!',
                footer: 'Problemas? sit@prigo.com.mx	',
            });
        }
    });

});

$(".main-panel").scroll(function() {
  console.log("entro");
  var y = $(this).scrollTop();
  if (y > 300) {
    $('.topTH').fadeIn();
  } else {
    $('.topTH').fadeOut();
  }
});

$("#evaluacion").html("{{ $evaluacion }}");
</script>
@endsection