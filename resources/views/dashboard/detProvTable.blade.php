<table class="table table-condensed table-striped">
	<thead>
		<tr>
			<th>Producto</th>							
			<th>Compra Total ($)</th>					
		</tr>
	</head>
	<tbody>
		@foreach($detProv AS $dato)
			<tr><td class="text-left">{{ $dato->itemName }}</td><td class="text-right">{{ number_format($dato->total,0,"",",") }}</td></tr>
		@endforeach
	</tbody>
</table>