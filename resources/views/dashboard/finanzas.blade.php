@extends('layouts.app')
@section('appmenu')
<li class="nav-item active">
	<a class="nav-link" data-toggle="collapse" href="#pagesExamples" aria-expanded="true">
		<i class="material-icons">assessment</i>
		<p> Dashboard <b class="caret"></b> </p>
    </a>
</li>
@endsection
@section('content')
<div class="row" style="margin-bottom: 10px;">
	<div class="col-md-3 mr-auto">
		<form id="typeTimeFrm"><select id="typeTime" class="form-control" onchange="changePage()"><option value="{{ route('dashfin') }}/1">Anual</option><option value="{{ route('dashfin') }}/2">Semestral</option><option value="{{ route('dashfin') }}/3">Trimestral</option><option value="{{ route('dashfin') }}/4">Mensual</option></select></form>
	</div>
	<div class="col-md-7 ml-auto">
		<ul class="nav nav-pills nav-pills-warning justify-content-right" role="tablist">
			<li class="nav-item">
			<a class="nav-link" href="{{ route('panel') }}" role="tablist">
			  Dashboard
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link active" href="{{ route('dashfin') }}" role="tablist">
			  Finanzas
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashop') }}" role="tablist">
			  Operaciones
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashcom') }}" role="tablist">
			  Compras
			</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="{{ route('dashrh') }}" role="tablist">
			  RH
			</a>
			</li>
		</ul>
	</div>
</div>
<div class="row">

	<div id="dashboard-panel-1" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Finanzas</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div class="col-md-12">
						<div id="kpiFinanzasTable" class="table-responsive">
							<table class="table table-condensed table-striped cctt">
								<tbody>
								<tr>
									<td></td>
									<td>{{ $meses[$mes][1] }}</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>									
								</tr>
								<tr>
									<td>
										<select id="mesKpiFinanzas" name="mes" class="form-control" onchange="loadKpiFinanzas()">
											<option value="0" selected="selected">Seleccione el mes</option>
											<option value="1">Enero</option>
											<option value="2">Febrero</option>
											<option value="3">Marzo</option>
											<option value="4">Abril</option>
											<option value="5">Mayo</option>
											<option value="6">Junio</option>
											<option value="7">Julio</option>
											<option value="8">Agosto</option>
											<option value="9">Septiembre</option>
											<option value="10">Octubre</option>
											<option value="11">Noviembre</option>
											<option value="12">Diciembre</option>
										</select>
									</td>
									<td>Actual</td>
									<td>Budget</td>
								  <td> </td>
									<td class="text-right">Mes Anterior</td>
									<td> </td>
									<td class="text-right">Acumulado</td>
								</tr>
								<tr class='cctt-collapsed'>
									<td class='cctt-control text-left'>Ingreso</td>
									<td class="text-right">{{$venta}}</td>
									<td class="text-right">{{ $venta_budget }}</td>
									<td class="text-right">
									@if( $alarmaVenta >= 100 ) 
										<i class="material-icons text-success">check</i>
									@elseif( $alarmaVenta >= 90 ) 
										<i class="material-icons text-warning">warning</i>
									@else 
										<i class="material-icons text-danger">error</i>
									@endif
									</td>
									<td class="text-right">{{ $venta_anterior }} </td>
									<td class="text-right">
										@if( $venta_anterior > $venta ) 
										<i class="material-icons text-warning">warning</i>
										@else 
										<i class="material-icons text-success">check</i>
										@endif										
									</td>
									<td class="text-right">{{ $venta_acumulada }} </td>
								  </tr>
								  @if( !empty($venta_mes_det) )
									  @foreach($venta_mes_det as $ventam)
									<tr style='display:none;'><td class="text-left">{{ $ventam->idSucursal }}</td><td class="text-right">{{ number_format($ventam->totalActual,0,'',',') }}</td><td>--</td><td>--</td><td class="text-right">{{ number_format($ventam->totalAnterior,0,'',',') }}</td><td></td><td class="text-right">{{ number_format($ventam->totalAcumulado,0,'',',') }}</td></tr>
									  @endforeach
								  @endif
								  <tr class='cctt-collapsed'>
									<td class='cctt-control text-left'>Costo de Venta</td>
									<td class="text-right">{{ $costo }}</td>
									<td class="text-right">{{ $costo_budget }}</td>
									<td class="text-right">
									@if( $alarmaCosto >= 100 ) 
										<i class="material-icons text-danger">error</i>
									@elseif( $alarmaCosto >= 90 ) 
										<i class="material-icons text-warning">warning</i>
									@else 
										<i class="material-icons text-success">check</i>
									@endif
							   	</td>
									<td class="text-right">{{ $costo_anterior }}</td>
									<td class="text-right">
											@if( $costo_anterior < $costo ) 
												<i class="material-icons text-warning">warning</i>
											@else 
												<i class="material-icons text-success">check</i>
											@endif
									</td>
									<td class="text-right">{{ $costo_acumulado }}</td>
									</tr>
								  @if( !empty($costo_mes_det) )
									  @foreach($costo_mes_det as $costom)
									<tr style='display:none;'><td class="text-left">{{ $costom->categoria }}</td><td class="text-right">{{ number_format($costom->totalActual,0,'',',') }}</td><td>--</td><td>--</td><td class="text-right">{{ number_format($costom->totalAnterior,0,'',',') }}</td><td></td><td class="text-right">{{ number_format($costom->totalAcumulado,0,'',',') }}</td></tr>
									  @endforeach
								  @endif
								  <tr class='cctt-collapsed'>
									<td class='cctt-control text-left'>Utilidad Neta</td>
									<td class="text-right">{{ $uneta }}</td>
									<td class="text-right">{{ $uneta_budget }}</td>
									<td class="text-right">
									@if( $alarmaUneta >= 100 ) 
										<i class="material-icons text-success">check</i>
									@elseif( $alarmaUneta >= 90 ) 
										<i class="material-icons text-warning">warning</i>
									@else 
										<i class="material-icons text-danger">error</i>
									@endif
									</td>
									<td class="text-right">{{ $uneta_anterior }}</td>
									<td class="text-right">
											@if( $alarmaUnetaAnt == 1 ) 
												<i class="material-icons text-success">check</i>
											@else 
												<i class="material-icons text-warning">warning</i>
											@endif
									</td>
									<td class="text-right">{{ $uneta_acumulada }}</td>
								  </tr>
								  <tr class='cctt-collapsed'>
									<td class='cctt-control text-left'>Gastos</td>
									<td class="text-right">{{ $gasto }}</td>
									<td class="text-right">{{ $gasto_budget }}</td>
									<td class="text-right">
									@if( $alarmaGasto >= 100 ) 
										<i class="material-icons text-danger">error</i>
									@elseif( $alarmaGasto >= 90 ) 
										<i class="material-icons text-warning">warning</i>
									@else 
										<i class="material-icons text-success">check</i>
									@endif</td>
									</td>
									<td class="text-right">{{ $gasto_anterior }}</td>
									<td class="text-right">
											@if( $gasto_anterior < $gasto ) 
												<i class="material-icons text-warning">warning</i>
											@else 
												<i class="material-icons text-success">check</i>
											@endif
									</td>
									<td class="text-right">{{ $gasto_acumulado }}</td>
								  </tr>
								  @if( !empty($gasto_mes_det) )
									  @foreach($gasto_mes_det as $gastom)
									<tr style='display:none;'><td class="text-left">{{ ucfirst(strtolower($gastom->categoria)) }}</td><td class="text-right">{{ number_format($gastom->totalActual,0,'',',') }}</td><td>--</td><td>--</td><td class="text-right">{{ number_format($gastom->totalAnterior,0,'',',') }}</td><td></td><td class="text-right">{{ number_format($gastom->totalAcumulado,0,'',',') }}</td></tr>
									  @endforeach
								  @endif
								  <tr class='cctt-collapsed'>
									<td class='cctt-control text-left'>Ebidta</td>
									<td class="text-right">{{ $ebidta }}</td>
									<td class="text-right stats">{{ $ebidta_budget }}</td>
									<td class="text-right stats">
									@if( $alarmaEbidta >= 100 ) 
										<i class="material-icons text-success">check</i>
									@elseif( $alarmaEbidta >= 90 ) 
										<i class="material-icons text-warning">warning</i>
									@else 
										<i class="material-icons text-danger">error</i>
									@endif
									</td>
									<td class="text-right">{{ $ebidta_anterior }}</td>
									<td class="text-right">
											@if( $alarmaEbidtaAnt == 1 ) 
												<i class="material-icons text-success">check</i>
											@else 
												<i class="material-icons text-warning">warning</i>
											@endif
									</td>
									<td class="text-right">{{ $ebidta_acumulado }}</td>
								  </tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12 ml-auto mr-auto">
						<h3>Ventas mensuales ($K)</h3>
						<div class="ct-chart" id="kpiVentasYTD"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="dashop-panel-3" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Ingreso Acumulado</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="ingAcumulaTabContent" class="col-md-12 ml-auto mr-auto" style="height: 300px !important; overflow-y: scroll;">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>Tienda</th>							
									@for ($i = 0; $i < 12; $i++)
										<th>{{ $meses[$i][0] }}</th>
									@endfor
									<th>Total</th>							
								</tr>
							</head>
							<tbody>
								@foreach($ventaAcumula AS $dato)
									<tr><td>{{ $dato->idSucursal }}</td>
									<td>{{ number_format($dato->Ene,0,"",",") }}</td>
									<td>{{ number_format($dato->Feb,0,"",",") }}</td>
									<td>{{ number_format($dato->Mar,0,"",",") }}</td>
									<td>{{ number_format($dato->Abr,0,"",",") }}</td>
									<td>{{ number_format($dato->May,0,"",",") }}</td>
									<td>{{ number_format($dato->Jun,0,"",",") }}</td>
									<td>{{ number_format($dato->Jul,0,"",",") }}</td>
									<td>{{ number_format($dato->Ago,0,"",",") }}</td>
									<td>{{ number_format($dato->Sep,0,"",",") }}</td>
									<td>{{ number_format($dato->Oct,0,"",",") }}</td>
									<td>{{ number_format($dato->Nov,0,"",",") }}</td>
									<td>{{ number_format($dato->Dic,0,"",",") }}</td>
									<td>{{ number_format($dato->total,0,"",",") }}</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="dashop-panel-4" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Valor del Inventario KAYSER</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="ingAcumulaTabContent" class="col-md-12 ml-auto mr-auto" style="height: 300px !important; overflow-y: scroll;">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>Tienda</th>
									<th>Mes Anterior</th>
									<th>{{ $meses[$mes][1]}}</th>
									<th>Diferencia</th>
								</tr>
							</head>
							<tbody>
								@foreach($inventario AS $dato)
									<tr><td class="text-left">{{ $dato->idSucursal }}</td>
									<td class="text-right">{{ number_format($dato->anterior,0,"",",") }}</td>
									<td class="text-right">{{ number_format($dato->actual,0,"",",") }}</td>
									<td class="text-right">{{ number_format($dato->Diferencia,0,"",",") }} %</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>		
	<div id="dashop-panel-4" class="col-md-12">
		<div class="card ">
			<div class="card-header card-header-success card-header-icon">
				<div class="card-icon">
					<i class="material-icons"></i>
				</div>
				<h4 class="card-title">Valor del Inventario PRIGO</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div id="ingAcumulaTabContent" class="col-md-12 ml-auto mr-auto" style="height: 300px !important; overflow-y: scroll;">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>Tienda</th>
									<th>Mes Anterior</th>
									<th>{{ $meses[$mes][1]}}</th>
									<th>Diferencia</th>
								</tr>
							</head>
							<tbody>
								@foreach($inventarioprigo AS $dato)
									<tr><td class="text-left">{{ $dato->idSucursal }}</td>
									<td class="text-right">{{ number_format($dato->anterior,0,"",",") }}</td>
									<td class="text-right">{{ number_format($dato->actual,0,"",",") }}</td>
									<td class="text-right">{{ number_format($dato->Diferencia,0,"",",") }} %</td></tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
	
</div>
@endsection
@section('aditionalScripts')
<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.ct-chart-bar .ct-series line {
  cursor:pointer;
}
.ct-series-a {
  .ct-line {
    animation: dash 5s linear forwards;
    animation-delay: 1s;
  }
  .ct-line, .ct-point, .ct-bar {
    stroke: #5b9bd5;
  }
}
.ct-series-b {
  .ct-line {
    animation: dash 5s linear forwards;
    animation-delay: 2s;
  }
  .ct-line, .ct-point, .ct-bar {
    stroke: #ed7d31;
  }
}
.ct-series-c {
  .ct-line {
    animation: dash 5s linear forwards;
    animation-delay: 3s;
  }
  .ct-line, .ct-point, .ct-bar {
    stroke: #a5a5a5;
  }
}
.key {
  font-size: 0.7rem;
  color: #555;
  padding: 0 30px 0 90px;
  .key-element {
    width: 32%;
    display: inline-block;
    padding-left: 22px;
    box-sizing: border-box;
    position: relative;
    &:before {
      content: "";
      display: block;
      width: 16px;
      height: 16px;
      position: absolute;
      top: -2px;
      left: 0;
    }
    &.key-series-a {
      &:before {
        background-color: #5b9bd5;
      }
    }
    &.key-series-b {
      &:before {
        background-color: #ed7d31;
      }
    }
  }
}
</style>
<link  href="{{ asset('MaterialBS/js/plugins/TreeTableCCTT/jquery.cctt.css') }}" rel="stylesheet">
<script src="{{ asset('MaterialBS/js/plugins/TreeTableCCTT/jquery.cctt.js') }}"></script>
<script>

$('#kpiVentasYTDvsRes').on('click', '.ct-chart-bar .ct-labels foreignobject', function(evt) {
  var index = $(this).index();
  var label = $(this).find('span.ct-label').text();
});

$('#kpiVentasYTDvsRes').on('click', '.ct-chart-bar .ct-series-a line, .ct-chart-bar .ct-series-b line, .ct-chart-bar .ct-series-c line', function(evt) {
	
  var index = $(this).index();
  var label = $(this).closest('.ct-chart-bar').find('.ct-labels foreignObject:nth-child('+(index+1)+') span').text();
  var value = $(this).attr('ct:value');
  var metadata = $(this).attr('ct:meta');
  var meta = metadata.split(":");
  
  $("#kpiVentasYTDvsResTab").removeClass("hidden");
  $("#kpiVentasYTDvsResTab").addClass("animated fadeIn");
  $("#kpiVentasYTDvsResTabContent").empty();
  $("#kpiVentasYTDvsResTabContent").append("<div class='loader'></div>");
  loadTable("#kpiVentasYTDvsResTabContent", {"anio": meta[0], "mes": ({{ $tiempo }} != 4 ? "":meta[1]), "filtro": ({{ $tiempo }} != 4 ? meta[1]:meta[2]), "tiempo": "{{ $tiempo }}","_token": "{{ csrf_token() }}" } );
  
});

$('#kpiVentasYTDvsAicm').on('click', '.ct-chart-bar .ct-series-a line, .ct-chart-bar .ct-series-b line, .ct-chart-bar .ct-series-c line', function(evt) {
	
  var index = $(this).index();
  var label = $(this).closest('.ct-chart-bar').find('.ct-labels foreignObject:nth-child('+(index+1)+') span').text();
  var value = $(this).attr('ct:value');
  var metadata = $(this).attr('ct:meta');
  var meta = metadata.split(":");
  
  $("#kpiVentasYTDvsAicmTab").removeClass("hidden");
  $("#kpiVentasYTDvsAicmTab").addClass("animated fadeIn");
  $("#kpiVentasYTDvsAicmTabContent").empty();
  $("#kpiVentasYTDvsAicmTabContent").append("<div class='loader'></div>");
  loadTable("#kpiVentasYTDvsAicmTabContent", {"anio": meta[0], "mes": ({{ $tiempo }} != 4 ? "":meta[1]), "filtro": ({{ $tiempo }} != 4 ? meta[1]:meta[2]), "tiempo": "{{ $tiempo }}","_token": "{{ csrf_token() }}" } );
  
});

$('#kpiVentasYTDvsConv').on('click', '.ct-chart-bar .ct-series-a line, .ct-chart-bar .ct-series-b line, .ct-chart-bar .ct-series-c line', function(evt) {
	
  var index = $(this).index();
  var label = $(this).closest('.ct-chart-bar').find('.ct-labels foreignObject:nth-child('+(index+1)+') span').text();
  var value = $(this).attr('ct:value');
  var metadata = $(this).attr('ct:meta');
  var meta = metadata.split(":");
  
  $("#kpiVentasYTDvsConvTab").removeClass("hidden");
  $("#kpiVentasYTDvsConvTab").addClass("animated fadeIn");
  $("#kpiVentasYTDvsConvTabContent").empty();
  $("#kpiVentasYTDvsConvTabContent").append("<div class='loader'></div>");
  loadTable("#kpiVentasYTDvsConvTabContent", {"anio": meta[0], "mes": ({{ $tiempo }} != 4 ? "":meta[1]), "filtro": ({{ $tiempo }} != 4 ? meta[1]:meta[2]), "tiempo": "{{ $tiempo }}","_token": "{{ csrf_token() }}" } );
  
});

$('#kpiVentasYTDvsMty').on('click', '.ct-chart-bar .ct-series-a line, .ct-chart-bar .ct-series-b line, .ct-chart-bar .ct-series-c line', function(evt) {
	
  var index = $(this).index();
  var label = $(this).closest('.ct-chart-bar').find('.ct-labels foreignObject:nth-child('+(index+1)+') span').text();
  var value = $(this).attr('ct:value');
  var metadata = $(this).attr('ct:meta');
  var meta = metadata.split(":");
  
  $("#kpiVentasYTDvsMtyTab").removeClass("hidden");
  $("#kpiVentasYTDvsMtyTab").addClass("animated fadeIn");
  $("#kpiVentasYTDvsMtyTabContent").empty();
  $("#kpiVentasYTDvsMtyTabContent").append("<div class='loader'></div>");
  loadTable("#kpiVentasYTDvsMtyTabContent", {"anio": meta[0], "mes": ({{ $tiempo }} != 4 ? "":meta[1]), "filtro": ({{ $tiempo }} != 4 ? meta[1]:meta[2]), "tiempo": "{{ $tiempo }}","_token": "{{ csrf_token() }}" } );
  
});

$('#kpiVentasYTDvsCorner').on('click', '.ct-chart-bar .ct-series-a line, .ct-chart-bar .ct-series-b line, .ct-chart-bar .ct-series-c line', function(evt) {
	
  var index = $(this).index();
  var label = $(this).closest('.ct-chart-bar').find('.ct-labels foreignObject:nth-child('+(index+1)+') span').text();
  var value = $(this).attr('ct:value');
  var metadata = $(this).attr('ct:meta');
  var meta = metadata.split(":");
  
  $("#kpiVentasYTDvsCornerTab").removeClass("hidden");
  $("#kpiVentasYTDvsCornerTab").addClass("animated fadeIn");
  $("#kpiVentasYTDvsCornerTabContent").empty();
  $("#kpiVentasYTDvsCornerTabContent").append("<div class='loader'></div>");
  loadTable("#kpiVentasYTDvsCornerTabContent", {"anio": meta[0], "mes": ({{ $tiempo }} != 4 ? "":meta[1]), "filtro": ({{ $tiempo }} != 4 ? meta[1]:meta[2]), "tiempo": "{{ $tiempo }}","_token": "{{ csrf_token() }}" } );
  
});

$('#kpiVentasYTDvsIsla').on('click', '.ct-chart-bar .ct-series-a line, .ct-chart-bar .ct-series-b line, .ct-chart-bar .ct-series-c line', function(evt) {
	
  var index = $(this).index();
  var label = $(this).closest('.ct-chart-bar').find('.ct-labels foreignObject:nth-child('+(index+1)+') span').text();
  var value = $(this).attr('ct:value');
  var metadata = $(this).attr('ct:meta');
  var meta = metadata.split(":");
  
  $("#kpiVentasYTDvsIslaTab").removeClass("hidden");
  $("#kpiVentasYTDvsIslaTab").addClass("animated fadeIn");
  $("#kpiVentasYTDvsIslaTabContent").empty();
  $("#kpiVentasYTDvsIslaTabContent").append("<div class='loader'></div>");
  loadTable("#kpiVentasYTDvsIslaTabContent", {"anio": meta[0], "mes": ({{ $tiempo }} != 4 ? "":meta[1]), "filtro": ({{ $tiempo }} != 4 ? meta[1]:meta[2]), "tiempo": "{{ $tiempo }}","_token": "{{ csrf_token() }}" } );
  
});

$('#kpiVentasYTDvs').on('click', '.ct-chart-bar .ct-series-a line, .ct-chart-bar .ct-series-b line, .ct-chart-bar .ct-series-c line', function(evt) {
	
  var index = $(this).index();
  var label = $(this).closest('.ct-chart-bar').find('.ct-labels foreignObject:nth-child('+(index+1)+') span').text();
  var value = $(this).attr('ct:value');
  var metadata = $(this).attr('ct:meta');
  var meta = metadata.split(":");
  
  $("#kpiVentasYTDvsTab").removeClass("hidden");
  $("#kpiVentasYTDvsTab").addClass("animated fadeIn");
  $("#kpiVentasYTDvsTabContent").empty();
  $("#kpiVentasYTDvsTabContent").append("<div class='loader'></div>");
  loadTable("#kpiVentasYTDvsTabContent", {"anio": meta[0], "mes": ({{ $tiempo }} != 4 ? "":meta[1]), "filtro": ({{ $tiempo }} != 4 ? meta[1]:meta[2]), "tiempo": "{{ $tiempo }}","_token": "{{ csrf_token() }}" } );
  
});

function loadTable(idDiv, params)
{
	$.ajax({ 
		type: "POST",
		url: "{{ route('kpiVentasTable') }}",
		data: params,
		success: function(msg){
			$(idDiv).empty();
			$(idDiv).append(msg);
		},
		error: function(){
			console.log("error");
		}
	});
}
function changePage()
{
	$('#typeTimeFrm').attr('action', $("#typeTime").val()).submit();
}
function graphClicked(index, label, value, meta) {
  console.log('---');
  console.log('index:', index);
  console.log('label:', label);
  console.log('meta:', meta);
  alert('Index: '+index+', Label: '+label+', Value: '+value);
}

function loadKpiFinanzas()
{
	var mes = $("#mesKpiFinanzas").val();
	if( mes != "" && mes > 0 )
	{
		$("#kpiFinanzasTable").empty();
		$.ajax({ 
			type: "POST",
			url: "{{ route('kpiFinanzasTable') }}",
			data: { "mes": mes, "_token": "{{ csrf_token() }}" },
			success: function(msg){
				$("#kpiFinanzasTable").empty();
				$("#kpiFinanzasTable").append(msg);
				$('.cctt').cctt();
			},
			error: function(){
				console.log("error");
			}
		});
	}
}

$().ready(function(){
	$('.cctt').cctt();
	
	var datakpiVentasYTDChart = {
		labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
		series: [[{{ $ventasMes }}]]
	};

	var optionskpiVentasYTDChart = {
		lineSmooth: Chartist.Interpolation.cardinal({
			tension: 0
		}),
		low: {{ $kpiVentasYTDMin }} - 1000,
		high: {{ $kpiVentasYTDMax }} + 1000, 
		chartPadding: {
			top: 0,
			right: 0,
			bottom: 0,
			left: 10
		}
	};
	var kpiVentasYTDChart = new Chartist.Line('#kpiVentasYTD', datakpiVentasYTDChart, optionskpiVentasYTDChart);
	// start animation for the Completed Tasks Chart - Line Chart
	md.startAnimationForLineChart(kpiVentasYTDChart);

});
</script>
<style>
.ct-chart-bar .ct-label.ct-horizontal.ct-end {
  
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  transform: rotate(-65deg);
  transform-origin: 50% 90%;
  text-align: right;
  max-height: 4em;
  min-width: 100px;
  max-width: 100px;
}
</style>
@endsection