<div class="row">
    <label class="col-sm-1 col-form-label">Usuario</label>
    <div class="col-sm-3">
        <div class="form-group bmd-form-group">
            <input id="findUser" type="text" class="form-control" >
        </div>
    </div>
    <div class="col-sm-1">
        <button id="findUserbtn" class="btn btn-white btn-round btn-just-icon">
        <i class="material-icons">search</i>
        <div class="ripple-container"></div>
        </button>
    </div>
    <div class="col-sm-1">
        <button id="addUsuario" class="btn btn-success btn-round btn-just-icon">
        <i class="material-icons">control_point</i>
        <div class="ripple-container"></div>
        </button>
    </div>
    
</div>
<div class="row">
<table id="datatables" class="table table-striped table-bordered" cellspacing="0" width="50%" style="width:100%">
	<thead>
	  <tr>
          <th>ID</th>
		  <th>Nombre</th>
		  <th>Correo</th>		  
          <th>Acción</th>
	  </tr>
	</thead>
	<tbody>
	</tbody>
</table>
</div>