<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthApiController@login');
    Route::post('signup', 'AuthApiController@signup');
  
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthApiController@logout');
        Route::get('user', 'AuthApiController@user');
    });
});


Route::any('uberstore/getNotification/webhook','UberController@getNotification');
Route::any('uberstore/getUberRequest/webhook','UberController@getUberRequest');

Route::any('pedidoprigo/getPendientes','PedidosController@getPendingRequest');
Route::any('pedidoprigo/getTest','PedidosController@getPendingTest');
Route::any('pedidoprigo/confirmation','PedidosController@confirmRequest');
Route::any('pedidoprigo/confirmationTest','PedidosController@confirmTest');

Route::any('transferprigo/getPendientes','PedidosController@getPendingTransfer');
Route::any('transferprigo/getTest','PedidosController@getPendingTransferTest');
Route::any('transferprigo/confirmation','PedidosController@confirmTransfer');
Route::any('transferprigo/confirmationTest','PedidosController@confirmTransferTest');